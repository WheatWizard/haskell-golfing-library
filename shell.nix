{ nixpkgs ? import (builtins.fetchGit {
    name = "nixpkgs-release-21.11";
    url = "https://github.com/nixos/nixpkgs.git";
    ref = "refs/heads/release-21.11";
    rev = "1a1ba3cdd57b0337c32851bcac037df5eceab749";
  })
  {}, compiler ? "ghc921", doBenchmark ? false }:
let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation
      , base
      , containers
      , array
      , hpack
      , hspec
      , hspec-discover
      , hspec-expectations
      , lib
      , QuickCheck
      , text
      , text-icu
      }:
      mkDerivation {
        pname = "haskell-golfing-lib";
        version = "0.1.1.0";
        src = ./.;
        libraryHaskellDepends = [ base containers text text-icu ];
        libraryToolDepends = [ hpack ];
        testHaskellDepends = [
          base containers hspec hspec-expectations QuickCheck
        ];
        testToolDepends = [ hspec-discover ];
        prePatch = "hpack";
        homepage = "https://gitlab.com/WheatWizard/haskell-golfing-library/-/blob/master/README.md";
        license = "AGPL";
      };

  haskellPackages = pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
