module Test.Arbitrary.Instances
  (
  )
  where


import Test.Hspec
import Test.QuickCheck


import Control.Applicative


import P.Algebra.Free.Group
import P.Algebra.Free.Kei
import P.Algebra.Free.Quandle
import P.Algebra.Loop.Moufang.Staple
import P.Algebra.Quandle
import P.Algebra.Rack
import P.Algebra.Shelf
import P.List.Fenceposts
import P.Bifunctor.Either


instance
  (
  )
    => Arbitrary2 AltList
  where
    liftArbitrary2 arbitraryA arbitraryB =
      frequency
        [ ( 1
          , pure Ending
          )
        , ( 7
          , liftA2 Fence arbitraryB (liftArbitrary2 arbitraryA arbitraryB)
          )
        ]

    liftShrink2 shrinkA shrinkB s =
      downsize s ++ shrinkFence s ++ shrinkPost s
      where
        downsize Ending =
          []
        downsize (Fence a (Post x xs)) =
          xs : ((Fence a . Post x) <$> downsize xs)

        shrinkFence Ending =
          []
        shrinkFence (Fence a (Post x xs)) =
          (flip Fence (Post x xs) <$> shrinkB a) ++ (Fence a . Post x <$> shrinkFence xs)

        shrinkPost Ending =
          []
        shrinkPost (Fence a (Post x xs)) =
          (Fence a . flip Post xs <$> shrinkA x) ++ (Fence a . Post x <$> shrinkPost xs)


instance
  (
  )
    => Arbitrary2 Fenceposts
  where
    liftArbitrary2 arbitraryA arbitraryB =
      liftA2 Post arbitraryA (liftArbitrary2 arbitraryA arbitraryB)

    liftShrink2 shrinkA shrinkB s =
      downsize s ++ shrinkPost s ++ shrinkFence s
      where
        downsize (Post a (Fence x xs)) =
          xs : (Post a . Fence x <$> downsize xs)

        shrinkPost (Post a (Fence x xs)) =
          (flip Post (Fence x xs) <$> shrinkA a) ++ (Post a . Fence x <$> shrinkPost xs)

        shrinkFence (Post a (Fence x xs)) =
          (Post a . flip Fence xs <$> shrinkB x) ++ (Post a . Fence x <$> shrinkFence xs)


instance
  ( Arbitrary a
  )
    => Arbitrary1 (Fenceposts a)
  where
    liftArbitrary =
      liftArbitrary2 arbitrary

    liftShrink =
      liftShrink2 shrink


instance
  ( Arbitrary a
  , Arbitrary b
  )
    => Arbitrary (Fenceposts a b)
  where
    arbitrary =
      arbitrary1

    shrink =
      shrink1


-- TODO add shrink
instance
  ( Arbitrary a
  , Eq a
  )
    => Arbitrary (FreeQuandle a)
  where
    arbitrary = do
      sized go
      where
        go n
          | n <= 1
          =
            QP <$> arbitrary
          | otherwise
          = do
            l <- chooseInt (0, n-1)
            elements [(!>), (<!)] <*> go l <*> go (n - 1 - l)


instance
  ( Arbitrary a
  , Eq a
  )
    => Arbitrary (FreeGroup a)
  where
    arbitrary = do
      s <- getSize
      start <- arbitrary
      sPos <- arbitrary
      fmap FGC $ go s start sPos
      where
        go 0 _ _ =
          return []
        go 1 x pos =
          return [WEP pos x]
        go n x pos = do
          oneof
            [ do
                next <- arbitrary
                fmap (WEP pos x :) $ go (n-1) next pos
            , do
                next <- arbitrary `suchThat` (/= x)
                fmap (WEP pos x :) $ go (n-1) next (not pos)
            ]


instance
  ( Arbitrary a
  , Eq a
  )
    => Arbitrary (FreeKei a)
  where
    arbitrary = do
      s <- getSize
      start <- arbitrary
      xs <- go s start
      return $ FKe $ reverse (tail xs) ++ xs
      where
        go 0 x =
          return [x]
        go n x = do
          next <- arbitrary `suchThat` (/= x)
          fmap (x :) $ go (n-1) next


instance
  ( Arbitrary a
  )
    => Arbitrary (Staple a)
  where
    arbitrary = do
      oneof
        [ fmap M2L arbitrary
        , fmap M2R arbitrary
        ]
