{-# Language UndecidableSuperClasses #-}
{-# Language MultiParamTypeClasses #-}
{-# Language ScopedTypeVariables #-}
{-# Language AllowAmbiguousTypes #-}
{-# Language FlexibleInstances #-}
{-# Language ConstraintKinds #-}
module Test.Algebra.Tests
  ( TestableAlgebra (..)
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Kei
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Shelf
import P.Category.Iso


class
  ( c a
  )
    => TestableAlgebra c a
  where
    test :: Spec


instance forall a.
  ( Semigroup a
  , Arbitrary a
  , Eq a
  , Show a
  )
    => TestableAlgebra Semigroup a
  where
    test =
      describe "Semigroup" $ do
        it "is associative" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            x <> (y <> z) `shouldBe` (x <> y) <> z


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Monoid a
  )
    => TestableAlgebra Monoid a
  where
    test =
      describe "Monoid" $ do
        it "satisfies left identity" $ property $
          \ (x :: a)
          ->
            mempty <> x `shouldBe` x
        it "satisfies right identity" $ property $
          \ (x :: a)
          ->
            x <> mempty `shouldBe` x


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Quasigroup a
  )
    => TestableAlgebra Quasigroup a
  where
    test =
      describe "Quasigroup" $ do
        it "satisfies right outer cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            (x %%% y) ~/~ y `shouldBe` x
        it "satisfies right inner cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            (x ~/~ y) %%% y `shouldBe` x
        it "satisfies left outer cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x ~\~ (x %%% y) `shouldBe` y
        it "satisfies left inner cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x %%% (x ~\~ y) `shouldBe` y
        context "lpi" $ do
          it "gives isomorphisms" $ property $
            \ (x :: a)
              (y :: a)
            ->
              bwd (lpi x) (fwd (lpi x) y) `shouldBe` y
        context "rpi" $ do
          it "gives isomorphisms" $ property $
            \ (x :: a)
              (y :: a)
            ->
              bwd (rpi x) (fwd (rpi x) y) `shouldBe` y


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Loop a
  )
    => TestableAlgebra Loop a
  where
    test =
      describe "Loop" $ do
        it "satisfies left identity" $ property $
          \ (x :: a)
          ->
            lem %%% x `shouldBe` x
        it "satisfies right identity" $ property $
          \ (x :: a)
          ->
            x %%% lem `shouldBe` x


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Moufang a
  )
    => TestableAlgebra Moufang a
  where
    test =
      describe "Loop" $ do
        it "satisfies M1" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            x %%% (y %%% (x %%% z)) `shouldBe` ((x %%% y) %%% x) %%% z
        it "satisfies M2" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            x %%% (y %%% (z %%% y)) `shouldBe` ((x %%% y) %%% z) %%% y
        it "satisfies M2" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            (x %%% y) %%% (z %%% x) `shouldBe` (x %%% (y %%% z)) %%% x
        it "satisfies M2" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            (x %%% y) %%% (z %%% x) `shouldBe` x %%% ((y %%% z) %%% x)


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Shelf a
  )
    => TestableAlgebra Shelf a
  where
    test =
      describe "Shelf" $ do
        it "is left self distributive" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
             x <! (y <! z) `shouldBe` (x <! y) <! (x <! z)


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Rack a
  )
    => TestableAlgebra Rack a
  where
    test =
      describe "Rack" $ do
        it "is right self distributive" $ property $
          \ (x :: a)
            (y :: a)
            (z :: a)
          ->
            (x !> y) !> z `shouldBe` (x !> z) !> (y !> z)
        it "has left cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            (x <! y) !> x `shouldBe` y
        it "has right cancellation" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x <! (y !> x) `shouldBe` y


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Quandle a
  )
    => TestableAlgebra Quandle a
  where
    test =
      describe "Quandle" $ do
        it "has left idempotency" $ property $
          \ (x :: a)
          ->
            x <! x `shouldBe` x
        it "has right idempotency" $ property $
          \ (x :: a)
          ->
            x !> x `shouldBe` x


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Group a
  )
    => TestableAlgebra Group a
  where
    test =
      describe "Group" $ do
        it "has right inverse" $ property $
          \ (x :: a)
          ->
            x <> IV x `shouldBe` mempty
        it "has left inverse" $ property $
          \ (x :: a)
          ->
            IV x <> x `shouldBe` mempty
        it "has compatible Semigroup and Magma operations" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x <> y `shouldBe` x %%% y
        it "has compatible Monoid and Loop identities" $
          (lem :: a) `shouldBe` mempty
        it "uses left conjugation as (<!)" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x <! y `shouldBe` x <> y <> IV x
        it "uses right conjugation as (!>)" $ property $
          \ (x :: a)
            (y :: a)
          ->
            x !> y `shouldBe` IV y <> x <> y


instance forall a.
  ( Arbitrary a
  , Eq a
  , Show a
  , Kei a
  )
    => TestableAlgebra Kei a
  where
    test =
      describe "Kei" $ do
        it "is involute" $ property $
          \ (x :: a)
            (y :: a)
          ->
            (x !> y) !> y `shouldBe` x
