{-# Language ScopedTypeVariables #-}
module P.ListSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Aliases
import P.List


spec :: Spec
spec = do
  describe "xts" $ do
    it "preserves the order of the input list" $ property $
      \ (x :: List Int)
      ->
        fst <$> xts x `shouldBe` x
    it "produces lists which are one shorter than the input list" $ property $
      \ (x :: List Int)
      ->
        xts x `shouldSatisfy` all (\ (_, y) -> length y == length x - 1)
  describe "dc" $ do
    it "always gives the empty string as the first value" $ property $
      \ (x :: String)
      ->
        head (dc x) `shouldBe` ""
    it "only uses symbols from the input in the outputs" $ property $
      \ (NonEmpty (x :: String))
        (NonNegative (i :: Int))
      ->
        (dc x) !! i `shouldSatisfy` all (flip elem x)
  describe "xdc" $ do
    it "always gives the empty string as the first value" $ property $
      \ (x :: List String)
      ->
        head (xdc x) `shouldBe` ""
  describe "iBw" $ do
    it "doesn't change the input when the index to insert is negative" $ property $
      \ (x :: List Int)
        (a :: Int)
        (Negative (i :: Int))
      ->
        iBw i a x `shouldBe` x
    it "doesn't change the input when the index to insert is greater than the length of the list" $ property $
      \ (x :: List Int)
        (a :: Int)
        (Positive (i :: Int))
      ->
        iBw (length x + i) a x `shouldBe` x
    it "adds the value to the front of the list when the index is 0" $ property $
      \ (x :: List Int)
        (a :: Int)
      ->
        iBw 0 a x `shouldBe` a : x
    it "adds the value to the end of the list when the index is the length of the list" $ property $
      \ (x :: List Int)
        (a :: Int)
      ->
        iBw (length x) a x `shouldBe` x ++ [a]
    it "increase the length of the list by one if the index is in bounds" $ property $
      \ (NonEmpty (x :: List Int))
        (a :: Int)
      ->
        forAll (choose (1, length x - 1)) $
          \ (i :: Int)
          ->
            length (iBw i a x) `shouldBe` length x + 1
    it "doesn't evaluate past the given index" $ property $
      \ (NonNegative (i :: Int))
      ->
        take (i+1) (iBw i i $ [0 .. i-1] ++ error "Evaluated past index") `shouldBe` [0 .. i]
