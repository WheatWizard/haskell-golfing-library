{-# Language ScopedTypeVariables #-}
module P.EnumSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Enum


spec :: Spec
spec = do
  describe "icn" $ do
    context "Integer" $ do
      it "returns true when the difference is 1" $ property $
        \ (x :: Integer)
          (y :: Integer)
        ->
          icn x y `shouldBe` (y - x) == 1
