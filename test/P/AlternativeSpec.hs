{-# Language ScopedTypeVariables #-}
module P.AlternativeSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util

import P.Alternative


spec :: Spec
spec = do
  describe "fr_" $ do
    it "is an identity when supplied with \"_\"" $ property $
      \ (xs :: String)
      ->
        fr_ "_" xs `shouldBe` xs
    it "returns the template when it contains no '_'s" $ property $
      forAll (arbitrary `suchThat` (notElem '_')) $
        \ (template :: String)
          (xs :: String)
        ->
          fr_ template xs `shouldBe` template
    it "gives a result containing '_' only when both strings contain '_'s" $ property $
      \ (template :: String)
        (xs :: String)
      ->
        elem '_' (fr_ template xs) `shouldBe` elem '_' template && elem '_' xs
  describe "frp" $ do
    it "is an identity when supplied with '%'" $ property $
      \ (xs :: String)
      ->
        frp "%" xs `shouldBe` xs
    it "returns the template when it contains no '%'s" $ property $
      forAll (arbitrary `suchThat` (notElem '%')) $
        \ (template :: String)
          (xs :: String)
        ->
          frp template xs `shouldBe` template
    it "gives a result containing '%' only when both strings contain '%'s" $ property $
      \ (template :: String)
        (xs :: String)
      ->
        elem '%' (frp template xs) `shouldBe` elem '%' template && elem '%' xs
