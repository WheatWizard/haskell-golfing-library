{-# Language ScopedTypeVariables #-}
module P.Foldable.CountSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import Test.Util


import P.Aliases
import P.Foldable.Count


spec :: Spec
spec = do
  describe "cn" $ do
    it "gives 0 for const False" $ property $
      \ (list :: List Int)
      ->
        cn (const False) list `shouldBe` 0
    it "gives the length for const True" $ property $
      \ (list :: List Int)
      ->
        cn (const True) list `shouldBe` length list
  describe "ce" $ do
    it "gives a non-negative result" $ property $
      \ (char :: Int)
        (list :: List Int)
      ->
        ce char list `shouldBeAtLeast` 0
    it "gives at most the length of the list" $ property $
      \ (char :: Int)
        (list :: List Int)
      ->
        ce char list `shouldBeAtMost` length list
    it "gives the length minus cne" $ property $
      \ (char :: Int)
        (list :: List Int)
      ->
        ce char list `shouldBe` length list - cne char list
  describe "ca" $ do
    it "gives zero when there are no characters to search for" $ property $
      \ (list :: List Int)
      ->
        ca [] list `shouldBe` 0
    it "gives the length when the two lists are the same" $ property $
      \ (list :: List Int)
      ->
        ca list list `shouldBe` length list
    it "gives the same result as ce when supplied with a single character" $ property $
      \ (char :: Int)
        (list :: List Int)
      ->
        ca [char] list `shouldBe` ce char list
    it "gives a non-negative result" $ property $
      \ (chars :: List Int)
        (list :: List Int)
      ->
        ca chars list `shouldBeAtLeast` 0
    it "gives at most the length of the list" $ property $
      \ (chars :: List Int)
        (list :: List Int)
      ->
        ca chars list `shouldBeAtMost` length list
    it "gives the length minus cna" $ property $
      \ (chars :: List Int)
        (list :: List Int)
      ->
        ca chars list `shouldBe` length list - cna chars list
    it "satisfies the triangle inequality" $ property $
      \ (chars1 :: List Int)
        (chars2 :: List Int)
        (list :: List Int)
      ->
        ca (chars1 ++ chars2) list `shouldBeAtMost` ca chars1 list + ca chars2 list
