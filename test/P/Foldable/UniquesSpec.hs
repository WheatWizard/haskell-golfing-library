{-# Language ScopedTypeVariables #-}
module P.Foldable.UniquesSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import Data.List


import P.Aliases
import P.Foldable.Uniques


spec :: Spec
spec = do
  describe "nbc" $ do
    it "is idempotent" $ property $
      \ (xs :: List Int)
      ->
        nbc (nbc xs) `shouldBe` nbc xs
    it "is idempotent with nub" $ property $
      \ (xs :: List Int)
      ->
        nb (nbc xs) `shouldBe` nb xs
    it "creates no groups longer than 1 element" $ property $
      \ (xs :: List Int)
      ->
        group (nbc xs) `shouldAllSatisfy` (==1) . length
    context "when the input is empty" $
      it "the output is empty" $
        nbc ([] :: List Int) `shouldBe` []
    context "when the input is non-empty" $
      it "the output is non-empty" $ property $
        \ (NonEmpty (xs :: List Int))
        ->
          nbc xs `shouldNotBe` []
  describe "ncW" $ do
    context "when the comparison always returns true" $ do
      it "returns a singleton" $ property $
        \ (NonEmpty (xs :: List Int))
        ->
          ncW (const $ const True) xs `shouldSatisfy` (==1) . length
    context "when the comparison always returns false" $ do
      it "returns the input" $ property $
        \ (xs :: List Int)
        ->
          ncW (const $ const False) xs `shouldBe` xs
