{-# Options_GHC -Wno-deprecations #-}
{-# Language ScopedTypeVariables #-}
module P.Foldable.MinMaxSpec
  ( spec
  )
  where


import Prelude hiding
  ( Ordering (..)
  )


import Test.Hspec
import Test.QuickCheck


import P.Arithmetic.Nat
import qualified P.Arithmetic.NatSpec as Nat
import P.Foldable.MinMax
import P.Ord


spec :: Spec
spec = do
  describe "mn" $ do
    it "doesn't evaluate past the first level of any element if the first element is Zero" $ property $
      \ (list :: [Nat]) ->
        mn (Zero : list ++ [Succ $ Prelude.error "ERROR!"]) `shouldBe` Zero
  describe "xW" $ do
    it "returns the earlier element when there is a tie" $
      xW (\_ _ -> EQ) [1..20] `shouldBe` 1
    it "returns an element which is is greater than or equal to every element in the list" $
      forAll (listOf1 chooseAny) $
        \ (list :: [Int]) ->
          xW cp list `shouldSatisfy` (\ x -> all (le x) list)
  describe "mW" $ do
    it "returns the earlier element when there is a tie" $
      mW (\_ _ -> EQ) [1..20] `shouldBe` 1
    it "returns an element which is is less than or equal to every element in the list" $
      forAll (listOf1 chooseAny) $
        \ (list :: [Int]) ->
          mW cp list `shouldSatisfy` (\ x -> all (ge x) list)
  describe "xB" $ do
    it "returns the earlier element when there is a tie" $
      xB (\_ -> 0 :: Int) [1..20] `shouldBe` 1
  describe "mB" $ do
    it "returns the earlier element when there is a tie" $
      mB (\_ -> 0 :: Int) [1..20] `shouldBe` 1
  describe "xb" $ do
    it "never produces an element smaller than the bound" $ property $
      \ (bound :: Int) ->
        \ (list :: [Int]) ->
          xb bound list `shouldSatisfy` ge bound
  describe "mb" $ do
    it "never produces an element larger than the bound" $ property $
      \ (bound :: Int) ->
        \ (list :: [Int]) ->
          mb bound list `shouldSatisfy` le bound
  describe "xdW" $ do
    it "returns the earlier element when there is a tie" $
      xdW (\_ _ -> EQ) 0 [1..20] `shouldBe` 1
    it "produces the default on an empty list" $ property $
      \ (def :: Int) ->
        xdW undefined def [] `shouldBe` def
    it "produces an element in the list when the list is non-empty" $ property $
      \ (def :: Int) ->
        forAll (listOf1 chooseAny) $
          \ (list :: [Int]) ->
            xdW cp def list `shouldSatisfy` flip elem list
    it "returns an element which is is greater than or equal to every element in the list" $ property $
      \ (def :: Int) ->
        \ (list :: [Int]) ->
          xdW cp def list `shouldSatisfy` (\ x -> all (le x) list)
  describe "ndW" $ do
    it "returns the earlier element when there is a tie" $
      ndW (\_ _ -> EQ) 0 [1..20] `shouldBe` 1
    it "produces the default on an empty list" $ property $
      \ (def :: Int) ->
        ndW undefined def [] `shouldBe` def
    it "produces an element in the list when the list is non-empty" $ property $
      \ (def :: Int) ->
        forAll (listOf1 chooseAny) $
          \ (list :: [Int]) ->
            ndW cp def list `shouldSatisfy` flip elem list
    it "returns an element which is is less than or equal to every element in the list" $ property $
      \ (def :: Int) ->
        \ (list :: [Int]) ->
          ndW cp def list `shouldSatisfy` (\ x -> all (ge x) list)
