{-# Language ScopedTypeVariables #-}
module P.List.PairsSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Aliases
import P.Arithmetic.Nat
import P.Foldable.Length
import P.List.Pairs


spec :: Spec
spec = do
  describe "pA" $ do
    it "produces the empty list on empty input" $
      pA ([] :: List Int) `shouldBe` ([] :: List (Int, Int))
    it "produces a list one shorter than the input on non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (x :: List Int)
        ->
          pA x `shouldSatisfy` (== lnt x) . (+ 1) . lnt
  describe "apa" $ do
    it "always succeeds on the empty list" $
      apa (\_ _ -> False) [] `shouldBe` True
    it "always succeeds on a list of length 1" $ property $
      \ (x :: Int)
      ->
        apa (\_ _ -> False) [x] `shouldBe` True
    it "always succeeds for the trivial predicate" $ property $
      \ (x :: List Int)
      ->
        apa (\_ _ -> True) x `shouldBe` True
    it "always fails on lists of length 2 or greater with the trivially false predicate" $ property $
      forAll (listOf2 arbitrary) $
        \ (x :: List Int)
        ->
          apa (\_ _ -> False) x `shouldBe` False
    it "produces a false when the input has mixed values" $ property $
      forAll (listOf2 arbitrary) $
        \ (x :: List Bool)
        ->
          apa (&&) x `shouldBe` and x
  describe "epa" $ do
    it "always fails on the empty list" $
      epa (\_ _ -> True) [] `shouldBe` False
    it "always fails on a list of length 1" $ property $
      \ (x :: Int)
      ->
        epa (\_ _ -> True) [x] `shouldBe` False
    it "always fails for the trivially false predicate" $ property $
      \ (x :: List Int)
      ->
        epa (\_ _ -> False) x `shouldBe` False
    it "always succeeds on lists of length 2 or greater with the trivial predicate" $ property $
      forAll (listOf2 arbitrary) $
        \ (x :: List Int)
        ->
          epa (\_ _ -> True) x `shouldBe` True
    it "produces a true when the input has mixed values" $ property $
      forAll (listOf2 arbitrary) $
        \ (x :: List Bool)
        ->
          epa (||) x `shouldBe` or x

