{-# Language ScopedTypeVariables #-}
module P.List.SplitSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import Data.Char


import P.Char
import P.Foldable.Count
  ( cn
  , ce
  )
import P.List.Expandable
  ( ic
  )
import P.List.Infix
import P.List.Split


charPredicate :: Gen (String, Char -> Bool)
charPredicate =
  elements
    [ ("const True", const True)
    , ("const False", const False)
    , ("isSpace", iW)
    , ("even . ord", even . ord)
    ]


spec :: Spec
spec = do
  describe "sY" $ do
    it "creates 1 more pieces than number of characters that match the predicate" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          length (sY pred string) `shouldBe` cn pred string + 1
    it "doesn't create any chunks with characters which satisfy the predicate" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY pred string `shouldAllSatisfy` not . any pred
    it "includes every character not passing the predicate in the input in some chunk" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY pred string `shouldSatisfy` (\ x -> all (\ y -> pred y || any (Prelude.elem y) x) string)
    it "only produces chunks which are an infix of the input" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY pred string `shouldAllSatisfy` fiw string
  describe "sY'" $ do
    it "only gives non-empty pieces" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY' pred string `shouldSatisfy` notElem []
    it "doesn't create any chunks with characters which satisfy the predicate" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY' pred string `shouldAllSatisfy` not . any pred
    it "includes every character not passing the predicate in the input in some chunk" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY' pred string `shouldSatisfy` (\ x -> all (\ y -> pred y || any (Prelude.elem y) x) string)
    it "only produces chunks which are an infix of the input" $
      forAllShow charPredicate fst $
        \ (_, pred)
          (string :: String)
        ->
          sY' pred string `shouldAllSatisfy` fiw string
  describe "sYe" $ do
    it "creates 1 more pieces than number of matching characters" $ property $
      \ (splitChars :: String)
        (string :: String)
      ->
        length (sYe splitChars string) `shouldBe` cn (flip elem splitChars) string + 1
    it "doesn't create any chunks including the splitting characters" $ property $
      \ (splitChars :: String)
        (string :: String)
      ->
         sYe splitChars string `shouldSatisfy` all (not . any (flip elem splitChars))
    it "only produces chunks which are an infix of the input" $ property $
      \ (splitChars :: String)
        (string :: String)
      ->
        sYe splitChars string `shouldSatisfy` all (fiw string)
  describe "sL" $ do
    it "creates 1 more pieces than number of matching characters" $ property $
      \ (splitChar :: Char)
        (string :: String)
      ->
        length (sL splitChar string) `shouldBe` ce splitChar string + 1
    it "is undone by intercalate" $ property $
      \ (splitChar :: Char)
        (string :: String)
      ->
        ic [splitChar] (sL splitChar string) `shouldBe` string

