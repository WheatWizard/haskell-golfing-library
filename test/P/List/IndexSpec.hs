{-# Language ScopedTypeVariables #-}
module P.List.IndexSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Aliases
import P.List.Index


spec :: Spec
spec = do
  describe "(!)" $ do
    it "gives the first value when indexing by zero" $ property $
      \ (NonEmpty (list :: List Int))
      ->
        list ! (0 :: Int) `shouldBe` head list
    it "gives the last value when indexing by negative one" $ property $
      \ (NonEmpty (list :: List Int))
      ->
        list ! (-1 :: Int) `shouldBe` last list
    it "indexing from the bitwise compliment is the same as indexing the list reversed" $ property $
      \ (index :: Int)
        (NonEmpty (list :: List Int))
      ->
        (reverse list) ! index `shouldBe` list ! (-1 - index)
    it "always gives a value in the list" $ property $
      \ (index :: Int)
        (NonEmpty (list :: List Int))
      ->
        list ! index `shouldSatisfy` flip elem list
