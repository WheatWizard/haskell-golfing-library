{-# Language ScopedTypeVariables #-}
module P.List.InfixSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Algebra.Monoid.Free.Substring
import P.Aliases
import P.List.Infix


spec :: Spec
spec = do
  describe "iw" $ do
    it "the empty list is an infix of any list" $ property $
      \ (list :: List Int)
      ->
        iw [] list `shouldBe` True
    it "every list is an infix of itself" $ property $
      \ (list :: List Int)
      ->
        iw list list `shouldBe` True
    it "a list is an infix of it sandwiched between two other lists" $ property $
      \ (list :: List Int)
        (inFront :: List Int)
        (behind :: List Int)
      ->
        iw list (inFront ++ list ++ behind) `shouldBe` True
    it "if two lists are mutually infixes they are equal" $ property $
      \ (list1 :: List Int)
        (list2 :: List Int)
      ->
          (iS list1 list2) && (iS list2 list1) `shouldBe` list1 == list2
  describe "iWW" $ do
    it "behaves liks iw when given eq" $ property $
      \ (list1 :: List Int)
        (list2 :: List Int)
      ->
        iWW (==) list1 list2 `shouldBe` iw list1 list2
    it "always finds the empty list as an infix" $ property $
      \ (list :: List Int)
      ->
        iWW undefined [] list `shouldBe` True
  describe "ih" $ do
    it "returns an infix of the input" $ property $
      \ (list :: List Int)
        (bound :: Int)
      ->
        ih (< bound) list `shouldSatisfy` fiw list
    it "returns the entire list when everything passes" $ property $
      \ (list :: List Int)
      ->
        ih (\ _ -> True) list `shouldSatisfy` (== list)
    it "returns a list with all elements satisfying the predicate" $ property $
      \ (list :: List Int)
        (bound :: Int)
      ->
        ih (< bound) list `shouldSatisfy` all (< bound)
  describe "cSt" $ do
    it "returns nothing on the always false predicate" $ property $
      \ (x :: List Int)
      ->
        cSt (const False) x `shouldBe` []
    it "returns only infixes of the list" $ property $
      \ (x :: List Int)
      ->
        cSt (even . sum) x `shouldAllSatisfy` fiw x
