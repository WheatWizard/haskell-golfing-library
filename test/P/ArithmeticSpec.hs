{-# Language ScopedTypeVariables #-}
{-# Language LambdaCase #-}
module P.ArithmeticSpec
  ( spec
  )
  where


import Prelude
  ( Integer
  , ($)
  , pure
  , Bool (..)
  , Eq (..)
  , (^)
  , (*)
  , (+)
  , (<)
  )


import Test.Hspec
import Test.QuickCheck


import Test.Util


import P.Arithmetic
import P.Arithmetic.Primes
import P.Foldable.Length


spec :: Spec
spec = do
  describe "xrt" $ do
    it "returns a positive result when the input is a perfect power" $ property $
      \ (b :: Integer) (Positive (p :: Integer)) ->
        xrt p (b^p) `shouldSatisfy` nø
  -- TODO BROKEN?
  --  it "works as a base when it gets a result" $ property $
  --    \ (NonNegative (p :: Integer)) (k :: Integer) ->
  --      xrt p k `shouldSatisfy` (\case [] -> True; [b] -> b^p == k)
  describe "xlg" $ do
    it "returns a positive result when the input is a perfect power of the base" $ property $
      \ (Positive (b :: Integer)) (NonNegative (p :: Integer)) ->
        xlg b (b^p) `shouldSatisfy` nø
    it "works as an exponent when it gets a result" $ property $
      \ (b :: Integer) (k :: Integer) ->
        xlg b k `shouldSatisfy` (\case [] -> True; [p] -> b^p == k)
  describe "kvD" $ do
    it "can be put back together to get the original number" $ property $
      \ (Positive (n :: Integer))
        (AtLeast2 (m :: Integer))
      ->
        kvD n m `shouldSatisfy` (\(a, b) -> m^a*b == n)
    it "doesn't give a remainder divisible by m" $ property $
      \ (Positive (n :: Integer))
        (AtLeast2 (m :: Integer))
       ->
        klg n m `shouldSatisfy` fnb m
  describe "xgD" $ do
    it "can be put back together to get the original number" $ property $
      \ (Positive (n :: Integer))
        (AtLeast2 (m :: Integer))
      ->
        xgD n m `shouldSatisfy` (\(a, b, c) -> m^a*(m*b+c) == n)
    it "gives the same first number as kvD" $ property $
      \ (Positive (n :: Integer))
        (AtLeast2 (m :: Integer))
      ->
        xgD n m `shouldSatisfy` (\(a, _, _) -> a == krm n m)
    it "gives a final remainder less than m" $ property $
      \ (Positive (n :: Integer))
        (AtLeast2 (m :: Integer))
      ->
        xgD n m `shouldSatisfy` (\(_, _, c) -> c < m)
