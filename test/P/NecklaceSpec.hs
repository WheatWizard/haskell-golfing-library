{-# Language ScopedTypeVariables #-}
module P.NecklaceSpec
  ( spec
  )
  where


import Prelude hiding
  ( (==)
  )


import Test.Hspec
import Test.QuickCheck
import Test.Util


import Control.Applicative


import P.Aliases
import P.Eq
import P.Foldable.Length
import P.List
import P.Necklace


spec :: Spec
spec = do
  describe "Eq" $ do
    it "succeeds on rotations of the base list" $ property $
      \ (s :: List Int)
        (i :: Int)
      ->
        Nk (ryl i s) `shouldBe` Nk s
    it "compares the elements of singletons" $ property $
      \ (x1 :: Int)
        (x2 :: Int)
      ->
        Nk [x1] == Nk [x2] `shouldBe` x1 == x2
    it "fails on lists of different lengths" $ property $
      \ (s1 :: List Int)
      ->
        forAll (arbitrary `suchThat` nlE s1) $
          \ (s2 :: List Int)
          ->
            Nk s1 `shouldNotBe` Nk s2
  describe "Eq1" $ do
    it "has the same result as eq when used with eq" $ property $
      \ (s1 :: List Int)
        (s2 :: List Int)
      ->
        q1 eq (Nk s1) (Nk s2) `shouldBe` eq (Nk s1) (Nk s2)
    it "succeeds in comparing two empty lists" $
      q1 (const (const False)) [] [] `shouldBe` True
    it "checks compares only the length of two lists with a trivially true predicate" $ property $
      \ (l :: Int)
      ->
        forAll (liftA2 (,) (vectorOf l arbitrary) (vectorOf l arbitrary)) $
          \ ( s1 :: List Int
            , s2 :: List Int
            )
          ->
            q1 (const $ const True) s1 s2 `shouldBe` True
