{-# Language ScopedTypeVariables #-}
module P.Algebra.Monoid.Free.PrefixSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Algebra.Monoid.Free.Prefix
import P.Aliases
import P.List.Comparison
  ( sMI
  )


spec :: Spec
spec = do
  describe "pSt" $ do
    it "returns one more prefix than the length of the input when the predicate is trivial" $ property $
      \ (x :: List Int)
      ->
        pSt (const True) x `shouldSatisfy` (\ ps -> length ps == length x + 1)
    it "returns nothing on the always false predicate" $ property $
      \ (x :: List Int)
      ->
        pSt (const False) x `shouldBe` []
    it "returns the outputs in order of increasing length" $ property $
      \ (x :: List Int)
      ->
        pSt (even . sum) x `shouldSatisfy` sMI . map length
    it "returns only prefixes of the list" $ property $
      \ (x :: List Int)
      ->
        pSt (even . sum) x `shouldAllSatisfy` sw x
  describe "swW" $ do
    it "behaves like sw when given eq" $ property $
      \ (list1 :: List Int)
        (list2 :: List Int)
      ->
        swW (==) list1 list2 `shouldBe` sw list1 list2
