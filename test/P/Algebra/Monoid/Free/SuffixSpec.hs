{-# Language ScopedTypeVariables #-}
module P.Algebra.Monoid.Free.SuffixSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Algebra.Monoid.Free.Suffix
import P.Aliases
import P.List.Comparison
  ( sMI
  )


spec :: Spec
spec = do
  describe "sst" $ do
    it "returns one more suffix than the length of the input when the predicate is trivial" $ property $
      \ (x :: List Int)
      ->
        sst (const True) x `shouldSatisfy` (\ ps -> length ps == length x + 1)
    it "returns nothing on the always false predicate" $ property $
      \ (x :: List Int)
      ->
        sst (const False) x `shouldBe` []
    it "returns the outputs in order of increasing length" $ property $
      \ (x :: List Int)
      ->
        sst (even . sum) x `shouldSatisfy` sMI . map length
    it "returns only suffixes of the list" $ property $
      \ (x :: List Int)
      ->
        sst (even . sum) x `shouldAllSatisfy` ew x
