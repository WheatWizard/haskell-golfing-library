{-# Language TypeApplications #-}
module P.Algebra.Loop.Moufang.StapleSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Algebra.Tests
import Test.Arbitrary.Instances


import P.Algebra.Free.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Loop.Moufang.Staple
import P.Algebra.Quasigroup


spec :: Spec
spec = do
  test @Quasigroup @(Staple (FreeGroup Int))
  test @Loop @(Staple (FreeGroup Int))
  test @Moufang @(Staple (FreeGroup Int))
