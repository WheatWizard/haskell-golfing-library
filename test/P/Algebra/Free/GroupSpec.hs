{-# Language ScopedTypeVariables #-}
{-# Language TypeApplications #-}
module P.Algebra.Free.GroupSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Algebra.Tests
import Test.Arbitrary.Instances


import P.Algebra.Free.Group
import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Shelf


spec :: Spec
spec = do
  test @Semigroup @(FreeGroup Int)
  test @Monoid @(FreeGroup Int)
  test @Quasigroup @(FreeGroup Int)
  test @Loop @(FreeGroup Int)
  test @Moufang @(FreeGroup Int)
  test @Shelf @(FreeGroup Int)
  test @Rack @(FreeGroup Int)
  test @Quandle @(FreeGroup Int)
  test @Group @(FreeGroup Int)
  it "if two elements commute then they are either equal or inverses" $ property $
    \ (x :: FreeGroup Int)
      (y :: FreeGroup Int)
    ->
      (x <> y) == (y <> x) `shouldBe` (x == IV y) || (x == y)
