{-# Language ScopedTypeVariables #-}
{-# Language TypeApplications #-}
module P.Algebra.Free.KeiSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Algebra.Tests
import Test.Arbitrary.Instances


import P.Algebra.Free.Kei
import P.Algebra.Kei
import P.Algebra.Quandle
import P.Algebra.Rack
import P.Algebra.Shelf


spec :: Spec
spec = do
  context "FreeKei" $ do
    test @Shelf @(FreeKei Int)
    test @Rack @(FreeKei Int)
    test @Quandle @(FreeKei Int)
    test @Kei @(FreeKei Int)
    it "doesn't consider distinct generators equal" $ property $
      \ (x :: Int)
        (y :: Int)
      ->
        KPr x == KPr y `shouldBe` x == y
    it "doesn't distribute the opposite way in the general position" $
      forAll (arbitrary `suchThat` (\(x,y,z)-> x /= y && y /= z && x /= z)) $
        \ ( x :: Int
          , y :: Int
          , z :: Int
          )
        ->
          KPr x !> (KPr y !> KPr z) `shouldNotBe` (KPr x !> KPr y) !> (KPr x !> KPr z)
    it "is not associative in the general position" $
      forAll (arbitrary `suchThat` (\(x,y,z)-> x /= y && y /= z && x /= z)) $
        \ ( x :: Int
          , y :: Int
          , z :: Int
          )
        ->
          KPr x !> (KPr y !> KPr z) `shouldNotBe` (KPr x !> KPr y) !> KPr z
    it "is not commutative in the general position" $
      forAll (arbitrary `suchThat` uncurry (/=)) $
        \ ( x :: Int
          , y :: Int
          )
        ->
          KPr x !> KPr y `shouldNotBe` KPr y !> KPr x
