{-# Language ScopedTypeVariables #-}
{-# Language TypeApplications #-}
module P.Algebra.Free.QuandleSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Algebra.Tests
import Test.Arbitrary.Instances


import P.Algebra.Free.Quandle
import P.Algebra.Quandle
import P.Algebra.Rack
import P.Algebra.Shelf


spec :: Spec
spec = do
  test @Shelf @(FreeQuandle Int)
  test @Rack @(FreeQuandle Int)
  test @Quandle @(FreeQuandle Int)
