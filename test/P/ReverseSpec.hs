{-# Language ScopedTypeVariables #-}
module P.ReverseSpec
  ( spec
  )
  where


import Test.Arbitrary.Instances
import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Bifunctor.Flip
import P.Foldable.Length
import P.List.Fenceposts
import P.Reverse


spec :: Spec
spec = do
  context "fencepost lists" $ do
    describe "_rv" $ do
      it "doesn't change the length" $ property $
        \ (xs :: Fenceposts Int Int)
        ->
           _rv xs `shouldSatisfy` lEq xs
      it "should contain no new fences" $ property $
        \ (xs :: Fenceposts Int Int)
        ->
          _rv xs `shouldAllSatisfy` flip elem xs
      it "should contain no new posts" $ property $
        \ (xs :: Fenceposts Int Int)
        ->
          Flp (_rv xs) `shouldAllSatisfy` flip elem (Flp xs)
      it "is involute" $ property $
        \ (xs :: Fenceposts Int Int)
        ->
          _rv (_rv xs) `shouldBe` xs
