{-# Options_GHC -Wno-deprecations #-}
{-# Language ScopedTypeVariables #-}
{-# Language TypeApplications #-}
module P.Parse.CombinatorSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import Data.Function


import P.Aliases
import P.Parse
import P.Parse.Combinator
import P.Parse.Extra
import P.Swap


spec :: Spec
spec = do
  describe "ʃ" $ do
    it "always parses the input string exactly" $ property $
      \ (s :: String) ->
        s `shouldSatisfy` x1 (ʃ s :: Parser List String String String)
    it "splits its string off the front" $ property $
      \ (s1 :: String) s2 ->
        uP (ʃ s1) (s1 <> s2) `shouldBe` [(s2, s1)]
  describe "lA" $ do
    it "never consumes input" $ property $
      \ (s1 :: String) (s2 :: String) ->
        gP (Sw $ lA $ ʃ $ s1) s2 `shouldAllBe` s2
    it "succeeds when the parser suceeds" $ property $
      \ (s1 :: String) s2 ->
        let
          parser :: Parser List String String String
          parser =
            ʃ $ s1
        in
          gP (lA parser) s2 `shouldBe` gP parser s2
  describe "nA" $ do
    it "never consumes input" $ property $
      \ (s1 :: String) (s2 :: String) ->
        gP (Sw $ nA $ ʃ $ s1) s2 `shouldAllBe` s2
    it "fails when the parser suceeds" $ property $
      \ (s1 :: String) s2 ->
        let
          parser :: Parser List String String String
          parser =
            ʃ $ s1
        in
          pP (nA parser) s2 `shouldBe` not (pP parser s2)
  describe "gre" $ do
    it "never has more than one parse" $ property $
      \ (s1 :: String) ->
        uP (gre h') s1 `shouldHaveAtMost` 1
  describe "laz" $ do
    it "never has more than one parse" $ property $
      \ (s1 :: String) ->
        uP (laz h') s1 `shouldHaveAtMost` 1
  describe "yju" $ do
    it "is idempotent" $ property $
      \ (s1 :: String) s2 ->
        let
          parser :: Parser List String String String
          parser =
            ʃ $ s1
        in
          uP (parser @<* parser) s2 `shouldBe` uP parser s2
  describe "ymu" $ do
    it "is idempotent" $ property $
      \ (s1 :: String) s2 ->
        let
          parser :: Parser List String String String
          parser =
            ʃ $ s1
        in
          uP (parser @^* parser) s2 `shouldBe` uP parser s2
  describe "hd" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP (hd :: Parser List String String Char)
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s@(x : xs) :: String) ->
          uP hd s `shouldBe` [(xs, x)]
  describe "hdS" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP (hdS :: Parser List String String String)
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s@(x : xs) :: String) ->
          uP hdS s `shouldBe` [(xs, [x])]
  describe "bhd" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP (bhd :: Parser List String String Char)
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
         \ (s :: String) ->
          uP bhd s `shouldBe` [(init s, last s)]
  describe "en" $ do
    it "matches empty input" $
      "" `shouldSatisfy` pP (en :: Parser List String String ())
    it "has no parse on nonempty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s :: String) ->
          s `shouldNotSatisfy` pP (en :: Parser List String String ())
  describe "blj" $ do
    context "(blj .* xys) \"(_\" \")_\"" $ do
      it "works on balanced strings" $
        forAll (genBalanced "()") $
          \ (s :: String)
          ->
            s `shouldSatisfy` cP (on blj xys "(_" ")_")
      it "works on balanced strings with substituted underscores" $
        forAll (genBalanced "()(__)") $
          \ (s :: String)
          ->
            s `shouldSatisfy` cP (on blj xys "(_" ")_")
      let
        t1 s =
          ')' : s `shouldNotSatisfy` cP (on blj xys "(_" ")_")
      it "fails on a string starting with a )" $ property t1
      it "fails on a passing string with ) prepended" $ forAll (genBalanced "()(__)") t1
      let
        t2 s =
          s ++ "(" `shouldNotSatisfy` cP (on blj xys "(_" ")_")
      it "fails on a string ending with a (" $ property t2
      it "fails on a passing string with ( appended" $ forAll (genBalanced "()(__)") t2
    context "blj opn cls" $ do
      it "works on balanced strings" $
        forAll (genBalanced "()") $
          \ (s :: String)
          ->
            s `shouldSatisfy` cP (blj opn cls)
      let
        t1 s =
          ')' : s `shouldNotSatisfy` cP (blj opn cls)
      it "fails on a string starting with a )" $ property t1
      it "fails on a balanced string prepended with )" $ forAll (genBalanced "()") t1
      let
        t2 s =
          s ++ "(" `shouldNotSatisfy` cP (blj opn cls)
      it "fails on a string ending with a (" $ property t2
      it "fails on a balanced string appended with (" $ forAll (genBalanced "()") t2

