{-# Options_GHC -Wno-deprecations #-}
module P.String.UnicodeSpec
  ( spec
  )
  where


import Data.Foldable
  ( for_
  )

import Test.Hspec
import Test.QuickCheck


import P.Char
import P.String.Unicode


combiningMarks :: String
combiningMarks =
  ['\768' .. '\846']
  ++ ['\848' .. '\855']
  ++ ['\861' .. '\866']


-- | Produces a random character from the combining diacritical marks block.
arbitraryCombiningMark :: Gen Char
arbitraryCombiningMark =
  elements combiningMarks


spec :: Spec
spec = do
  describe "nfd" $ do
    it "doesn't error" $ property $
      \ x ->
        nfd x `shouldSatisfy` const True
  describe "hsD" $ do
    it "returns true on all the combining diacritics" $ conjoin $
      flip map combiningMarks $
        \ x ->
          x `shouldSatisfy` hsD
  describe "rmD" $ do
    it "produces the correct result on specific characters" $ conjoin $
      map
        (\ (x, y) -> rmD x `shouldBe` y)
        [ ('ǽ', 'æ')
        , ('ώ', 'ω')
        , ('Ё', 'Е')
        , ('ı', 'ı')
        , ('\47999', '\47999')
        , ('\63987', '\63987')
        , ('\67492', '\NUL')
        , ('\119136', '\119136')
        , ('\194802', '\194802')
        ]
    it "it doesn't do anything if it doesn't have a diacritic" $ property $
      withMaxSuccess 500 $
        forAll (arbitraryUnicodeChar `suchThat` nhD) $
          \ x ->
            rmD x `shouldBe` x
    it "it doesn't do anything if it isn't a letter" $ property $
      withMaxSuccess 500 $
        forAll (arbitraryUnicodeChar `suchThat` (not . isLetter)) $
          \ x ->
            rmD x `shouldBe` x
    it "doesn't error on a combining mark" $ conjoin $
      flip map combiningMarks $
        \ x ->
          rmD x `shouldSatisfy` const True
    context "when given a letter" $ do
      it "always returns a letter" $ property $
        withMaxSuccess 5000 $
          forAll (arbitraryUnicodeChar `suchThat` isLetter) $
            \ x ->
              rmD x `shouldSatisfy` (\y -> isLetter y || y == '\NUL' )
      it "always returns a char without a diacritic" $ property $
        withMaxSuccess 5000 $
          forAll (arbitraryUnicodeChar `suchThat` isLetter) $
            \ x ->
              rmD x `shouldSatisfy` nhD
