{-# Language ScopedTypeVariables #-}
module P.StringSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.List
import P.String


spec :: Spec
spec = do
  go bla "bla" '<' '>'
  go blc "blc" '{' '}'
  go blp "blp" '(' ')'
  go bls "bls" '[' ']'
  describe "bld" $ do
    it "returns true on the empty string" $ property $
      \ (s :: String)
      ->
        "" `shouldSatisfy` bld s
    it "considers everything balanced with no delimeters" $ property $
      \ (s :: String)
      ->
        s `shouldSatisfy` bld ""
    it "considers a single brace as an illegal symbol" $ property $
      \ (c :: Int)
        (s :: [Int])
      ->
        bld [c] s `shouldBe` notElem c s
    -- it "" $
    --   forAll (genBalanced "()(**)") $
    --     \ (s :: String)
    --     ->
    --       s `shouldSatisfy` bld "()(**)"
  describe "blF" $ do
    it "matches balanced string consisting of one type of brace" $
      forAll (elements ["[]", "{}", "<>", "()"] >>= genBalanced) $
        \ s
        ->
          s `shouldSatisfy` blF
    it "matches a balanced string" $
      forAll (genBalanced "(){}[]<>") $
        \ (s :: String)
        ->
          s `shouldSatisfy` blF
    it "fails on a mismatched pair anywhere" $ property $
      \ (s1 :: String)
        (s2 :: String)
      -> do
        s1 ++ "{)" ++ s2 `shouldNotSatisfy` blF
        s1 ++ "<]" ++ s2 `shouldNotSatisfy` blF
  where
    go func funcName open close =
      describe funcName $ do
        it "works on balanced strings" $
          forAll (genBalanced [open, close]) $
            \ (s :: String)
            ->
              s `shouldSatisfy` func
        it "works on balanced strings with an arbitrary non-brace sequence appended to the end" $
          forAll (genBalanced [open, close]) $
            \ (s :: String)
            ->
              s ++ "test" `shouldSatisfy` func
        it ("fails on a string starting with a " ++ [close]) $ property $
          \ (s :: String)
          ->
            close : s `shouldNotSatisfy` func
        it ("fails on a string ending with a " ++ [open]) $ property $
          \ (s :: String)
          ->
            s ++ [open] `shouldNotSatisfy` func
