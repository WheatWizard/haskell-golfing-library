{-# Language ScopedTypeVariables #-}
module P.Ord.Compare3Spec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Aliases
import P.Ord.Compare3


-- | The positive even numbers
evens :: List Int
evens =
  (2*) <$> [1..]


spec :: Spec
spec =
  describe "eI" $ do
    it "finds positive even numbers in the positive even numbers" $ property $
      \ (Positive (n :: Int)) ->
        eI (2*n) evens `shouldBe` True
    it "doesn't find positive odd numbers in the positive even numbers" $ property $
      \ (Positive (n :: Int)) ->
        eI (2*n+1) evens `shouldBe` False
    it "doesn't find negative numbers in the positive even numbers" $ property $
      \ (Negative (n :: Int)) ->
        eI n evens `shouldBe` False

    it "finds negative even numbers in the negative even numbers" $ property $
      \ (Negative (n :: Int)) ->
        eI (2*n) (negate <$> evens) `shouldBe` True
    it "doesn't find negative odd numbers in the negative even numbers" $ property $
      \ (Negative (n :: Int)) ->
        eI (2*n-1) (negate <$> evens) `shouldBe` False
    it "doesn't find positive numbers in the negative even numbers" $ property $
      \ (Positive (n :: Int)) ->
        eI n (negate <$> evens) `shouldBe` False
