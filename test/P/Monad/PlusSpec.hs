{-# Language ScopedTypeVariables #-}
module P.Monad.PlusSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Monad.Plus
import P.Parse
import P.Parse.Combinator


spec :: Spec
spec = do
  describe "xmy" $ do
    it "gets the correct results for xmy (yʃ < P1) 0" $ property $
      \ (Positive (x :: Int)) ->
        gc (xmy (yʃ . (+1)) 0) ([1 .. x] >>= show) `shouldBe` [[1 .. x]]
  describe "bmy" $ do
    it "parses runs of consecutive numbers with ν >~* (yʃ < P1)" $ property $
      \ (Positive (x :: Int)) ->
        forAll (arbitrary `suchThat` (> x)) $
          \ y ->
            glk (ν >~* (yʃ . (+1))) ([x .. y] >>= show) `shouldBe` [x .. y]
  describe "bso" $ do
    it "parses runs of at least 2 consecutive numbers with ν >~+ (yʃ < P1)" $ property $
      \ (Positive (x :: Int)) ->
        forAll (arbitrary `suchThat` (> x)) $
          \ y ->
            gc (ν >~+ (yʃ . (+1))) ([x .. y] >>= show) `shouldBe` [[x .. y]]
