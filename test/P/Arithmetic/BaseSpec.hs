{-# Language ScopedTypeVariables #-}
module P.Arithmetic.BaseSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import Test.Util


import P.Aliases
import P.Arithmetic.Base


-- | Outputs an integer other than 0 or 1.
validBase ::
  (
  )
    => Gen (Base Integer)
validBase =
  arbitrary `suchThat` ((> 1) . abs)


spec :: Spec
spec = do
  describe "bs" $ do
    context "positive bases" $ do
      it "produces the empty list when the input is zero" $ property $
        \ (AtLeast2 (base :: Int))
        ->
          bs base 0 `shouldBe` []
      it "doesn't produce negative digits" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          bs base x `shouldAllSatisfy` (>= 0)
      it "doesn't produce any digits greater than or equal to the base" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          bs base x `shouldAllSatisfy` (< base)
      it "produces different outputs for different numbers and the same base" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
          (NonNegative (y :: Int))
        ->
          bs base x == bs base y `shouldBe` x == y
      it "is undone by ubs" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          ubs base (bs base x) `shouldBe` x
    context "negative bases" $ do
      it "produces the empty list when the input is zero" $ property $
        \ (AtLeast2 (base :: Int))
        ->
          bs (negate base) 0 `shouldBe` []
      it "doesn't produce negative digits" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          bs (negate base) x `shouldAllSatisfy` (>= 0)
      it "doesn't produce any digits greater than or equal to the absolute value of the base" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          bs (negate base) x `shouldAllSatisfy` (< base)
      it "produces different outputs for different numbers and the same base" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
          (y :: Int)
        ->
          bs (negate base) x == bs (negate base) y `shouldBe` x == y
      it "is undone by ubs" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          ubs (negate base) (bs (negate base) x) `shouldBe` x
  describe "ubs" $ do
    context "positive bases" $
      it "is consistent with ubS" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          ubs base (bs base x) `shouldBe` ubS base (bS base x)
    context "negative bases" $
      it "is consistent with ubS" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          ubs (negate base) (bs (negate base) x) `shouldBe` ubS (negate base) (bS (negate base) x)
  describe "ubi" $ do
    it "is the inverse of bi" $ property $
      \ (Positive (x :: Int))
      ->
        ubi (bi x) `shouldBe` x
    it "is inverted by bi when the list ends with True" $ property $
      \ (xs :: List Bool)
      ->
        bi (ubi (xs ++ [True]) :: Integer) `shouldBe` xs ++ [True]
  describe "lB" $ do
    context "positive bases" $ do
      it "gives one more than the exponent if the value is a perfect power of the base" $ property $
        \ (AtLeast2 (base :: Integer))
          (NonNegative (exponent :: Integer))
        ->
          lB base (base ^ exponent) `shouldBe` exponent + 1
      it "gives the exponent if the value is one less than a perfect power of the base" $ property $
        \ (AtLeast2 (base :: Integer))
          (NonNegative (exponent :: Integer))
        ->
          lB base (base ^ exponent - 1) `shouldBe` exponent
      it "gives the same value for a perfect power of the base and for a value less than twice that perfect power" $ property $
        \ (AtLeast2 (base :: Integer))
          (NonNegative (exponent :: Integer))
        ->
          forAll (choose (0, base ^ exponent - 1)) $
            \ (noise :: Integer)
            ->
              lB base (base ^ exponent + noise) `shouldBe` lB base (base ^ exponent)
  describe "dgs" $ do
    it "is less than or equal to the input" $
      forAll validBase $
        \ base
          (NonNegative (x :: Integer))
        ->
          dgs base x `shouldSatisfy` (<= x)
  describe "dgR" $ do
    it "is a fixed point for dgs" $
      forAll validBase $
        \ base
          (NonNegative (x :: Integer))
        ->
          dgs base (dgR base x) `shouldBe` dgR base x
    context "positive bases" $ do
      it "is less than or equal to the base" $ property $
        \ (AtLeast2 (base :: Integer))
          (NonNegative (x :: Integer))
        ->
          dgR base x `shouldSatisfy` (<= abs base) . abs

