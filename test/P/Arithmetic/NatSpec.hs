{-# Options_GHC -Wno-deprecations #-}
{-# Language ScopedTypeVariables #-}
module P.Arithmetic.NatSpec
  ( spec
  )
  where


import Prelude
  ( error
  , ($)
  , Bool (..)
  , (<$>)
  , pure
  )


import Test.Hspec
import Test.QuickCheck


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Arithmetic.Nat
import P.Eq
import P.Ord


instance
  (
  )
    => Arbitrary Nat
  where
    arbitrary =
      oneof
        [ pure Zero
        , Succ <$> arbitrary
        ]

    shrink Zero =
      []
    shrink (Succ x) =
      [x]


spec :: Spec
spec = do
  describe "eq" $ do
    it "is symmetric" $ property $
      \ (n :: Nat) m ->
        n == m `shouldBe` m == n
    it "is reflexive" $ property $
      \ (n :: Nat) ->
        n `shouldBe` n
    it "considers no element equal to its successor" $ property $
      \ n ->
        n `shouldNotBe` Succ n
    it "doesn't evaluate excessive terms" $ do
      Zero == Succ (error "evaluated excessive terms on the right") `shouldBe` False
      Succ (error "evaluated excessive terms on the left") == Zero `shouldBe` False

  describe "cp" $ do
    it "doesn't contradict eq" $ property $
      \ (n :: Nat) m ->
        cp n m == EQ `shouldSatisfy` (== (n == m))
    it "doesn't evaluate excessive terms" $ do
      cp Zero (Succ $ error "evaluated excessive terms on the right") `shouldBe` LT
      cp (Succ $ error "evaluated excessive terms on the left") Zero `shouldBe` GT

  describe "<>" $ do
    it "is associative" $ property $
      \ (x :: Nat) y z ->
        (x <> y) <> z `shouldBe` x <> (y <> z)

  describe "mempty" $ do
    it "is a left identity for <>" $ property $
      \ (x :: Nat) ->
        x <> i `shouldBe` x
    it "is a right identity for <>" $ property $
      \ (x :: Nat) ->
        i <> x `shouldBe` x
