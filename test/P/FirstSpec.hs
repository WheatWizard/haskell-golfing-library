{-# Language ScopedTypeVariables #-}
module P.FirstSpec
  ( spec
  )
  where


import Prelude hiding
  ( Maybe
  )


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Algebra.Monoid.Free.Prefix
  ( sw
  )
import P.Algebra.Monoid.Free.Suffix
  ( ew
  )
import P.Aliases
import P.First
import P.List.Fenceposts


-- | Constructs a fencepost list with the an evaluation trap past a certain point.
-- The final post allows evaluation to be forced without triggering the trap, but attempting to uncons at the final post causes an error.
testFenceposts :: Int -> Fenceposts Int Int
testFenceposts 0 =
  Post 0 $ error "Evaluated past the end of the structure"
testFenceposts n =
  Post n $ Fence n $ testFenceposts (n - 1)


spec :: Spec
spec = do
  context "on fencepost lists" $ do
    describe "dr" $ do
      it "doesn't evaluate past the necessary point" $ property $
        \ (NonNegative (i :: Int))
        ->
          i `shouldBe` i
  context "on lists" $ do
    describe "tk" $ do
      it "always gives a prefix of the list" $ property $
        \ (x :: List Int)
          (i :: Int)
        ->
          tk i x `shouldSatisfy` sw x
      it "gives the correct number of elements when taking a non-negative number of elements from an infinite list" $ property $
        \ (NonNegative (i :: Int))
        ->
          length (tk i (Prelude.undefined <$ [1..])) `shouldBe` i
      it "never evaluates past the size given" $ property $
        \ (NonNegative (i :: Int))
        ->
          tk i ([1 .. i] ++ error "Evaluated past the end of the structure") `shouldSatisfy` (== i) . length
    describe "dr" $ do
      it "always gives a suffix of the list" $ property $
        \ (x :: List Int)
          (i :: Int)
        ->
          dr i x `shouldSatisfy` ew x
      it "never evaluates past the size given" $ property $
        \ (NonNegative (i :: Int))
        ->
          dr i (testFenceposts i) `shouldSatisfy` (== 0) . hp
    describe "tW" $ do
      it "gives the entire list on the trivial predicate" $ property $
        \ (x :: List Int)
        ->
          tW (const True) x `shouldBe` x
      it "every element in the result should satsify the predicate" $ property $
        \ (x :: List Int)
        ->
          tW even x `shouldAllSatisfy` even
    describe "dW" $ do
      it "gives the entire list on the null predicate" $ property $
        \ (x :: List Int)
        ->
          dW (const False) x `shouldBe` x
      it "gives the empty list on the trivial predicate" $ property $
        \ (x :: List Int)
        ->
          dW (const True) x `shouldBe` []
