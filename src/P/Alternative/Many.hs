{-|
Module :
  P.Alternative.Many
Description :
  Functions related to the "many" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.Many
  ( my
  , my'
  , mY
  , mY'
  , lMy
  , lMY
  , myt
  , mYt
  , myw
  , myW
  , (**>)
  , amy
  , may
  , (<**)
  , mya
  , yma
  -- * Deprecated
  , many
  , manyTill
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import qualified Control.Applicative


import P.Algebra.Monoid
import P.Aliases
import P.Alternative
import P.Applicative
import P.Arithmetic.Nat
import P.Category
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Functor


{-# Deprecated many "Use my instead" #-}
-- | Long version of 'my'.
many ::
  ( Alternative t
  )
    => t a -> t (List a)
many =
  my


-- | Zero or more.
--
-- Equivalent to 'Control.Applicative.many'.
my ::
  ( Alternative t
  )
    => t a -> t (List a)
my =
  Control.Applicative.many


-- | Zero or more.
--
-- Similar to 'my' but in the reverse order.
-- Smaller results come earlier.
my' ::
  ( Alternative t
  )
    => t a -> t (List a)
my' x =
  go
  where
    go =
      p [] ++ l2 (:) x go


-- | Works like 'my', but the result is combined into one element using the monoidal product.
mY ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
mY =
  m fo < my


-- | A combination of 'mY' and 'my''.
--
-- Parses zero or more of a monoid with a preference for fewer.
mY' ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a
mY' x =
  go
  where
    go =
      p i ++ x *^* go



-- | Works like 'my' but combines results using a user defined function.
myw ::
  ( Alternative t
  )
    => (b -> a -> a) -> a -> t b -> t a
myw f s a =
  sow f s a ++ p s
  where
    sow f s a =
      l2 f a $ myw f s a


-- | Works like 'my'' but combines results using a user defined function.
--
-- 'myw' with inverted priority.
myW ::
  ( Alternative t
  )
    => (b -> a -> a) -> a -> t b -> t a
myW f s a =
  p s ++ soW f s a
  where
    soW f s a =
      l2 f a $ myW f s a


-- | Works like 'my', but counts the number of results instead.
lMy ::
  ( Alternative t
  )
    => t a -> t Nat
lMy =
  m l < my


-- | Like 'lMy' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lMy' does not.
lMY ::
  ( Alternative t
  , Integral n
  )
    => t a -> t n
lMY =
  m l < my


{-# Deprecated manyTill "Use myt instead" #-}
-- | Long version of 'myt'.
manyTill ::
  ( Alternative t
  )
    => t a -> t end -> t (List a)
manyTill =
  myt


-- | Takes two parsers and applies the first repeatedly until the second succeeds.
--
-- Results from the first parser are collected in a list.
myt ::
  ( Alternative t
  )
    => t a -> t end -> t (List a)
myt parser end =
  go
  where
    go =
      ([] <$ end) ++ l2 (:) parser go


-- | Takes two parsers and applies the first repeatedly until the second succeeds.
--
-- Results from the first parser are combined with the monoid action.
mYt ::
  ( Monoid m
  , Alternative t
  )
    => t m -> t end -> t m
mYt parser end =
  go
  where
    go =
      (i <$ end) ++ parser *^* go


-- | Perform something and then many of something else.
-- Returns the value of the left-hand side ignoring the many results from the right.
--
-- @
-- x <** y ≡ x <* my y
-- @
(<**) ::
  ( Alternative t
  )
    => t m -> t a -> t m
x <** y =
  x <* my y


-- | Prefix of '(<**)'.
amy ::
  ( Alternative t
  )
    => t m -> t a -> t m
amy =
  (<**)


-- | Flip of 'amy'.
may ::
  ( Alternative t
  )
    => t a -> t m -> t m
may =
  f' amy


-- | Perform many of something and then perform something else.
-- Returns the value of the right-hand side ignoring the many results from the left.
--
-- @
-- x **> y ≡ my x *> y
-- @
(**>) ::
  ( Alternative t
  )
    => t a -> t m -> t m
x **> y =
  my x *> y


-- | Prefix of '(**>)'.
mya ::
  ( Alternative t
  )
    => t a -> t m -> t m
mya =
  (**>)


-- | Flip of 'mya'.
yma ::
  ( Alternative t
  )
    => t m -> t a -> t m
yma =
  f' mya
