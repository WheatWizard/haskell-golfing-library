{-|
Module :
  P.Alternative.Extra
Description :
  Additional functions for the P.Alternative library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Alternative library.
-}
module P.Alternative.Extra
  ( rl2
  , rl3
  , rl4
  , rl5
  , rl6
  , rl7
  , rl8
  , rl9
  , rl0
  , rl1
  , am1
  , aM1
  , al1
  , aL1
  , am2
  , aM2
  , al2
  , aL2
  , am3
  , aM3
  , al3
  , aL3
  , am4
  , aM4
  , al4
  , aL4
  , am5
  , aM5
  , al5
  , aL5
  , am6
  , aM6
  , al6
  , aL6
  , am7
  , aM7
  , al7
  , aL7
  , am8
  , aM8
  , al8
  , aL8
  , am9
  , aM9
  , al9
  , aL9
  , am0
  , aM0
  , al0
  , aL0
  )
  where


import P.Aliases
import P.Alternative
import P.Alternative.AtLeast
import P.Alternative.AtMost
import P.Alternative.Many


-- | Take an element @a@ and make an 'Alternative' of exactly 2 @a@s.
-- Shorthand for @'rl' 2@
rl2 ::
  ( Alternative t
  )
    => a -> t a
rl2 =
  rl (2 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 3 @a@s.
-- Shorthand for @'rl' 3@
rl3 ::
  ( Alternative t
  )
    => a -> t a
rl3 =
  rl (3 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 4 @a@s.
-- Shorthand for @'rl' 4@
rl4 ::
  ( Alternative t
  )
    => a -> t a
rl4 =
  rl (4 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 5 @a@s.
-- Shorthand for @'rl' 5@
rl5 ::
  ( Alternative t
  )
    => a -> t a
rl5 =
  rl (5 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 6 @a@s.
-- Shorthand for @'rl' 6@
rl6 ::
  ( Alternative t
  )
    => a -> t a
rl6 =
  rl (6 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 7 @a@s.
-- Shorthand for @'rl' 7@
rl7 ::
  ( Alternative t
  )
    => a -> t a
rl7 =
  rl (7 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 8 @a@s.
-- Shorthand for @'rl' 8@
rl8 ::
  ( Alternative t
  )
    => a -> t a
rl8 =
  rl (8 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 9 @a@s.
-- Shorthand for @'rl' 9@
rl9 ::
  ( Alternative t
  )
    => a -> t a
rl9 =
  rl (9 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 10 @a@s.
-- Shorthand for @'rl' 10@
rl0 ::
  ( Alternative t
  )
    => a -> t a
rl0 =
  rl (10 :: Int)

-- | Take an element @a@ and make an 'Alternative' of exactly 11 @a@s.
-- Shorthand for @'rl' 11@
--
-- For @rl 1@ see 'P.Applicative.p'.
rl1 ::
  ( Alternative t
  )
    => a -> t a
rl1 =
  rl (11 :: Int)

-- | At most 2.
-- Shorthand for @'am' 2@.
am2 ::
  ( Alternative t
  )
    => t a -> t (List a)
am2 =
  am (2 :: Int)

-- | At most 2 combined with the monoidal product.
-- Shorthand for @'aM' 2@.
aM2 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM2 =
  aM (2 :: Int)

-- | At least 2.
-- Shorthand for @'al' 2@.
al2 ::
  ( Alternative t
  )
    => t a -> t (List a)
al2 =
  al (2 :: Int)

-- | At least 2 combined with the monoidal product.
-- Shorthand for @'aL' 2@.
aL2 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL2 =
  aL (2 :: Int)


-- | At most 3.
-- Shorthand for @'am' 3@.
am3 ::
  ( Alternative t
  )
    => t a -> t (List a)
am3 =
  am (3 :: Int)

-- | At most 3 combined with the monoidal product.
-- Shorthand for @'aM' 3@.
aM3 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM3 =
  aM (3 :: Int)

-- | At least 3.
-- Shorthand for @'al' 3@.
al3 ::
  ( Alternative t
  )
    => t a -> t (List a)
al3 =
  al (3 :: Int)

-- | At least 3 combined with the monoidal product.
-- Shorthand for @'aL' 3@.
aL3 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL3 =
  aL (3 :: Int)


-- | At most 4.
-- Shorthand for @'am' 4@.
am4 ::
  ( Alternative t
  )
    => t a -> t (List a)
am4 =
  am (4 :: Int)

-- | At most 4 combined with the monoidal product.
-- Shorthand for @'aM' 4@.
aM4 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM4 =
  aM (4 :: Int)

-- | At least 4.
-- Shorthand for @'al' 4@.
al4 ::
  ( Alternative t
  )
    => t a -> t (List a)
al4 =
  al (4 :: Int)

-- | At least 4 combined with the monoidal product.
-- Shorthand for @'aL' 4@.
aL4 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL4 =
  aL (4 :: Int)


-- | At most 5.
-- Shorthand for @'am' 5@.
am5 ::
  ( Alternative t
  )
    => t a -> t (List a)
am5 =
  am (5 :: Int)

-- | At most 5 combined with the monoidal product.
-- Shorthand for @'aM' 5@.
aM5 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM5 =
  aM (5 :: Int)

-- | At least 5.
-- Shorthand for @'al' 5@.
al5 ::
  ( Alternative t
  )
    => t a -> t (List a)
al5 =
  al (5 :: Int)

-- | At least 5 combined with the monoidal product.
-- Shorthand for @'aL' 5@.
aL5 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL5 =
  aL (5 :: Int)


-- | At most 6.
-- Shorthand for @'am' 6@.
am6 ::
  ( Alternative t
  )
    => t a -> t (List a)
am6 =
  am (6 :: Int)

-- | At most 6 combined with the monoidal product.
-- Shorthand for @'aM' 6@.
aM6 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM6 =
  aM (6 :: Int)

-- | At least 6.
-- Shorthand for @'al' 6@.
al6 ::
  ( Alternative t
  )
    => t a -> t (List a)
al6 =
  al (6 :: Int)

-- | At least 6 combined with the monoidal product.
-- Shorthand for @'aL' 6@.
aL6 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL6 =
  aL (6 :: Int)


-- | At most 7.
-- Shorthand for @'am' 7@.
am7 ::
  ( Alternative t
  )
    => t a -> t (List a)
am7 =
  am (7 :: Int)

-- | At most 7 combined with the monoidal product.
-- Shorthand for @'aM' 7@.
aM7 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM7 =
  aM (7 :: Int)

-- | At least 7.
-- Shorthand for @'al' 7@.
al7 ::
  ( Alternative t
  )
    => t a -> t (List a)
al7 =
  al (7 :: Int)

-- | At least 7 combined with the monoidal product.
-- Shorthand for @'aL' 7@.
aL7 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL7 =
  aL (7 :: Int)


-- | At most 8.
-- Shorthand for @'am' 8@.
am8 ::
  ( Alternative t
  )
    => t a -> t (List a)
am8 =
  am (8 :: Int)

-- | At most 8 combined with the monoidal product.
-- Shorthand for @'aM' 8@.
aM8 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM8 =
  aM (8 :: Int)

-- | At least 8.
-- Shorthand for @'al' 8@.
al8 ::
  ( Alternative t
  )
    => t a -> t (List a)
al8 =
  al (8 :: Int)

-- | At least 8 combined with the monoidal product.
-- Shorthand for @'aL' 8@.
aL8 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL8 =
  aL (8 :: Int)


-- | At most 9.
-- Shorthand for @'am' 9@.
am9 ::
  ( Alternative t
  )
    => t a -> t (List a)
am9 =
  am (9 :: Int)

-- | At most 9 combined with the monoidal product.
-- Shorthand for @'aM' 9@.
aM9 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM9 =
  aM (9 :: Int)

-- | At least 9.
-- Shorthand for @'al' 9@.
al9 ::
  ( Alternative t
  )
    => t a -> t (List a)
al9 =
  al (9 :: Int)

-- | At least 9 combined with the monoidal product.
-- Shorthand for @'aL' 9@.
aL9 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL9 =
  aL (9 :: Int)


-- | At most 10.
-- Shorthand for @'am' 10@.
am0 ::
  ( Alternative t
  )
    => t a -> t (List a)
am0 =
  am (10 :: Int)

-- | At most 10 combined with the monoidal product.
-- Shorthand for @'aM' 10@.
aM0 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM0 =
  aM (10 :: Int)

-- | At least 10.
-- Shorthand for @'al' 10@.
al0 ::
  ( Alternative t
  )
    => t a -> t (List a)
al0 =
  al (10 :: Int)

-- | At least 10 combined with the monoidal product.
-- Shorthand for @'aL' 10@.
aL0 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL0 =
  aL (10 :: Int)


-- | At most 11.
-- Shorthand for @'am' 11@.
am1 ::
  ( Alternative t
  )
    => t a -> t (List a)
am1 =
  am (11 :: Int)

-- | At most 11 combined with the monoidal product.
-- Shorthand for @'aM' 11@.
aM1 ::
  ( Alternative t
  , Monoid a
  )
    => t a -> t a
aM1 =
  aM (11 :: Int)

-- | At least 11.
-- Shorthand for @'al' 11@.
al1 ::
  ( Alternative t
  )
    => t a -> t (List a)
al1 =
  al (11 :: Int)

-- | At least 11 combined with the monoidal product.
-- Shorthand for @'aL' 11@.
aL1 ::
  ( Alternative t
  , Monoid a
  )
  => t a -> t a
aL1 =
  aL (11 :: Int)



