{-|
Module :
  P.Alternative.AtMost
Description :
  Functions related to the "atMost" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.AtMost
  ( am
  , fam
  , am'
  , fAm
  , aM
  , faM
  , aM'
  , fAM
  , lAm
  , lAM
  -- * Deprecated
  , atMost
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Algebra.Monoid
import P.Aliases
import P.Alternative
import P.Applicative
import P.Arithmetic.Nat
import P.Enum
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Ord


{-# Deprecated atMost "Use am instead" #-}
-- | Long version of 'am'.
atMost ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
atMost =
  am


-- | Takes @n@ and an alternative and applies that alternative at most @n@ times.
--
-- ==== __Examples__
--
-- Get every binary string up to length 3
--
-- >>> am 3 "01"
-- ["000","001","00","010","011","01","0","100","101","10","110","111","11","1",""]
am ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
am n x
  | 1 > n
  =
    p []
  | n > 0
  =
    l2 (:) x (am (Pv n) x)
    ++ p []


-- | Flip of 'am'.
fam ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => t a -> n -> t (List a)
fam =
  f' am


-- | Takes @n@ and an alternative and applies that alternative at most @n@ times.
--
-- Similar to 'am' but in the reverse order.
-- Smaller results come earlier.
--
-- ==== __Examples__
--
-- Get every binary string up to length 3
--
-- >>> am' 3 "01"
-- ["","0","00","000","001","01","010","011","1","10","100","101","11","110","111"]
am' ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
am' n x
  | 1 > n
  =
    p []
  | n > 0
  =
    p []
    ++ l2 (:) x (am (Pv n) x)


-- | Flip of 'am''.
fAm ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => t a -> n -> t (List a)
fAm =
  f' am'


-- | Works like 'am', but the result is combined into one element using the monoidal product.
aM ::
  ( Alternative t
  , Monoid a
  , Integral n
  , Ord n
  )
    => n -> t a -> t a
aM =
  m fo << am


-- | Flip of 'aM'
faM ::
  ( Integral n
  , Ord n
  , Alternative t
  , Monoid a
  )
    => t a -> n -> t a
faM =
  f' aM


-- | A combination of 'aM' and 'am''.
--
-- Parses at most @n@ of a monoid with a preference for fewer.
aM' ::
  ( Alternative t
  , Monoid a
  , Integral n
  , Ord n
  )
    => n -> t a -> t a
aM' =
  m fo << am'


-- | Flip of 'aM''
fAM ::
  ( Integral n
  , Ord n
  , Alternative t
  , Monoid a
  )
    => t a -> n -> t a
fAM =
  f' aM'


-- | Works like 'am', but counts the number of results instead.
lAm ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t Nat
lAm =
  lAM


-- | Like 'lAm' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lAm' does not.
lAM ::
  ( Integral n
  , Ord n
  , Alternative t
  , Integral m
  )
    => n -> t a -> t m
lAM =
  m l << am

