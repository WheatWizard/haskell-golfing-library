{-|
Module :
  P.Alternative.Intersperse
Description :
  Functions for interspersing alternatives
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.Intersperse
  ( isa
  , (>|-)
  , isA
  , (>|+)
  )
  where


import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Alternative.Possible
import P.Applicative
import P.Foldable
import P.Function.Compose


-- | Intersperse two alternatives.
isa ::
  ( Alternative t
  )
    => t a -> t a -> t (List a)
isa x y =
  py y *^* mY (l2 (:) x $ m p y) *^* py x


-- | Infix of 'isa'.
(>|-) ::
  ( Alternative t
  )
    => t a -> t a -> t (List a)
(>|-) =
  isa


-- | Intersperse two alternatives concatenating the results.
isA ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
isA =
  m fo << isa


-- | Infix of 'isA'.
(>|+) ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
(>|+) =
  isA
