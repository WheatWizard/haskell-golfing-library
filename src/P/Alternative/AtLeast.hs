{-|
Module :
  P.Alternative.AtLeast
Description :
  Functions related to the "atLeast" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.AtLeast
  ( al
  , fal
  , al'
  , fAl
  , aL
  , faL
  , aL'
  , fAL
  , lAl
  , lAL
  -- * Deprecated
  , atLeast
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Algebra.Monoid
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Applicative
import P.Arithmetic.Nat
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Ord


{-# Deprecated atLeast "Use al instead" #-}
-- | Long version of 'al'.
atLeast ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
atLeast =
  al


-- | Takes @n@ and an alternative and applies that alternative at least @n@ times.
--
-- ==== __Examples__
--
-- Parse at least 4 characters
--
-- >>> gP (al 4 hd) "Axolotl"
-- ["Axolotl","Axolot","Axolo","Axol"]
al ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
al n alternative =
  xly n alternative *^* my alternative


-- | Flip of 'al'.
fal ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => t a -> n -> t (List a)
fal =
  F al


-- | Takes @n@ and an alternative and applies that alternative at least @n@ times.
--
-- Similar to 'al' but in the reverse order.
-- Smaller results come earlier.
--
-- ==== __Examples__
--
-- Parse at least 4 characters.
--
-- >>> gP (al' 4 hd) "Axolotl"
-- ["Axol","Axolo","Axolot","Axolotl"]
al' ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t (List a)
al' n alternative =
  xly n alternative *^* my' alternative


-- | Flip of 'al''.
fAl ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => t a -> n -> t (List a)
fAl =
  F al'


-- | Works like 'al', but the result is combined into one element using the monoidal product.
aL  ::
  ( Alternative t
  , Monoid a
  , Integral n
  , Ord n
  )
    => n -> t a -> t a
aL =
  m fo << al


-- | Flip of 'aL'
faL ::
  ( Integral n
  , Ord n
  , Alternative t
  , Monoid a
  )
    => t a -> n -> t a
faL =
  F aL


-- | A combination of 'aL' and 'al''.
--
-- Parses at least @n@ of a monoid with a preference for fewer.
aL'  ::
  ( Alternative t
  , Monoid a
  , Integral n
  , Ord n
  )
    => n -> t a -> t a
aL' =
  m fo << al


-- | Flip of 'aL''
fAL ::
  ( Integral n
  , Ord n
  , Alternative t
  , Monoid a
  )
    => t a -> n -> t a
fAL =
  F aL'


-- | Works like 'al', but counts the number of results instead.
--
-- ==== __Examples__
--
-- In this example we use @lAl@ with '(P.Monad.>~)' and 'fXy'.
-- We parse at least 3 @a@s, then parse the same number of @b@s returning the result.
--
-- >>> f = lAl 3 (χ 'a') >~ l << fXy (χ 'b')
-- >>> gP f "aabb"
-- []
-- >>> gP f "aaabbb"
-- [3]
-- >>> gP f "aaaabbbb"
-- [4]
-- >>> gP f "aaaaabbbb"
-- []
lAl ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> t a -> t Nat
lAl =
  lAL


-- | Like 'lAl' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lAl' does not.
lAL ::
  ( Integral n
  , Ord n
  , Alternative t
  , Integral m
  )
    => n -> t a -> t m
lAL =
  m l << al
