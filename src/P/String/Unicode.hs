module P.String.Unicode
  ( nfd
  , nfc
  , rmD
  , hsD
  , nhD
  )
  where


import qualified Prelude


import qualified Data.Text as Text
import Data.Text.ICU.Char
import Data.Text.ICU.Normalize


import P.Algebra.Monoid
import P.Bool
import P.Category
import P.Char
import P.Function.Compose
import P.String


-- | Perform the canonical decomposition of a unicode string.
nfd ::
  (
  )
    => String -> String
nfd =
  Text.unpack < normalize NFD < Text.pack


-- | Perform the canonical composition of a unicode string.
nfc ::
  (
  )
    => String -> String
nfc =
  Text.unpack < normalize NFC < Text.pack


-- | Remove diacritics from a character.
--
-- ==== __Technical details__
--
-- First the function checks the following two things:
--
-- * Is the character a letter?
-- * Is does the character's canonical decomposition contain a diacritic? (A diacritic is any character with the diacritic property in the unicode standard)
--
-- If the answer to either of these is "no" the function returns the input unchanged.
--
-- Otherwise it performs the decomposition, and filters out diacritics.
-- If the result was empty, e.g. the character was a diacritical mark to begin with, then the null character was returned.
-- Otherwise we perform the canonical composition.
-- If the result is more then one character (no known examples) it will error, otherwise it returns that character.
--
-- This does not always behave exactly according to expectations.
-- In particular some unexpected characters are diacritics, and some unexpected characters are not diacritics.
rmD ::
  (
  )
    => Char -> Char
rmD x =
   let
     normed =
       normalize NFD $ Text.pack [x]
   in
     if
       Prelude.any (property Diacritic) (Text.unpack $ normed)
       <> α x
     then
       case
         Text.unpack
         $ normalize NFC
         $ Text.filter (n < property Diacritic)
         $ normed
       of
         [] ->
           '\000'
         [v] ->
           v
         _ ->
           Prelude.error "Unable to recanonicalize character with diacritics removed"
     else
       x


-- | Determines whether a character has a diacritical mark.
hsD ::
  (
  )
    => Char -> Bool
hsD x =
  Prelude.any (property Diacritic) $ Text.unpack $ normalize NFD $ Text.pack [x]


-- | Gives the opposite result as 'hsD'.
nhD ::
  (
  )
    => Char -> Bool
nhD =
  n < hsD
