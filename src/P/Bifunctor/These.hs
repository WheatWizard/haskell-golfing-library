{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Bifunctor.These
  ( These (..)
  , pattern TfM
  )
  where


import qualified Prelude


import Data.Bifunctor
  ( Bifunctor
  )
import qualified Data.Bifunctor


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative.Unpure
import P.Assoc
import P.Bifunctor.Either
import P.Bool
import P.Eq
import P.Function.Compose
import P.Functor
import P.Maybe
import P.Ord
import P.Swap


-- | Represents two non-exclusive possibilities.
data These a b
  = Ti a   -- ^ This
  | Ta b   -- ^ That
  | Te a b -- ^ These
  deriving
    ( Eq
    , Prelude.Ord
      -- ^ 'P.Ord' should be used in all cases.
      -- We maintain a instance for internal use and this just in case.
    , Prelude.Show
    , Prelude.Read
    )


instance Functor (These a) where
  fmap _ (Ti a) =
    Ti a
  fmap func (Ta b) =
    Ta (func b)
  fmap func (Te a b) =
    Te a (func b)


instance Unpure (These a) where
  pure' =
    Ta
  unp (Ta x) =
    [ x ]
  unp _ =
    []


instance
  ( Eq a
  )
    => Eq1 (These a)
  where
    q1 =
      q1'


instance
  (
  )
    => Eq2 These
  where
    q2 userEq1 _ (Ti x) (Ti y) =
      userEq1 x y
    q2 _ userEq2 (Ta x) (Ta y) =
      userEq2 x y
    q2 userEq1 userEq2 (Te x1 x2) (Te y1 y2) =
      userEq1 x1 y1 <> userEq2 x2 y2
    q2 _ _ _ _ =
      B


instance
  ( Ord a
  , Ord b
  )
    => Ord (These a b)
  where
    cp =
      lcp cp


instance
  ( Ord a
  )
    => Ord1 (These a)
  where
    lcp _ (Ti x) (Ti y) =
      cp x y
    lcp _ (Ti _) _ =
      LT
    lcp _ _ (Ti _) =
      GT
    lcp func (Ta x) (Ta y) =
      func x y
    lcp _ (Ta _) _ =
      LT
    lcp func (Te x1 x2) (Te y1 y2) =
      cp x1 y1 <> func x2 y2


instance Bifunctor These where
  bimap func1 _ (Ti x) =
    Ti (func1 x)
  bimap _ func2 (Ta x) =
    Ta (func2 x)
  bimap func1 func2 (Te x y) =
    Te (func1 x) (func2 y)


instance Swap These where
  swap (Ti x) =
    Ta x
  swap (Ta x) =
    Ti x
  swap (Te x y) =
    Te y x


instance Assoc These where
  assoc (Ti x) =
    Ti (Ti x)
  assoc (Ta (Ti x)) =
    Ti (Ta x)
  assoc (Ta (Ta x)) =
    Ta x
  assoc (Ta (Te x y)) =
    Te (Ta x) y
  assoc (Te x (Ti y)) =
    Ti (Te x y)
  assoc (Te x (Ta y)) =
    Te (Ti x) y
  assoc (Te x (Te y z)) =
    Te (Te x y) z

  unassoc (Ti (Ti x)) =
    Ti x
  unassoc (Ti (Ta x)) =
    Ta (Ti x)
  unassoc (Ta x) =
    Ta (Ta x)
  unassoc (Te (Ta x) y) =
    Ta (Te x y)
  unassoc (Ti (Te x y)) =
    Te x (Ti y)
  unassoc (Te (Ti x) y) =
    Te x (Ta y)
  unassoc (Te (Te x y) z) =
    Te x (Te y z)


-- | Internal helper for 'TfM'.
_toMaybes ::
  (
  )
    => Maybe (These a b) -> (Maybe a, Maybe b)
_toMaybes (Lef _) =
  ( Lef ()
  , Lef ()
  )
_toMaybes (Rit (Ti x)) =
  ( Rit x
  , Lef ()
  )
_toMaybes (Rit (Ta y)) =
  ( Lef ()
  , Rit y
  )
_toMaybes (Rit (Te x y)) =
  ( Rit x
  , Rit y
  )


-- | Converts two 'Maybe's into a 'Maybe These'.
pattern TfM ::
  (
  )
    => Maybe a -> Maybe b -> Maybe (These a b)
pattern TfM x y <- (_toMaybes -> (x, y)) where
  TfM (Lef _) (Lef _) =
    Lef ()
  TfM (Rit x) (Lef _) =
    Rit (Ti x)
  TfM (Lef _) (Rit y) =
    Rit (Ta y)
  TfM (Rit x) (Rit y) =
    Rit (Te x y)
