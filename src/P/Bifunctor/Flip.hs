{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language PolyKinds #-}
{-# Language FlexibleInstances #-}
{-# Language TypeOperators #-}
{-|
Module :
  P.Bifunctor.Flip
Description :
  The flip function on types
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Bifunctor.Flip
  ( Flip (..)
  , pattern UFl
  , flI
  , ufi
  , pattern LF2
  , Flip2 (..)
  , mF2
  , m22
  )
  where


import Prelude
  ( ($)
  , fmap
  )


import P.Category.Iso
import P.Function.Compose
import P.Functor hiding
  ( fmap
  )


-- | A type level flip.
--
-- A flip function has the type:
--
-- @
-- flip :: (a -> b -> c) -> (b -> a -> c)
-- @
--
-- This flip type has kind:
--
-- @
-- Flip :: (α -> β -> *) -> (β -> α -> *)
-- @
newtype Flip f (a :: β) (b :: α) =
  -- | Flips a type not to be confused with 'P.Function.Flip.F' which flips a function.
  Flp
    { uFl ::
      f b a
    }


-- | Unflip function and pattern.
-- The opposite of 'Flp'.
pattern UFl ::
  (
  )
    => Flip f a b -> f b a
pattern UFl x <- (Flp -> x) where
  UFl =
    uFl


-- | 'Flp' as an isomorphism.
flI ::
  (
  )
    => f b a <-> Flip f a b
flI =
  Iso Flp UFl


-- | 'Flp' as an isomorphism.
ufi ::
  (
  )
    => Flip f a b <-> f b a
ufi =
  Iso UFl Flp


pattern LF2 ::
  (
  )
    => (Flip f a b -> Flip g c d -> e) -> f b a -> g d c -> e
pattern LF2 x <- ((\ func x y -> func (uFl x) (uFl y)) -> x) where
  LF2 func x y =
    func (Flp x) (Flp y)


instance
  ( Functor (p a)
  )
    => Functor (Flip (Flip p) a)
  where
    fmap func (Flp (Flp x)) =
      Flp $ Flp $ fmap func x


instance Functor (Flip (,) a) where
  fmap func (Flp (x, y)) =
    Flp (func x, y)


instance Functor (Flip ((,,) a) b) where
  fmap func (Flp (x, y, z)) =
    Flp (x, func y, z)


instance Functor (Flip ((,,,) a b) c) where
  fmap func (Flp (w, x, y, z)) =
    Flp (w, x, func y, z)


instance Functor (Flip ((,,,,) a b c) d) where
  fmap func (Flp (v, w, x, y, z)) =
    Flp (v, w, x, func y, z)


instance Functor (Flip ((,,,,,) a b c d) e) where
  fmap func (Flp (u, v, w, x, y, z)) =
    Flp (u, v, w, x, func y, z)


instance Functor (Flip ((,,,,,,) a b c d e) f) where
  fmap func (Flp (t, u, v, w, x, y, z)) =
    Flp (t, u, v, w, x, func y, z)


instance Functor (Flip ((,,,,,,,) a b c d e f) g) where
  fmap func (Flp (s, t, u, v, w, x, y, z)) =
    Flp (s, t, u, v, w, x, func y, z)



-- | A type level flip that moves two arguments.
--
-- This flip type has kind:
--
-- @
-- Flip2 :: (α -> β -> γ -> *) -> (γ -> α -> β -> *)
-- @
--
-- The analogous function has type:
--
-- @
-- flip2 :: (a -> b -> c -> d) -> (c -> a -> b -> d)
-- @
--
newtype Flip2 f (a :: β) (b :: α) (c :: γ) =
  Fl2
    { uF2 ::
      f b c a
    }


instance Functor (Flip2 (,,) a b) where
  fmap func (Fl2 (x, y, z)) =
    Fl2 (x, func y, z)


instance Functor (Flip2 ((,,,) a) b c) where
  fmap func (Fl2 (w, x, y, z)) =
    Fl2 (w, x, func y, z)


instance Functor (Flip2 ((,,,,) a b) c d) where
  fmap func (Fl2 (v, w, x, y, z)) =
    Fl2 (v, w, x, func y, z)


instance Functor (Flip2 ((,,,,,) a b c) d e) where
  fmap func (Fl2 (u, v, w, x, y, z)) =
    Fl2 (u, v, w, x, func y, z)


instance Functor (Flip2 ((,,,,,,) a b c d) e f) where
  fmap func (Fl2 (t, u, v, w, x, y, z)) =
    Fl2 (t, u, v, w, x, func y, z)


instance Functor (Flip2 ((,,,,,,,) a b c d e) f g) where
  fmap func (Fl2 (s, t, u, v, w, x, y, z)) =
    Fl2 (s, t, u, v, w, x, func y, z)


-- | Take a function on 'Flip2' and lift it to a function without 'Flip2'.
mF2 ::
  (
  )
    => (Flip2 p a b c -> Flip2 q d e f) -> p b c a -> q e f d
mF2 =
  uF2 << fm Fl2


-- | Lift a binary function on 'Flip2' to a binary function with 'Flip2'.
m22 ::
  (
  )
    => (Flip2 p a b c -> Flip2 q d e f -> Flip2 r g h i) -> p b c a -> q e f d -> r h i g
m22 f g =
  uF2 < f (Fl2 g) < Fl2
