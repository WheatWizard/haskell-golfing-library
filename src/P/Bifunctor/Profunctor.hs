{-# Language QuantifiedConstraints #-}
module P.Bifunctor.Profunctor
  ( Profunctor (..)
  , arr
  , jbp
  , rmp
  -- * Deprecated
  , lmap
  , rmap
  , dimap
  )
  where


import qualified Prelude


import P.Category
import P.Function.Compose
import P.Functor
import P.Monad


infixr 1 <@^


class
  ( forall a. Functor (p a)
  )
    => Profunctor p
  where
    {-# Minimal dmp | (<@^) #-}
    -- | Precomposition of an profunctor with a pure function.
    -- Also a contravariant map over the first position.
    --
    -- More general version of '(Control.Arrow.<<^)' and '(Prelude..)'.
    (<@^) :: p b c -> (a -> b) -> p a c
    f <@^ g =
      dmp g id f

    -- | Map across both sides of a profunctor.
    --
    -- Equivalent to 'dimap'
    dmp :: (a -> b) -> (c -> d) -> p b c -> p a d
    dmp f g x =
      g < x <@^ f


instance
  (
  )
    => Profunctor (->)
  where
    (<@^) =
      (<)


-- | Lift a pure function into a category using profunctor composition.
--
-- More general version of 'Control.Arrow.arr'.
arr ::
  ( Category p
  , Profunctor p
  )
    => (a -> b) -> p a b
arr =
  (id <@^)


-- | Contravariant map over the first argument of a profunctor.
--
-- Similar to 'fm'.
rmp ::
  ( Profunctor p
  )
    => (a -> b) -> p b c -> p a c
rmp f g =
  g <@^ f


-- | Fanout the identity morphism.
-- For functions this makes a pair where both elements are the same.
--
-- A less general version of this 'P.Tuple.Jbp', also allows pattern matching.
jbp ::
  ( Category p
  , Profunctor p
  )
    => p a (a, a)
jbp =
  arr $ jn (,)


{-# Deprecated lmap "Use m or (<) instead" #-}
-- | Long version of 'P.Functor.m' and '(<)'.
lmap ::
  ( Profunctor p
  )
    => (b -> c) -> p a b -> p a c
lmap =
  m


{-# Deprecated rmap "Use rmp instead" #-}
-- | Long version of 'rmp'.
rmap ::
  ( Profunctor p
  )
    => (a -> b) -> p b c -> p a c
rmap =
  rmp


{-# Deprecated dimap "Use dmp instead" #-}
-- | Long version of 'dmp'.
dimap ::
  ( Profunctor p
  )
    => (a -> b) -> (c -> d) -> p b c -> p a d
dimap =
  dmp
