{-|
Module :
  P.Eq.Extra
Description :
  Additional functions for the P.Eq library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental


A large number of less important functions that have been moved to this file to reduce clutter in the main P.Eq library.
-}
module P.Eq.Extra
  ( q0
  , nq0
  , eq1
  , nq1
  , eq2
  , nq2
  , eq3
  , nq3
  , eq4
  , nq4
  , eq5
  , nq5
  , eq6
  , nq6
  , eq7
  , nq7
  , eq8
  , nq8
  , eq9
  , nq9
  , eq0
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Ring
import P.Eq


-- | Equals 0.
q0 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
q0 =
  eq 0


{-# Deprecated eq0 "Use q0 instead" #-}
-- | Equals 0.
eq0 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq0 =
  eq 0


-- | Doesn't equal 0.
nq0 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
nq0 =
  nq 0


-- | Equals 1.
eq1 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq1 =
  eq 1


-- | Doesn't equal 1.
nq1 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq1 =
  nq 1


-- | Equals 2.
eq2 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq2 =
  eq 2


-- | Doesn't equal 2.
nq2 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq2 =
  nq 2


-- | Equals 3.
eq3 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq3 =
  eq 3


-- | Doesn't equal 3.
nq3 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq3 =
  nq 3


-- | Equals 4.
eq4 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq4 =
  eq 4


-- | Doesn't equal 4.
nq4 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq4 =
  nq 4


-- | Equals 5.
eq5 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq5 =
  eq 5


-- | Doesn't equal 5.
nq5 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq5 =
  nq 5


-- | Equals 6.
eq6 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq6 =
  eq 6


-- | Doesn't equal 6.
nq6 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq6 =
  nq 6


-- | Equals 7.
eq7 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq7 =
  eq 7


-- | Doesn't equal 7.
nq7 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq7 =
  nq 7


-- | Equals 8.
eq8 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq8 =
  eq 8


-- | Doesn't equal 8.
nq8 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq8 =
  nq 8


-- | Equals 9.
eq9 ::
  ( Eq a
  , Ring a
  )
    => Predicate a
eq9 =
  eq 9


-- | Doesn't equal 9.
nq9 ::
  ( Eq a
  , Ring a
  )    => Predicate a
nq9 =
  nq 9
