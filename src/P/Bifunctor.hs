{-# Language RankNTypes #-}
{-# Language QuantifiedConstraints #-}
{-# Language PolyKinds #-}
{-# Language KindSignatures #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-# Language ImpredicativeTypes #-}
{-# Language ConstraintKinds #-}
{-# Language TypeFamilies #-}
{-# Language ImpredicativeTypes #-}
module P.Bifunctor
  ( Bifunctor
  , bm
  , fbm
  , (***)
  , mst
  , fMt
  , m2t
  , f2t
  , bAp
  , fBp
  -- * Parallel maps
  , jB
  , (&@)
  , fjB
  , mjB
  , (&@<)
  , mJB
  , (&<<)
  , jBm
  , (<&@)
  , pjB
  , fpJ
  , qjB
  , fqJ
  , kjB
  , fkJ
  -- * Deprecated
  , bimap
  , first
  , second
  )
  where


import qualified Prelude
import Prelude
  (
  )


import Data.Kind


import P.Bifunctor.Flip
import P.Category
import P.Category.Iso
import P.Functor
import P.Function.Compose
import P.Function.Flip


infixr 9 &@, &@<, &<<, <&@


-- | A bifunctor class.
-- No new instances of this can be made it is equivalent to its requirements.
class
  ( forall a. Functor (p a)
  , forall a. Functor (Flip p a)
  )
    => Bifunctor p


instance
  ( forall a. Functor (p a)
  , forall a. Functor (Flip p a)
  )
    => Bifunctor p


{-# Deprecated bimap "use bm instead" #-}
-- | Long version of 'bm'.
bimap ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> (c -> d) -> p a c -> p b d
bimap f g =
  UFl < m f < Flp < m g


-- | Map over both arguments of a bifunctor.
--
-- Equivalent to 'Data.Bifunctor.bimap'.
-- Similar to 'P.Arrow.ot'.
bm ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> (c -> d) -> p a c -> p b d
bm func1 func2 =
  mst func1 < m func2


-- | Flip of 'bm'.
--
-- Similar to 'P.Arrow.fot'.
fbm ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (c -> d) -> (a -> b) -> p a c -> p b d
fbm =
  F bm


-- | Infix version of 'bm'.
--
-- Similar to '(Control.Arrow.***)' for its equivalent see '(P.Arrow.=:)'.
(***) ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> (c -> d) -> p a c -> p b d
(***) =
  bm


{-# Deprecated first "use mst or mSt instead" #-}
-- | Long version of 'mst'.
--
-- For the function 'Control.Arrow.first' from @Control.Arrow@ see 'P.Arrow.mSt'.
first ::
  ( Functor (Flip p c)
  )
    => (a -> b) -> p a c -> p b c
first =
  mst


{-# Deprecated second "use m or mNd instead" #-}
-- | Long version of 'm'.
--
-- For the function 'Control.Arrow.second' from @Control.Arrow@ see 'P.Arrow.mNd'.
second ::
  ( Functor (p c)
  )
    => (a -> b) -> p c a -> p c b
second =
  m


-- | Maps a function over the first argument of a bifunctor.
--
-- Similar to 'P.Arrow.mSt'.
mst ::
  ( Functor (Flip p c)
  )
    => (a -> b) -> p a c -> p b c
mst func =
  UFl < m func < Flp


-- | Flip of 'mst'.
fMt ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p a c -> (a -> b) -> p b c
fMt =
  F mst


-- | This does what the type says.
-- A two level map over the first argument of a bifunctor.
m2t ::
  ( Functor f
  , forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> f (p a c) -> f (p b c)
m2t =
  m < mst


-- | Flip of 'm2t'.
f2t ::
  ( Functor f
  , forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => f (p a c) -> (a -> b) -> f (p b c)
f2t =
  F m2t


-- | Map over both parts of a bifunctor with the same function.
--
-- Similar to 'P.Arrow.syr'
--
-- ==== __Examples__
--
-- This can be used on tuples to perform an action on both elements.
--
-- >>> jB (+3) (1, 2)
-- (4, 5)
--
jB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> p a a -> p b b
jB func =
  bm func func


-- | Infix version of 'jB'.
(&@) ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (a -> b) -> p a a -> p b b
(&@) =
  jB


-- | Flip of 'jB'.
fjB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p a a -> (a -> b) -> p b b
fjB =
  F jB


-- | 'jB' applied on the inside of a functor via a map.
--
-- ==== __Examples__
--
-- >>> mjB (+2) [(1,2),(4,5)]
-- [(3,4),(6,7)]
--
mjB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  )
    => (a -> b) -> f (p a a) -> f (p b b)
mjB =
  m < jB


-- | Infix version of 'mjB'.
(&@<) ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  )
    => (a -> b) -> f (p a a) -> f (p b b)
(&@<) =
  mjB


-- | 'jB' applied two levels into functors via maps, or 'mjB' applied on the inside of a functor via a map.
mJB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  , Functor g
  )
    => (a -> b) -> f (g (p a a)) -> f (g (p b b))
mJB =
  m < mjB


-- | Infix version of 'mJB'.
(&<<) ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  , Functor g
  )
    => (a -> b) -> f (g (p a a)) -> f (g (p b b))
(&<<) =
  mJB


-- | Perform a map across both arguments of a 'Bifunctor'.
--
-- ==== __Examples__
--
-- >>> jBm (+1) ([3,4,3],[6,7])
-- ([4,5,4],[7,8])
--
jBm ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  , Functor g
  )
    => (a -> b) -> p (f a) (g a) -> p (f b) (g b)
jBm func =
  bm (m func) (m func)


-- | Infix version of 'jBm'.
(<&@) ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  , Functor f
  , Functor g
  )
    => (a -> b) -> p (f a) (g a) -> p (f b) (g b)
(<&@) =
  jBm


-- | If we define 'jB' as follows
--
-- @
-- jB func =
--   bm func func
-- @
--
-- Then in Haskell there is no single most general type for 'jB'.
-- 'jB' is defined with the type that Haskell infers by default.
-- 'pjB' is defined with a different potentially useful type.
pjB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (forall k . k -> f k) -> p a b -> p (f a) (f b)
pjB func =
  bm func func


-- | Flip of 'pjB'.
fpJ ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p a b -> (forall k . k -> f k) -> p (f a) (f b)
fpJ x func =
  bm func func x


-- | A version of 'jB' with a different incompatible type.
-- See 'pjB' for an explanation.
qjB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (forall k . f k -> k) -> p (f a) (f b) -> p a b
qjB func =
  bm func func


-- | Flip of 'qjB'.
fqJ ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p (f a) (f b) -> (forall k . f k -> k) -> p a b
fqJ x func =
  bm func func x


-- | A version of 'jB' with a different incompatible type.
-- See 'pjB' for an explanation.
kjB ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => (forall k. f k -> g k) -> p (f a) (f a) -> p (g a) (g a)
kjB func =
  bm func func


-- | Flip of 'kjB'.
fkJ ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p (f a) (f a) -> (forall k. f k -> g k) -> p (g a) (g a)
fkJ x func =
  bm func func x


-- | Takes a bifunctor of functions and two values.
-- Passes all of the functions in the first place the first value,
-- and all of the functions in the second place the second value.
bAp ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => p (a -> b) (c -> d) -> a -> c -> p b d
bAp funcs x y =
  bm ($ x) ($ y) funcs


-- | Flip of 'bAp'.
fBp ::
  ( forall x. Functor (p x)
  , forall x. Functor (Flip p x)
  )
    => a -> p (a -> b) (c -> d) -> c -> p b d
fBp =
  F bAp
