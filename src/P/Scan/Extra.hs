{-|
Module :
  P.Scan.Extra
Description :
  Additional functions for the P.Scan module
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions related to scan that have been moved to this file to reduce clutter in the main P.Scan.
-}
module P.Scan.Extra where


import qualified Prelude


import P.Algebra.Ring
import P.Scan


-- | Index map starting at 1.
xo1 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo1 =
  ixo 1


-- | Index map starting at 2.
xo2 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo2 =
  ixo 2


-- | Index map starting at 3.
xo3 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo3 =
  ixo 3


-- | Index map starting at 4.
xo4 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo4 =
  ixo 4


-- | Index map starting at 5.
xo5 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo5 =
  ixo 5


-- | Index map starting at 6.
xo6 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo6 =
  ixo 6


-- | Index map starting at 7.
xo7 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo7 =
  ixo 7


-- | Index map starting at 8.
xo8 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo8 =
  ixo 8


-- | Index map starting at 9.
xo9 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo9 =
  ixo 9


-- | Index map starting at 10.
xo0 ::
  ( Scan t
  , Ring a
  )
    => (a -> b -> c) -> t b -> t c
xo0 =
  ixo 10


