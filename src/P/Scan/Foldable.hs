{-|
Module :
  P.Scan.Foldable
Description :
  Functions for Scan and Foldable
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library for functions using both the 'P.Scan.Scan' and 'P.Foldable.Foldable' type classes.
-}
module P.Scan.Foldable
  ( mAa
  )
  where


import Prelude
  (
  )
import qualified Prelude


import P.Foldable
import P.Function.Compose
import P.Scan
import P.Tuple


-- TODO is it possible to make this with one traversal?
-- | A map scan returning the accumulator.
--
-- Like 'P.Scan.mA' but it returns accumulator as well as the scanned structure.
mAa ::
  ( Foldable t
  , Scan t
  )
    => (b -> a -> (b, c)) -> b -> t a -> (b, t c)
mAa func accum coll =
  ( lF (st << func) accum coll
  , mA func accum coll
  )
