{-# Language NoMonomorphismRestriction #-}
{-# Language ImpredicativeTypes #-}
{-# Language RankNTypes #-}
module P.Function
  ( fbF
  , ffF
  -- * on
  , on
  , (.*)
  , fon
  , on3
  , fO3
  -- ** Polymorphs
  , pOn
  , fpO
  , qOn
  , fqO
  , kOn
  , fkO
  , xOn
  , fxO
  , yOn
  , fyO
  -- * Deprecated
  , const
  , fpOn
  , fqOn
  , fkOn
  , fxOn
  , fyOn
  )
  where


import qualified Prelude
import Prelude
  (
  )
import qualified Data.Function
import Control.Monad
  ( join
  )


import P.Function.Compose
import P.Function.Flip


infixl 5 .*


-- | Composes a function with both inputs of a binary function.
--
-- Equivalent to 'Data.Function.on'.
on ::
  ( Functor f
  )
    => (a -> a -> b) -> f a -> f (f b)
on =
  join < dcp


-- | Infix version of 'on'.
(.*) ::
  ( Functor f
  )
    => (b -> b -> c) -> f b -> f (f c)
(.*) =
  on


-- | Flip of 'on'.
fon ::
  ( Functor f
  )
    => f b -> (b -> b -> c) -> f (f c)
fon =
  f' on


-- | Composes a function on all of the arguments of a ternary function.
on3 ::
  ( Functor f
  )
    => (a -> a -> a -> b) -> f a -> f (f (f b))
on3 =
  join < join < dc2


-- | Flip of 'on3'
fO3 ::
  ( Functor f
  )
    => f a -> (a -> a -> a -> b) -> f (f (f b))
fO3 =
  f' on3


-- | If you define the function 'on' as
--
-- @
-- on biFunc func x y =
--   biFunc (func x) (func y)
-- @
--
-- There is no single most general type in Haskell.
--
-- The type inferred by Haskell is
--
-- @
-- on :: (b -> b -> c) -> (a -> b) -> a -> a -> c
-- @
--
-- (The default 'on' uses a generalization of this type.)
--
-- However we can notice that the two @b@ inputs to @biFunc@ no not need to be the same type.
-- For example we had @bifunc :: [a] -> [b] -> Int@ then our @func@ could have the type @c -> [c]@.
--
-- @pon@ is one possible other type which could be given to 'on'.
--
pOn ::
  (
  )
    => (f a -> f b -> c) -> (forall k. k -> f k) -> a -> b -> c
pOn biFunc func x y =
  biFunc (func x) (func y)


-- | Flip of 'pOn'.
fpO ::
  (
  )
    => (forall k. k -> f k) -> (f a -> f b -> c) -> a -> b -> c
fpO func =
  (`pOn` func)


{-# Deprecated fpOn "Use fpO instead"#-}
-- | Long version of 'fpO'.
fpOn ::
  (
  )
    => (forall k. k -> f k) -> (f a -> f b -> c) -> a -> b -> c
fpOn =
  fpO


-- | @qOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
qOn ::
  (
  )
    => (a -> b -> c) -> (forall k. f k -> k) -> f a -> f b -> c
qOn biFunc func x =
  biFunc (func x) < func


-- | Flip of 'qOn'.
fqO ::
  (
  )
    => (forall k. f k -> k) -> (a -> b -> c) -> f a -> f b -> c
fqO func =
  (`qOn` func)


{-# Deprecated fqOn "Use fqO instead" #-}
-- | Long version of 'fqO'.
fqOn ::
  (
  )
    => (forall k. f k -> k) -> (a -> b -> c) -> f a -> f b -> c
fqOn =
  fqO


-- | @kOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
kOn ::
  (
  )
    => (g a -> g b -> c) -> (forall k. f k -> g k) -> f a -> f b -> c
kOn biFunc func x =
  biFunc (func x) < func


-- | Flip of 'kOn'.
fkO ::
  (
  )
    => (forall k. f k -> g k) -> (g a -> g b -> c) -> f a -> f b -> c
fkO func =
  (`kOn` func)


{-# Deprecated fkOn "Use fkO instead" #-}
-- | Long version of 'fkO'.
fkOn ::
  (
  )
    => (forall k. f k -> g k) -> (g a -> g b -> c) -> f a -> f b -> c
fkOn =
  fkO


-- | @xOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
--
-- Useful with functor composition.
--
xOn ::
  (
  )
    => (g (h a) -> g (h b) -> c) -> (forall k. f k -> g (h k)) -> f a -> f b -> c
xOn biFunc func x =
  biFunc (func x) < func


-- | Flip of 'xOn'.
fxO ::
  (
  )
    => (forall k. f k -> g (h k)) -> (g (h a) -> g (h b) -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fxO func biFunc x =
  biFunc (func x) < func


{-# Deprecated fxOn "Use fxO instead" #-}
-- | Long version of 'fxO'.
fxOn ::
  (
  )
    => (forall k. f k -> g (h k)) -> (g (h a) -> g (h b) -> c) -> f a -> f b -> c
-- Defined this way since 'F' doesn't play nice with RankNTypes
fxOn =
  fxO


-- | @yOn@ is an alternative minimal type for the 'on' function.
-- See 'pOn' for more information.
--
-- Useful with functor composition.
--
yOn ::
  (
  )
    => (h a -> h b -> c) -> (forall k. f (g k) -> h k) -> f (g a) -> f (g b) -> c
yOn biFunc func x =
  biFunc (func x) < func


-- | Flip of 'yOn'.
fyO ::
  (
  )
    => (forall k. f (g k) -> h k) -> (h a -> h b -> c) -> f (g a) -> f (g b) -> c
fyO func biFunc x =
  biFunc (func x) < func


{-# Deprecated fyOn "Use fyO instead" #-}
-- | Long version of 'fyO'.
fyOn ::
  (
  )
    => (forall k. f (g k) -> h k) -> (h a -> h b -> c) -> f (g a) -> f (g b) -> c
fyOn =
  fyO


{-# Deprecated const "Use p instead" #-}
-- | Take two arguments and return the first.
-- Long version of 'P.Applicative.p'.
const ::
  (
  )
    => a -> b -> a
const x _ =
  x


-- | A function combinator.
-- 'P.Monad.jn' joins the first two arguments of a function, this joins the first and third arguments.
fbF ::
  (
  )
    => (a -> b -> a -> c) -> (a -> b -> c)
fbF =
  (Prelude.=<<) F


-- | A variant of 'fbF'.
-- The resulting function takes its arguments in the opposite order.
ffF ::
  (
  )
    => (a -> b -> a -> c) -> (b -> a -> c)
ffF =
  F < fbF
