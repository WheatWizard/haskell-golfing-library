{-|
Module :
  P.Arithmetic.Floating
Description :
  Functions for floating point arithmetic
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Arithmetic.Floating
  ( (/)
  , iv
  , fiv
  , rc
  -- * Deprecated
  , recip
  )
  where


import qualified Prelude
import Prelude
  ( Fractional
  )


import P.Function.Flip


-- | Fractional division.
(/) ::
  ( Fractional a
  )
    => a -> a -> a
(/) =
  (Prelude./)


-- | Prefix version of '(/)'.
iv ::
  ( Fractional a
  )
    => a -> a -> a
iv =
  (Prelude./)


-- | Flip of 'iv'.
fiv ::
  ( Fractional a
  )
    => a -> a -> a
fiv =
  f' iv


-- | Gets the reciprocal of a fraction.
rc ::
  ( Fractional a
  )
    => a -> a
rc =
  Prelude.recip


{-# Deprecated recip "Use rc instead" #-}
-- | Long version of 'rc'.
recip ::
  ( Fractional a
  )
    => a -> a
recip =
  rc
