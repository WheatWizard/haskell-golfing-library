{-# Language PatternSynonyms #-}
{-|
Module :
  P.Arithmetic.Extra
Description :
  Additional functions for the P.Arithmetic library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Arithmetic library.
-}
module P.Arithmetic.Extra
  (
  -- * Multiplication
    t4
  , t5
  , t6
  , t7
  , t8
  , t9
  , t0
  , t1
  -- * Division
  , dv2
  , dv3
  , dv4
  , dv5
  , dv6
  , dv7
  , dv8
  , dv9
  , dv0
  , dv1
  -- * Modulo
  , mu2
  , mu3
  , mu4
  , mu5
  , mu6
  , mu7
  , mu8
  , mu9
  , mu0
  , mu1
  -- * Exponentiation
  , pw2
  , pw3
  , pw4
  , pw5
  , pw6
  , pw7
  , pw8
  , pw9
  , pw0
  , pw1
  , pN1
  , pN2
  , pN3
  , pN4
  , pN5
  , pN6
  , pN7
  , pN8
  , pN9
  , pN0
  -- * Absolute difference
  , aD1
  , aD2
  , aD3
  , aD4
  , aD5
  , aD6
  , aD7
  , aD8
  , aD9
  , aD0
  -- * Subtraction
  , pattern S1
  , pattern S2
  , pattern S3
  , pattern S4
  , pattern S5
  , pattern S6
  , pattern S7
  , pattern S8
  , pattern S9
  , pattern S0
  -- * Addition
  , pattern P1
  , pattern P2
  , pattern P3
  , pattern P4
  , pattern P5
  , pattern P6
  , pattern P7
  , pattern P8
  , pattern P9
  , pattern P0
  -- * Roots
  , xr2
  , xr3
  , xr4
  , xr5
  , xr6
  , xr7
  , xr8
  , xr9
  , xr0
  , xr1
  , wt2
  , wt3
  , wt4
  , wt5
  , wt6
  , wt7
  , wt8
  , wt9
  , wt0
  , wt1
  -- * Logs
  , xl2
  , xl3
  , xl4
  , xl5
  , xl6
  , xl7
  , xl8
  , xl9
  , xl0
  , xl1
  , wo2
  , wo3
  , wo4
  , wo5
  , wo6
  , wo7
  , wo8
  , wo9
  , wo0
  , wo1
  -- * Constants
  , pattern N1
  , pattern N2
  , pattern N3
  , pattern N4
  , pattern N5
  , pattern N6
  , pattern N7
  , pattern N8
  , pattern N9
  , pattern N0
  )
  where


import Prelude
  ( Integral
  , Integer
  , Bool
  )


import P.Aliases
import P.Algebra.Ring
import P.Arithmetic
import P.Arithmetic.Number
import P.Arithmetic.Pattern
import P.Eq
import P.Ord


-- | Multiply by 4.
t4 ::
  ( Ring a
  )
    => a -> a
t4 =
  ml 4

-- | Multiply by 5.
t5 ::
  ( Ring a
  )
    => a -> a
t5 =
  ml 5

-- | Multiply by 6.
t6 ::
  ( Ring a
  )
    => a -> a
t6 =
  ml 6

-- | Multiply by 7.
t7 ::
  ( Ring a
  )
    => a -> a
t7 =
  ml 7

-- | Multiply by 8.
t8 ::
  ( Ring a
  )
    => a -> a
t8 =
  ml 8

-- | Multiply by 9.
t9 ::
  ( Ring a
  )
    => a -> a
t9 =
  ml 9

-- | Multiply by 10.
--
-- To multiply by 0 use 'p 0'.
t0 ::
  ( Ring a
  )
    => a -> a
t0 =
  ml 10

-- | Multiply by 11.
--
-- To multiply by 1 use 'id'.
t1 ::
  ( Ring a
  )
    => a -> a
t1 =
  ml 11

-- | Integer division by 2.
dv2 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv2 =
  (`dv` 2)

-- | Integer division by 3.
dv3 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv3 =
  (`dv` 3)

-- | Integer division by 4.
dv4 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv4 =
  (`dv` 4)

-- | Integer division by 5.
dv5 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv5 =
  (`dv` 5)

-- | Integer division by 6.
dv6 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv6 =
  (`dv` 6)

-- | Integer division by 7.
dv7 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv7 =
  (`dv` 7)

-- | Integer division by 8.
dv8 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv8 =
  (`dv` 8)

-- | Integer division by 9.
dv9 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv9 =
  (`dv` 9)

-- | Integer division by 10.
dv0 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv0 =
  (`dv` 10)

-- | Integer division by 11.
dv1 ::
  ( Ring a
  , Integral a
  )
    => a -> a
dv1 =
  (`dv` 11)

-- | Given @n@ returns 2 to the power of @n@.
pw2 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw2 =
  pw 2


-- | Given @n@ returns 3 to the power of @n@.
pw3 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw3 =
  pw 3


-- | Given @n@ returns 4 to the power of @n@.
pw4 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw4 =
  pw 4


-- | Given @n@ returns 5 to the power of @n@.
pw5 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw5 =
  pw 5


-- | Given @n@ returns 6 to the power of @n@.
pw6 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw6 =
  pw 6


-- | Given @n@ returns 7 to the power of @n@.
pw7 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw7 =
  pw 7


-- | Given @n@ returns 8 to the power of @n@.
pw8 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw8 =
  pw 8


-- | Given @n@ returns 9 to the power of @n@.
pw9 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw9 =
  pw 9


-- | Given @n@ returns 10 to the power of @n@.
pw0 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw0 =
  pw 10


-- | Given @n@ returns 11 to the power of @n@.
pw1 ::
  ( Integral a
  , Ring b
  )
    => a -> b
pw1 =
  pw 11


-- | Given @n@ returns -1 to the power of @n@.
pN1 ::
  ( Integral a
  )
    => a -> Integer
pN1 =
  pw (-1)


-- | Given @n@ returns -2 to the power of @n@.
pN2 ::
  ( Integral a
  )
    => a -> Integer
pN2 =
  pw (-2)


-- | Given @n@ returns -3 to the power of @n@.
pN3 ::
  ( Integral a
  )
    => a -> Integer
pN3 =
  pw (-3)


-- | Given @n@ returns -4 to the power of @n@.
pN4 ::
  ( Integral a
  )
    => a -> Integer
pN4 =
  pw (-4)


-- | Given @n@ returns -5 to the power of @n@.
pN5 ::
  ( Integral a
  )
    => a -> Integer
pN5 =
  pw (-5)


-- | Given @n@ returns -6 to the power of @n@.
pN6 ::
  ( Integral a
  )
    => a -> Integer
pN6 =
  pw (-6)


-- | Given @n@ returns -7 to the power of @n@.
pN7 ::
  ( Integral a
  )
    => a -> Integer
pN7 =
  pw (-7)


-- | Given @n@ returns -8 to the power of @n@.
pN8 ::
  ( Integral a
  )
    => a -> Integer
pN8 =
  pw (-8)


-- | Given @n@ returns -9 to the power of @n@.
pN9 ::
  ( Integral a
  )
    => a -> Integer
pN9 =
  pw (-9)


-- | Given @n@ returns -10 to the power of @n@.
pN0 ::
  ( Integral a
  )
    => a -> Integer
pN0 =
  pw (-10)


-- | Absolute difference from 1.
aD1 ::
  ( Number a
  )
    => a -> a
aD1 =
  aD 1


-- | Absolute difference from 2.
aD2 ::
  ( Number a
  )
    => a -> a
aD2 =
  aD 2


-- | Absolute difference from 3.
aD3 ::
  ( Number a
  )
    => a -> a
aD3 =
  aD 3


-- | Absolute difference from 4.
aD4 ::
  ( Number a
  )
    => a -> a
aD4 =
  aD 4


-- | Absolute difference from 5.
aD5 ::
  ( Number a
  )
    => a -> a
aD5 =
  aD 5


-- | Absolute difference from 6.
aD6 ::
  ( Number a
  )
    => a -> a
aD6 =
  aD 6


-- | Absolute difference from 7.
aD7 ::
  ( Number a
  )
    => a -> a
aD7 =
  aD 7


-- | Absolute difference from 8.
aD8 ::
  ( Number a
  )
    => a -> a
aD8 =
  aD 8


-- | Absolute difference from 9.
aD9 ::
  ( Number a
  )
    => a -> a
aD9 =
  aD 9


-- | Absolute difference from -1.
aD0 ::
  ( Number a
  )
    => a -> a
aD0 =
  aD (-1)


-- | Modulo 2.
mu2 ::
  ( Integral a
  )
    => a -> a
mu2 =
  fmu 2


-- | Modulo 3.
mu3 ::
  ( Integral a
  )
    => a -> a
mu3 =
  fmu 3


-- | Modulo 4.
mu4 ::
  ( Integral a
  )
    => a -> a
mu4 =
  fmu 4


-- | Modulo 5.
mu5 ::
  ( Integral a
  )
    => a -> a
mu5 =
  fmu 5


-- | Modulo 6.
mu6 ::
  ( Integral a
  )
    => a -> a
mu6 =
  fmu 6


-- | Modulo 7.
mu7 ::
  ( Integral a
  )
    => a -> a
mu7 =
  fmu 7


-- | Modulo 8.
mu8 ::
  ( Integral a
  )
    => a -> a
mu8 =
  fmu 8


-- | Modulo 9.
mu9 ::
  ( Integral a
  )
    => a -> a
mu9 =
  fmu 9


-- | Modulo 10.
mu0 ::
  ( Integral a
  )
    => a -> a
mu0 =
  fmu 10


-- | Modulo 11.
mu1 ::
  ( Integral a
  )
    => a -> a
mu1 =
  fmu 11


-- | Determine the exact square root of the input.
-- If the input is not a perfect square this returns @Nothing@.
xr2 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr2 =
  xrt 2


-- | Determine if the input is a perfect square.
wt2 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt2 =
  pwt 2


-- | Determine if log of the input in base 2.
-- If the input is not a perfect power of 2 the result is @Nothing@.
xl2 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl2 =
  xlg 2


-- | Determine if the input is a power of 2.
wo2 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo2 =
  pwo 2


-- | Determine the exact cube root of the input.
-- If the input is not a perfect cube this returns @Nothing@.
xr3 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr3 =
  xrt 3


-- | Determine if the input is a perfect cube.
wt3 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt3 =
  pwt 3


-- | Determine if log of the input in base 3.
-- If the input is not a perfect power of 3 the result is @Nothing@.
xl3 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl3 =
  xlg 3


-- | Determine if the input is a power of 3.
wo3 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo3 =
  pwo 3


-- | Determine the exact 4th root of the input.
-- If the input is not a perfect 4th power this returns @Nothing@.
xr4 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr4 =
  xrt 4


-- | Determine if the input is a perfect 4th power.
wt4 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt4 =
  pwt 4


-- | Determine if log of the input in base 4.
-- If the input is not a perfect power of 4 the result is @Nothing@.
xl4 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl4 =
  xlg 4


-- | Determine if the input is a power of 4.
wo4 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo4 =
  pwo 4


-- | Determine the exact 5th root of the input.
-- If the input is not a perfect 5th power this returns @Nothing@.
xr5 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr5 =
  xrt 5


-- | Determine if the input is a perfect 5th power.
wt5 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt5 =
  pwt 5


-- | Determine if log of the input in base 5.
-- If the input is not a perfect power of 5 the result is @Nothing@.
xl5 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl5 =
  xlg 5


-- | Determine if the input is a power of 5.
wo5 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo5 =
  pwo 5


-- | Determine the exact 6th root of the input.
-- If the input is not a perfect 6th power this returns @Nothing@.
xr6 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr6 =
  xrt 6


-- | Determine if the input is a perfect 6th power.
wt6 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt6 =
  pwt 6


-- | Determine if log of the input in base 6.
-- If the input is not a perfect power of 6 the result is @Nothing@.
xl6 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl6 =
  xlg 6


-- | Determine if the input is a power of 6.
wo6 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo6 =
  pwo 6


-- | Determine the exact 7th root of the input.
-- If the input is not a perfect 7th power this returns @Nothing@.
xr7 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr7 =
  xrt 7


-- | Determine if the input is a perfect 7th power.
wt7 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt7 =
  pwt 7


-- | Determine if log of the input in base 7.
-- If the input is not a perfect power of 7 the result is @Nothing@.
xl7 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl7 =
  xlg 7


-- | Determine if the input is a power of 7.
wo7 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo7 =
  pwo 7


-- | Determine the exact 8th root of the input.
-- If the input is not a perfect 8th power this returns @Nothing@.
xr8 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr8 =
  xrt 8


-- | Determine if the input is a perfect 8th power.
wt8 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt8 =
  pwt 8


-- | Determine if log of the input in base 8.
-- If the input is not a perfect power of 8 the result is @Nothing@.
xl8 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl8 =
  xlg 8


-- | Determine if the input is a power of 8.
wo8 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo8 =
  pwo 8


-- | Determine the exact 9th root of the input.
-- If the input is not a perfect 9th power this returns @Nothing@.
xr9 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr9 =
  xrt 9


-- | Determine if the input is a perfect 9th power.
wt9 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt9 =
  pwt 9


-- | Determine if log of the input in base 9.
-- If the input is not a perfect power of 9 the result is @Nothing@.
xl9 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl9 =
  xlg 9


-- | Determine if the input is a power of 9.
wo9 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo9 =
  pwo 9


-- | Determine the exact 10th root of the input.
-- If the input is not a perfect 10th power this returns @Nothing@.
xr0 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr0 =
  xrt 10


-- | Determine if the input is a perfect 10th power.
wt0 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt0 =
  pwt 10


-- | Determine if log of the input in base 10.
-- If the input is not a perfect power of 10 the result is @Nothing@.
xl0 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl0 =
  xlg 10


-- | Determine if the input is a power of 10.
wo0 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo0 =
  pwo 10


-- | Determine the exact 11th root of the input.
-- If the input is not a perfect 11th power this returns @Nothing@.
xr1 ::
  ( Integral a
  , Ord a
  )
    => a -> Maybe' a
xr1 =
  xrt 11


-- | Determine if the input is a perfect 11th power.
wt1 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wt1 =
  pwt 11


-- | Determine if log of the input in base 11.
-- If the input is not a perfect power of 11 the result is @Nothing@.
xl1 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
xl1 =
  xlg 11


-- | Determine if the input is a power of 11.
wo1 ::
  ( Integral a
  , Ord a
  )
    => a -> Bool
wo1 =
  pwo 11


-- | Shortcut for @(-1)@.
pattern N1 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N1 =
  -1


-- | Shortcut for @(-2)@.
pattern N2 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N2 =
  -2


-- | Shortcut for @(-3)@.
pattern N3 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N3 =
  -3


-- | Shortcut for @(-4)@.
pattern N4 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N4 =
  -4


-- | Shortcut for @(-5)@.
pattern N5 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N5 =
  -5


-- | Shortcut for @(-6)@.
pattern N6 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N6 =
  -6


-- | Shortcut for @(-7)@.
pattern N7 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N7 =
  -7


-- | Shortcut for @(-8)@.
pattern N8 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N8 =
  -8


-- | Shortcut for @(-9)@.
pattern N9 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N9 =
  -9


-- | Shortcut for @(-10)@.
pattern N0 ::
  ( Ring a
  , Eq a
  )
    => a
pattern N0 =
  -10
