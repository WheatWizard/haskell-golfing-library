{-|
Module :
  P.Arithmetic.Base.Extra
Description :
  Additional base conversion functions
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for base conversion that have been moved to this file to reduce clutter in the main P.Arithmetic.Base library.
-}
module P.Arithmetic.Base.Extra
  where


import qualified Prelude
import Prelude
  ( Integral
  , Integer
  )


import P.Aliases
import P.Arithmetic.Base
import P.Foldable
import P.Ord


-- | Convert a natural number to little-endian base 2.
bs2 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs2 =
  bs 2


-- | Convert a natural number to little-endian base 3.
bs3 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs3 =
  bs 3


-- | Convert a natural number to little-endian base 4.
bs4 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs4 =
  bs 4


-- | Convert a natural number to little-endian base 5.
bs5 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs5 =
  bs 5


-- | Convert a natural number to little-endian base 6.
bs6 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs6 =
  bs 6


-- | Convert a natural number to little-endian base 7.
bs7 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs7 =
  bs 7


-- | Convert a natural number to little-endian base 8.
bs8 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs8 =
  bs 8


-- | Convert a natural number to little-endian base 9.
bs9 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs9 =
  bs 9


-- | Convert a natural number to little-endian base 10.
bs0 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs0 =
  bs 10


-- | Convert a natural number to little-endian base 11.
bs1 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs1 =
  bs 11



-- | Convert a natural number to big-endian base 2.
bS2 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS2 =
  bS 2


-- | Convert a natural number to big-endian base 3.
bS3 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS3 =
  bS 3


-- | Convert a natural number to big-endian base 4.
bS4 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS4 =
  bS 4


-- | Convert a natural number to big-endian base 5.
bS5 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS5 =
  bS 5


-- | Convert a natural number to big-endian base 6.
bS6 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS6 =
  bS 6


-- | Convert a natural number to big-endian base 7.
bS7 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS7 =
  bS 7


-- | Convert a natural number to big-endian base 8.
bS8 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS8 =
  bS 8


-- | Convert a natural number to big-endian base 9.
bS9 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS9 =
  bS 9


-- | Convert a natural number to big-endian base 10.
bS0 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS0 =
  bS 10


-- | Convert a natural number to big-endian base 11.
bS1 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS1 =
  bS 11


-- | Get the length of an integer in base 2.
--
-- This is the bit length of the input.
lB2 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB2 =
  lB 2


-- | Get the length of an integer in base 3.
lB3 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB3 =
  lB 3


-- | Get the length of an integer in base 4.
lB4 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB4 =
  lB 4


-- | Get the length of an integer in base 5.
lB5 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB5 =
  lB 5


-- | Get the length of an integer in base 6.
lB6 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB6 =
  lB 6


-- | Get the length of an integer in base 7.
lB7 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB7 =
  lB 7


-- | Get the length of an integer in base 8.
lB8 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB8 =
  lB 8


-- | Get the length of an integer in base 9.
lB9 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB9 =
  lB 9


-- | Get the length of an integer in base 10.
lB0 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB0 =
  lB 10


-- | Get the length of an integer in base 11.
lB1 ::
  ( Integral i
  , Ord i
  )
    => i -> Integer
lB1 =
  lB 11


-- | Convert a number from little-endian base 2.
ub2 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub2 =
  ubs 2


-- | Convert a number from big-endian base 2.
uB2 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB2 =
  ubS 2


-- | Convert a number from little-endian base 3.
ub3 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub3 =
  ubs 3


-- | Convert a number from big-endian base 3.
uB3 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB3 =
  ubS 3


-- | Convert a number from little-endian base 4.
ub4 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub4 =
  ubs 4


-- | Convert a number from big-endian base 4.
uB4 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB4 =
  ubS 4


-- | Convert a number from little-endian base 5.
ub5 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub5 =
  ubs 5


-- | Convert a number from big-endian base 5.
uB5 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB5 =
  ubS 5


-- | Convert a number from little-endian base 6.
ub6 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub6 =
  ubs 6


-- | Convert a number from big-endian base 6.
uB6 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB6 =
  ubS 6


-- | Convert a number from little-endian base 7.
ub7 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub7 =
  ubs 7


-- | Convert a number from big-endian base 7.
uB7 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB7 =
  ubS 7


-- | Convert a number from little-endian base 8.
ub8 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub8 =
  ubs 8


-- | Convert a number from big-endian base 8.
uB8 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB8 =
  ubS 8


-- | Convert a number from little-endian base 9.
ub9 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub9 =
  ubs 9


-- | Convert a number from big-endian base 9.
uB9 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB9 =
  ubS 9


-- | Convert a number from little-endian base 10.
ub0 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub0 =
  ubs 10


-- | Convert a number from big-endian base 10.
uB0 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB0 =
  ubS 10


-- | Convert a number from little-endian base 11.
ub1 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
ub1 =
  ubs 11


-- | Convert a number from big-endian base 11.
uB1 ::
  ( Integral i
  , Foldable t
  )
    => t i -> i
uB1 =
  ubS 11
