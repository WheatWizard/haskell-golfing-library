{-|
Module :
  P.Arithmetic.Number
Description :
  The Number class and related functions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Arithmetic.Number
  ( Number (..)
  , sig
  , aD
  , (-|)
  )
  where


import qualified Prelude
import Prelude
  ( Int
  , Integer
  , Word
  , Float
  , Double
  )


import Data.Int
  ( Int8
  , Int16
  , Int32
  , Int64
  )
import Data.Word
  ( Word8
  , Word16
  , Word32
  , Word64
  )


import P.Algebra.Ring
import P.Function.Compose


-- | This class extends the 'Ring' class to have the missing functions from 'Prelude.Num'.
class
  ( Ring a
  )
    => Number a
  where
    -- | Absolute value.
    av :: a -> a

    -- | Signum.
    sig :: a -> a


instance
  (
  )
    => Number Int
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Int8
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Int16
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Int32
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Int64
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Integer
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Word
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Word8
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Word16
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Word32
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Word64
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Float
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


instance
  (
  )
    => Number Double
  where
    av =
      Prelude.abs
    sig =
      Prelude.signum


-- | Absolute difference.
aD ::
  ( Number a
  )
    => a -> a -> a
aD =
  mm av sb


-- | Infix of 'aD'.
(-|) ::
  ( Number a
  )
    => a -> a -> a
(-|) =
  aD

