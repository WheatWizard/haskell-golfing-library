{-|
Module :
  P.Arithmetic.Base
Description :
  Base conversion functions
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions for base conversion.
-}
module P.Arithmetic.Base
  ( Base (..)
  , hx
  , bs
  , fbs
  , bS
  , fbS
  , lB
  , flB
  , bi
  , ubi
  , biS
  , ubs
  , fub
  , ubS
  , fuB
  , dgs
  , fDS
  , dgR
  , fDR
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import qualified Data.List


import P.Aliases
import P.Algebra.Ring
import P.Arithmetic
import P.Bool
import P.Category
import P.Fix
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Foldable.Length
import P.Foldable.Unfold
import P.Ord
import P.String
import P.Zip


-- | An alias used to make type signatures more expressive.
--
-- Indicates that some value is the base used in the computation.
type Base i = i


-- | Converts a positive integer to a string of its hexidecimal representation.
hx ::
  ( Integral i
  , Ord i
  )
    => i -> String
hx x = do
  val <- bs 16 x & m (f' Data.List.genericDrop "0123456789ABCDEF")
  val & Prelude.take 1 & Prelude.reverse


-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bs ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> List i
bs base x
  | x Prelude.== 0
  =
    []
  | (quot, rem) <- qR x base
  =
    if
      0 > rem
    then
      (rem - base) : bs base (quot + 1)
    else
      rem : bs base quot


-- | Flip of 'bs'.
fbs ::
  ( Integral i
  , Ord i
  )
    => i -> Base i -> List i
fbs =
  f' bs


-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is big endian, with the most significant place being the first in the list.
bS ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> List i
bS =
  Prelude.reverse << bs


-- | Flip of 'bS'.
fbS ::
  ( Integral i
  , Ord i
  )
    => i -> Base i -> List i
fbS =
  f' bS


-- | Gets the length of a integer in a particular base.
--
-- This can be seen as a sort of logarithm.
lB ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> Integer
lB =
  l << bs


-- | Flip of 'lB'.
flB ::
  ( Integral i
  , Ord i
  )
    => i -> Base i -> Integer
flB =
  f' lB


-- | Converts an integer to binary in terms of boolean values.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bi ::
  ( Integral i
  , Ord i
  )
    => i -> List Bool
bi =
  (Prelude.> 0) << bs 2


-- | Inverse of 'bi'.
-- Converts from a list of booleans representing a binary string to a positive integer.
ubi ::
  ( Integral i
  , Ord i
  )
    => List Bool -> i
ubi =
  ubs 2 < m (1 ?. 0)


-- | Converts an integer to a binary string.
biS ::
  ( Integral i
  , Ord i
  )
    => i -> String
biS =
  ("01" Prelude.!!) << Prelude.fromInteger << Prelude.toInteger << bs 2


-- | Converts a little endian base representation to an integer.
ubs ::
  ( Integral i
  , Foldable t
  )
    => Base i -> t i -> i
ubs base x =
  sm $ zW (*) (tL x) $ ix (* base) 1


-- | Flip of 'ubs'.
fub ::
  ( Integral i
  , Foldable t
  )
    => t i -> Base i -> i
fub =
  f' ubs


-- | Converts a big endian base representation to an integer.
ubS ::
  ( Integral i
  , Foldable t
  )
    => Base i -> t i -> i
ubS base =
  lF' ((+) < (* base)) 0


-- | Flip of 'ubS'.
fuB ::
  ( Integral i
  , Foldable t
  )
    => t i -> Base i -> i
fuB =
  f' ubS


-- | Compute the digital sum of a number in a particular base.
dgs ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> i
dgs =
  sm << bs


-- | Flip of 'dgS'.
fDS ::
  ( Integral i
  , Ord i
  )
    => i -> Base i -> i
fDS =
  f' dgs


-- | Compute the digital root of a number in a particular base.
dgR ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> i
dgR =
  yyc < dgs


-- | Flip of 'dgR'.
fDR ::
  ( Integral i
  , Ord i
  )
    => Base i -> i -> i
fDR =
  f' dgR
