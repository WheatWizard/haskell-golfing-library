{-|
Module :
  P.Arithmetic.Combinatorics.Partition
Description :
  Integer partitions
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Provides functions for integer partitions.
-}
module P.Arithmetic.Combinatorics.Partition
  ( -- * Ordered integer partitions
    ipt
  , ipS
  , ipW
  , ipe
  , ipx
  , pxW
  , ifx
  , ipX
  , ifX
  , pXW
    -- * Descending integer partitions
  , dip
  , dpS
  , dpW
  , dpe
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Aliases
import P.Algebra.Ring
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Monad.Plus.Filter
import P.Ord


-- | Find all integer partitions of a given integer.
-- Gives finer partitions first.
--
-- For list partitions see 'P.List.Partition.pt'.
ipt ::
  ( Integral i
  )
    => i -> List (List i)
ipt =
  ipW (\_ -> T)


-- | Find all integer partitions of a given integer matching a predicate.
--
-- ==== __Examples__
--
-- Get all partitions into equal elements:
--
-- >>> ipS lq 9
-- [[1,1,1,1,1,1,1,1,1],[3,3,3],[9]]
--
ipS ::
  ( Integral i
  )
    => Predicate (List i) -> i -> List (List i)
ipS pred =
  fl pred < ipt


-- | Find all integer partitions of a given integer where each element matches a predicate.
--
-- ==== __Examples__
--
-- Find all partitions into odd sized parts
--
-- >>> ipW od 4
-- [[1,1,1,1],[1,3],[3,1]]
--
ipW ::
  ( Integral i
  )
    => Predicate i -> i -> List (List i)
ipW _ 0 =
  [[]]
ipW pred n = do
  x <- fl pred [1 .. n]
  (x :) < ipW pred (n - x)


-- | Find all descending integer partitions of a given integer where each element in the partition is from a given list.
ipe ::
  ( Integral i
  , Eq i
  , Foldable f
  )
    => f i -> i -> List (List i)
ipe =
  ipW < fe


-- | Find all descending integer partitions of a given integer.
-- Descending partitions makes this function a version for when you don't care about the order only the size.
--
-- For a version that does count order see 'ipt'.
dip ::
  ( Integral i
  )
    => i -> List (List i)
dip =
  dpW (\ _ -> T)


-- | Find all integer partitions of a given integer matching a predicate.
--
-- ==== __Examples__
--
-- Get all partitions into unique elements:
--
-- >>> dpS uq 9
-- [[4,3,2],[5,3,1],[5,4],[6,2,1],[6,3],[7,2],[8,1],[9]]
--
-- This is also the same as getting the strictly decreasing integer partitions.
--
dpS ::
  ( Integral i
  )
    => Predicate (List i) -> i -> List (List i)
dpS pred =
  fl pred < dip


-- | Find all descending integer partitions of a given integer where each element in the partition matches a predicate.
-- Descending partitions makes this function a version for when you don't care about the order only the size.
--
-- For a version that does count order see 'ipW'.
--
-- ==== __Examples__
--
-- Find all partitions into odd sized parts where order doesn't mater.
--
-- >>> dpW od 7
-- [[1,1,1,1,1,1,1],[3,1,1,1,1],[3,3,1],[5,1,1],[7]]
--
-- Find all partitions into coin denominations of the European union.
--
-- >>> dpW (fe[1,5,10,20,50,100]) 12
-- [[1,1,1,1,1,1,1,1,1,1,1,1],[5,1,1,1,1,1,1,1],[5,5,1,1],[10,1,1]]
--
dpW ::
  ( Integral i
  )
    => Predicate i -> i -> List (List i)
dpW pred x =
  go x x
  where
    go _ 0 =
      [[]]
    go n i = do
      x <- fl pred [1 .. Prelude.min n i]
      (x :) < go x (i - x)


-- | Find all descending integer partitions of a given integer where each element in the partition is from a given list.
dpe ::
  ( Integral i
  , Eq i
  , Foldable f
  )
    => f i -> i -> List (List i)
dpe =
  dpW < fe


-- | Gives all ways to partition an integer n into m parts.
-- Parts may be empty.
ipx ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => j -> i -> List (List i)
ipx =
  pxW (\_ -> T)


-- | Takes a predicate, an integer @m@, and an integer @n@, and finds all integer partitions of @n@ which satisfy the predicate and split @n@ into @m@ parts.
--
-- ==== __Examples__
--
-- Get all partitions of 2 into 2 distinct elements.
--
-- >>> ifx (uq) 2 2
-- [[0,1],[0,2],[1,0],[2,0]]
--
ifx ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => Predicate (List i) -> j -> i -> List (List i)
ifx pred =
  fl pred << ipx


-- | Gives all ways to partition an integer n into m parts where each element in the partition statisfies a predicate.
-- Parts may be empty.
pxW ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => Predicate i -> j -> i -> List (List i)
pxW pred 0 x =
  [[]]
pxW pred n x
  | 1 > n
  =
    []
  | T
  = do
    k <- fl pred [0 .. x]
    (k :) < pxW pred (n - 1) (x - k)


-- | Gives all ways to partition an integer n into m nonzero parts.
ipX ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => j -> i -> List (List i)
ipX =
  pXW (\_ -> T)


-- | Takes a predicate, an integer @m@, and an integer @n@, and finds all integer partitions of @n@ which satisfy the predicate and split @n@ into @m@ non-zero parts.
--
-- ==== __Examples__
--
-- Get all partitions of 6 into 3 distinct non-zero elements.
--
-- >>> ifX uq 3 6
-- [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
--
ifX ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => Predicate (List i) -> j -> i -> List (List i)
ifX pred =
  fl pred << ipX


-- | Gives all ways to partition an integer n into m nonzero parts where each element in the partition statisfies a predicate.
pXW ::
  ( Integral i
  , Integral j
  , Ord j
  )
    => Predicate i -> j -> i -> List (List i)
pXW pred 0 x =
  [[]]
pXW pred n x
  | 1 > n
  =
    []
  | T
  = do
    k <- fl pred [1 .. x]
    (k :) < pXW pred (n - 1) (x - k)
