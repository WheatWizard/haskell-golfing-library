{-|
Module :
  P.Arithmetic.Primes.Extra
Description :
  Additional functions for the P.Arithmetic.Primes library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Arithmetic.Primes library.
-}
module P.Arithmetic.Primes.Extra
  (
  -- * Divisibility
    by3
  , nb3
  , by4
  , nb4
  , by5
  , nb5
  , by6
  , nb6
  , by7
  , nb7
  , by8
  , nb8
  , by9
  , nb9
  , by0
  , nb0
  , by1
  , nb1
  , by2
  , nb2
  )
  where


import P.Arithmetic.Primes


-- | Determine if a number is divisible by 3.
by3 ::
  ( Integral i
  )
    => i -> Bool
by3 =
  fby 3


-- | Determine if a number is not divisible by 3.
nb3 ::
  ( Integral i
  )
    => i -> Bool
nb3 =
  fnb 3


-- | Determine if a number is divisible by 4.
by4 ::
  ( Integral i
  )
    => i -> Bool
by4 =
  fby 4


-- | Determine if a number is not divisible by 4.
nb4 ::
  ( Integral i
  )
    => i -> Bool
nb4 =
  fnb 4


-- | Determine if a number is divisible by 5.
by5 ::
  ( Integral i
  )
    => i -> Bool
by5 =
  fby 5


-- | Determine if a number is not divisible by 5.
nb5 ::
  ( Integral i
  )
    => i -> Bool
nb5 =
  fnb 5


-- | Determine if a number is divisible by 6.
by6 ::
  ( Integral i
  )
    => i -> Bool
by6 =
  fby 6


-- | Determine if a number is not divisible by 6.
nb6 ::
  ( Integral i
  )
    => i -> Bool
nb6 =
  fnb 6


-- | Determine if a number is divisible by 7.
by7 ::
  ( Integral i
  )
    => i -> Bool
by7 =
  fby 7


-- | Determine if a number is not divisible by 7.
nb7 ::
  ( Integral i
  )
    => i -> Bool
nb7 =
  fnb 7


-- | Determine if a number is divisible by 8.
by8 ::
  ( Integral i
  )
    => i -> Bool
by8 =
  fby 8


-- | Determine if a number is not divisible by 8.
nb8 ::
  ( Integral i
  )
    => i -> Bool
nb8 =
  fnb 8


-- | Determine if a number is divisible by 9.
by9 ::
  ( Integral i
  )
    => i -> Bool
by9 =
  fby 9


-- | Determine if a number is not divisible by 9.
nb9 ::
  ( Integral i
  )
    => i -> Bool
nb9 =
  fnb 9


-- | Determine if a number is divisible by 10.
by0 ::
  ( Integral i
  )
    => i -> Bool
by0 =
  fby 10


-- | Determine if a number is not divisible by 10.
nb0 ::
  ( Integral i
  )
    => i -> Bool
nb0 =
  fnb 10


-- | Determine if a number is divisible by 11.
by1 ::
  ( Integral i
  )
    => i -> Bool
by1 =
  fby 11


-- | Determine if a number is not divisible by 11.
nb1 ::
  ( Integral i
  )
    => i -> Bool
nb1 =
  fnb 11


-- | Determine if a number is divisible by 12.
by2 ::
  ( Integral i
  )
    => i -> Bool
by2 =
  fby 12


-- | Determine if a number is not divisible by 12.
nb2 ::
  ( Integral i
  )
    => i -> Bool
nb2 =
  fnb 12
