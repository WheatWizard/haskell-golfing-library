{-# Language ScopedTypeVariables #-}
{-# Language LambdaCase #-}
{-# Language BangPatterns #-}
module P.Arithmetic
  ( Prelude.Int
  , Prelude.Integer
  , Prelude.Integral
  , (%)
  , mu
  , fmu
  , (//)
  , dv
  , fdv
  , vD
  , fvD
  , qR
  , fqR
  , tNu
  , ev
  , od
  , xrt
  , fxr
  , xlg
  , fxl
  , pwt
  , fpt
  , pwo
  , fpo
  , kvD
  , fkv
  , klg
  , fkl
  , krm
  , xgD
  , gcd
  , lcm
  , rPr
  , nrP
  -- * Deprecated
  , even
  , divMod
  , quotRem
  , exactLog2
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , String
  , Bool
  )


import qualified Data.List
import Data.Foldable as Foldable


import P.Aliases
import P.Algebra.Ring
import P.Bool
import P.Category
import P.Eq
import P.Function.Compose
import P.Function.Flip
import P.Ord


-- | Integer division.
-- Result is truncated towards negative infinity.
(//) ::
  ( Integral i
  )
    => i -> i -> i
(//) =
  Prelude.div


-- | Integer division.
-- Result is truncated towards negative infinity.
dv ::
  ( Integral i
  )
    => i -> i -> i
dv =
  Prelude.div


-- | Flip of 'dv'.
fdv ::
  ( Integral i
  )
    => i -> i -> i
fdv =
  F Prelude.div


-- | Integer modulus function.
--
-- Equivalent to 'Prelude.mod'.
(%) ::
  ( Integral i
  )
    => i -> i -> i
(%) =
  Prelude.mod


-- | Integer modulus function.
--
-- Equivalent to 'Prelude.mod'.
mu ::
  ( Integral i
  )
    => i -> i -> i
mu =
  (%)


-- | Flip of 'mu'.
fmu ::
  ( Integral i
  )
    => i -> i -> i
fmu =
  F mu


{-# Deprecated divMod "Use vD instead" #-}
-- | Long version of 'vD'.
divMod ::
  ( Integral i
  )
    => i -> i -> (i, i)
divMod =
  vD


-- | Divmod function.
--
-- Produces an error when a zero divisor is given.
--
-- Equivalent to 'Prelude.divMod'.
vD ::
  ( Integral i
  )
    => i -> i -> (i, i)
vD =
  Prelude.divMod


-- | Flip of 'vD'.
fvD ::
  ( Integral i
  )
    => i -> i -> (i, i)
fvD =
  f' vD


{-# Deprecated quotRem "Use qR instead" #-}
-- | Long version of 'qR'.
quotRem ::
  ( Integral i
  )
    => i -> i -> (i, i)
quotRem =
  qR


-- | Quotrem function.
--
-- Produces an error when a zero divisor is given.
--
-- Equivalent to 'Prelude.quotRem'.
qR ::
  ( Integral i
  )
    => i -> i -> (i, i)
qR =
  Prelude.quotRem


-- | Flip of 'qR'.
fqR ::
  ( Integral i
  )
    => i -> i -> (i, i)
fqR =
  f' qR


-- | Convert from any 'Integral' type to any `Ring` type.
tNu ::
  ( Integral a
  , Ring b
  )
    => a -> b
tNu =
  Prelude.fromInteger < Prelude.toInteger


{-# Deprecated even "use ev isntead" #-}
-- | Long version of 'ev'.
even ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
even =
  ev


-- | Determines if a number is even.
-- Returns @True@ if the input is divisible by 2 and @False@ otherwise.
ev ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
ev =
  (0 ==) < (% 2)


-- | Determines if a number is odd.
-- Returns @False@ if the input is divisible by 2 and @True@ otherwise.
od ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
od =
  (1 ==) < (% 2)


-- | Exact root.
-- Gives the @n@th root of @x@ if @x@ is an @n@th power.
--
-- Does not work for negative exponents.
xrt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Maybe' a
xrt p y
  | 0 > y
  = do
    _ <- [() | od p ]
    Prelude.negate < xrt p (-y)
xrt 1 y =
  [y]
xrt x y =
  go 0
  where
    go n =
      case
        cp y (n^x)
      of
        EQ ->
          [n]
        GT ->
          go (n+1)
        LT ->
          []


-- | Flip of 'xrt'.
fxr ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Maybe' a
fxr =
  f' xrt


-- | Exact log.
-- Gives the logarithm of @x@ in base @b@ if @x@ is an exact power of @b@.
--
-- ==== Special cases
-- Always returns @Nothing@ when the base is 0.
--
-- In the case of ambiguities the power is always the smallest non-negative solution.
-- So @xlg 1 1@ results in @Just 0@ and @xlg (-1) (-1)@ is @Just 1@.
xlg ::
  ( Integral a
  , Ring b
  )
    => a -> a -> Maybe' b
xlg 0 =
  \_ -> []
xlg (-1) =
  \case
    (-1) ->
      [1]
    1 ->
      [0]
    _ ->
      []
xlg 1 =
  \case
    1 ->
     [0]
    _ ->
     []
xlg y =
  go 0
  where
    go i 1 =
      [i]
    go i 0 =
      []
    go i x =
      case
        vD x y
      of
        (m, 0) ->
          go (i+1) m
        (_, _) ->
          []


-- | Flip of 'xlg'.
fxl ::
  ( Integral a
  , Ring b
  )
    => a -> a -> Maybe' b
fxl =
  f' xlg


-- | Determines if the input is a perfect @n@th power.
pwt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
pwt =
  Prelude.not << Foldable.null << xrt


-- | Determines if the input is a perfect power of some base.
pwo ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
pwo =
  Prelude.not << Foldable.null << xlg


-- | Flip of 'pwt'.
fpt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
fpt =
  f' pwt


-- | Flip of 'pwo'.
fpo ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
fpo =
  f' pwo


{-# Deprecated exactLog2 "Use xl2 instead." #-}
-- | Long verrsion of 'P.Arithmetic.Extra.xl2'.
exactLog2 ::
  ( Integral a
  , Ring b
  )
    => a -> Maybe' b
exactLog2 =
  xlg 2


-- | Takes @n@ and @m@ and determines the largest power of @m@ which divides @n@.
klg ::
  ( Integral a
  )
    => a -> a -> a
klg =
  Prelude.snd << kvD


-- | Flip of 'klg'.
fkl ::
  ( Integral a
  )
    => a -> a -> a
fkl =
  f' klg


-- | Takes @n@ and @m@ and removes all factors of @m@ from @n@.
krm ::
  ( Integral a
  )
    => a -> a -> a
krm =
  Prelude.fst << kvD


-- | Takes an @n@ and an @m@ and gives @a@ and @b@ such that @m^a*b=n@ maximizing the value of @a@.
--
-- This can be seen as analogous to 'divMod', but for multiplication and exponentation rather than addition and multiplication.
-- However @b@ does not approximate the logarithm.
--
-- ==== __Examples__
--
-- >>> kvD 2312 2
-- (3,289)
-- >>> kvD 1312 2
-- (5,41)
-- >>> kvD 3283 7
-- (2,67)
--
kvD ::
  ( Integral a
  )
    => a -> a -> (a, a)
kvD =
  go 0
  where
    go _ _ 1 =
      Prelude.error "Exception: log by 1"
    go !n x y
      | (x', 0) <- vD x y
      =
        go (n+1) x' y
      | Prelude.otherwise
      =
        (n, x)


-- | Flip of 'kvD'.
fkv ::
  ( Integral a
  )
    => a -> a -> (a, a)
fkv =
  f' kvD


-- | Takes an @n@ and an @m@ and gives @a@, @b@, and @c@ such that @m^a*(m*b+c)=n@ maximizing first the value of @a@ then the value of @b@.
--
-- The @c@ value is the last non-zero digit in the base @m@ expansion of @n@.
--
-- This combines @kvD@ with @vD@.
--
-- ==== __Examples__
--
-- >>> xgD 2312 2
-- (3,144,1)
-- >>> xgD 1312 2
-- (5,20,1)
-- >>> xgD 3283 7
-- (2,9,4)
xgD ::
  ( Integral a
  )
    => a -> a -> (a, a, a)
xgD =
  go 0
  where
    go _ _ 1 =
      Prelude.error "Exception: log by 1"
    go !n x y =
      case
        vD x y
      of
        (x', 0) ->
          go (n+1) x' y
        (x', y') ->
          (n, x', y')


-- | Get the greatest common divisor of two values.
gcd ::
  ( Integral a
  )
    => a -> a -> a
gcd =
  Prelude.gcd


-- | Get the least common multiple of two values.
lcm ::
  ( Integral a
  )
    => a -> a -> a
lcm =
  Prelude.lcm


-- | Determines if two values are relatively prime.
-- That is whether the greatest common divisor is equal to one.
rPr ::
  ( Integral a
  )
    => a -> a -> Bool
rPr =
  eq 1 << gcd


-- | Negation of 'rPr'.
nrP ::
  ( Integral a
  )
    => a -> a -> Bool
nrP =
  n << rPr
