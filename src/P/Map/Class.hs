{-# Language MultiParamTypeClasses #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language GADTs #-}
{-# Language ScopedTypeVariables #-}
{-# Language InstanceSigs #-}
{-|
Module :
  P.Map.Class
Description :
  A class for map-like objects
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Map.Class
  ( Indexable (..)
  , qx
  , fqx
  , qxd
  , fqd
  , nmr
  , fns
  , nR
  , fnR
  , iM
  , fiM
  , (!<)
  , fjt
  , ajs
  , aJs
  , ReverseIndexable (..)
  , fIx
  , fIw
  , Map (..)
  , nZ
  , fnZ
  , nzs
  , fzs
  , dks
  , fdk
  , lk
  -- * Deprecated
  , member
  , adjust
  , elemIndices
  , findIndices
  , lookup
  , assocs
  , indices
  )
  where


import Prelude
  ( Integral
  , otherwise
  )
import qualified Prelude


import P.Aliases
import P.Alternative
import P.Applicative
import P.Applicative.Unpure
import P.Arithmetic
import P.Arithmetic.Number
import P.Arithmetic.Pattern
import P.Arrow.Cokleisli
import P.Arrow.Kleisli
import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Bool
import P.Category
import P.Comonad
import P.Enum
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Function.Curry
import P.Functor
import P.Functor.Compose
import P.Maybe
import P.Monad
import P.Ord
import P.Scan
import P.Tuple
import P.Vector
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A functor which can be indexed.
class
  ( Functor f
  )
    => Indexable f k
  where
    {-# Minimal (!?), (ns | ajt) #-}
    -- | Indexes a structure.
    --
    -- More general form of '(Data.Map.Strict.!?)'.
    -- See also '(Prelude.!!)'.
    (!?) :: f a -> k -> Maybe' a

    -- | Determines if some key is a member of the structure.
    -- Returns @True@ when indexing would give a value and @False@ when it would return nothing.
    mr :: k -> f a -> Bool
    mr key map =
      nø $ map !? key

    -- | Map a function across the value at a specific key.
    --
    -- More general form of 'Data.Map.Strict.adjust'.
    ajt :: k -> (a -> a) -> f a -> f a
    ajt key func map =
      case
        map !? key
      of
        Pu oldVal ->
          ns key (func oldVal) map
        [] ->
          map

    -- | Weak insert.
    -- Replaces the value at an existing key.
    ns :: k -> a -> f a -> f a
    ns key = ajt key < p


instance
  ( Integral k
  , Eq k
  )
    => Indexable (Either a) k
  where
    Rit x !? 0 =
      [x]
    _ !? _ =
      []

    mr =
      p < (0 ==)

    ns 0 val _ =
      Rit val
    ns _ _ struct =
      struct


instance
  ( Eq k
  )
    => Indexable ((->) k) k
  where
    func !? x =
      p $ func x

    mr _ _ =
      T

    ns x val func y
      | x == y
      =
        val
      | Prelude.otherwise
      =
        func y

    ajt key transform func x
      | x == key
      =
        transform (func x)
      | Prelude.otherwise
      =
        func x


instance
  ( Indexable f k
  , SemiAlign f
  )
    => Indexable (Comp f Maybe) k
  where
    Co xs !? key =
      iM [0] ~< xs !? key

    ajt key transform (Co xs) =
      Co $ ajt key (m transform) xs



instance
  ( Eq a
  )
    => Indexable (Blackbird [] (,) a) a
  where
    MM xs !? key =
      tL $ Prelude.lookup key xs

    ajt key transform (MM xs) =
      MM $ go xs
      where
        go [] =
          []
        go ((key1, oldVal) : xs)
          | key1 == key
          =
            (key, transform oldVal) : xs
          | Prelude.otherwise
          =
            (key1, oldVal) : go xs


instance
  ( Integral i
  , Number i
  )
    => Indexable [] i
  where
    [] !? _ =
      []
    (x : _) !? 0 =
      p x
    (_ : xs) !? index@X =
      xs !? Pv index
    _ !? _ =
      []

    ajt _ _ [] =
      []
    ajt index@X transform (x : xs) =
      x : ajt (Pv index) transform xs
    ajt 0 transform (x : xs) =
      transform x : xs
    ajt _ transform xs =
      xs


instance
  ( Integral i
  , Number i
  )
    => Indexable (Vec n) i
  where
    Ev !? _ =
      []
    _ !? N =
      []
    (x :++ _) !? 0 =
      p x
    (_ :++ xs) !? index =
      xs !? Pv index

    ajt _ _ Ev =
      Ev
    ajt index@X transform (x :++ xs) =
      x :++ ajt (Pv index) transform xs
    ajt 0 transform (x :++ xs) =
      transform x :++ xs
    ajt _ transform xs =
      xs


instance
  ( Eq k
  )
    => Indexable (Kleisli Maybe k) k
  where
    Kle arrow !? key =
      tL $ arrow key

    ajt key conv (Kle arrow) =
      Kle $ \ x ->
        if
          x == key
        then
          conv < arrow x
        else
          arrow x


instance
  ( Eq (w k)
  )
    => Indexable (Cokleisli w k) (w k)
  where
    Cok arrow !? key =
      [arrow key]

    ajt key conv (Cok arrow) =
      Cok $ \ x ->
        if
          x == key
        then
          conv $ arrow x
        else
          arrow x


{-# Deprecated member "Use mr instead" #-}
-- | Long version of 'mr'.
member ::
  ( Indexable f k
  )
    => k -> f a -> Bool
member =
  mr


{-# Deprecated adjust "Use ajt instead" #-}
-- | Long version of 'ajt'.
adjust ::
  ( Indexable f k
  )
    => k -> (a -> a) -> f a -> f a
adjust =
  ajt


-- | Prefix version of '(!?)'.
qx ::
  ( Indexable f k
  )
    => k -> f a -> Maybe' a
qx =
  F (!?)


-- | Flip of 'qx'.
fqx ::
  ( Indexable f k
  )
    => f a -> k -> Maybe' a
fqx =
  (!?)


-- | Indexes with a default value to be returned when the index cannot be found.
qxd ::
  ( Indexable f k
  )
    => a -> k -> f a -> a
qxd def key map =
  case
    qx key map
  of
    [] ->
      def
    val : _ ->
      val


-- | Flip of qxd.
fqd ::
  ( Indexable f k
  )
    => k -> a -> f a -> a
fqd =
  f' qxd


-- | Yields @True@ when the input is not a member of the map and @False@ when it is.
nmr ::
  ( Indexable f k
  )
    => k -> f a -> Bool
nmr =
  n << mr


-- | Flip of ns
fns ::
  ( Indexable f k
  )
    => a -> k -> f a -> f a
fns =
  F ns


-- | Takes a list of indices and values and inserts them into a list with 'ns'.
nR ::
  ( Indexable f k
  , Foldable t
  )
    => t (k, a) -> f a -> f a
nR =
  F fnR


-- | Flip of 'nR'.
fnR ::
  ( Indexable f k
  , Foldable t
  )
    => f a -> t (k, a) -> f a
fnR =
  rF $ U ns


-- | Flip of 'ajt'.
fjt ::
  ( Indexable f k
  )
    => (a -> a) -> k -> f a -> f a
fjt =
  F ajt


-- | Takes a list of indices and function and applies that function to all elements at those indices.
ajs ::
  ( Indexable f k
  , Foldable t
  )
    => (a -> a) -> t k -> f a -> f a
ajs =
  F < rF < fjt


-- | Flip of 'ajs'.
aJs ::
  ( Indexable f k
  , Foldable t
  )
    => t k -> (a -> a) -> f a -> f a
aJs =
  F ajs


-- | Takes a list of indexes and an indexable structure and produces a list of results.
--
-- ==== __Examples__
--
-- >>> iM [0,1,-2,0,4] [1,2,3]
-- [1,2,1]
--
-- >>> iM [1,2,3,6,0,-4] []
-- []
iM ::
  ( Indexable f k
  , Monad m
  , Alternative m
  )
    => m k -> f a -> m a
iM =
  F fiM


-- | Flip of 'iM'
fiM ::
  ( Indexable f k
  , Monad m
  , Alternative m
  )
    => f a -> m k -> m a
fiM struct indexes = do
  index <- indexes
  case
    struct !? index
    of
      [] ->
        em
      (x : _) ->
        p x


-- | Infix of 'iM' and 'fiM'.
(!<) ::
  ( Indexable f k
  , Monad m
  , Alternative m
  )
    => f a -> m k -> m a
(!<) =
  fiM


class
  ( Indexable f k
  )
    => ReverseIndexable f k
  where
    -- | Get all indexes that map to an element.
    isx ::
      ( Eq a
      )
        => a -> f a -> List k
    isx =
      ixW < eq

    -- | Get all indexes that map to elements satisfying a predicate.
    ixW :: Predicate a -> f a -> List k
    ixW pred =
      m st < Prelude.filter (pred < cr) < tAL

    -- | Return all key value pairs.
    tAL :: f a -> List (k, a)

    -- | Get all indices.
    aix :: f a -> List k
    aix =
      m st < tAL


{-# Deprecated assocs "Use asc or tAL instead" #-}
-- | Long version of 'tAL'.
assocs ::
  ( ReverseIndexable f k
  )
    => f a -> List (k, a)
assocs =
  tAL


{-# Deprecated indices "Use aix instead" #-}
-- | Long version of 'aix'.
indices ::
  ( ReverseIndexable f k
  )
    => f a -> List k
indices =
  aix


instance
  ( Integral i
  , Number i
  )
    => ReverseIndexable [] i
  where
    tAL =
      eu

    aix =
      m st < eu


{-# Deprecated elemIndices "Use isx instead" #-}
-- | Long version of 'isx'.
elemIndices ::
  ( ReverseIndexable f k
  , Eq a
  )
    => a -> f a -> List k
elemIndices =
  isx


{-# Deprecated findIndices "Use ixW instead" #-}
-- | Long version of 'ixW'.
findIndices ::
  ( ReverseIndexable f k
  )
    => Predicate a -> f a -> List k
findIndices =
  ixW


-- | Flip of 'isx'.
fIx ::
  ( ReverseIndexable f k
  , Eq a
  )
    => f a -> a -> List k
fIx =
  f' isx


-- | Flip of 'ixW'.
fIw ::
  ( ReverseIndexable f k
  )
    => f a -> Predicate a -> List k
fIw =
  f' ixW


-- | A @Map@ is an 'Indexable' structure that also allows the creation and deletion of keys.
class
  ( Indexable f k
  )
    => Map f k
  where
    -- | Insert a value at a particular key, replacing a value if one already exists for that key.
    nz :: k -> a -> f a -> f a

    -- | Delete a key from the map
    dlt :: k -> f a -> f a


instance
  ( Eq k
  )
    => Map (Blackbird [] (,) k) k
  where
    nz key val (MM xs) =
      MM $ go xs
      where
        go [] =
          [(key, val)]
        go ((key1, val1) : xs)
          | key1 == key
          =
            (key, val) : xs
          | Prelude.otherwise
          =
            (key1, val1) : go xs

    dlt key (MM xs) =
      MM $ go xs
      where
        go [] =
          []
        go ((key1, val1) : xs)
          | key1 == key
          =
            xs
          | Prelude.otherwise
          =
            (key1, val1) : go xs

instance
  ( Eq k
  )
    => Map (Kleisli Maybe k) k
  where
    nz key val (Kle arrow) =
      Kle $ \ x ->
        if
          x == key
        then
          p val
        else
          arrow x

    dlt key (Kle arrow) =
      Kle $ \ x ->
        if
          x == key
        then
          aId
        else
          arrow x


-- | Takes a list of indices and values and inserts them into a list with 'nz'.
nZ ::
  ( Map f k
  , Foldable t
  )
    => t (k, a) -> f a -> f a
nZ =
  F fnZ


-- | Flip of 'nZ'.
fnZ ::
  ( Map f k
  , Foldable t
  )
    => f a -> t (k, a) -> f a
fnZ =
  rF $ U nz


-- | Inserts a value at a key if the key does not already exist.
-- If the key exists it leaves the map unchanged.
--
-- ==== __Examples__
--
nzs ::
  ( Map f k
  )
    => k -> a -> f a -> f a
nzs key val map
  | mr key map
  =
    map
  | otherwise
  =
    nz key val map


-- | Flip of 'nzs'.
fzs ::
  ( Map f k
  )
    => a -> k -> f a -> f a
fzs =
  F nzs


-- | Takes a list of keys and deletes all elements with those keys.
dks ::
  ( Map f k
  , Foldable t
  )
    => t k -> f a -> f a
dks =
  F fdk


-- | Flip of 'dks'.
fdk ::
  ( Map f k
  , Foldable t
  )
    => f a -> t k -> f a
fdk =
  rF dlt


{-# Deprecated lookup "Use lk instead" #-}
-- | Long version of 'lk'.
lookup ::
  ( Indexable (Blackbird f p a) k
  )
    => k -> f (p a b) -> Maybe' b
lookup =
  lk


-- | Lookup a value in a list of tuples.
--
-- Version of 'Prelude.lookup'.
lk ::
  ( Indexable (Blackbird f p a) k
  )
    => k -> f (p a b) -> Maybe' b
lk k xs =
  MM xs !? k
