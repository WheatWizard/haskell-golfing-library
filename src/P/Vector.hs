{-# Language GADTs #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language TypeFamilies #-}
{-# Language MultiParamTypeClasses #-}
{-# Language UndecidableInstances #-}
module P.Vector
  ( Zr
  , Nx
  , Vec (..)
  , pattern (:+)
  , Vec0
  , pattern V0
  , Vec1
  , pattern V1
  , Vec2
  , pattern V2
  , Vec3
  , pattern V3
  , Vec4
  , pattern V4
  , Vec5
  , pattern V5
  , Vec6
  , pattern V6
  , Vec7
  , pattern V7
  , pattern VUC
  , pattern VUS
  )
  where


import Prelude
  ( (*)
  , (+)
  )
import qualified Prelude


import qualified Control.Applicative
import qualified Data.Ix


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Aliases
import P.Applicative
import P.Applicative.Ring
import P.Bifunctor.These
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.First
import P.Function.Compose
import P.Function.Curry
import P.Ix
import P.Last hiding
  ( snoc
  )
import P.Monad
import P.Ord
import P.Reverse
import P.Scan
import P.Show
import P.Sort.Class
import P.Traversable
import P.Zip
import P.Zip.SemiAlign
import P.Zip.WeakAlign
import P.Zip.UnitalZip


infixr 5 :+, :++


-- | Zero type for type level arithmetic.
data Zr
-- | Successor type for type level arithmetic.
data Nx a


-- | A vector type.
-- These are lists with a compile time fixed size.
data Vec n a where
  -- | Internal constructor.
  --
  -- Prefer 'V0' for most purposes.
  Ev :: Vec Zr a
  -- | Internal constructor.
  --
  -- Prefer '(:+)' for most purposes.
  (:++) :: a -> Vec n a -> Vec (Nx n) a


-- | Vector cons.
-- Takes a vector and a head element and makes a vector one larger with the new head element.
--
-- There is also the internal constructor '(:++)', which does the same thing but requires the @GADTs@ extension to be used on a pattern.
--
-- ==== __Examples__
--
-- To use '(:++)' in a pattern match requires the GADTs extension.
--
-- >>> f (_ :++ _) = ()
-- <interactive>:1:4: error:
--     • A pattern match on a GADT requires the GADTs or TypeFamilies language extension
--     • In the pattern: _ :++ _
--       In an equation for ‘f’: f (_ :++ _) = ()
--
-- '(:+)' can be used without an extra extensions.
--
-- >>> f (_ :+ _) = ()
--
-- However to do certain pattern matches you need to use '(:++)' instead of '(:+)':
--
-- >>> set -XGADTs
-- >>> f::Vec n a->();f V0 = ();f (_ :+ _) = ()
-- <interactive>:7:18: error:
--     • Couldn't match type ‘n’ with ‘Zr’
--       Expected: Vec n a
--         Actual: Vec0 a
--       ‘n’ is a rigid type variable bound by
--         the type signature for:
--           f :: forall n a. Vec n a -> ()
--         at <interactive>:7:1-14
--     • In the pattern: V0
--       In an equation for ‘f’: f V0 = ()
--     • Relevant bindings include
--         f :: Vec n a -> () (bound at <interactive>:7:16)
-- >>> f::Vec n a->();f Ev = ();f (_ :++ _) = ()
--
{-# Complete (:+) #-}
pattern (:+) ::
  (
  )
    => a -> Vec n a -> (Vec (Nx n)) a
pattern x :+ y =
  x :++ y


-- | Alias for the empty vector type.
-- Use 'Ev' as it's constructor.
type Vec0 =
  Vec Zr


-- | Constructor for 'Vec0'.
--
-- ==== __Examples__
--
-- To use 'Ev' in a pattern match requires the GADTs extension.
--
-- >>> f Ev = 1
-- <interactive>:13:3: error:
--     • A pattern match on a GADT requires the GADTs or TypeFamilies language extension
--     • In the pattern: Ev
--       In an equation for ‘f’: f Ev = 1
--
-- 'V0' can be used without an extra extensions.
--
-- >>> f V0 = 1
--
-- However to do certain pattern matches you need to use 'Ev' instead of 'V0':
--
-- >>> set -XGADTs
-- >>> f::Vec n a->();f V0 = ();f (_ :+ _) = ()
-- <interactive>:7:18: error:
--     • Couldn't match type ‘n’ with ‘Zr’
--       Expected: Vec n a
--         Actual: Vec0 a
--       ‘n’ is a rigid type variable bound by
--         the type signature for:
--           f :: forall n a. Vec n a -> ()
--         at <interactive>:7:1-14
--     • In the pattern: V0
--       In an equation for ‘f’: f V0 = ()
--     • Relevant bindings include
--         f :: Vec n a -> () (bound at <interactive>:7:16)
-- >>> f::Vec n a->();f Ev = ();f (_ :++ _) = ()
--
{-# Complete V0 #-}
pattern V0 ::
  (
  )
    => Vec0 a
pattern V0 =
  Ev


-- | Alias for vectors with one element.
type Vec1 =
  Vec (Nx Zr)


-- | Constructor for 'Vec1'.
{-# Complete V1 #-}
pattern V1 ::
  (
  )
    => a -> Vec1 a
pattern V1 x =
  x :+ V0


-- | Alias for vectors with two elements.
type Vec2 =
  Vec (Nx (Nx Zr))


-- | Constructor for 'Vec2'.
{-# Complete V2 #-}
pattern V2 ::
  (
  )
    => a -> a -> Vec2 a
pattern V2 x y =
  x :+ y :+ V0


-- | Alias for vectors with three elements.
type Vec3 =
  Vec (Nx (Nx (Nx (Zr))))


-- | Constructor for 'Vec3'.
{-# Complete V3 #-}
pattern V3 ::
  (
  )
    => a -> a -> a -> Vec3 a
pattern V3 x y z =
  x :+ y :+ z :+ V0


-- | Alias for vectors with four elements.
type Vec4 =
  Vec (Nx (Nx (Nx (Nx Zr))))


-- | Constructor for 'Vec4'.
{-# Complete V4 #-}
pattern V4 ::
  (
  )
    => a -> a -> a -> a -> Vec4 a
pattern V4 x1 x2 x3 x4 =
  x1 :+ x2 :+ x3 :+ x4 :+ V0


-- | Alias for vectors with five elements.
type Vec5 =
  Vec (Nx (Nx (Nx (Nx (Nx Zr)))))


-- | Constructor for 'Vec5'.
{-# Complete V5 #-}
pattern V5 ::
  (
  )
    => a -> a -> a -> a -> a -> Vec5 a
pattern V5 x1 x2 x3 x4 x5 =
  x1 :+ x2 :+ x3 :+ x4 :+ x5 :+ V0


-- | Alias for vectors with six elements.
type Vec6 =
  Vec (Nx (Nx (Nx (Nx (Nx (Nx Zr))))))


-- | Constructor for 'Vec6'.
{-# Complete V6 #-}
pattern V6 ::
  (
  )
    => a -> a -> a -> a -> a -> a -> Vec6 a
pattern V6 x1 x2 x3 x4 x5 x6 =
  x1 :+ x2 :+ x3 :+ x4 :+ x5 :+ x6 :+ V0


-- | Alias for vectors with six elements.
type Vec7 =
  Vec (Nx (Nx (Nx (Nx (Nx (Nx (Nx Zr)))))))


-- | Constructor for 'Vec7'.
{-# Complete V7 #-}
pattern V7 ::
  (
  )
    => a -> a -> a -> a -> a -> a -> a -> Vec7 a
pattern V7 x1 x2 x3 x4 x5 x6 x7 =
  x1 :+ x2 :+ x3 :+ x4 :+ x5 :+ x6 :+ x7 :+ V0


-- | Uncons on a vector.
--
-- Splits a nonempty vector into a tuple of its head and tail.
--
pattern VUC ::
  (
  )
    => Vec (Nx n) a -> (a, Vec n a)
pattern VUC a <- (U (:+) -> a) where
  VUC (x :+ xs) =
    (x, xs)


-- | Unsnoc on a vector.
--
-- Splits a nonempty vector into a tuple of its last element and initial segment.
--
pattern VUS ::
  (
  )
    => Vec (Nx n) a -> (a, Vec n a)
pattern VUS a <- (U snoc -> a) where
  VUS (x :++ Ev) =
    (x, V0)
  VUS (x :++ xs@(_ :++ _)) =
    let
      (last, init) =
        VUS xs
    in
      (last, x :++ init)


{-# Deprecated unsafeFromList "unsafe" #-}
-- | Converts a list to a vector.
--
-- Errors when the list is the wrong size.
unsafeFromList ::
  (
  )
    => List a -> Vec n b -> Vec n a
unsafeFromList [] Ev =
  Ev
unsafeFromList (x : xs) (_ :++ ys) =
  x :++ unsafeFromList xs ys
unsafeFromList (_ : _) Ev =
  Prelude.errorWithoutStackTrace "unsafeFromList: List is too long"
unsafeFromList [] (_ :++ _) =
  Prelude.errorWithoutStackTrace "unsafeFromList: List is too short"


snoc ::
  (
  )
    => a -> Vec n a -> Vec (Nx n) a
snoc x Ev =
  V1 x
snoc x (y :++ ys) =
  y :+ snoc x ys


instance Monad Vec0 where
  Ev >>= _ =
    Ev


instance Functor (Vec n) where
  fmap _ Ev =
    V0
  fmap func (x :++ xs) =
    func x :+ m func xs


instance Foldable (Vec n)
  where
    foldr _ accum Ev =
      accum
    foldr func accum (x :++ xs) =
      func x (rF func accum xs)
    -- foldMap _ Ev =
    --   i
    -- foldMap func (x :++ xs) =
    --   func x <> mF func xs


instance Traversable (Vec n) where
  traverse func Ev =
    p V0
  traverse func (x :++ xs) =
    l2 (:+) (func x) $ tv func xs

  sequenceA Ev =
    p V0
  sequenceA (x :++ xs) =
    l2 (:+) x $ sQ xs


instance Applicative Vec0 where
  pure _ =
    V0
  liftA2 _ _ _ =
    V0


instance (Applicative (Vec n)) => Applicative (Vec (Nx n)) where
  pure x =
    x :+ p x
  liftA2 func (x :++ xs) (y :++ ys) =
    func x y :+ l2 func xs ys


instance RingApplicative Vec0 where
  ml2 =
    p3 Ev


instance
  ( Applicative (Vec n)
  )
    => RingApplicative (Vec (Nx n))
  where
    ml2 =
      l2


instance
  (
  )
    => Uncons (Vec (Nx n)) (Vec n)
  where
    _uC (x :+ xs) =
      [(x, xs)]


instance
  (
  )
    => Firstable (Vec (Nx n)) (Vec n)
  where
    cons =
      (:+)


instance Reversable (Vec n)
  where
    _rv Ev =
      V0
    _rv (x :++ xs) =
      snoc x (_rv xs)


instance ReversableFunctor (Vec n)
  where
    rvW _ Ev =
      V0
    rvW func (x :++ xs) =
      snoc (func x) (rvW func xs)


instance Eq1 (Vec n) where
  q1 _ Ev Ev =
    T
  q1 userEq (x :++ xs) (y :++ ys) =
    userEq x y <> q1 userEq xs ys


instance Ord1 (Vec n) where
  lcp _ Ev Ev =
    EQ
  lcp userComp (x :++ xs) (y :++ ys) =
    case
      userComp x y
    of
      EQ ->
        lcp userComp xs ys
      other ->
        other


instance Eq (Vec0 a) where
  (==) =
    pp T


instance Eq a => Eq (Vec (Nx n) a) where
  (==) =
    q1 eq


instance Ord (Vec0 a) where
  cp =
    pp EQ


instance Ord a => Ord (Vec (Nx n) a) where
  cp =
    lcp cp


instance Prelude.Ord (Vec0 a) where
  compare =
    otp << cp


instance Prelude.Ord a => Prelude.Ord (Vec (Nx n) a) where
  compare =
    otp << lcp (ofp << Prelude.compare)


instance Show (Vec0 a) where
  show _ =
    "[]"


instance
  ( Show a
  )
    => Show (Vec (Nx n) a)
  where
    show =
      sh < tL


instance Scan (Vec n) where
  mA _ _ Ev =
    V0
  mA func accum (x :++ xs) =
    let
      (accum', x') =
        func accum x
    in
      x' :+ mA func accum' xs

  sc _ _ Ev =
    V0
  sc func accum (x :++ xs) =
    let
      x' =
        func accum x
    in
      x' :+ sc func x' xs


instance Zip (Vec n) where
  zW _ Ev Ev =
    V0
  zW func (x :++ xs) (y :++ ys) =
    func x y :+ zW func xs ys


instance
  (
  )
    => WeakAlign (Vec n)
  where
    cnL =
      p


instance
  (
  )
    => SemiAlign (Vec n)
  where
    aln Ev Ev =
      V0
    aln (x :++ xs) (y :++ ys) =
      Te x y :+ aln xs ys

    alW _ Ev Ev =
      V0
    alW func (x :++ xs) (y :++ ys) =
      func (Te x y) :+ alW func xs ys


instance
  (
  )
    => UnitalZip Vec0
  where
    aId =
      V0


instance
  (
  )
    => Sortable (Vec n)
  where
    sr vec =
      unsafeFromList (sr $ tL vec) vec

    sW func vec =
      unsafeFromList (sW func $ tL vec) vec


instance
  (
  )
    => Ix (Vec0 a)
  where
    uRg _ =
      [V0]

    uDx _ _ =
      0

    uIr _ _ =
      T


instance
  ( Ix (Vec n a)
  , Ix a
  )
    => Ix (Vec (Nx n) a)
  where
    uRg (x :++ xs, y :++ ys) =
      l2 (:++) (rg x y) (rg xs ys)

    uIr (x :++ xs, y :++ ys) (z :++ zs) =
      uIr (x, y) z <> uIr (xs, ys) zs

    uDx (x :++ xs, y :++ ys) (z :++ zs) =
      uDx (xs, ys) zs * gz x y + uDx (x, y) z
