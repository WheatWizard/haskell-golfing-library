{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language PatternSynonyms #-}
{-# Language DeriveFunctor #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Ord
Description :
  Functions having to do with the @Ord@ class.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions having to do with the @Ord@ class, for use in code golf.
Please do not use this in real code.
-}
module P.Ord
  ( Ord (..)
  , gt
  , ge
  , lt
  , le
  , (>)
  , (>=)
  , (<.)
  , (^^)
  , (!^)
  , Ordering (..)
  , fcp
  , tcp
  , cb
  , gb
  , geb
  , ltb
  , lb
  , pattern RO
  , pattern RC
  , Ord1 (..)
  , Ord2 (..)
  , c1'
  -- * Utility
  , ofp
  , otp
  , POrd (..)
  -- * Deprecated
  , compare
  , liftCompare
  , liftCompare2
  )
  where


import qualified Prelude
import Prelude
  ( Num
  , Integer
  , Int
  , Float
  , Char
  , Show
  , Word
  )


import Data.Word
  ( Word8
  , Word16
  , Word32
  , Word64
  )
import Data.Char
  ( GeneralCategory
  )


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Aliases
import P.Bool
import P.Eq
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Bifunctor.Flip


infixr 4 >, <., >=


-- | Represents the possible results of comparison under a total ordering.
data Ordering
  = LT -- ^ Less than
  | EQ -- ^ Equal to
  | GT -- ^ Greater than
  deriving
    ( Show
    )


instance Eq Ordering where
  LT == LT =
   T
  EQ == EQ =
   T
  GT == GT =
   T
  _ == _ =
   B


instance Ord Ordering where
  cp x y
    | x == y
    =
      EQ
  cp LT _ =
    LT
  cp GT _ =
    GT
  cp x y =
    RO (cp y x)


instance Semigroup Ordering where
  LT <> _ =
    LT
  EQ <> y =
    y
  GT <> _ =
    GT


instance Monoid Ordering where
  mempty =
    EQ


-- | Converts between prelude 'Prelude.Ordering's and local 'Ordering's.
ofp ::
  (
  )
    => Prelude.Ordering -> Ordering
ofp Prelude.LT =
  LT
ofp Prelude.EQ =
  EQ
ofp Prelude.GT =
  GT


-- | Converts between local 'Ordering's and prelude 'Prelude.Ordering's.
otp ::
  (
  )
    => Ordering -> Prelude.Ordering
otp LT =
  Prelude.LT
otp EQ =
  Prelude.EQ
otp GT =
  Prelude.GT


-- | Wrap something with an local 'Ord' instance to get a 'Prelude.Ord' instance instead.
newtype POrd a =
  POrd a
  deriving
    ( Eq
    , Functor
    )


instance
  ( Ord a
  )
    => Prelude.Ord (POrd a)
  where
    compare (POrd x) (POrd y) =
      otp (cp x y)
    POrd x <= POrd y =
      x <= y


instance
  (
  )
    => Eq1 POrd
  where
    q1 userEq (POrd x) (POrd y) =
      userEq x y


-- | The trivial comparison.
-- Always gives 'EQ' for any two objects.
tcp ::
  (
  )
    => a -> b -> Ordering
tcp _ _ =
  EQ


-- | The Ord class is used for totally ordered datatypes.
--
-- The Ordering datatype allows a single comparison to determine the precise ordering of two objects.
--
-- Ord, as defined by the Haskell report, implements a total order and has the following properties:
--
-- ==== Comparability
--
-- prop> x <= y || y <= x = True
--
-- ==== Transitivity
--
-- prop> if x <= y && y <= z = True, then x <= z = True
--
-- ==== Reflexivity
--
-- prop> x <= x = True
--
-- ==== Antisymmetry
--
-- prop> if x <= y && y <= x = True, then x == y = True
--
-- Minimal complete definition: either compare or <=. Using compare can be more efficient for complex types.
class Eq a => Ord a where
  {-# Minimal cp | (<=) #-}
  -- | Compare two values.
  --
  -- Equivalent to 'Prelude.compare'.
  cp :: a -> a -> Ordering
  cp x y
    | x == y
    =
      EQ
    | x <= y
    =
      LT
    | T
    =
      GT
  -- | Less than or equal to.
  --
  -- Equivalent to '(Prelude.<=)'.
  (<=) :: a -> a -> Bool
  x <= y =
    cp x y /= GT
  -- | Take two elements and get the larger one.
  -- If the two elements are equal it defaults to the first one.
  --
  -- Equivalent to 'Prelude.max'.
  ma :: a -> a -> a
  ma x y =
    case
      cp x y
    of
      LT ->
        y
      _ ->
        x
  -- | Take two elements and get the smaller one.
  -- If the two elements are equal it defaults to the first one.
  --
  -- Equivalent to 'Prelude.min'.
  mN :: a -> a -> a
  mN x y =
    case
      cp x y
    of
      GT ->
        y
      _ ->
        x


-- | Takes the minimum of two elements.
--
-- Infix of 'mN'.
--
-- Generalization of '(Prelude.&&)'
(!^) ::
  ( Ord a
  )
    => a -> a -> a
(!^) =
  mN


-- | Takes the maximum of two elements.
--
-- Infix of 'mN'.
--
-- Generalization of '(Prelude.||)'
(^^) ::
  ( Ord a
  )
    => a -> a -> a
(^^) =
  ma


instance Ord Integer where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Int where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Float where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Char where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Word where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Word8 where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Word16 where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Word32 where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Word64 where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord GeneralCategory where
  cp =
    mm ofp Prelude.compare
  (<=) =
    (Prelude.<=)
  ma =
    Prelude.max
  mN =
    Prelude.min


instance Ord Bool where
  cp T T =
    EQ
  cp B B =
    EQ
  cp T B =
    GT
  cp B T =
    LT

  -- Implement with short-circuiting.
  B <= _ =
    T
  _ <= T =
    T
  T <= B =
    B

  -- Implement with short-circuiting
  -- | Logical or.
  ma =
    (Prelude.||)

  -- Implement with short-circuiting
  -- | Logical and.
  mN =
    (Prelude.&&)


instance Ord () where
  cp _ _ =
    EQ

  _ <= _ =
    T

  ma _ _ =
    ()

  mN _ _ =
    ()


instance
  ( Ord a1
  , Ord a2
  )
    => Ord
      ( a1
      , a2
      )
  where
    cp
      ( x1
      , x2
      )
      ( y1
      , y2
      )
      =
      cp x1 y1
      <> cp x2 y2


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  )
    => Ord
      ( a1
      , a2
      , a3
      )
  where
    cp
      ( x1
      , x2
      , x3
      )
      ( y1
      , y2
      , y3
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      )
      ( y1
      , y2
      , y3
      , y4
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  , Ord a11
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      , x11
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      , y11
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10
      <> cp x11 y11


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  , Ord a11
  , Ord a12
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      , x11
      , x12
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      , y11
      , y12
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10
      <> cp x11 y11
      <> cp x12 y12


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  , Ord a11
  , Ord a12
  , Ord a13
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      , x11
      , x12
      , x13
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      , y11
      , y12
      , y13
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10
      <> cp x11 y11
      <> cp x12 y12
      <> cp x13 y13


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  , Ord a11
  , Ord a12
  , Ord a13
  , Ord a14
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      , a14
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      , x11
      , x12
      , x13
      , x14
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      , y11
      , y12
      , y13
      , y14
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10
      <> cp x11 y11
      <> cp x12 y12
      <> cp x13 y13
      <> cp x14 y14


instance
  ( Ord a1
  , Ord a2
  , Ord a3
  , Ord a4
  , Ord a5
  , Ord a6
  , Ord a7
  , Ord a8
  , Ord a9
  , Ord a10
  , Ord a11
  , Ord a12
  , Ord a13
  , Ord a14
  , Ord a15
  )
    => Ord
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      , a14
      , a15
      )
  where
    cp
      ( x1
      , x2
      , x3
      , x4
      , x5
      , x6
      , x7
      , x8
      , x9
      , x10
      , x11
      , x12
      , x13
      , x14
      , x15
      )
      ( y1
      , y2
      , y3
      , y4
      , y5
      , y6
      , y7
      , y8
      , y9
      , y10
      , y11
      , y12
      , y13
      , y14
      , y15
      )
      =
      cp x1 y1
      <> cp x2 y2
      <> cp x3 y3
      <> cp x4 y4
      <> cp x5 y5
      <> cp x6 y6
      <> cp x7 y7
      <> cp x8 y8
      <> cp x9 y9
      <> cp x10 y10
      <> cp x11 y11
      <> cp x12 y12
      <> cp x13 y13
      <> cp x14 y14
      <> cp x15 y15


instance
  ( Ord a
  )
    => Ord (List a)
  where
    cp =
      lcp cp


{-# Deprecated compare "Use cp instead" #-}
-- | Long version of 'cp'.
compare ::
  ( Ord a
  )
    => a -> a -> Ordering
compare =
  cp


-- | Greater than.
gt ::
  ( Ord a
  )
    => a -> a -> Bool
gt x y =
  cp y x == GT


-- | Greater than or equal to.
ge ::
  ( Ord a
  )
    => a -> a -> Bool
ge =
  (<=)


-- | Less than.
lt ::
  ( Ord a
  )
    => a -> a -> Bool
lt x y =
  cp y x == LT


-- | Less than or Equal to.
le ::
  ( Ord a
  )
    => a -> a -> Bool
le =
  F ge


-- | Greater than.
--
-- Equivalent to '(Prelude.>)'.
(>) ::
  ( Ord a
  )
    => a -> a -> Bool
(>) =
  lt


-- | Greater than or equal to.
--
-- Equivalent to '(Prelude.>=)'.
(>=) ::
  ( Ord a
  )
    => a -> a -> Bool
(>=) =
  le


-- | Less than.
--
-- Prefer to use the shorter '(>)'.
--
-- Equivalent to '(Prelude.<)'.
(<.) ::
  ( Ord a
  )
    => a -> a -> Bool
(<.) =
  gt


-- | Flip of 'cp'.
fcp ::
  ( Ord a
  )
    => a -> a -> Ordering
fcp =
  F cp


-- | Compare with a user defined conversion function.
--
-- ==== __Examples__
--
-- To compare two lists by length use @cb lg@.
--
cb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Ordering
cb =
  on cp


-- | Determine if one value is greater than another with a user defined conversion function.
gb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Bool
gb =
  on gt


-- | Determine if one value is greater than or equal to another with a user defined conversion function.
geb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Bool
geb =
  on ge


-- | Determine if one value is less than another with a user defined conversion function.
ltb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Bool
ltb =
  on lt


-- | Determine if one value is less than or equal to another with a user defined conversion function.
lb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> Bool
lb =
  on le


-- | Internal function to swap an 'Ordering' to the opposite value.
--
-- Instead exports the pattern 'RO'.
_rO ::
  (
  )
    => Ordering -> Ordering
_rO LT =
  GT
_rO EQ =
  EQ
_rO GT =
  LT


{-# Complete RO #-}
-- | Swaps an 'Ordering' to the opposite value.
pattern RO ::
  (
  )
    => Ordering -> Ordering
pattern RO x <- (_rO -> x) where
  RO =
    _rO


-- | To be used to take a ordering function and create the opposite ordering function.
--
-- Has a much more general type simply because it can.
--
-- ==== __Examples__
--
-- Reverse the normal comparison:
--
-- >>> cp 1 2
-- LT
-- >>> rC cp 1 2
-- GT
--
-- Reverse a more complicated comparison:
--
-- >>> on cp sh 9 19
-- GT
-- >> rC (on cp sh) 9 19
-- LT
--
-- It's more general type can be used in some circumstances:
--
-- >>> rC [(2, GT), (1, LT), (3, EQ), (3, GT)]
-- [(2,LT),(1,GT),(3,EQ),(3,LT)]
--
pattern RC ::
  ( Functor f
  , Functor g
  )
    => f (g Ordering) -> f (g Ordering)
pattern RC x <- (mm RO -> x) where
  RC =
    mm RO


class Eq1 f => Ord1 f where
  -- | Lifts a user defined comparison function up onto the structure.
  --
  -- Equivalent to 'Data.Functor.Classes.liftCompare'.
  lcp :: (a -> b -> Ordering) -> (f a -> f b -> Ordering)


instance
  (
  )
    => Ord1 List
  where
    lcp userComp (x : xs) (y : ys) =
      userComp x y <> lcp userComp xs ys
    lcp _ [] [] =
      EQ
    lcp _ [] (_ : _) =
      LT
    lcp _ (_ : _) [] =
      GT


instance
  ( Ord a
  )
    => Ord1 ((,) a)
  where
    lcp =
      c1'


instance
  ( Ord a
  , Ord b
  )
    => Ord1 ((,,) a b)
  where
    lcp =
      c1'


instance
  ( Ord a
  )
    => Ord1 (Flip (,) a)
  where
    lcp =
      c1'


instance
  ( Ord a
  , Ord b
  )
    => Ord1 (Flip ((,,) a) b)
  where
    lcp =
      c1'


instance
  ( Ord a
  , Ord b
  , Ord c
  )
    => Ord1 (Flip ((,,,) a b) c)
  where
    lcp =
      c1'


{-# Deprecated liftCompare "Use lcp instead." #-}
-- | Long version of 'lcp'.
liftCompare ::
  ( Ord1 f
  )
    => (a -> b -> Ordering) -> (f a -> f b -> Ordering)
liftCompare =
  lcp


class
  ( Eq2 p
  )
    => Ord2 p
  where
    cp2 :: (a -> b -> Ordering) -> (c -> d -> Ordering) -> p a c -> p b d -> Ordering


instance
  (
  )
    => Ord2 (,)
  where
    cp2 userComp1 userComp2 (x1, x2) (y1, y2) =
      userComp1 x1 y1 <> userComp2 x2 y2


instance
  ( Ord a
  )
    => Ord2 ((,,) a)
  where
    cp2 userComp1 userComp2 (x1, x2, x3) (y1, y2, y3) =
      cp x1 y1 <> userComp1 x2 y2 <> userComp2 x3 y3


instance
  ( Ord a
  , Ord b
  )
    => Ord2 ((,,,) a b)
  where
    cp2 userComp1 userComp2 (x1, x2, x3, x4) (y1, y2, y3, y4) =
      cp x1 y1 <> cp x2 y2 <> userComp1 x3 y3 <> userComp2 x4 y4


instance
  ( Ord a
  , Ord b
  , Ord c
  )
    => Ord2 ((,,,,) a b c)
  where
    cp2 userComp1 userComp2 (x1, x2, x3, x4, x5) (y1, y2, y3, y4, y5) =
      cp x1 y1 <> cp x2 y2 <> cp x3 y3 <> userComp1 x4 y4 <> userComp2 x5 y5


instance
  ( Ord2 p
  )
    => Ord2 (Flip p)
  where
    cp2 userComp1 userComp2 (Flp x) (Flp y) =
      cp2 userComp2 userComp1 x y


{-# Deprecated liftCompare2 "Use cp2 instead" #-}
-- | Long version of 'cp2'.
liftCompare2 ::
  ( Ord2 p
  )
    => (a -> b -> Ordering) -> (c -> d -> Ordering) -> p a c -> p b d -> Ordering
liftCompare2 =
  cp2


-- | A useful version of 'lcp' defined for members of 'Ord2'.
-- Useful to reduce duplication between instance declarations.
c1' ::
  ( Ord2 p
  , Ord c
  )
    => (a -> b -> Ordering) -> p c a -> p c b -> Ordering
c1' =
  cp2 cp
