{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language PolyKinds #-}
{-# Language QuantifiedConstraints #-}
{-# Language FlexibleContexts #-}
{-# Language UndecidableInstances #-}
{-# Language TypeOperators #-}
{-# Language GeneralizedNewtypeDeriving #-}
{-|
Module :
  P.Type.Combinator.W
Description :
  The W combinator on types
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Type.Combinator.W
  ( W (..)
  , pattern UW
  , wi
  , uwi
  , lfw
  )
  where


import qualified Prelude
import Prelude
  ( showsPrec
  )


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Bifunctor.Flip
import P.Category
import P.Category.Iso
import P.Eq
import P.Function.Compose
import P.Ord
import P.Show
import P.Swap


-- | The w-combinator on types.
--
-- Takes a type level function and feeds a single type into both of its arguments.
newtype W (f :: α -> α -> *) (a :: α) =
  W
    { _uW ::
      f a a
    }


-- | The inverse pattern of 'W'.
--
-- As a function this unwraps the 'W' combinator from the type leaving the contents identical.
pattern UW ::
  (
  )
    => W f a -> f a a
pattern UW x <- (W -> x) where
  UW =
    _uW


-- | 'W' as an isomorphism.
wi ::
  (
  )
    => f a a <-> W f a
wi =
  Iso W UW


-- | 'UW' as an isomorphism.
uwi ::
  (
  )
    => W f a <-> f a a
uwi =
  Sw wi


-- | Lift a function to the @W@ category.
lfw ::
  (
  )
    => (f a a -> g b b) -> W f a -> W g b
lfw f =
  W < f < UW


instance
  ( forall a . Functor (f a)
  , forall a . Functor (Flip f a)
  )
    => Functor (W f)
  where
    fmap f =
      lfw (UFl < m f < Flp < m f)


instance
  ( Semigroupoid p
  )
    => Semigroup (W p a)
  where
    W x <> W y =
      W (x <@ y)


instance
  ( Category p
  )
    => Monoid (W p a)
  where
    mempty =
      W id


instance
  ( Prelude.Eq (f a a)
  )
    => Prelude.Eq (W f a)
  where
    W x == W y =
      x Prelude.== y


instance
  ( Prelude.Ord (f a a)
  )
    => Prelude.Ord (W f a)
  where
    compare (W x) (W y) =
      Prelude.compare x y


instance
  ( Ord (f a a)
  )
    => Ord (W f a)
  where
    cp (W x) (W y) =
      cp x y


instance
  ( Eq2 f
  )
    => Eq1 (W f)
  where
    q1 f (W x) (W y) =
      q2 f f x y


instance
  ( Ord2 f
  )
    => Ord1 (W f)
  where
    lcp f (W x) (W y) =
      cp2 f f x y


instance
  ( Show (f a a)
  )
    => Show (W f a)
  where
    showsPrec n (W xs) trail
      | n Prelude.< 11
      =
        "W " <> showsPrec 10 xs trail
      | Prelude.otherwise
      =
        "(W " <> showsPrec 10 xs (')' : trail)
