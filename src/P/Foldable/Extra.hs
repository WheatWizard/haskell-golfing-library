{-|
Module :
  P.Foldable.Extra
Description :
  Additional functions for the P.Foldable library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable library.
-}
module P.Foldable.Extra
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Algebra.Ring
import P.Bool
import P.Eq
import P.Foldable


-- | Determines if a structure contains 0.
h0 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h0 =
  e 0


-- | Negation of 'h0'. Returns the opposite result.
ne0 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne0 =
  ne 0


-- | Determines if a structure contains 1.
h1 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h1 =
  e 1


-- | Negation of 'h1'. Returns the opposite result.
ne1 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne1 =
  ne 1


-- | Determines if a structure contains 2.
h2 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h2 =
  e 2


-- | Negation of 'h2'. Returns the opposite result.
ne2 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne2 =
  ne 2


-- | Determines if a structure contains 3.
h3 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h3 =
  e 3


-- | Negation of 'h3'. Returns the opposite result.
ne3 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne3 =
  ne 3


-- | Determines if a structure contains 4.
h4 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h4 =
  e 4


-- | Negation of 'h4'. Returns the opposite result.
ne4 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne4 =
  ne 4


-- | Determines if a structure contains 5.
h5 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h5 =
  e 5


-- | Negation of 'h5'. Returns the opposite result.
ne5 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne5 =
  ne 5


-- | Determines if a structure contains 6.
h6 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h6 =
  e 6


-- | Negation of 'h6'. Returns the opposite result.
ne6 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne6 =
  ne 6


-- | Determines if a structure contains 7.
h7 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h7 =
  e 7


-- | Negation of 'h7'. Returns the opposite result.
ne7 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne7 =
  ne 7


-- | Determines if a structure contains 8.
h8 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h8 =
  e 8


-- | Negation of 'h8'. Returns the opposite result.
ne8 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne8 =
  ne 8


-- | Determines if a structure contains 9.
h9 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h9 =
  e 9


-- | Negation of 'h9'. Returns the opposite result.
ne9 ::
  ( Ring i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne9 =
  ne 9
