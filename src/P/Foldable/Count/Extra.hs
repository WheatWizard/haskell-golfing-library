{-|
Module :
  P.Foldable.Count.Extra
Description :
  Additional functions for the P.Foldable.Count library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable.Count library.
-}
module P.Foldable.Count.Extra
  where


import P.Algebra.Ring
import P.Foldable.Count


-- | Counts the number of times a structure contains 0.
ce0 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce0 =
  ce 0


-- | Counts the number of elements in a structure not equal to 0.
cE0 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE0 =
  cne 0


-- | Counts the number of times a structure contains 1.
ce1 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce1 =
  ce 1


-- | Counts the number of elements in a structure not equal to 1.
cE1 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE1 =
  cne 1


-- | Counts the number of times a structure contains 2.
ce2 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce2 =
  ce 2


-- | Counts the number of elements in a structure not equal to 2.
cE2 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE2 =
  cne 2


-- | Counts the number of times a structure contains 3.
ce3 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce3 =
  ce 3


-- | Counts the number of elements in a structure not equal to 3.
cE3 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE3 =
  cne 3


-- | Counts the number of times a structure contains 4.
ce4 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce4 =
  ce 4


-- | Counts the number of elements in a structure not equal to 4.
cE4 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE4 =
  cne 4


-- | Counts the number of times a structure contains 6.
ce6 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce6 =
  ce 6


-- | Counts the number of elements in a structure not equal to 6.
cE6 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE6 =
  cne 6


-- | Counts the number of times a structure contains 5.
ce5 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce5 =
  ce 5


-- | Counts the number of elements in a structure not equal to 5.
cE5 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE5 =
  cne 5


-- | Counts the number of times a structure contains 7.
ce7 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce7 =
  ce 7


-- | Counts the number of elements in a structure not equal to 7.
cE7 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE7 =
  cne 7


-- | Counts the number of times a structure contains 8.
ce8 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce8 =
  ce 8


-- | Counts the number of elements in a structure not equal to 8.
cE8 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE8 =
  cne 8


-- | Counts the number of times a structure contains 9.
ce9 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce9 =
  ce 9


-- | Counts the number of elements in a structure not equal to 9.
cE9 ::
  ( Ring i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE9 =
  cne 9
