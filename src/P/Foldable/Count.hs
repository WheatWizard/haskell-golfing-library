{-|
Module :
  P.Foldable.Count
Description :
  Counting elements of a structure
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Foldable.Count
  ( cn
  , fcn
  , ce
  , fce
  , cne
  , fcN
  , cnT
  , cnn
  , ca
  , fca
  , cna
  , fcA
  -- * Tallies
  , tY
  , tyB
  , tBl
  , etY
  -- * Cumulative counts
  , cc
  , fcc
  , ccn
  , fCn
  , ccQ
  , fcQ
  , cnQ
  , fnQ
  , cca
  , fCa
  , ccA
  , fCA
  -- * Deprecated
  , count
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , (+)
  , Integer
  )


import P.Aliases
import P.Bifunctor.Blackbird
import P.Bool
import P.Category
import P.Comonad
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Map
import P.Map.Class
import P.Ord
import P.Scan


{-# Deprecated count "Use cn instead" #-}
-- | Long version of 'cn'.
count ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> t a -> i
count =
  cn


-- | Counts the number of elements of a structure satisfy a predicate.
cn ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> t a -> i
cn sig =
  rF (\ a count -> if sig a then count + 1 else count) 0


-- | Flip of 'cn'.
fcn ::
  ( Foldable t
  , Integral i
  )
    => t a -> Predicate a -> i
fcn =
  F cn


-- | Counts the number of times an element appears in a structure.
ce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => a -> t a -> i
ce =
  cn < (==)


-- | Flip of 'ce'.
fce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => t a -> a -> i
fce =
  F ce


-- | Counts the number of elements in a structure not equal to a certain value.
cne ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => a -> t a -> i
cne =
  cn < nq


-- | Flip of 'cne'.
fcN ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => t a -> a -> i
fcN =
  F cne


-- | Counts the number of 'True' elements in a structure.
cnT ::
  ( Integral i
  , Foldable t
  )
    => t Bool -> i
cnT =
  cn id


-- | Counts the number of 'False' elements in a structure.
cnn ::
  ( Integral i
  , Foldable t
  )
    => t Bool -> i
cnn =
  cn n


-- | Takes two structures and counts the number of elements in the second present in the first.
--
-- Can be used as a substitute for 'ce' when looking for a specific character.
-- e.g. @ca"_"@ is shorter than @ce '_'@.
ca ::
  ( Integral i
  , Foldable f
  , Foldable g
  , Eq a
  )
    => f a -> g a -> i
ca =
  cn < fe


-- | Flip of 'ca'.
fca ::
  ( Integral i
  , Foldable f
  , Foldable g
  , Eq a
  )
    => f a -> g a -> i
fca =
  f' ca


-- | Negation of 'ca'.
-- Counts the number of elements in the second structure not present in the first.
cna ::
  ( Integral i
  , Foldable f
  , Foldable g
  , Eq a
  )
    => f a -> g a -> i
cna =
  cn < n << fe


-- | Flip of 'cna'.
fcA ::
  ( Integral i
  , Foldable f
  , Foldable g
  , Eq a
  )
    => f a -> g a -> i
fcA =
  f' cna


-- | Cumulative counts of elements satisfying a predicate.
--
-- ===== __Examples__
--
--
-- >>> cc (gt 6) [1,3,2,9,9,3,5,6,7,4,3,8,6,1,2,6,4,7,3,4,9]
-- [0,0,0,1,2,2,2,2,3,3,3,4,4,4,4,4,4,5,5,5,6]
cc ::
  ( Scan t
  , Integral i
  )
    => Predicate a -> t a -> t i
cc p =
  zc (+) < m go
  where
    go x
      | p x
      =
        1
      | T
      =
        0


-- | Flip of 'cc'.
fcc ::
  ( Scan t
  , Integral i
  )
    => t a -> Predicate a -> t i
fcc =
  f' cc


-- | Cumulative counts of elements failing a predicate.
ccn ::
  ( Scan t
  , Integral i
  )
    => Predicate a -> t a -> t i
ccn =
  cc < m n


-- | Flip of 'ccn'.
fCn ::
  ( Scan t
  , Integral i
  )
    => t a -> Predicate a -> t i
fCn =
  f' ccn


-- | Cumulative counts of elements equal to a particular value.
ccQ ::
  ( Scan t
  , Eq a
  , Integral i
  )
    => a -> t a -> t i
ccQ =
  cc < eq


-- | Flip of 'ccQ'.
fcQ ::
  ( Scan t
  , Eq a
  , Integral i
  )
    => t a -> a -> t i
fcQ =
  f' ccQ


-- | Cumulative counts of elements not equal to a particular value.
cnQ ::
  ( Scan t
  , Eq a
  , Integral i
  )
    => a -> t a -> t i
cnQ =
  cc < nq


-- | Flip of 'cnQ'.
fnQ ::
  ( Scan t
  , Eq a
  , Integral i
  )
    => t a -> a -> t i
fnQ =
  f' cnQ


-- | Cumulative counts of elements appearing in a list.
--
-- Elements are weighted by how many times they appear in the list.
--
-- ==== __Examples__
--
-- >>> cca "112" "01222112"
-- [0,2,3,4,5,7,9,10]
cca ::
  ( Foldable f
  , Scan t
  , Eq a
  , Integral i
  )
    => f a -> t a -> t i
cca xs =
  zc (+) < m (fce xs)


-- | Flip of 'cca'.
fCa ::
  ( Foldable f
  , Scan t
  , Eq a
  , Integral i
  )
    => t a -> f a -> t i
fCa =
  f' cca


-- | Cumulative counts of elements not appearing in a list.
ccA ::
  ( Foldable f
  , Scan t
  , Eq a
  , Integral i
  )
    => f a -> t a -> t i
ccA xs =
  zc (+) < m (fcN xs)


-- | Flip of 'ccA'.
fCA ::
  ( Foldable f
  , Scan t
  , Eq a
  , Integral i
  )
    => t a -> f a -> t i
fCA =
  f' ccA


-- Could probably be more efficient.
-- | "Tally".
-- Get the number of times each distinct element appears.
tY ::
  ( Eq a
  , Foldable f
  )
    => f a -> List Integer
tY =
  go (MM []) < tL
  where
    go :: Eq a => Blackbird [] (,) a Integer -> List a -> List Integer
    go s [] =
      m cr $ uMM s
    go s (x : xs) =
      case
        s !? x
      of
        [] ->
          go (nz x 1 s) xs
        _ ->
          go (ajt x (+ 1) s) xs


-- | Map over a structure and then tally.
tyB ::
  ( Eq b
  , Foldable f
  , Functor f -- TODO eliminate this
  )
    => (a -> b) -> f a -> List Integer
tyB =
  tY << m


-- | Tally the lengths of the items in a structure.
tBl ::
  ( Foldable f
  , Functor f -- TODO eliminate this
  , Foldable g
  )
    => f (g a) -> List Integer
tBl =
  etY < m Prelude.length


-- | More efficient version of 'tY' using an 'Ord' instance.
etY ::
  ( Ord a
  , Foldable f
  )
    => f a -> List Integer
etY =
  go eSm < tL
  where
    go :: Ord a => StrictMap a Integer -> List a -> List Integer
    go s [] =
      m cr $ asc s
    go s (x : xs) =
      case
        s !? x
      of
        [] ->
          go (nz x 1 s) xs
        _ ->
          go (ajt x (+ 1) s) xs
