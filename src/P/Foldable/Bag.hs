module P.Foldable.Bag
  ( bg
  , bgW
  , bgB
  , rbg
  , rbW
  , rbB
  , bG
  , bGW
  , bGB
  )
  where

import qualified Prelude

import P.Aliases
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Sort.Class


-- TODO make this faster
-- | Internal function used to build bagging.
_add ::
  (
  )
    => (a -> a -> Bool) -> a -> List (List a) -> List (List a)
_add _ x [] =
  [[x]]
_add userEq x (ys@(y : _) : yss)
  | userEq x y
  =
    (x : ys) : yss
_add userEq x (ys : yss) =
  ys : _add userEq x yss


-- | Takes a foldable and produces groups it into a list of equal elements.
--
-- The resulting groups are sorted in descending size.
--
-- ==== __Examples__
--
-- >>> bg [1,2,3,1,2,9,2,1]
-- [[1,1,1],[2,2,2],[9],[3]]
--
bg ::
  ( Eq a
  , Foldable t
  )
    => t a -> List (List a)
bg =
  bgW eq


-- | Like 'bg' but the resulting groups are sorted in ascending size.
rbg ::
  ( Eq a
  , Foldable t
  )
    => t a -> List (List a)
rbg =
  rbW eq


-- | Like 'bg' but the resulting groups are sorted by how early their earliest element appears.
bG ::
  ( Eq a
  , Foldable t
  )
    => t a -> List (List a)
bG =
  bGW eq


-- | Like 'bG' but uses a user defined equality function.
bGW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List (List a)
bGW userEq =
  lF (F $ _add userEq) []


-- | Like 'bg' but uses a user defined equality function.
bgW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List (List a)
bgW =
  sJ << bGW


-- | Like 'bgW' but the resulting groups are sorted in ascending size.
rbW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List (List a)
rbW =
  sj << bGW


-- | Like 'bg' but uses a user defined conversion function to determine equality.
bgB ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> List (List a)
bgB =
  bgW < on eq


-- | Like 'bgB' but the resulting groups are sorted in ascending size.
rbB ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> List (List a)
rbB =
  rbW < on eq


-- | Like 'bG' but uses a user defined conversion function to determine equality.
bGB ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> List (List a)
bGB =
  bGW < on eq
