{-|
Module :
  P.Foldable.Index.Extra
Description :
  Additional indexing functions for the P.Foldable library
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable library.
-}
module P.Foldable.Index.Extra
  where


import P.Aliases
import P.Foldable
import P.List.Index


-- | Index a foldable structure by 0.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx0 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx0 =
  fkx (0 :: Int)


-- | Index a foldable structure by 1.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx1 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx1 =
  fkx (1 :: Int)


-- | Index a foldable structure by 2.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx2 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx2 =
  fkx (2 :: Int)


-- | Index a foldable structure by 3.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx3 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx3 =
  fkx (3 :: Int)


-- | Index a foldable structure by 4.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx4 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx4 =
  fkx (4 :: Int)


-- | Index a foldable structure by 5.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx5 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx5 =
  fkx (5 :: Int)


-- | Index a foldable structure by 6.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx6 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx6 =
  fkx (6 :: Int)


-- | Index a foldable structure by 7.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx7 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx7 =
  fkx (7 :: Int)


-- | Index a foldable structure by 8.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx8 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx8 =
  fkx (8 :: Int)


-- | Index a foldable structure by 9.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx9 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx9 =
  fkx (9 :: Int)


-- | Remove the 1st element of a list.
ri1 ::
  (
  )
    => List a -> List a
ri1 =
  ri (1 :: Int)


-- | Remove the 2nd element of a list.
ri2 ::
  (
  )
    => List a -> List a
ri2 =
  ri (2 :: Int)


-- | Remove the 3rd element of a list.
ri3 ::
  (
  )
    => List a -> List a
ri3 =
  ri (3 :: Int)


-- | Remove the 4th element of a list.
ri4 ::
  (
  )
    => List a -> List a
ri4 =
  ri (4 :: Int)


-- | Remove the 5th element of a list.
ri5 ::
  (
  )
    => List a -> List a
ri5 =
  ri (5 :: Int)


-- | Remove the 6th element of a list.
ri6 ::
  (
  )
    => List a -> List a
ri6 =
  ri (6 :: Int)


-- | Remove the 7th element of a list.
ri7 ::
  (
  )
    => List a -> List a
ri7 =
  ri (7 :: Int)


-- | Remove the 8th element of a list.
ri8 ::
  (
  )
    => List a -> List a
ri8 =
  ri (8 :: Int)


-- | Remove the 9th element of a list.
ri9 ::
  (
  )
    => List a -> List a
ri9 =
  ri (9 :: Int)


-- | Remove the 10th element of a list.
--
-- To remove the 0th element of a list use 'tl'.
ri0 ::
  (
  )
    => List a -> List a
ri0 =
  ri (10 :: Int)

