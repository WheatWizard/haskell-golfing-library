{-|
Module :
  P.Foldable.Length.Extra
Description :
  Additional functions for the P.Foldable.Length library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable.Length library.
-}
module P.Foldable.Length.Extra
  where


import P.Foldable.Length


-- | Determine if the size of a structure is less than 2.
--
-- It is lazy on the structure so it never evaluates more than 2 elements.
lL2 ::
  ( Foldable t
  )
    => t a -> Bool
lL2 =
  lL (2 :: Int)


-- | Determine if the size of a structure is greater than 2.
--
-- It is lazy on the structure so it never evaluates more than 2 elements.
mL2 ::
  ( Foldable t
  )
    => t a -> Bool
mL2 =
  mL (2 :: Int)


-- | Determine if the size of a structure is less than 3.
--
-- It is lazy on the structure so it never evaluates more than 3 elements.
lL3 ::
  ( Foldable t
  )
    => t a -> Bool
lL3 =
  lL (3 :: Int)


-- | Determine if the size of a structure is greater than 3.
--
-- It is lazy on the structure so it never evaluates more than 3 elements.
mL3 ::
  ( Foldable t
  )
    => t a -> Bool
mL3 =
  mL (3 :: Int)


-- | Determine if the size of a structure is less than 4.
--
-- It is lazy on the structure so it never evaluates more than 4 elements.
lL4 ::
  ( Foldable t
  )
    => t a -> Bool
lL4 =
  lL (4 :: Int)


-- | Determine if the size of a structure is greater than 4.
--
-- It is lazy on the structure so it never evaluates more than 4 elements.
mL4 ::
  ( Foldable t
  )
    => t a -> Bool
mL4 =
  mL (4 :: Int)


-- | Determine if the size of a structure is less than 5.
--
-- It is lazy on the structure so it never evaluates more than 5 elements.
lL5 ::
  ( Foldable t
  )
    => t a -> Bool
lL5 =
  lL (5 :: Int)


-- | Determine if the size of a structure is greater than 5.
--
-- It is lazy on the structure so it never evaluates more than 5 elements.
mL5 ::
  ( Foldable t
  )
    => t a -> Bool
mL5 =
  mL (5 :: Int)


-- | Determine if the size of a structure is less than 6.
--
-- It is lazy on the structure so it never evaluates more than 6 elements.
lL6 ::
  ( Foldable t
  )
    => t a -> Bool
lL6 =
  lL (6 :: Int)


-- | Determine if the size of a structure is greater than 6.
--
-- It is lazy on the structure so it never evaluates more than 6 elements.
mL6 ::
  ( Foldable t
  )
    => t a -> Bool
mL6 =
  mL (6 :: Int)


-- | Determine if the size of a structure is less than 7.
--
-- It is lazy on the structure so it never evaluates more than 7 elements.
lL7 ::
  ( Foldable t
  )
    => t a -> Bool
lL7 =
  lL (7 :: Int)


-- | Determine if the size of a structure is greater than 7.
--
-- It is lazy on the structure so it never evaluates more than 7 elements.
mL7 ::
  ( Foldable t
  )
    => t a -> Bool
mL7 =
  mL (7 :: Int)


-- | Determine if the size of a structure is less than 8.
--
-- It is lazy on the structure so it never evaluates more than 8 elements.
lL8 ::
  ( Foldable t
  )
    => t a -> Bool
lL8 =
  lL (8 :: Int)


-- | Determine if the size of a structure is greater than 8.
--
-- It is lazy on the structure so it never evaluates more than 8 elements.
mL8 ::
  ( Foldable t
  )
    => t a -> Bool
mL8 =
  mL (8 :: Int)


-- | Determine if the size of a structure is less than 9.
--
-- It is lazy on the structure so it never evaluates more than 9 elements.
lL9 ::
  ( Foldable t
  )
    => t a -> Bool
lL9 =
  lL (9 :: Int)


-- | Determine if the size of a structure is greater than 9.
--
-- It is lazy on the structure so it never evaluates more than 9 elements.
mL9 ::
  ( Foldable t
  )
    => t a -> Bool
mL9 =
  mL (9 :: Int)


-- | Determine if the size of a structure is less than 10.
--
-- It is lazy on the structure so it never evaluates more than 10 elements.
lL0 ::
  ( Foldable t
  )
    => t a -> Bool
lL0 =
  lL (10 :: Int)


-- | Determine if the size of a structure is greater than 10.
--
-- It is lazy on the structure so it never evaluates more than 10 elements.
--
-- To determine if a structure has size greater than 0 use 'nø'.
mL0 ::
  ( Foldable t
  )
    => t a -> Bool
mL0 =
  mL (10 :: Int)


-- | Determine if the size of a structure is less than 11.
--
-- It is lazy on the structure so it never evaluates more than 11 elements.
lL1 ::
  ( Foldable t
  )
    => t a -> Bool
lL1 =
  lL (11 :: Int)


-- | Determine if the size of a structure is greater than 1.
--
-- It is lazy on the structure so it never evaluates more than 1 elements.
mL1 ::
  ( Foldable t
  )
    => t a -> Bool
mL1 =
  mL (1 :: Int)


-- | Determine if the size of a structure is exactly 1.
--
-- It is lazy on the structure.
eL1 ::
  ( Foldable t
  )
    => t a -> Bool
eL1 =
  eL (1 :: Int)


-- | Determine if the size of a structure is exactly 2.
--
-- It is lazy on the structure.
eL2 ::
  ( Foldable t
  )
    => t a -> Bool
eL2 =
  eL (2 :: Int)


-- | Determine if the size of a structure is exactly 3.
--
-- It is lazy on the structure.
eL3 ::
  ( Foldable t
  )
    => t a -> Bool
eL3 =
  eL (3 :: Int)


-- | Determine if the size of a structure is exactly 4.
--
-- It is lazy on the structure.
eL4 ::
  ( Foldable t
  )
    => t a -> Bool
eL4 =
  eL (4 :: Int)


-- | Determine if the size of a structure is exactly 5.
--
-- It is lazy on the structure.
eL5 ::
  ( Foldable t
  )
    => t a -> Bool
eL5 =
  eL (5 :: Int)


-- | Determine if the size of a structure is exactly 6.
--
-- It is lazy on the structure.
eL6 ::
  ( Foldable t
  )
    => t a -> Bool
eL6 =
  eL (6 :: Int)


-- | Determine if the size of a structure is exactly 7.
--
-- It is lazy on the structure.
eL7 ::
  ( Foldable t
  )
    => t a -> Bool
eL7 =
  eL (7 :: Int)


-- | Determine if the size of a structure is exactly 8.
--
-- It is lazy on the structure.
eL8 ::
  ( Foldable t
  )
    => t a -> Bool
eL8 =
  eL (8 :: Int)


-- | Determine if the size of a structure is exactly 9.
--
-- It is lazy on the structure.
eL9 ::
  ( Foldable t
  )
    => t a -> Bool
eL9 =
  eL (9 :: Int)


-- | Determine if the size of a structure is exactly 10.
--
-- It is lazy on the structure.
eL0 ::
  ( Foldable t
  )
    => t a -> Bool
eL0 =
  eL (10 :: Int)

