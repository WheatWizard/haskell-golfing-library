{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Shelf
Description :
  Shelves
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Shelf
  ( Shelf (..)
  , cj
  )
  where


import P.Applicative
  ( l2
  )
import P.Category
import P.Category.Iso


-- | Shelves are a structure with a self distributive structure.
--
-- __Self-distribution__
--
-- prop> a <! (b <! c) ≡ (a <! b) <! (a <! c)
class
  (
  )
    => Shelf m
  where
    -- | The shelf action.
    --
    -- If @m@ is a 'P.Alegbra.Group.Group' then this is left conjugation.
    (<!) :: m -> m -> m


-- | Prefix version of '(<!)'.
cj ::
  ( Shelf m
  )
    => m -> m -> m
cj =
  (<!)


instance
  (
  )
    => Shelf ()
  where
    _ <! _ =
      ()


instance
  ( Semigroupoid p
  )
    => Shelf (Iso p a a)
  where
    Iso f1 b1 <! Iso f2 b2 =
      Iso (f1 <@ f2 <@ b1) (f1 <@ b2 <@ b1)


instance
  (
  )
    => Shelf Int
  where
    _ <! y =
      y


instance
  (
  )
    => Shelf Integer
  where
    _ <! y =
      y


instance
  ( Shelf b
  )
    => Shelf (a -> b)
  where
    (<!) =
      l2 (<!)


instance
  ( Shelf a
  , Shelf b
  )
    => Shelf (a, b)
  where
    (a1, a2) <! (b1, b2) =
      ( a1 <! b1
      , a2 <! b2
      )


instance
  ( Shelf a
  , Shelf b
  , Shelf c
  )
    => Shelf (a, b, c)
  where
    (a1, a2, a3) <! (b1, b2, b3) =
      ( a1 <! b1
      , a2 <! b2
      , a3 <! b3
      )


instance
  ( Shelf a
  , Shelf b
  , Shelf c
  , Shelf d
  )
    => Shelf (a, b, c, d)
  where
    (a1, a2, a3, a4) <! (b1, b2, b3, b4) =
      ( a1 <! b1
      , a2 <! b2
      , a3 <! b3
      , a4 <! b4
      )
