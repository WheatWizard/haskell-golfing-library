{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Magma
Description :
  Quasigroups
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Loop
  ( Loop (..)
  )
  where


import qualified Prelude
import Prelude
  ( Integer
  , Int
  )


import P.Algebra.Quasigroup
import P.Category
import P.Category.Iso


-- | A loop.
--
-- An instance should satisfy the following.
--
-- prop> lem %%% a ≡ a
-- prop> a %%% lem ≡ a
-- prop> lIv a %%% a ≡ lem
-- prop> a %%% rIv a ≡ lem
class
  ( Quasigroup m
  )
    => Loop m
  where
    -- | Loop identity.
    lem :: m

    -- | Right inverse.
    rIv :: m -> m
    rIv x =
       x ~\~ lem

    -- | Left inverse.
    lIv :: m -> m
    lIv x =
       lem ~/~ lem


instance
  (
  )
    => Loop ()
  where
    lem =
      ()

    rIv _ =
      ()

    lIv _ =
      ()


instance
  ( Category p
  )
    => Loop (Iso p a a)
  where
    lem =
      Iso id id

    rIv (Iso forward backward) =
      Iso backward forward

    lIv =
      rIv


instance
  (
  )
    => Loop Integer
  where
    lem =
      0

    rIv =
      Prelude.negate

    lIv =
      Prelude.negate


instance
  (
  )
    => Loop Int
  where
    lem =
      0

    rIv =
      Prelude.negate

    lIv =
      Prelude.negate


instance
  ( Loop a
  , Loop b
  )
    => Loop (a, b)
  where
    lem =
      ( lem
      , lem
      )

    rIv (x1, x2) =
      ( rIv x1
      , rIv x2
      )

    lIv (x1, x2) =
      ( lIv x1
      , lIv x2
      )


instance
  ( Loop a
  , Loop b
  , Loop c
  )
    => Loop (a, b, c)
  where
    lem =
      ( lem
      , lem
      , lem
      )

    rIv (x1, x2, x3) =
      ( rIv x1
      , rIv x2
      , rIv x3
      )

    lIv (x1, x2, x3) =
      ( lIv x1
      , lIv x2
      , lIv x3
      )


instance
  ( Loop a
  , Loop b
  , Loop c
  , Loop d
  )
    => Loop (a, b, c, d)
  where
    lem =
      ( lem
      , lem
      , lem
      , lem
      )

    rIv (x1, x2, x3, x4) =
      ( rIv x1
      , rIv x2
      , rIv x3
      , rIv x4
      )

    lIv (x1, x2, x3, x4) =
      ( lIv x1
      , lIv x2
      , lIv x3
      , lIv x4
      )


instance
  ( Loop b
  )
    => Loop (a -> b)
  where
    lem _ =
      lem

    rIv f x =
      rIv (f x)

    lIv f x =
      lIv (f x)
