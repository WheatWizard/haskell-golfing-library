module P.Algebra.Semigroup
  ( Semigroup (..)
  , mp
  , fmp
  , mp3
  , fP3
  , cy
  , swg
  , fsg
  , omp
  , fom
  , om3
  , fh3
  -- * Deprecated
  , mappend
  , cycle
  )
  where


import Prelude
  ( Int
  , Integer
  , Semigroup (..)
  )
import qualified Prelude


import qualified Data.Function


import P.Bool
import P.Function
import P.Function.Compose
import P.Function.Flip


{-# Deprecated mappend "Use mp instead" #-}
-- | Long version of 'mp'.
mappend ::
  ( Semigroup a
  )
    => a -> a -> a
mappend =
  mp


-- | Semigroup operation.
--
-- Prefix version of '(<>)'.
-- More general version of 'Prelude.mappend'.
mp ::
  ( Semigroup a
  )
    => a -> a -> a
mp =
  (<>)


-- | Flip of 'mp'.
fmp ::
  ( Semigroup a
  )
    => a -> a -> a
fmp =
  f' mp


-- | Takes three semigroup elements and combines them.
mp3 ::
  ( Semigroup a
  )
    => a -> a -> a -> a
mp3 x y z =
  x <> y <> z


-- | Flip of 'mp3'
fP3 ::
  ( Semigroup a
  )
    => a -> a -> a -> a
fP3 =
  f' mp3


{-# Deprecated cycle "Use cy instead" #-}
-- | Long version of 'cy'.
cycle ::
  ( Semigroup a
  )
    => a -> a
cycle =
  cy


-- | The fixed point of some element of a 'Semigroup'.
--
-- Takes an element of a semigroup @m@
-- and combines an infinite number of @m@s using '(<>)'.
--
-- It is right associative, so:
--
-- @
-- cy m =
--   m <> (m <> (m <> ...))
-- @
--
-- More general version of 'Data.List.cycle'.
--
-- ==== __Examples__
--
-- On list this behaves like 'Data.List.cycle':
--
-- >>> tk 10 $ cy [1,2,3]
-- [1,2,3,1,2,3,1,2,3,1]
--
cy ::
  ( Semigroup m
  )
    => m -> m
cy =
  Data.Function.fix < mp


-- | Sandwich an element of a semigroup between two copies of anohter element.
swg ::
  ( Semigroup m
  )
    => m -> m -> m
swg x y =
  x <> y <> x


-- | Flip of 'swg'.
fsg ::
  ( Semigroup m
  )
    => m -> m -> m
fsg =
  f' swg


-- | Short cut for 'on' 'mp'.
--
-- Takes a functor and for each element @x@ map @mp x@ over the functor.
--
-- For functions this uses a function to convert two elements to semigroup elements and combines them with 'mp'.
--
-- ==== __Examples__
--
-- >>> omp ["hello","world"]
-- [["hellohello","helloworld"],["worldhello","worldworld"]]
-- >>> omp sh [1,2] [3,4]
-- "[1,2][3,4]"
--
omp ::
  ( Functor f
  , Semigroup a
  )
    => f a -> f (f a)
omp =
  on mp


-- | Flip of 'omp'.
fom ::
  ( Semigroup b
  )
    => a -> (a -> b) -> a -> b
fom =
  f' omp


-- | Short cut for 'on3' 'mp3'.
--
-- For functions this uses a function to convert three arguments to semigroup elements and combines them with 'mp'.
--
-- ==== __Examples__
--
-- >>> om3 ["hello","world"]
-- [[["hellohellohello","hellohelloworld"],["helloworldhello","helloworldworld"]],[["worldhellohello","worldhelloworld"],["worldworldhello","worldworldworld"]]]
-- >>> om3 sh [1,2] [3,4] [5,6]
-- "[1,2][3,4][5,6]"
--
om3 ::
  ( Semigroup b
  , Functor f
  )
    => f b -> f (f (f b))
om3 =
  on3 mp3


-- | Flip of 'om3'.
fh3 ::
  ( Semigroup b
  )
    => a -> (a -> b) -> a -> a -> b
fh3 =
  f' om3


instance
  (
  )
    => Semigroup Int
  where
    (<>) =
      (Prelude.+)


instance
  (
  )
    => Semigroup Integer
  where
    (<>) =
      (Prelude.+)


instance
  (
  )
    => Semigroup Bool
  where
    (<>) =
      (Prelude.&&)
