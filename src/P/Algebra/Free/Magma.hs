{-# Language GeneralizedNewtypeDeriving #-}
{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language KindSignatures #-}
{-# Language DeriveFunctor #-}
{-# Language DataKinds #-}
module P.Algebra.Free.Magma
  ( FreeMagma (..)
  , FreeNMagma (..)
  , fMc
  , fMC
  , fMa
  , fMA
  )
  where


import qualified Prelude
import Prelude
  ( showsPrec
  , (++)
  , (<>)
  )


import P.Algebra.Magma
import P.Applicative
import P.Applicative.Unpure
import P.Arithmetic.Nat
import P.Bifunctor.Either
import P.Category
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Monad
import P.Monad.Free
import P.Ord
import P.Show
import P.Vector


-- | \(n\)-ary free magmata.
-- Serves as the underlying type for the ordinary free magmata.
newtype FreeNMagma n a
  = FMg
    { uFM ::
      FreeM (Vec n) a
    }
  deriving
    ( Eq
    , Eq1
    , Functor
    , Applicative
    , Monad
    , Unpure
    )


-- | Free magmata.
-- Alternatively ordered fully binary trees.
-- Also the free monad of the 2-vector.
type FreeMagma
  = FreeNMagma (Nx (Nx Zr))


-- | Catamorphism of the free magma.
-- Also the Böhm-Berarducci encoding of the 'FreeMagma' type.
fMc ::
  (
  )
    => (a -> c) -> (c -> c -> c) -> FreeMagma a -> c
fMc f1 f2 (FMg (Fe (V2 x y))) =
  f2 (fMc f1 f2 (FMg x)) (fMc f1 f2 (FMg y))
fMc f1 f2 (FMg (Pur x)) =
  f1 x


-- | Flip of 'fMc'.
fMC ::
  (
  )
    => (c -> c -> c) -> (a -> c) -> FreeMagma a -> c
fMC =
  f' fMc


-- | Anamorphism of the free magma.
fMa ::
  (
  )
    => (a -> Either b (Vec n a)) -> a -> FreeNMagma n b
fMa func =
  FMg < go
  where
    go a =
      case
        func a
      of
        Lef x ->
          Pur x
        Rit ys ->
          Fe (go < ys)


-- | Flip of 'fMa'.
fMA ::
  (
  )
    => a -> (a -> Either b (Vec n a)) -> FreeNMagma n b
fMA =
  f' fMa


instance
  (
  )
    => Magma (FreeMagma a)
  where
    FMg x %%% FMg y =
      FMg (Fe (V2 x y))


instance
  ( Show a
  )
    => Show (FreeMagma a)
  where
    showsPrec n (FMg (Pur x)) rest
      | n >= 10
      =
        "(Pu " ++ showsPrec 11 x (')' : rest)
      | Prelude.otherwise
      =
        "Pu " ++ showsPrec 11 x rest
    showsPrec n (FMg (Fe (V2 x y))) rest
      | n >= 6
      =
        '(' : showsPrec 6 (FMg x) (" %%% " ++ showsPrec 6 (FMg y) (')' : rest))
      | Prelude.otherwise
      =
        showsPrec 6 (FMg x) $ " %%% " ++ showsPrec 6 (FMg y) rest


instance
  (
  )
    => Foldable FreeMagma
  where
    foldMap f (FMg (Pur x)) =
      f x
    foldMap f (FMg (Fe (V2 x y))) =
      mF f (FMg x) <> mF f (FMg y)
