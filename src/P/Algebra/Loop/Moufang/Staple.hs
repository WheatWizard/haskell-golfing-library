{-# Language GeneralizedNewtypeDeriving #-}
{-# Language PatternSynonyms #-}
{-|
Module :
  P.Algebra.Loop.Moufang.Staple
Description :
  Moufang loops by adding an element to a group
Copyright :
  (c) E. Olive, 2025
License :
 GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Loop.Moufang.Staple
  ( Staple (..)
  , pattern M2L
  , pattern M2R
  )
  where


import qualified Prelude
import Prelude
  ( showsPrec
  )


import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Quasigroup
import P.Algebra.Semigroup
import P.Bifunctor.Either
import P.Bool
import P.Eq
import P.Functor
import P.Ord
import P.Show
import P.Type.Combinator.W


-- | A functor from groups to Moufang loops.
--
-- It can be defined as a tagged union with constructors \(L : G \rightarrow \mathrm{Staple}\,G\) and \(R : G \rightarrow \mathrm{Staple}\,G\) with the product defined as follows:
--
-- \[
-- L(g)L(h) = L(gh) \\
-- L(g)R(h) = R(hg) \\
-- R(g)L(h) = R(g/h) \\
-- R(g)R(h) = L(h\backslash g) \\
-- \]
--
-- This is typically called \(M(G,2)\) in the literature, but it is called @Staple@ here for want of a better name.
newtype Staple g
  = M2 (W Either g)
  deriving
    ( Functor
    , Eq
    , Ord
    , Prelude.Ord
    )


pattern M2L ::
  (
  )
    => g -> Staple g
pattern M2L x =
  M2 (W (Lef x))


pattern M2R ::
  (
  )
    => g -> Staple g
pattern M2R x =
  M2 (W (Rit x))


instance
  ( Quasigroup g
  )
    => Magma (Staple g)
  where
    M2L g %%% M2L h =
      M2L (g %%% h)
    M2L g %%% M2R h =
      M2R (h %%% g)
    M2R g %%% M2L h =
      M2R (g ~/~ h)
    M2R g %%% M2R h =
      M2L (h ~\~ g)


-- Proof that the below implementation is a quasigroup:
--
-- (xu / yu) * yu = xu
-- (y \ x) * yu = (y * (y \ x))u = xu
--
-- (x / yu) * yu = x
-- (y * x)u * yu = y \ (y * x) = x
--
-- (xu / y) * y = xu
-- (x * y)u * y = ((x * y) / y)u = xu
--
-- xu * (xu \ yu) = yu
-- xu * (y \ x) = (x / (y \ x))u = yu
--
-- x * (x \ yu) = yu
-- x * (y / x)u = ((y / x) * x)u = yu
--
-- xu * (xu \ y) = y
-- xu * (x / y)u = ((x / y) \ x) = y


instance
  ( Quasigroup g
  )
    => Quasigroup (Staple g)
  where
    M2L g ~/~ M2L h =
      M2L (g ~/~ h)
    M2L g ~/~ M2R h =
      M2R (h %%% g)
    M2R g ~/~ M2L h =
      M2R (g %%% h)
    M2R g ~/~ M2R h =
      M2L (h ~\~ g)

    M2L g ~\~ M2L h =
      M2L (g ~\~ h)
    M2L g ~\~ M2R h =
      M2R (h ~/~ g)
    M2R g ~\~ M2L h =
      M2R (g ~/~ h)
    M2R g ~\~ M2R h =
      M2L (h ~\~ g)


instance
  ( Loop g
  )
    => Loop (Staple g)
  where
    lem =
      M2L lem


instance
  ( Group g
  )
    => Moufang (Staple g)
  where
    _iv (M2L g) =
      M2L (IV g)
    _iv (M2R g) =
      M2R g


instance
  (
  )
    => Eq1 Staple
  where
    q1 f (M2L x) (M2L y) =
      f x y
    q1 f (M2R x) (M2R y) =
      f x y
    q1 _ _ _ =
      B


instance
  ( Show a
  )
    => Show (Staple a)
  where
    showsPrec n (M2L x) trail
      | 11 > n
      =
        "M2L " <> showsPrec 10 x trail
      | Prelude.otherwise
      =
        "(M2L " <> showsPrec 10 x (')' : trail)
    showsPrec n (M2R x) trail
      | 11 > n
      =
        "M2R " <> showsPrec 10 x trail
      | Prelude.otherwise
      =
        "(M2R " <> showsPrec 10 x (')' : trail)

