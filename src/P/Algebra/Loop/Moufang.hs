{-# Language FlexibleInstances #-}
{-# Language PatternSynonyms #-}
{-# Language TypeOperators #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Algebra.Loop.Moufang
Description :
  Moufang loops
Copyright :
  (c) E. Olive, 2025
License :
 GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

The group class and related functions.
-}
module P.Algebra.Loop.Moufang
  ( Moufang (..)
  , pattern IV
  , iiv
  )
  where


import qualified Prelude
import Prelude
  ( Int
  , Integer
  , fmap
  )


import P.Algebra.Loop
import P.Category
import P.Category.Iso


-- | The class of Moufang loops.
--
-- prop> x %%% (y %%% (x %%% z)) ≡ ((x %%% y) %%% x) %%% z
-- prop> x %%% (y %%% (z %%% y)) ≡ ((x %%% y) %%% z) %%% y
-- prop> (x %%% y) %%% (z %%% x) ≡ (x %%% (y %%% z)) %%% x
-- prop> (x %%% y) %%% (z %%% x) ≡ x %%% ((y %%% z) %%% x)
class
  ( Loop m
  )
    => Moufang m
  where
    -- | Double sided inverse.
    _iv :: m -> m
    _iv =
      lIv


-- | Inverse as a pattern.
pattern IV ::
  ( Moufang m
  )
    => m -> m
pattern IV x <- (_iv -> x) where
  IV =
    _iv


-- | Group inversion as an isomorphism.
iiv ::
  ( Moufang m
  )
    => m <-> m
iiv =
  Iso _iv _iv



instance
  (
  )
    => Moufang Int


instance
  (
  )
    => Moufang Integer


instance
  ( Moufang b
  )
    => Moufang (a -> b)
  where
    _iv =
      fmap _iv


instance
  ( Moufang a
  , Moufang b
  )
    => Moufang (a, b)
  where
    _iv (x, y) =
      ( IV x
      , IV y
      )


instance
  ( Moufang a
  , Moufang b
  , Moufang c
  )
    => Moufang (a, b, c)
  where
    _iv (x, y, z) =
      ( IV x
      , IV y
      , IV z
      )


instance
  ( Moufang a
  , Moufang b
  , Moufang c
  , Moufang d
  )
    => Moufang (a, b, c, d)
  where
    _iv (w, x, y, z) =
      ( IV w
      , IV x
      , IV y
      , IV z
      )


instance
  ( Category p
  )
    => Moufang (Iso p a a)
