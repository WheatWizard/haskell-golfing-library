{-|
Module :
  P.Algebra.Semigroup.Reversed
Description :
  Flips the semigroup action
Copyright :
  (c) E. Olive, 2025
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Semigroup.Reversed
  ( Reversed (..)
  )
  where


import qualified Prelude


import P.Algebra.Group
import P.Algebra.Kei
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Shelf
import P.Applicative
import P.Bifunctor
import P.Comonad
import P.Eq
import P.Function
import P.Function.Compose
import P.Monad
import P.Ord
import P.Swap


-- | Takes a semigroup and produces a semigroup with the flipped operation.
--
-- If @a@ is a semigroup with operation @sgAct@ then @Reversed a@ is a semigroup with operation @flip sgAct@.
--
-- Also flips the 'Magma' operation for compatibility.
newtype Reversed a =
  Rs
    { uRs ::
      a
    }


instance
  ( Magma a
  )
    => Magma (Reversed a)
  where
    Rs x %%% Rs y =
      Rs (y %%% x)


instance
  ( Semigroup a
  )
    => Semigroup (Reversed a)
  where
    Rs x <> Rs y =
      Rs (y <> x)


instance
  ( Monoid a
  )
    => Monoid (Reversed a)
  where
    mempty =
      Rs i


instance
  ( FreeMonoid a
  )
    => FreeMonoid (Reversed a)
  where
    bkO =
      Sw << Rs &<< bKO < uRs

    bKO =
      Sw << Rs &<< bkO < uRs

    rvg =
      Rs < rvg < uRs


instance
  ( Quasigroup a
  )
    => Quasigroup (Reversed a)
  where
    Rs x ~/~ Rs y =
      Rs (y ~/~ x)
    Rs x ~\~ Rs y =
      Rs (y ~\~ x)


instance
  ( Loop a
  )
    => Loop (Reversed a)
  where
    lem =
      Rs lem


instance
  ( Moufang a
  )
    => Moufang (Reversed a)
  where
    _iv =
      Rs < _iv < uRs


instance
  ( Rack a
  )
    => Shelf (Reversed a)
  where
    Rs x <! Rs y =
      Rs (y !> x)


instance
  ( Rack a
  )
    => Rack (Reversed a)
  where
    Rs x !> Rs y =
      Rs (y <! x)


instance
  ( Quandle a
  )
    => Quandle (Reversed a)


instance
  ( Group a
  )
    => Group (Reversed a)


instance
  ( Kei a
  )
    => Kei (Reversed a)


instance
  (
  )
    => Functor Reversed
  where
    fmap f (Rs a) =
      Rs (f a)


instance
  (
  )
    => Applicative Reversed
  where
    pure =
      Rs

    Rs x <*> Rs y =
      Rs (x y)


instance
  (
  )
    => Monad Reversed
  where
    Rs x >>= y =
      y x


instance
  (
  )
    => Comonad Reversed
  where
    cr =
      uRs

    dyp =
      Rs


instance
  (
  )
    => Eq1 Reversed
  where
    q1 func (Rs x) =
      func x < uRs


instance
  (
  )
    => Ord1 Reversed
  where
    lcp func (Rs x) =
      func x < uRs


instance
  ( Eq a
  )
    => Eq (Reversed a)
  where
    (==) =
      qb uRs


instance
  ( Ord a
  )
    => Ord (Reversed a)
  where
    cp =
      on cp uRs


instance
  ( Commutative a
  )
    => Commutative (Reversed a)
