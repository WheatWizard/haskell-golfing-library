{-# Language PatternSynonyms #-}
{-# Language FlexibleInstances #-}
{-# Language TypeOperators #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Algebra.Group
Description :
  The group class and related functions
Copyright :
  (c) E. Olive, 2024
License :
 GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

The group class and related functions.
-}
module P.Algebra.Group
  ( Group (..)
  , (~~)
  , iiv
  )
  where


import Prelude
  ( Int
  , Integer
  , fmap
  )


import P.Algebra.Monoid
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Category
import P.Category.Iso


-- | The class of groups.
--
-- That is types whose 'Monoid' instance has a inverse satisfying the following:
--
-- prop> IV x <> x ≡ i
-- prop> IV (x <> y) ≡ IV x <> IV y
-- prop> IV (Iv x) ≡ x
--
-- It is also required that the 'Monoid' and 'Loop' instances are compatible:
--
-- prop> (<>) ≡ (%%%)
--
-- It is also required that the 'Quandle' instance is compatible:
--
-- prop> a <! b ≡ a <> b <> IV a
-- prop> a !> b ≡ IV b <> a <> b
class
  ( Monoid m
  , Moufang m
  , Quandle m
  )
    => Group m


-- | Subtraction on groups.
(~~) ::
  ( Group m
  )
    => m -> m -> m
x ~~ y =
  x <> IV y


instance
  (
  )
    => Group Int


instance
  (
  )
    => Group Integer


instance
  ( Group b
  )
    => Group (a -> b)


instance
  ( Group a
  , Group b
  )
    => Group (a, b)


instance
  ( Group a
  , Group b
  , Group c
  )
    => Group (a, b, c)


instance
  ( Group a
  , Group b
  , Group c
  , Group d
  )
    => Group (a, b, c, d)


instance
  ( Category p
  )
    => Group (Iso p a a)
