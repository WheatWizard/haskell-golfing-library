{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language ConstraintKinds #-}
module P.Algebra.Ring
  ( Ring
  , (+)
  , pl
  , (-)
  , sb
  , fsb
  , (*)
  , ml
  , db
  , tp
  , (^)
  , pw
  , fpw
  , sq
  , pattern Ng
  -- * Deprecated
  , negate
  , subtract
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Function.Flip


infixl 6  +, -
infixl 7  *


-- | This is an alias for prelude's 'Prelude.Num' class.
--
-- The 'Prelude.abs' and 'Prelude.signum' functions are hidden so that 'Num' can act like a true ring class.
-- Thus this class should only be used with the assumption it is a ring.
--
-- For abs and signum see the superclass 'P.Arithmetic.Number'.
type Ring =
  Prelude.Num


-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
(+) ::
  ( Ring a
  )
    => a -> a -> a
(+) =
  (Prelude.+)


-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
-- Prefix version of '(+)'
pl ::
  ( Ring a
  )
    => a -> a -> a
pl =
  (+)


-- | Subtraction.
--
-- Equivalent to '(Prelude.-)'.
(-) ::
  ( Ring a
  )
    => a -> a -> a
(-) =
  (Prelude.-)


{-# Deprecated subtract "Use sb instead" #-}
-- | Long version of 'sb'.
subtract ::
  ( Ring a
  )
    => a -> a -> a
subtract =
  sb


-- | Subtraction.
--
-- Equivalent to 'Prelude.subtract'.
sb ::
  ( Ring a
  )
    => a -> a -> a
sb =
  Prelude.subtract


-- | Flip of 'sb'.
fsb ::
  ( Ring a
  )
    => a -> a -> a
fsb =
  f' sb


-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
(*) ::
  ( Ring a
  )
    => a -> a -> a
(*) =
  (Prelude.*)


-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
ml ::
  ( Ring a
  )
    => a -> a -> a
ml =
  (*)


-- | Double or multiply by 2.
db ::
  ( Ring a
  )
    => a -> a
db =
  ml 2


-- | Triple or multiply by 3.
tp ::
  ( Ring a
  )
    => a -> a
tp =
  ml 3


-- | Squares a number.
sq ::
  ( Ring a
  )
    => a -> a
sq x =
  x * x


-- | Raise to the power.
--
-- Equivalent to '(Prelude.^)'.
(^) ::
  ( Integral b
  , Ring a
  )
    => a -> b -> a
(^) =
  (Prelude.^)


-- | Raise to the power.
--
-- Prefix version of '(^)'.
pw ::
  ( Integral b
  , Ring a
  )
    => a -> b -> a
pw =
  (^)


-- | Flip of 'pw'.
fpw ::
  ( Integral b
  , Ring a
  )
    => b -> a -> a
fpw =
  f' pw


{-# Deprecated negate "Use Ng instead" #-}
-- | Long version of 'Ng'.
negate ::
  ( Ring a
  )
    => a -> a
negate =
  Ng


-- | Negate a number.
pattern Ng ::
  ( Ring a
  )
    => a -> a
pattern Ng x <- (Prelude.negate -> x) where
  Ng =
    Prelude.negate

