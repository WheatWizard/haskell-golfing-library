{-# Language GeneralizedNewtypeDeriving #-}
{-# Language DeriveFunctor #-}
{-|
Module :
  P.Algebra.Rack.Core
Description :
  The core kei
Copyright :
  (c) E. Olive, 2025
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Kei.Core
  ( Core (..)
  , lCr
  , (!@>)
  )
  where


import qualified Prelude


import P.Algebra.Group
import P.Algebra.Kei
import P.Algebra.Loop.Moufang
import P.Algebra.Quandle
import P.Algebra.Rack
import P.Algebra.Semigroup
import P.Algebra.Shelf
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Functor
import P.Functor.Identity.Class
import P.Eq
import P.Monad
import P.Ord


{-# Deprecated _unCore "Use cr instead" #-}
-- | Given a group its core kei is the rack given by the operation
-- \[
-- a \lhd b = ab^{-1}a
-- \]
--
-- This operation satisfies the laws to form a 'Kei' (and thus also a 'Rack' and 'Quandle').
newtype Core a
  = Cor
    { _unCore :: a
    }
  deriving
    ( Eq
    , Ord
    , Prelude.Ord
    , Functor
    )


-- | Type restricted form of 'Ul2'
--
-- Takes a binary function on 'Core's and lifts it by the dual of 'Core'.
lCr ::
  (
  )
    => (Core a -> Core b -> Core c) -> (a -> b -> c)
lCr =
  Ul2


-- | '(!>)' lifted by the dual of the 'Core' functor.
--
-- That way it applies the core kei operation on any group.
--
-- @@
-- x !@> y =
--   y <> IV x <> y
-- @@
(!@>) ::
  ( Group a
  )
    => a -> a -> a
(!@>) =
  lCr (!>)


instance
  ( Group a
  )
    => Shelf (Core a)
  where
    Cor h <! Cor g =
      Cor (h <> IV g <> h)


instance
  ( Group a
  )
    => Rack (Core a)
  where
    (!>) =
      Prelude.flip (<!)


instance
  ( Group a
  )
    => Quandle (Core a)


instance
  ( Group a
  )
    => Kei (Core a)


instance
  (
  )
    => Applicative Core
  where
    pure =
      Cor
    Cor f <*> Cor x =
      Cor (f x)


instance
  (
  )
    => Monad Core
  where
    return =
      Cor
    Cor x >>= f =
      f x


instance
  (
  )
    => Unpure Core
  where
    unp (Cor a) =
      []
    pure' =
      Cor


instance
  (
  )
    => Comonad Core
  where
    cr =
      _unCore

    dyp =
      Cor


instance
  (
  )
    => Identity Core


instance
  (
  )
    => Eq1 Core
  where
    q1 f (Cor x) (Cor y) =
      f x y


instance
  (
  )
    => Ord1 Core
  where
    lcp f (Cor x) (Cor y) =
      f x y
