{-|
Module :
  P.Algebra.Linear
Description :
  Linear algebra functions for the P library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Linear
  ( dt
  , (·)
  )
  where


import P.Algebra.Ring
import P.Foldable
import P.Function.Compose
import P.Zip.SemiAlign


-- | The dot product.
dt ::
  ( Foldable t
  , SemiAlign t
  , Ring a
  )
    => t a -> t a -> a
dt =
  sm << zd ml 0


-- | Infix of 'dt'.
(·) ::
  ( Foldable t
  , SemiAlign t
  , Ring a
  )
    => t a -> t a -> a
(·) =
  dt
