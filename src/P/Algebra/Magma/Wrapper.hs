{-# Language GeneralizedNewtypeDeriving #-}
{-# Language DeriveFunctor #-}
{-|
Module :
  P.Algebra.Magma.Wrapper
Description :
  The WrappedSemigroup class
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Magma.Wrapper
  ( WrappedSemigroup (..)
  )
  where


import qualified Prelude
import Prelude
  ( Functor
  , Applicative
  , Monad
  )


import P.Algebra.Magma
import P.Algebra.Semigroup
import P.Applicative.Unpure
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Comonad
import P.Eq
import P.Functor.Identity.Class
import P.Ord
import P.Show


-- | Gives a 'Magma' instance where there is a 'Semigroup'.
newtype WrappedSemigroup a
  = WSa a
  deriving
    ( Eq
    , Show
    , Ord
    , Semigroup
    , Monoid
    , FreeMonoid
    , Functor
    )


instance
  (
  )
    => Applicative WrappedSemigroup
  where
    pure =
      WSa

    WSa f <*> WSa x =
      WSa (f x)


instance
  (
  )
    => Monad WrappedSemigroup
  where
    WSa x >>= f =
      f x


instance
  (
  )
    => Comonad WrappedSemigroup
  where
    cr (WSa x) =
      x
    dyp =
      WSa


instance
  (
  )
    => Unpure WrappedSemigroup
  where
    pure' =
      WSa
    unp (WSa x) =
      [x]


instance
  (
  )
    => Identity WrappedSemigroup


instance
  (
  )
    => Eq1 WrappedSemigroup
  where
    q1 userEq (WSa x) (WSa y) =
      userEq x y


instance
  (
  )
    => Ord1 WrappedSemigroup
  where
    lcp userCp (WSa x) (WSa y) =
      userCp x y


instance
  ( Semigroup a
  )
    => Magma (WrappedSemigroup a)
  where
    WSa x %%% WSa y =
      WSa (x <> y)
