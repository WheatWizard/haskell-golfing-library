{-# Language FlexibleInstances #-}
{-# Language TypeOperators #-}
{-|
Module :
  P.Algebra.Magma
Description :
  Quasigroups
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Quasigroup
  ( Quasigroup (..)
  )
  where


import P.Algebra.Magma
import P.Category
import P.Category.Iso


-- | A quasigroup.
--
-- Instances should satisfy the following properties:
--
-- prop> (a ~/~ b) %%% b ≡ a
-- prop> (a %%% b) ~/~ b ≡ a
-- prop> a %%% (a ~\~ b) ≡ b
-- prop> a ~\~ (a %%% b) ≡ a
class
  ( Magma m
  )
    => Quasigroup m
  where
    {-# Minimal ((~\~) | lpi), ((~/~) | rpi)#-}
    -- | Right cancellation.
    (~\~) :: m -> m -> m
    x ~\~ y =
      bwd (lpi x) y

    -- | Left cancellation.
    (~/~) :: m -> m -> m
    x ~/~ y =
      bwd (rpi y) x

    -- | '(%%%)' as an isomorphism.
    lpi :: m -> (m <-> m)
    lpi x =
      Iso
        { fwd =
          (x %%%)
        , bwd =
          (x ~\~)
        }

    -- | Flip of '(%%%)' as an isomorphism.
    rpi :: m -> (m <-> m)
    rpi x =
      Iso
        { fwd =
          (%%% x)
        , bwd =
          (~/~ x)
        }


instance
  (
  )
    => Quasigroup ()
  where
    _ ~\~ _ =
      ()

    _ ~/~ _ =
      ()


instance
  ( Semigroupoid p
  )
    => Quasigroup (Iso p a a)
  where
    Iso f1 b1 ~\~ Iso f2 b2 =
      Iso (f1 <@ b2) (b1 <@ f2)

    Iso f1 b1 ~/~ Iso f2 b2 =
      Iso (b1 <@ f2) (f1 <@ b2)


instance
  ( Quasigroup a
  , Quasigroup b
  )
    => Quasigroup (a, b)
  where
    (x1, y1) ~\~ (x2, y2) =
      ( x1 ~\~ x2
      , y1 ~\~ y2
      )
    (x1, y1) ~/~ (x2, y2) =
      ( x1 ~/~ x2
      , y1 ~/~ y2
      )


instance
  ( Quasigroup a
  , Quasigroup b
  , Quasigroup c
  )
    => Quasigroup (a, b, c)
  where
    (x1, y1, z1) ~\~ (x2, y2, z2) =
      ( x1 ~\~ x2
      , y1 ~\~ y2
      , z1 ~\~ z2
      )
    (x1, y1, z1) ~/~ (x2, y2, z2) =
      ( x1 ~/~ x2
      , y1 ~/~ y2
      , z1 ~/~ z2
      )


instance
  ( Quasigroup a
  , Quasigroup b
  , Quasigroup c
  , Quasigroup d
  )
    => Quasigroup (a, b, c, d)
  where
    (w1, x1, y1, z1) ~\~ (w2, x2, y2, z2) =
      ( w1 ~\~ w2
      , x1 ~\~ x2
      , y1 ~\~ y2
      , z1 ~\~ z2
      )
    (w1, x1, y1, z1) ~/~ (w2, x2, y2, z2) =
      ( w1 ~/~ w2
      , x1 ~/~ x2
      , y1 ~/~ y2
      , z1 ~/~ z2
      )


instance
  (
  )
    => Quasigroup Int
  where
    x ~\~ y =
      x - y
    x ~/~ y =
      y - x


instance
  (
  )
    => Quasigroup Integer
  where
    x ~\~ y =
      x - y
    x ~/~ y =
      y - x


instance
  ( Quasigroup b
  )
    => Quasigroup (a -> b)
  where
    (f1 ~\~ f2) x =
      f1 x ~\~ f2 x
    (f1 ~/~ f2) x =
      f1 x ~/~ f2 x
