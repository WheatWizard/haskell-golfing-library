{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Quandle
Description :
  Quandles
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Quandle
  ( Quandle (..)
  )
  where


import P.Algebra.Rack
import P.Category
import P.Category.Iso


-- | Idempotent racks.
--
-- Instances obey the following laws:
--
-- __Idempotency__
--
-- prop> a ◁ a ≡ a
-- prop> a ▷ a ≡ a
class
  ( Rack m
  )
    => Quandle m


instance
  (
  )
    => Quandle ()


instance
  ( Semigroupoid p
  )
    => Quandle (Iso p a a)


instance
  ( Quandle b
  )
    => Quandle (a -> b)


instance
  (
  )
    => Quandle Int


instance
  (
  )
    => Quandle Integer


instance
  ( Quandle a
  , Quandle b
  )
    => Quandle (a, b)


instance
  ( Quandle a
  , Quandle b
  , Quandle c
  )
    => Quandle (a, b, c)


instance
  ( Quandle a
  , Quandle b
  , Quandle c
  , Quandle d
  )
    => Quandle (a, b, c, d)
