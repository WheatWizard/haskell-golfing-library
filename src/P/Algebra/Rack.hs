{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Rack
Description :
  racks
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Rack
  ( Rack (..)
  , jc
  )
  where


import P.Algebra.Shelf
import P.Applicative
  ( l2
  )
import P.Category
import P.Category.Iso


-- | Types that form a rack.
--
-- __Self-distribution__
--
-- prop> a <! (b <! c) ≡ (a <! b) <! (a <! c)
-- prop> (a !> b) !> c ≡ (a !> c) !> (b !> c)
--
-- __Cancellation__
--
-- prop> a <! (b !> a) ≡ b
-- prop> (a <! b) !> a ≡ b
class
  ( Shelf m
  )
    => Rack m
  where
    -- | The right shelf action.
    --
    -- If @m@ is a 'P.Alegbra.Group.Group' then this is right conjugation.
    (!>) :: m -> m -> m


-- | Prefix version of '(!>)'.
jc ::
  ( Rack m
  )
    => m -> m -> m
jc =
  (!>)


instance
  (
  )
    => Rack ()
  where
    _ !> _ =
      ()


instance
  ( Semigroupoid p
  )
    => Rack (Iso p a a)
  where
    Iso f1 b1 !> Iso f2 b2 =
      Iso (b2 <@ f1 <@ f2) (b2 <@ b1 <@ f2)


instance
  (
  )
    => Rack Int
  where
    x !> _ =
      x


instance
  (
  )
    => Rack Integer
  where
    x !> _ =
      x


instance
  ( Rack b
  )
    => Rack (a -> b)
  where
    (!>) =
      l2 (<!)


instance
  ( Rack a
  , Rack b
  )
    => Rack (a, b)
  where
    (a1, a2) !> (b1, b2) =
      ( a1 !> b1
      , a2 !> b2
      )


instance
  ( Rack a
  , Rack b
  , Rack c
  )
    => Rack (a, b, c)
  where
    (a1, a2, a3) !> (b1, b2, b3) =
      ( a1 !> b1
      , a2 !> b2
      , a3 !> b3
      )


instance
  ( Rack a
  , Rack b
  , Rack c
  , Rack d
  )
    => Rack (a, b, c, d)
  where
    (a1, a2, a3, a4) !> (b1, b2, b3, b4) =
      ( a1 !> b1
      , a2 !> b2
      , a3 !> b3
      , a4 !> b4
      )
