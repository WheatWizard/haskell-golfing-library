{-# Language MultiParamTypeClasses #-}
{-# Language FunctionalDependencies #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
{-|
Module :
  P.Algebra.Monoid.Action
Description :
  Monoid actions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Monoid.Action
  ( Action (..)
  , ($$)
  , faq
  , aqE
  )
  where


import qualified Prelude


import P.Algebra.Group
import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Aliases
import P.Category
import P.Category.Endo
import P.Category.Iso
import P.Function.Flip


-- | Associate a monoid with an action on a fixed type.
--
-- Should follow the ordinary monoid action laws:
--
-- prop> aq (f <> g)  x = aq f (aq g x)
--
-- prop> aq mempty x = x
class
  ( Monoid m
  )
    => Action m a
    | m -> a
  where
    -- | Homomorphism from the monoid @m@ into @a -> a@.
    aq :: m -> (a -> a)


infixr 1 $$
-- | Infix version of 'aq'.
($$) ::
  ( Action m a
  )
    => m -> (a -> a)
($$) =
  aq


-- | Flip of 'aq'.
faq ::
  ( Action m a
  )
    => a -> m -> a
faq =
  f' aq


-- | Like 'aq' but outputting an endomorphism.
aqE ::
  ( Action m a
  )
    => m -> Endo (->) a
aqE x =
  E $ aq x


instance
  ( Action a x
  , Action b y
  )
    => Action (a, b) (x, y)
  where
    aq (f, g) (x, y) =
      ( f $$ x
      , g $$ y
      )


instance
  ( Action a x
  , Action b y
  , Action c z
  )
    => Action (a, b, c) (x, y, z)
  where
    aq (f, g, h) (x, y, z) =
      ( f $$ x
      , g $$ y
      , h $$ z
      )


instance
  ( Action m x
  )
    => Action (List m) x
  where
    aq =
      f' $ Prelude.foldr aq


instance
  (
  )
    => Action (Endo (->) a) a
  where
    aq (E f) =
      f

instance
  (
  )
    => Action (Iso (->) a a) a
  where
    aq x =
      fwd x
