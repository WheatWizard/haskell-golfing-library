{-|
Module :
  P.Algebra.Monoid.Aligned
Description :
  Monoids from applicatives
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

We can make a semigroup out of 'P.Zip.SemiAlign.SemiAlign' instances.
-}
module P.Algebra.Monoid.Aligned
  ( Aligned (..)
  )
  where


import qualified Prelude


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Eq
import P.Function
import P.Function.Compose
import P.Functor
import P.Functor.Identity.Class
import P.Monad
import P.Ord
import P.Show
import P.Zip
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A wrapper which replaces the semigroup instance with one based on the 'P.Zip.SemiAlign.SemiAlign' instance of @f@.
-- '(<>)' on @Aligned@s is equivalent to @zd' (<>)@.
newtype Aligned f a =
  Agn
    { uAg :: f a
    }


instance
  ( Show (f a)
  )
    => Show (Aligned f a)
  where
    show =
      sh < uAg


instance
  ( Functor f
  )
    => Functor (Aligned f)
  where
    fmap func =
      Agn < m func < uAg


instance
  ( Applicative f
  )
    => Applicative (Aligned f)
  where
    pure =
      Agn < p

    (<*>) (Agn func) =
      Agn < (*^) func < uAg


instance
  ( Monad f
  )
    => Monad (Aligned f)
  where
    x >>= y =
      Agn (uAg x >~ (uAg < y))


instance
  ( Comonad f
  )
    => Comonad (Aligned f)
  where
    cr =
      cr < uAg

    dyp =
      Agn < m Agn < dyp < uAg


instance
  ( Unpure f
  )
    => Unpure (Aligned f)
  where
    unp =
      unp < uAg

    pure' =
      Agn < Pu


instance
  ( Identity f
  )
    => Identity (Aligned f)


instance
  ( Eq (f a)
  )
    => Eq (Aligned f a)
  where
    (==) =
      qb uAg


instance
  ( Ord (f a)
  )
    => Ord (Aligned f a)
  where
    cp =
      on cp uAg


instance
  ( Eq1 f
  )
    => Eq1 (Aligned f)
  where
    q1 func (Agn x) =
      q1 func x < uAg


instance
  ( Ord1 f
  )
    => Ord1 (Aligned f)
  where
    lcp func (Agn x) =
      lcp func x < uAg


instance
  ( Semigroup a
  , SemiAlign f
  )
    => Semigroup (Aligned f a)
  where
    (<>) =
      zd' (<>)


instance
  ( Semigroup a
  , SemiAlign f
  , UnitalZip f
  )
    => Monoid (Aligned f a)
  where
    mempty =
      Agn aId


instance
  ( WeakAlign f
  )
    => WeakAlign (Aligned f)
  where
    Agn x #| Agn y =
      Agn (x #| y)

    cnL (Agn x) (Agn y) =
      Agn (cnL x y)

    cnR (Agn x) (Agn y) =
      Agn (cnR x y)

    alL (Agn x) (Agn y) =
      Agn (alL x y)

    alR (Agn x) (Agn y) =
      Agn (alR x y)


instance
  ( SemiAlign f
  )
    => SemiAlign (Aligned f)
  where
    aln (Agn x) (Agn y) =
      Agn (aln x y)

    alW f (Agn x) (Agn y) =
      Agn (alW f x y)


instance
  ( UnitalZip f
  )
    => UnitalZip (Aligned f)
  where
    aId =
      Agn aId

instance
  ( Zip f
  )
    => Zip (Aligned f)
  where
    zW f (Agn x) (Agn y) =
      Agn (zW f x y)

    zp (Agn x) (Agn y) =
      Agn (zp x y)
