module P.Algebra.Monoid.Free.Suffix
  ( sxn
  , sXn
  , sxs
  , sXs
  , sxS
  , sXS
  -- * Suffix maps
  , ms
  , fms
  , mS
  , fmS
  , msn
  , mSn
  , mss
  , mSs
  , msS
  , mSS
  -- * Other
  , ew
  , (>%)
  , few
  , new
  , fNw
  , sst
  , fss
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Monoid.Free
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.First
import P.Last
import P.Monad.Plus.Filter


-- | Gives all non-empty suffixes in order of decreasing size.
sxn ::
  ( FreeMonoid m
  )
    => m -> List m
sxn =
  nt < sx


-- | Gives all non-empty suffixes in order of increasing size.
-- Will stall on infinite lists.
sXn ::
  ( FreeMonoid m
  )
    => m -> List m
sXn =
  tl < sX


-- | Gives all strict suffixes in order of decreasing size.
sxs ::
  ( FreeMonoid m
  )
    => m -> List m
sxs =
  tl < sx


-- | Gives all strict suffixes in order of increasing size.
--
-- Will stall after the first element on infinite lists.
sXs ::
  ( FreeMonoid m
  )
    => m -> List m
sXs =
  nt < sX


-- | Gives all non-empty strict suffixes in order of decreasing size.
sxS ::
  ( FreeMonoid m
  )
    => m -> List m
sxS =
  nt < sxs


-- | Gives all non-empty strict suffixes in order of increasing size.
--
-- Will stall on infinite lists.
sXS ::
  ( FreeMonoid m
  )
    => m -> List m
sXS =
  tl < sXs


-- | Maps a function over the suffixes of an element in order of decreasing size.
ms ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
ms func =
  m func < sx


-- | Flip of 'ms'.
fms ::
  ( FreeMonoid m
  )
    => m -> (m -> b) -> List b
fms =
  F ms


-- | Maps a function over the suffixes of a list in order of increasing size.
mS ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mS func =
  m func < sX


-- | Flip of 'mS'.
fmS ::
  ( FreeMonoid m
  )
    => m -> (m -> b) -> List b
fmS =
  F mS


-- | Maps a function over the non-empty suffixes of a list in order of decreasing size.
msn ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
msn func =
  m func < sxn


-- | Maps a function over the non-empty suffixes of a list in order of increasing size.
mSn ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mSn func =
  m func < sXn


-- | Maps a function over the strict suffixes of a list in order of decreasing size.
mss ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mss func =
  m func < sxs


-- | Maps a function over the strict suffixes of a list in order of increasing size.
mSs ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mSs func =
  m func < sXs


-- | Maps a function over the non-empty strict suffixes of a list in order of decreasing size.
msS ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
msS func =
  m func < sxS


-- | Maps a function over the non-empty strict suffixes of a list in order of increasing size.
mSS ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mSS func =
  m func < sXS


-- | Takes two lists and determines whether the first list ends with the second.
ew ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
ew =
  fe < sx


-- | Infix version of 'ew'.
(>%) ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
(>%) =
  ew


-- | Flip of 'ew'.
few ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
few =
  F ew


-- | Negation of 'ew'.
new ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
new =
  mm Prelude.not ew


-- | Flip of 'new'.
fNw ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
fNw =
  f' new


-- | Get all suffixes satisfying a predicate.
--
-- Results are ordered in increasing size.
sst ::
  ( FreeMonoid m
  )
    => Predicate m -> m -> List m
sst pred =
  fl pred < mS (\x -> x)


-- | Flip of 'sst'.
fss ::
  ( FreeMonoid m
  )
    => m -> Predicate m -> List m
fss =
  f' sst
