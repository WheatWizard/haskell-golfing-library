{-|
Module :
  P.Algebra.Monoid.Free.Split.Extra
Description :
  Additional functions for the P.Algebra.Monoid.Free.Split library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Algebra.Monoid.Free.Split module.
-}
module P.Algebra.Monoid.Free.Split.Extra
  where


import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Split


-- | Split a free monoid into chunks of size 1.
-- Equivalent to @'cxS' 1@.
cx1 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx1 =
  cxS (1 :: Int)


-- | Split a free monoid into chunks of size 2.
-- Equivalent to @'cxS' 2@.
cx2 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx2 =
  cxS (2 :: Int)


-- | Split a free monoid into chunks of size 3.
-- Equivalent to @'cxS' 3@.
cx3 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx3 =
  cxS (3 :: Int)


-- | Split a free monoid into chunks of size 4.
-- Equivalent to @'cxS' 4@.
cx4 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx4 =
  cxS (4 :: Int)


-- | Split a free monoid into chunks of size 5.
-- Equivalent to @'cxS' 5@.
cx5 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx5 =
  cxS (5 :: Int)


-- | Split a free monoid into chunks of size 6.
-- Equivalent to @'cxS' 6@.
cx6 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx6 =
  cxS (6 :: Int)


-- | Split a free monoid into chunks of size 7.
-- Equivalent to @'cxS' 7@.
cx7 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx7 =
  cxS (7 :: Int)


-- | Split a free monoid into chunks of size 8.
-- Equivalent to @'cxS' 8@.
cx8 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx8 =
  cxS (8 :: Int)


-- | Split a free monoid into chunks of size 9.
-- Equivalent to @'cxS' 9@.
cx9 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx9 =
  cxS (9 :: Int)


-- | Split a free monoid into chunks of size 10.
-- Equivalent to @'cxS' 10@.
cx0 ::
  ( FreeMonoid m
  )
    => m -> Partition m
cx0 =
  cxS (10 :: Int)
