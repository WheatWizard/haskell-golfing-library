{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
{-|
Module :
  P.Algebra.Monoid.Action
Description :
  Monoid actions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Monoid.Action.Regular
  ( Regular (..)
  )
  where


import qualified Prelude


import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Monoid
import P.Algebra.Monoid.Action
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Shelf
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Eq
import P.Functor
import P.Functor.Identity.Class
import P.Monad
import P.Ord


-- | Any monoid acts on itself by left multiplication.
--
-- This is a wrapper around monoid items which implements that action.
newtype Regular m =
  Rg
    -- | Use 'cr' instead.
    { gRg ::
      m
    }


instance
  ( Semigroup m
  )
    => Semigroup (Regular m)
  where
    (Rg x) <> (Rg y) =
      Rg (x <> y)


instance
  ( Monoid m
  )
    => Monoid (Regular m)
  where
    mempty =
      Rg i


instance
  ( Magma m
  )
    => Magma (Regular m)
  where
    Rg x %%% Rg y =
      Rg (x %%% y)


instance
  ( Quasigroup m
  )
    => Quasigroup (Regular m)
  where
    Rg x ~/~ Rg y =
      Rg (x ~/~ y)
    Rg x ~\~ Rg y =
      Rg (x ~\~ y)


instance
  ( Loop m
  )
    => Loop (Regular m)
  where
    lem =
      Rg lem


instance
  ( Moufang m
  )
    => Moufang (Regular m)
  where
    _iv (Rg x) =
      Rg (IV x)


instance
  ( Shelf m
  )
    => Shelf (Regular m)
  where
    Rg x <! Rg y =
      Rg (x <! y)


instance
  ( Rack m
  )
    => Rack (Regular m)
  where
    Rg x !> Rg y =
      Rg (x !> y)


instance
  ( Quandle m
  )
    => Quandle (Regular m)


instance
  ( Group m
  )
    => Group (Regular m)


instance
  ( Monoid m
  )
    => Action (Regular m) m
  where
    aq (Rg x) y =
      x <> y


instance
  ( Commutative m
  )
    => Commutative (Regular m)


instance
  (
  )
    => Functor Regular
  where
    fmap f (Rg x) =
      Rg (f x)


instance
  (
  )
    => Applicative Regular
  where
    pure =
      Rg
    Rg f <*> Rg y =
      Rg (f y)


instance
  (
  )
    => Monad Regular
  where
    Rg x >>= f =
      f x


instance
  (
  )
    => Comonad Regular
  where
    cr =
      gRg

    dyp =
      Rg


instance
  (
  )
    => Unpure Regular
  where
    unp (Rg x) =
      [x]
    pure' =
      Rg


instance
  (
  )
    => Identity Regular


instance
  (
  )
    => Eq1 Regular
  where
    q1 f (Rg x) (Rg y) =
      f x y


instance
  ( Eq a
  )
    => Eq (Regular a)
  where
    (==) =
      q1 eq


instance
  (
  )
    => Ord1 Regular
  where
    lcp f (Rg x) (Rg y) =
      f x y


instance
  ( Ord a
  )
    => Ord (Regular a)
  where
    cp =
      lcp cp
