{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
{-|
Module :
  P.Algebra.Monoid.Conjugate
Description :
  The conjugate monoid action
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Monoid.Action.Conjugate
  ( Conjugate (..)
  )
  where


import qualified Prelude


import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Monoid
import P.Algebra.Monoid.Action
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Shelf
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Eq
import P.Functor
import P.Functor.Identity.Class
import P.Monad
import P.Ord


-- | Any group acts on itself by conjugation.
--
-- This is a wrapper around monoid items which implements that action.
newtype Conjugate m =
  Cx
    { gCx ::
      m
    }


instance
  ( Magma m
  )
    => Magma (Conjugate m)
  where
    Cx x %%% Cx y =
      Cx (x %%% y)


instance
  ( Semigroup m
  )
    => Semigroup (Conjugate m)
  where
    (Cx x) <> (Cx y) =
      Cx (x <> y)


instance
  ( Monoid m
  )
    => Monoid (Conjugate m)
  where
    mempty =
      Cx i


instance
  ( Quasigroup m
  )
    => Quasigroup (Conjugate m)
  where
    Cx x ~/~ Cx y =
      Cx (x ~/~ y)
    Cx x ~\~ Cx y =
      Cx (x ~\~ y)


instance
  ( Loop m
  )
    => Loop (Conjugate m)
  where
    lem =
      Cx lem


instance
  ( Moufang m
  )
    => Moufang (Conjugate m)
  where
    _iv (Cx x) =
      Cx (IV x)


instance
  ( Shelf m
  )
    => Shelf (Conjugate m)
  where
    Cx x <! Cx y =
      Cx (x <! y)


instance
  ( Rack m
  )
    => Rack (Conjugate m)
  where
    Cx x !> Cx y =
      Cx (x !> y)


instance
  ( Quandle m
  )
    => Quandle (Conjugate m)


instance
  ( Group m
  )
    => Group (Conjugate m)


instance
  ( Group m
  )
    => Action (Conjugate m) m
  where
    aq (Cx x) y =
      x <> y ~~ x


instance
  ( Commutative m
  )
    => Commutative (Conjugate m)


instance
  (
  )
    => Functor Conjugate
  where
    fmap f (Cx x) =
      Cx (f x)


instance
  (
  )
    => Applicative Conjugate
  where
    pure =
      Cx
    Cx f <*> Cx x =
      Cx (f x)


instance
  (
  )
    => Monad Conjugate
  where
    Cx x >>= f =
      f x


instance
  (
  )
    => Comonad Conjugate
  where
    cr =
      gCx

    dyp =
      Cx


instance
  (
  )
    => Unpure Conjugate
  where
    unp (Cx x) =
      [x]
    pure' =
      Cx


instance
  (
  )
    => Identity Conjugate


instance
  (
  )
    => Eq1 Conjugate
  where
    q1 f (Cx x) (Cx y) =
      f x y


instance
  ( Eq a
  )
    => Eq (Conjugate a)
  where
    (==) =
      q1 eq


instance
  (
  )
    => Ord1 Conjugate
  where
    lcp f (Cx x) (Cx y) =
      f x y


instance
  ( Ord a
  )
    => Ord (Conjugate a)
  where
    cp =
      lcp cp
