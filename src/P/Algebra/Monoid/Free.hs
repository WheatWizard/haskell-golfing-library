{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Algebra.Monoid.Free
Description :
  Free monoid class
Copyright :
  (c) E. Olive, 2024
License :
 GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Monoid.Free
  ( FreeMonoid (..)
  , pattern Id
  , pattern (:*<)
  , Partition (..)
  -- * Partitions
  , pt
  , pST
  , pWK
  , iRf
  , fiR
  , rfn
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Applicative
import P.Arithmetic.Nat
import P.Bifunctor
import P.Bool
import P.Category
import P.Eq
import P.First
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Last
import P.Monad
import P.Monad.Plus.Filter
import P.Swap


-- | A partition of an element of a free monoid is a list of elements which when combined in order with the monoid operation ('fo'), yield that element.
-- A partition can easily be represented as a list of elements.
-- This type alias is that it just allows more expressive type signatures.
type Partition m =
  List m


-- | A free monoid is a monoid that is isomorphic to a list.
--
-- __Identity__
--
-- prop> gGn i ≡ []
--
-- __Homomorphism__
--
-- prop> gGn (a <> b) ≡ gGn a <> gGn b
--
-- __Isomorphism__
--
-- prop> fo < gGn ≡ id
--
-- __Idempotency__
--
-- prop> gGn +> gGn ≡ gGn
--
class
  ( Monoid m
  )
    => FreeMonoid m
  where
    {-# Minimal bkO, bKO | gGn #-}
    -- | Get the generators of an element.
    --
    -- This is the finest partition of an element into non-trivial parts.
    gGn :: m -> Partition m
    gGn x = do
      (h, m) <- bkO x
      h : gGn m

    -- | Break off the first generator of an element, return 'P.Maybe.Nothing' if the element is the identity.
    --
    -- The first element should be a generator of the monoid.
    bkO :: m -> Maybe' (m, m)
    bkO x =
      case
        gGn x
      of
        [] ->
          []
        g1 : gs ->
          p (g1, fo gs)

    -- | Break off the last generator of an element, return 'P.Maybe.Nothing' if the element is the identity.
    --
    -- The second element should be a generator of the monoid.
    bKO :: m -> Maybe' (m, m)
    bKO =
      go < gGn
      where
        go [] =
          []
        go [x] =
          p (i, x)
        go [x, y] =
          p (x, y)
        go (x : y : z) =
          go (x <> y : z)

    -- | Give all prefixes of an element in order of increasing size.
    -- The first element is always 'i'.
    px :: m -> List m
    px =
      lS (<>) i < gGn

    -- | Give all prefixes of an element in order of decreasing size.
    -- The last element is always 'i'.
    pX :: m -> List m
    pX x = do
      (m, _) <- bKO x
      x : pX m

    -- | Give all suffixes of an element in order of decreasing size.
    -- The last element is always 'i'.
    sx :: m -> List m
    sx =
      rS (<>) i < gGn

    -- | Give all suffixes of an element in order of increasing size.
    -- The first element is always 'i'
    sX :: m -> List m
    sX =
      (i :) < tl < Prelude.reverse < sx

    -- | Reverse the order of the generators.
    rvg :: m -> m
    rvg =
      fo < Prelude.reverse < gGn


instance
  (
  )
    => FreeMonoid (List a)
  where
    bkO [] =
      []
    bkO (a : b) =
      [([a], b)]

    pX [] =
      [ i ]
    pX x =
      x : pX (nt x)

    gGn =
      m (: [])

    rvg =
      Prelude.reverse

    sx =
      rS (:) []


instance
  (
  )
    => FreeMonoid Nat
  where
    bkO 0 =
      []
    bkO (Succ n) =
      [(1, n)]

    bKO =
      bkO

    px 0 =
      [ 0 ]
    px (Succ n) =
      0 : Succ < px n

    pX 0 =
      [ i ]
    pX (Succ n) =
      Succ n : pX n

    rvg =
      Prelude.id


instance
  (
  )
    => FreeMonoid ()
  where
    bkO _ =
      []

    bKO =
      bkO

    pX _ =
      [()]

    rvg =
      Prelude.id



-- | Gives all partitions of a free monoid into non-trivial parts.
pt ::
  ( FreeMonoid m
  )
    => m -> List (Partition m)
pt =
  pWK (\ _ -> T)


-- | Gives all partitions that match a predicate.
--
-- ==== __Examples__
--
-- Get all partitions of a list into equal elements:
--
-- >>> pST lq "abababab"
-- [["ab","ab","ab","ab"],["abab","abab"],["abababab"]]
--
pST ::
  ( FreeMonoid m
  )
    => Predicate (List m) -> m -> List (Partition m)
pST pred =
  fl pred < pt


-- | Gives all partitions where each part matches a predicate.
--
-- ==== __Examples__
--
-- Get all partitions into elements with an odd size:
--
-- >>> pWK (od < l) [1,2,3,4]
-- [[[1],[2],[3],[4]],[[1],[2,3,4]],[[1,2,3],[4]]]
--
-- Get all integer partitions into odd elements:
--
-- >>> pWK od (6 :: Nat)
-- [[1,1,1,1,1,1],[1,1,1,3],[1,1,3,1],[1,3,1,1],[1,5],[3,1,1,1],[3,3],[5,1]]
pWK ::
  ( FreeMonoid m
  )
    => Predicate m -> m -> List (Partition m)
pWK pred y =
  case
    gGn y
  of
    (x : xs) ->
      go x xs
    [] ->
      []
  where
    go n [] =
      [ [n]
      | pred n
      ]
    go n (x : xs)
      | pred n
      =
        (n :) < go x xs <> go (n <> x) xs
      | T
      =
        go (n <> x) xs


-- | Takes two partitions and determines if the first partition is a finer partition of the same value as the second partition.
-- A finer partition is a partition that splits in all of the same places but may additionally split in new places.
-- Fineness for this function is not strict, so it returns @True@ when given two copies of the same partition.
--
-- Flip of 'fiR'.
--
-- ==== __Examples__
--
-- >>> iRf [[1,2],[3,4]] [[1,2,3,4]]
-- True
-- >>> iRf [[1,2],[3,4]] [[1],[2,3,4]]
-- False
-- >>> iRf [3 :: Nat, 3, 1, 2, 2, 6] [6, 5, 6]
-- True
-- >>> iRf [2 :: Nat, 4] [3, 3]
-- False
--
iRf ::
  ( Eq m
  , FreeMonoid m
  )
    => Partition m -> Partition m -> Bool
iRf x y =
  go (gGn < x) (gGn < y)
  where
    go [] [] =
      T
    go ([] : xss) ([] : yss) =
      go xss yss
    go ([] : xss) ((y : ys) : yss) =
      go xss ((y : ys) : yss)
    go ((x : xs) : xss) ((y : ys) : yss)
      | x == y
      =
        go (xs : xss) (ys : yss)
    go _ _ =
      B


-- | Takes two partitions and determines if the first partition is a coarser partition of the same value as the second partition.
-- A coarser partition is a partition that splits in the same or fewer places in the original and splits in no new places.
-- Coarseness for this function is not strict, so it returns true when given two copies of the same partition.
--
-- Flip of 'iRf'.
--
fiR ::
  ( Eq m
  , FreeMonoid m
  )
    => Partition m -> Partition m -> Bool
fiR =
  F iRf


-- | Gives all refinements of a given partition.
--
-- ==== __Examples__
--
-- >>> m_ pr $ rfn [1 :: Nat, 2, 3]
-- [1,1,1,1,1,1]
-- [1,1,1,1,2]
-- [1,1,1,2,1]
-- [1,1,1,3]
-- [1,2,1,1,1]
-- [1,2,1,2]
-- [1,2,2,1]
-- [1,2,3]
--
rfn ::
  ( FreeMonoid m
  )
    => Partition m -> List (Partition m)
rfn =
   m jn < mM id < m pt


{-# Complete Id, (:*<) #-}
-- | A pattern matching the identity of a free monoid.
pattern Id ::
  ( FreeMonoid m
  )
    => m
pattern Id <- (gGn -> []) where
  Id =
    i


-- | A pattern matching the left-most generator of a word.
pattern (:*<) ::
  ( FreeMonoid m
  )
    => m -> m -> m
pattern x :*< xs <- (bkO -> [(x,xs)]) where
  (:*<) =
    (<>)
