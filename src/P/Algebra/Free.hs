{-# Language ScopedTypeVariables #-}
{-# Language FlexibleInstances #-}
{-# Language ConstraintKinds #-}
{-# Language KindSignatures #-}
{-# Language DeriveFunctor #-}
{-# Language InstanceSigs #-}
{-# Language RankNTypes #-}
{-|
Module :
  P.Algebra.Free
Description :
  Free structures
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

+--------------+-----------------------+--------------------+-------+
| Constraint   | Unstructured version  | Structured version | 'Eq'? |
+==============+=======================+====================+=======+
| 'Magma'      | @'Free' 'Magma'@      | 'FreeMagma'        | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Semigroup'  | @'Free' 'Semigroup'@  |                    | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Monoid'     | @'Free' 'Monoid'@     | 'List'             | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Quasigroup' | @'Free' 'Quasigroup'@ |                    | No    |
+--------------+-----------------------+--------------------+-------+
| 'Loop'       | @'Free' 'Loop'@       |                    | No    |
+--------------+-----------------------+--------------------+-------+
| 'Moufang'    | @'Free' 'Moufang'@    |                    | No    |
+--------------+-----------------------+--------------------+-------+
| 'Shelf'      | @'Free' 'Shelf'@      |                    | No    |
+--------------+-----------------------+--------------------+-------+
| 'Rack'       | @'Free' 'Rack'@       |                    | No    |
+--------------+-----------------------+--------------------+-------+
| 'Quandle'    | @'Free' 'Quandle'@    | 'FreeQuandle'      | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Group'      | @'Free' 'Group'@      | 'FreeGroup'        | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Kei'        | @'Free' 'Kei'@        | 'FreeKei'          | Yes   |
+--------------+-----------------------+--------------------+-------+
| 'Ring'       | @'Free' 'Ring'@       |                    | No    |
+--------------+-----------------------+--------------------+-------+

-}
module P.Algebra.Free
  ( Free (..)
  , fuF
  , fmc
  , fmC
  , fmn
  , fmN
  )
  where


import qualified Prelude
import qualified Control.Applicative
import qualified Control.Monad
import Data.Kind


import P.Algebra.Free.Group
import P.Algebra.Free.Magma
import P.Algebra.Free.Kei
import P.Algebra.Free.Quandle
import P.Algebra.Group
import P.Algebra.Kei
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Magma.Wrapper
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Ring
import P.Algebra.Semigroup
import P.Algebra.Shelf
import P.Aliases
import P.Alternative
import P.Applicative
import P.Applicative.Unpure
import P.Bifunctor.Either
import P.Category
import P.Comonad
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Monad
import P.Vector


-- | Takes a constraint (e.g. 'P.Algebra.Semigroup.Semigroup', 'P.Algebra.Monoid.Monoid', 'P.Algebra.Group.Group') and creates the type of free structures meeting that constraint.
data Free (s :: Type -> Constraint) (a :: Type)
  = FR
    { uFR ::
      forall g. s g => (a -> g) -> g
    }
  deriving
    ( Functor
    )


-- | Flip of 'uFR'.
fuF ::
  ( s g
  )
    => (a -> g) -> Free s a -> g
fuF f m =
  uFR m f



-- | Catamorphism of the free magma.
-- Also the Böhm-Berarducci encoding of the 'FreeMagma' type.
fmc ::
  (
  )
    => (a -> c) -> (c -> c -> c) -> Free Magma a -> c
fmc f1 f2 (FR func) =
  fMc f1 f2 $ func $ FMg < p


-- | Flip of 'fmc'.
fmC ::
  (
  )
    => (c -> c -> c) -> (a -> c) -> Free Magma a -> c
fmC =
  f' fmc


-- | Anamorphism of the free magma.
fmn ::
  (
  )
    => (a -> Either b (Vec2 a)) -> a -> Free Magma b
fmn func s =
  case
    func s
  of
    Lef x ->
      FR ($ x)
    Rit (V2 y1 y2) ->
      fmn func y1 %%% fmn func y2


-- | Flip of 'fmn'.
fmN ::
  (
  )
    => a -> (a -> Either b (Vec2 a)) -> Free Magma b
fmN =
  f' fmn


instance
  (
  )
    => Applicative (Free s)
  where
    pure x =
      FR ($ x)

    liftA2 =
      Control.Monad.liftM2


instance
  (
  )
    => Monad (Free s)
  where
    xs >>= f =
      join $ f < xs
      where
        join :: Free s (Free s a) -> Free s a
        join (FR xs) =
           FR (xs < fuF)


instance
  (
  )
    => Magma (Free Magma a)
  where
    FR xs %%% FR ys =
      FR go
      where
        go x =
          xs x %%% ys x


instance
  (
  )
    => Magma (Free Semigroup a)
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Semigroup (Free Semigroup a)
  where
    FR xs <> FR ys =
      FR go
      where
        go x =
          xs x <> ys x


instance
  (
  )
    => Magma (Free Monoid a)
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Semigroup (Free Monoid a)
  where
    FR xs <> FR ys =
      FR go
      where
        go x =
          xs x <> ys x


instance
  (
  )
    => Monoid (Free Monoid a)
  where
    mempty =
      FR (p i)


instance
  (
  )
    => Magma (Free Quasigroup a)
  where
    FR xs %%% FR ys =
      FR go
      where
        go x =
          xs x %%% ys x


instance
  (
  )
    => Quasigroup (Free Quasigroup a)
  where
    FR xs ~/~ FR ys =
      FR go
      where
        go x =
          xs x ~/~ ys x
    FR xs ~\~ FR ys =
      FR go
      where
        go x =
          xs x ~\~ ys x


instance
  (
  )
    => Magma (Free Loop a)
  where
    FR xs %%% FR ys =
      FR go
      where
        go x =
          xs x %%% ys x


instance
  (
  )
    => Quasigroup (Free Loop a)
  where
    FR xs ~/~ FR ys =
      FR go
      where
        go x =
          xs x ~/~ ys x
    FR xs ~\~ FR ys =
      FR go
      where
        go x =
          xs x ~\~ ys x


instance
  (
  )
    => Loop (Free Loop a)
  where
    lem =
      FR (p lem)


instance
  (
  )
    => Magma (Free Moufang a)
  where
    FR xs %%% FR ys =
      FR go
      where
        go x =
          xs x %%% ys x


instance
  (
  )
    => Quasigroup (Free Moufang a)
  where
    FR xs ~/~ FR ys =
      FR go
      where
        go x =
          xs x ~/~ ys x
    FR xs ~\~ FR ys =
      FR go
      where
        go x =
          xs x ~\~ ys x


instance
  (
  )
    => Loop (Free Moufang a)
  where
    lem =
      FR (p lem)


instance
  (
  )
    => Moufang (Free Moufang a)
  where
    _iv (FR xs) =
       FR (_iv < xs)


instance
  (
  )
    => Magma (Free Group a)
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Semigroup (Free Group a)
  where
    FR xs <> FR ys =
      FR go
      where
        go x =
          xs x <> ys x


instance
  (
  )
    => Monoid (Free Group a)
  where
    mempty =
      FR (p i)


instance
  (
  )
    => Quasigroup (Free Group a)
  where
    FR xs ~/~ FR ys =
      FR go
      where
        go x =
          xs x ~/~ ys x
    FR xs ~\~ FR ys =
      FR go
      where
        go x =
          xs x ~\~ ys x


instance
  (
  )
    => Loop (Free Group a)
  where
    lem =
      FR (p lem)


instance
  (
  )
    => Moufang (Free Group a)
  where
    _iv (FR xs) =
       FR (_iv < xs)


instance
  (
  )
    => Shelf (Free Group a)
  where
    FR xs <! FR ys =
      FR go
      where
        go x =
          xs x <! ys x


instance
  (
  )
    => Rack (Free Group a)
  where
    FR xs !> FR ys =
      FR go
      where
        go x =
          xs x !> ys x


instance
  (
  )
    => Quandle (Free Group a)


instance
  (
  )
    => Group (Free Group a)


instance
  (
  )
    => Shelf (Free Kei a)
  where
    FR xs <! FR ys =
      FR go
      where
        go x =
          xs x <! ys x


instance
  (
  )
    => Rack (Free Kei a)
  where
    FR xs !> FR ys =
      FR go
      where
        go x =
          xs x !> ys x


instance
  (
  )
    => Quandle (Free Kei a)


instance
  (
  )
    => Kei (Free Kei a)


instance
  (
  )
    => Shelf (Free Shelf a)
  where
    FR xs <! FR ys =
      FR go
      where
        go x =
          xs x <! ys x


instance
  (
  )
    => Shelf (Free Rack a)
  where
    FR xs <! FR ys =
      FR go
      where
        go x =
          xs x <! ys x


instance
  (
  )
    => Rack (Free Rack a)
  where
    FR xs !> FR ys =
      FR go
      where
        go x =
          xs x !> ys x


instance
  (
  )
    => Shelf (Free Quandle a)
  where
    FR xs <! FR ys =
      FR go
      where
        go x =
          xs x <! ys x


instance
  (
  )
    => Rack (Free Quandle a)
  where
    FR xs !> FR ys =
      FR go
      where
        go x =
          xs x !> ys x


instance
  (
  )
    => Quandle (Free Quandle a)


instance
  (
  )
    => Prelude.Num (Free Ring a)
  where
    FR xs + FR ys =
      FR go
      where
        go x =
          xs x + ys x

    FR xs - FR ys =
      FR go
      where
        go x =
          xs x - ys x

    FR xs * FR ys =
      FR go
      where
        go x =
          xs x * ys x

    negate (FR xs) =
      FR (Prelude.negate < xs)

    abs (FR xs) =
      FR (Prelude.abs < xs)

    fromInteger z =
      FR (p $ Prelude.fromInteger z)

    signum (FR xs) =
      FR (Prelude.signum < xs)


instance
  (
  )
    => Foldable (Free Magma)
  where
    foldMap f =
      cr < fuF (WSa < f)


instance
  (
  )
    => Foldable (Free Semigroup)
  where
    foldMap =
      fuF


instance
  (
  )
    => Foldable (Free Monoid)
  where
    foldMap =
      fuF


instance
  (
  )
    => Alternative (Free Monoid)
  where
    empty =
      FR (p i)

    x <|> y =
      x <> y


instance
  (
  )
    => Alternative (Free Group)
  where
    empty =
      FR (p i)

    x <|> y =
      x <> y


instance
  (
  )
    => Eq1 (Free Magma)
  where
    q1 userEq (FR xs) (FR ys) =
      q1 userEq (xs toFM) (ys toFM)
      where
        toFM :: a -> FreeMagma a
        toFM =
          p


instance
  (
  )
    => Eq1 (Free Semigroup)
  where
    q1 userEq xs ys =
      q1 userEq (tL xs) (tL ys)


instance
  (
  )
    => Eq1 (Free Monoid)
  where
    q1 userEq xs ys =
      q1 userEq (tL xs) (tL ys)


instance
  ( Eq a
  )
    => Eq (Free Magma a)
  where
    (==) =
      q1 eq

instance
  ( Eq a
  )
    => Eq (Free Semigroup a)
  where
    (==) =
      q1 eq


instance
  ( Eq a
  )
    => Eq (Free Monoid a)
  where
    (==) =
      q1 eq


instance
  ( Eq a
  )
    => Eq (Free Quandle a)
  where
    FR f == FR g =
      f QP == g QP


instance
  ( Eq a
  )
    => Eq (Free Group a)
  where
    FR f == FR g =
      f GP == g GP


instance
  ( Eq a
  )
    => Eq (Free Kei a)
  where
    FR f == FR g =
      f KPr == g KPr


instance
  (
  )
    => Unpure (Free Magma)
  where
    unp :: forall a . Free Magma a -> Maybe' a
    unp (FR xs) =
      unp (xs p :: FreeMagma a)
    pure' =
      p


instance
  (
  )
    => Unpure (Free Semigroup)
  where
    unp :: forall a . Free Semigroup a -> Maybe' a
    unp (FR xs) =
      unp (xs p :: List a)
    pure' =
      p


instance
  (
  )
    => Unpure (Free Monoid)
  where
    unp :: forall a . Free Monoid a -> Maybe' a
    unp (FR xs) =
      unp (xs p :: List a)
    pure' =
      p



instance
  (
  )
    => FreeMonoid (Free Monoid a)
  where
    gGn =
      m p < tL

    bkO x =
      case
        tL x
      of
        [] ->
          []
        (a : as) ->
          [ ( p a
            , mF p as
            )
          ]
