{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Magma
Description :
  Magmata
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Magma
  ( Magma (..)
  , mgp
  )
  where


import qualified Prelude
import Prelude
  ( Bool
  , Integer
  , Int
  )


import P.Aliases


import P.Algebra.Monoid
import P.Category
import P.Category.Iso


infixr 6 %%%


-- | A magma is a set with a closed binary operation.
--
-- No further laws are required.
class
  (
  )
    => Magma m
  where
    -- | The binary magma operation.
    (%%%) :: m -> m -> m


-- | Prefix version of '(%%%)'.
mgp ::
  ( Magma m
  )
    => m -> m -> m
mgp =
  (%%%)


instance
  (
  )
    => Magma [a]
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Magma Bool
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Magma ()
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Magma Integer
  where
    (%%%) =
      (<>)


instance
  (
  )
    => Magma Int
  where
    (%%%) =
      (<>)


instance
  ( Semigroupoid p
  )
    => Magma (Iso p a a)
  where
    (%%%) =
      (<>)


instance
  ( Magma a
  , Magma b
  )
    => Magma (a, b)
  where
    (x1, y1) %%% (x2, y2) =
      ( x1 %%% x2
      , y1 %%% y2
      )


instance
  ( Magma a
  , Magma b
  , Magma c
  )
    => Magma (a, b, c)
  where
    (x1, y1, z1) %%% (x2, y2, z2) =
      ( x1 %%% x2
      , y1 %%% y2
      , z1 %%% z2
      )


instance
  ( Magma a
  , Magma b
  , Magma c
  , Magma d
  )
    => Magma (a, b, c, d)
  where
    (w1, x1, y1, z1) %%% (w2, x2, y2, z2) =
      ( w1 %%% w2
      , x1 %%% x2
      , y1 %%% y2
      , z1 %%% z2
      )


instance
  ( Magma b
  )
    => Magma (a -> b)
  where
    (f1 %%% f2) x =
      f1 x %%% f2 x
