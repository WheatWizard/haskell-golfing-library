{-# Language FlexibleInstances #-}
{-|
Module :
  P.Algebra.Kei
Description :
  Kei
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Kei
  ( Kei (..)
  )
  where


import P.Algebra.Quandle


-- | Types that form a involute quandle or 'kei'.
--
-- __Involution__
--
-- prop> a <! (a <! b) ≡ b
-- prop> (a !> b) !> b ≡ a
class
  ( Quandle m
  )
    => Kei m


instance
  (
  )
    => Kei ()


instance
  ( Kei a
  , Kei b
  )
    => Kei (a, b)


instance
  ( Kei a
  , Kei b
  , Kei c
  )
    => Kei (a, b, c)


instance
  ( Kei a
  , Kei b
  , Kei c
  , Kei d
  )
    => Kei (a, b, c, d)
