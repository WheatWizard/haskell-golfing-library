module P.Ord.MinMax
  (
  -- * Mins
    mNW
  , mNw
  , mNB
  , mNb
  , mNM
  , mNL
  , mNl
  -- * Maxes
  , maW
  , maw
  , maB
  , mab
  , maM
  , maL
  , mal
  -- * Minmax
  , xN
  , xNW
  , xNB
  , xNl
  , nX
  , nXW
  , nXB
  , nXl
  )
  where


import Prelude
  ( Foldable
  )
import qualified Prelude


import P.Foldable.Length
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Ord


-- | Takes two elements and a comparison and gives the smaller one.
-- If the two elements are equal it defaults to the first one.
mNW ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> a
mNW func x y =
  case
    func x y
  of
    LT ->
      x
    GT ->
      y
    EQ ->
      x


-- | Takes two elements and a comparison and gives the smaller one.
-- If the two elements are equal it defaults to the second one.
mNw ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> a
mNw =
  F < mNW


-- | Takes two elements and a comparison and gives the larger one.
-- If the two elements are equal it defaults to the first one.
maW ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> a
maW func x y =
  case
    func x y
  of
    LT ->
      y
    GT ->
      x
    EQ ->
      x


-- | Takes two elements and a comparison and gives the larger one.
-- If the two elements are equal it defaults to the second one.
maw ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> a
maw =
  F < maW


-- | Takes a conversion function and compares two elements based on their converted value returning the smaller one.
-- If the two elements are equal it defaults to the first one.
--
-- ===== __Examples__
--
-- Get the shorter of two strings with @mNB@ 'l'.
--
-- >>> mNB l "test" "string"
-- "test"
-- >>> mNB l "test1" "test2"
-- "test1"
--
mNB ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> a
mNB =
  mNW < cb


-- | Takes a conversion function and compares two elements based on their converted value returning the smaller one.
-- If the two elements are equal it defaults to the second one.
--
-- ===== __Examples__
--
-- Get the shorter of two strings with @mNb@ 'l'.
--
-- >>> mNb l "test" "string"
-- "test"
-- >>> mNb l "test1" "test2"
-- "test2"
--
mNb ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> a
mNb =
  mNw < cb


-- | Takes a conversion function and compares two elements based on their converted value returning the larger one.
-- If the two elements are equal it defaults to the first one.
--
-- ===== __Examples__
--
-- Get the shorter of two strings with @maB@ 'l'.
--
-- >>> maB l "test" "string"
-- "string"
-- >>> maB l "test1" "test2"
-- "test1"
--
maB ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> a
maB =
  maW < cb


-- | Takes a conversion function and compares two elements based on their converted value returning the larger one.
-- If the two elements are equal it defaults to the second one.
--
-- ===== __Examples__
--
-- Get the shorter of two strings with @mab@ 'l'.
--
-- >>> mab l "test" "string"
-- "string"
-- >>> mab l "test1" "test2"
-- "test2"
--
mab ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> a
mab =
  maw < cb


-- | Takes a function applies it to two things and gives the smaller result.
mNM ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> b
mNM =
  on mN


-- | Takes a function applies it to two things and gives the larger result.
maM ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> b
maM =
  on ma


-- | Takes two structures and gives the smaller one.
-- If the two structures are of the same size it gives the first one.
--
-- Evaluates only as far as the shorter list.
mNL ::
  ( Foldable f
  )
    => f a -> f a -> f a
mNL =
  mNB lnt


-- | Takes two structures and gives the smaller one.
-- If the two structures are of the same size it gives the second one.
--
-- Evaluates only as far as the shorter list.
mNl ::
  ( Foldable f
  )
    => f a -> f a -> f a
mNl =
  mNB lnt


-- | Takes two structures and gives the larger one.
-- If the two structures are of the same size it gives the first one.
--
-- Evaluates only as far as the shorter list.
maL ::
  ( Foldable f
  )
    => f a -> f a -> f a
maL =
  maB lnt


-- | Takes two structures and gives the larger one.
-- If the two structures are of the same size it gives the second one.
--
-- Evaluates only as far as the shorter list.
mal ::
  ( Foldable f
  )
    => f a -> f a -> f a
mal =
  mab lnt


-- | Takes two elements and returns a tuple with the larger element first.
xN ::
  ( Ord a
  )
    => a -> a -> (a, a)
xN =
  xNW cp


-- | 'xN' but with a user defined comparison function.
--
-- If the comparison gives an 'EQ' the arguments are returned in the order they were given.
xNW ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> (a, a)
xNW userCp x y =
  case
    userCp x y
  of
    LT ->
      (y, x)
    _ ->
      (x, y)


-- | 'xN' but with a user defined conversion function.
xNB ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> (a, a)
xNB =
  xNW < cb


-- | Takes two elements and returns a tuple with the longer element first.
--
-- If the elements are the same length the arguments are returned in the order they were given.
xNl ::
  ( Foldable f
  )
    => f a -> f a -> (f a, f a)
xNl =
  xNB lnt


-- | Takes two elements and returns a tuple with the smaller element first.
nX ::
  ( Ord a
  )
    => a -> a -> (a, a)
nX =
  xNW fcp


-- | 'nX' but with a user defined comparison function.
nXW ::
  (
  )
    => (a -> a -> Ordering) -> a -> a -> (a, a)
nXW =
  xNW < f'


-- | 'nX' but with a user defined conversion function.
nXB ::
  ( Ord b
  )
    => (a -> b) -> a -> a -> (a, a)
nXB =
  nXW < cb


-- | Takes two elements and returns a tuple with the shorter element first.
--
-- If the elements are the same length the arguments are returned in the order they were given.
nXl ::
  ( Foldable f
  )
    => f a -> f a -> (f a, f a)
nXl =
  nXB lnt
