{-|
Module :
  P.Ord
Description :
  Functions for comparing 3 elements
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Ord.Compare3
  ( Ordering3 (..)
  , cp3
  , eI
  )
  where


import qualified Prelude
import Prelude
  ( Show
  )


import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Ord


-- | All the ways three elements can be ordered
data Ordering3
  = AAA -- ^ All equal
  | AAB
  | ABA
  | ABB
  | ABC -- ^ Strictly monotonic increasing
  | ACB
  | BAA
  | BAB
  | BAC
  | BBA
  | BCA
  | CAB
  | CBA -- ^ Strictly monotonic decreasing
  deriving
    ( Eq
    , Show
    )


-- | Take three elements and determines how they are ordered.
cp3 ::
  ( Ord a
  )
    => a -> a -> a -> Ordering3
cp3 x y z =
  case
    cp x y
  of
    EQ ->
      case
        cp y z
      of
        EQ ->
          AAA
        LT ->
          AAB
        GT ->
          BBA
    LT ->
      case
        cp y z
      of
        EQ ->
          ABB
        LT ->
          ABC
        GT ->
          case
            cp x z
          of
            EQ ->
              ABA
            LT ->
              BCA
            GT ->
              ACB
    GT ->
      case
        cp y z
      of
        EQ ->
          BAA
        LT ->
          case
            cp x z
          of
            EQ ->
              BAB
            LT ->
              BAC
            GT ->
              CAB
        GT ->
          CBA


-- | Check for membership in a monotonic collection.
--
-- ==== __Examples__
--
-- >>> eI 28 $ sq < nN
-- False
-- >>> eI 928 $ sq < nN
-- False
-- >>> eI 336 $ sq < nN
-- False
-- >>> eI 256 $ sq < nN
-- True
-- >>> eI (-256) $ Ng < sq < nN
-- True
-- >>> eI 256 $ Ng < sq < nN
-- False
-- >>> eI 293 $ Ng < sq < nN
-- False
eI ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> Bool
eI x =
  go < tL
  where
    go [] =
      B
    go [a] =
      a == x
    go (a : b : rem) =
      case
        cp3 a x b
      of
        AAA ->
          T
        AAB ->
          T
        BAA ->
          T
        BBA ->
          T
        ABB ->
          T
        ABC ->
          B
        CBA ->
          B
        BAC ->
          B
        ACB ->
          B
        _ ->
          go (b : rem)
