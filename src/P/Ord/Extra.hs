{-|
Module :
  P.Ord.Extra
Description :
  Additional functions for the P.Ord library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental


A large number of less important functions that have been moved to this file to reduce clutter in the main P.Ord library.
-}
module P.Ord.Extra
  ( lt0
  , gt0
  , lt1
  , gt1
  , lt2
  , gt2
  , lt3
  , gt3
  , lt4
  , gt4
  , lt5
  , gt5
  , lt6
  , gt6
  , lt7
  , gt7
  , lt8
  , gt8
  , lt9
  , gt9
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Ring
import P.Ord


-- | Checks if the input is less than 0.
lt0 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt0 =
  lt 0


-- | Checks if the input is greater than 0.
gt0 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt0 =
  gt 0


-- | Checks if the input is less than 1.
lt1 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt1 =
  lt 1


-- | Checks if the input is greater than 1.
gt1 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt1 =
  gt 1


-- | Checks if the input is less than 2.
lt2 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt2 =
  lt 2


-- | Checks if the input is greater than 2.
gt2 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt2 =
  gt 2


-- | Checks if the input is less than 3.
lt3 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt3 =
  lt 3


-- | Checks if the input is greater than 3.
gt3 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt3 =
  gt 3


-- | Checks if the input is less than 4.
lt4 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt4 =
  lt 4


-- | Checks if the input is greater than 4.
gt4 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt4 =
  gt 4


-- | Checks if the input is less than 5.
lt5 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt5 =
  lt 5


-- | Checks if the input is greater than 5.
gt5 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt5 =
  gt 5


-- | Checks if the input is less than 6.
lt6 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt6 =
  lt 6


-- | Checks if the input is greater than 6.
gt6 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt6 =
  gt 6


-- | Checks if the input is less than 7.
lt7 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt7 =
  lt 7


-- | Checks if the input is greater than 7.
gt7 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt7 =
  gt 7


-- | Checks if the input is less than 8.
lt8 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt8 =
  lt 8


-- | Checks if the input is greater than 8.
gt8 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt8 =
  gt 8


-- | Checks if the input is less than 9.
lt9 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
lt9 =
  lt 9


-- | Checks if the input is greater than 9.
gt9 ::
  ( Ord a
  , Ring a
  )
    => Predicate a
gt9 =
  gt 9
