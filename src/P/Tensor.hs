{-# Language FunctionalDependencies #-}
{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language InstanceSigs #-}
{-# Language GADTs #-}
module P.Tensor
  ( Tensor (..)
  , ta
  , pattern Mk
  , ti
  )
  where

import Prelude
  (
  )

import P.Algebra.Ring
import P.Applicative.Ring
import P.Category
import P.Function.Compose

infixr 0 #$
infixr 9 <#

-- | A class for objects that form tensors.
--
-- If @Tensor f g h@ then @ta f@ must be a linear function from @g@ to @h@.
class
  ( RingApplicative g
  )
    => Tensor f g h
    | f -> g h
    , g h -> f
  where
    -- | Apply a tensor as a linear function.
    (#$) ::
      ( Ring r
      )
        => f r -> g r -> h r
    -- | Takes a linear function and produces a tensor.
    make ::
      ( Ring r
      )
        => (g r -> h r) -> f r
    -- | Composes two tensors.
    --
    -- For matrices this is matrix "multiplication".
    (<#) ::
      ( Ring r
      , Tensor k h t
      , Tensor q g t
      )
        => k r -> f r -> q r
    tensor1 <# tensor2 =
      make $ ta tensor1 < ta tensor2

-- | Applies a tensor as a linear function.
--
-- Prefix version of '(#$)'.
ta ::
  ( Ring r
  , Tensor f g h
  )
    => f r -> g r -> h r
ta =
  (#$)

-- | Takes a linear function and makes a tensor.
pattern Mk ::
  ( Tensor f g h
  , Ring r
  )
    => (g r -> h r) -> f r
pattern Mk x <- (ta -> x) where
  Mk =
    make

-- | Identity tensor.
ti ::
  ( Tensor f g g
  , Ring r
  )
    => f r
ti =
  Mk id
