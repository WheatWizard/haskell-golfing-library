module P.Sort.Class
  ( Sortable (..)
  , rsr
  , fsW
  , rsW
  , fRW
  , sB
  , fsB
  , rsB
  , fRB
  , frsB
  , sj
  , sJ
  , xsr
  , xsW
  , xsB
  -- * Deprecated
  , sort
  , sortBy
  , sortOn
  )
  where

import Prelude
  ( Foldable
  , Integral
  )

import qualified Data.List

import P.Bifunctor.Flip
import P.Category
import P.Comonad
import P.Foldable.Length
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Ord
import P.Scan

-- | Structures which can be sorted.
class
  (
  )
    => Sortable f
  where
    -- | Sorts using a user defined comparison.
    --
    -- More general version of 'Data.List.sortBy'.
    sW :: (a -> a -> Ordering) -> f a -> f a

    -- | Sorts in ascending order.
    -- More general version of to 'Data.List.sort'.
    sr ::
      ( Ord a
      )
        => f a -> f a
    sr =
      sW cp

instance
  (
  )
    => Sortable []
  where
    sW =
      Data.List.sortBy < mm otp

{-# Deprecated sort "Use sr instead"#-}
-- | Long version of 'sr'.
-- Sorts a list.
sort ::
  ( Ord a
  , Sortable f
  )
    => f a -> f a
sort =
  sr

-- | Sorts in descending order.
rsr ::
  ( Ord a
  , Sortable f
  )
    => f a -> f a
rsr =
  rsW cp

{-# Deprecated sortBy "Use sW instead"#-}
-- | Long version of 'sW'.
-- Sorts using a user defined comparison.
sortBy ::
  ( Sortable f
  )
    => (a -> a -> Ordering) -> f a -> f a
sortBy =
  sW

-- | Flip of 'sW'.
fsW ::
  ( Sortable f
  )
    => f a -> (a -> a -> Ordering) -> f a
fsW =
  F sW

-- | Sorts a list in descending order list using a ordering function.
rsW ::
  ( Sortable f
  )
    => (a -> a -> Ordering) -> f a -> f a
rsW  =
  sW < RC

-- | Flip of 'rsW'.
fRW ::
  ( Sortable f
  )
    => f a -> (a -> a -> Ordering) -> f a
fRW  =
  F rsW

{-# Deprecated sortOn "Use sB instead"#-}
-- | Long version of 'sB'.
-- Sorts the list in ascending order as if the values were the result of the conversion function.
sortOn ::
  ( Ord b
  , Sortable f
  )
    => (a -> b) -> f a -> f a
sortOn =
  sB

-- | Takes a conversion function and a list.
-- Sorts the list in ascending order as if the values were the result of the conversion function.
--
-- More general version of 'Data.List.sortOn'.
--
-- ==== __Examples__
--
-- If we use 'P.Show.sh', then we sort by lexographical order:
--
-- >>> sB sh (0 ## 25)
-- [0,1,10,11,12,13,14,15,16,17,18,19,2,20,21,22,23,24,25,3,4,5,6,7,8,9]
--
-- If we use 'l', then we sort by length:
--
-- >>> sB l ["Sort", "me", "by", "length", "!"]
-- ["!","me","by","Sort","length"]
sB ::
  ( Ord b
  , Sortable f
  )
    => (a -> b) -> f a -> f a
sB =
  sW < on cp

-- | Flip of 'sB'.
fsB ::
  ( Ord b
  , Sortable f
  )
    => f a -> (a -> b) -> f a
fsB =
  F sB

-- | Takes a conversion function and a list.
-- Sorts in descending order as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- If we use `P.Show.sh`, then we sort by reverse lexographical order:
--
-- >>> rsB sh (0 '##' 25)
-- [9,8,7,6,5,4,3,25,24,23,22,21,20,2,19,18,17,16,15,14,13,12,11,10,1,0]
--
-- If we use 'l', then we sort by length:
--
-- >>> rsB l ["Sort", "me", "by", "length", "!"]
-- ["length","Sort","me","by","!"]
rsB ::
  ( Ord b
  , Sortable f
  )
    => (a -> b) -> f a -> f a
rsB =
  sW < on fcp

-- | Flip of 'rsB'.
fRB ::
  ( Ord b
  , Sortable f
  )
    => f a -> (a -> b) -> f a
fRB =
  F rsB

{-# Deprecated frsB "Use fRB instead" #-}
-- | Long version of 'fRB'.
-- Flip of 'rsB'.
frsB ::
  ( Ord b
  , Sortable f
  )
    => f a -> (a -> b) -> f a
frsB =
  fRB

-- | Sort a list of lists in ascending order of length.
sj ::
  ( Foldable t
  , Sortable f
  )
    => f (t a) -> f (t a)
sj =
  sB lg

-- | Sort a list of lists in descending order of length.
sJ ::
  ( Foldable t
  , Sortable f
  )
    => f (t a) -> f (t a)
sJ =
  rsB lg

-- | Sort a structure and replace each element in the result with its index before sorting.
--
-- ==== __Examples__
--
-- >>> xsr ["Hello", "AAA", "Bob"]
-- [1,2,0]
-- >>> xsr ["Hello", "AAA", "Bob", ""]
-- [3,1,2,0]
-- >>> xsr $ V3 2819 29 78
-- V3 1 2 0
xsr ::
  ( Ord a
  , Integral i
  , Scan f
  , Sortable f
  )
    => f a -> f i
xsr =
  xsW cp

-- | Sort a structure using a user defined comparison and replace each element in the result with its index before sorting.
xsW ::
  ( Sortable f
  , Scan f
  , Integral i
  )
    => (a -> a -> Ordering) -> f a -> f i
xsW userComp =
  m cr < sW (LF2 $ cp2 tcp userComp) < ixm (F (,))

-- | Sort a structure using a user defined conversion and replace each element in the result with its index before sorting.
xsB ::
  ( Sortable f
  , Scan f
  , Integral i
  , Ord b
  )
    => (a -> b) -> f a -> f i
xsB userConv =
  m cr < sW (LF2 $ cp2 tcp cp) < ixm (F $ (,) < userConv)
