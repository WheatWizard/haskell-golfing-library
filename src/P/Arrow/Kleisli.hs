module P.Arrow.Kleisli
  ( Kleisli (..)
  )
  where


import qualified Prelude


import P.Applicative
import P.Arrow
import P.Bifunctor.Profunctor
import P.Category
import P.Function.Compose
import P.Functor
import P.Monad


-- | A type corresponding to the kleisli arrows of a monad @m@.
newtype Kleisli m a b =
  Kle
    { rKe ::
      a -> m b
    }


instance
  ( Monad m
  )
    => Semigroupoid (Kleisli m)
  where
    Kle k1 <@ Kle k2 =
      Kle $ k1 <+ k2


instance
  ( Monad m
  )
    => Category (Kleisli m)
  where
    id =
      Kle p


instance
  ( Monad m
  )
    => LooseArrow (Kleisli m)
  where
    mSt (Kle f) =
      Kle $ \ ~(a, b) -> do
        c <- f a
        p (c, b)

    mNd (Kle f) =
      Kle $ \ ~(a, b) -> do
        c <- f b
        p (a, c)

    aSw =
      arr aSw


instance
  ( Functor f
  )
    => Profunctor (Kleisli f)
  where
    Kle f <@^ g =
      Kle $ f < g


instance
  ( Monad m
  )
    => Arrow (Kleisli m)


instance
  ( Functor m
  )
    => Functor (Kleisli m a)
  where
    fmap f (Kle k) =
      Kle $ f << k


instance
  ( Applicative m
  )
    => Applicative (Kleisli m a)
  where
    pure =
      Kle < p < p

    Kle k1 <*> Kle k2 =
      Kle $ \ x -> k1 x *^ k2 x


instance
  ( Monad m
  )
    => Monad (Kleisli m a)
  where
    Kle k1 >>= f =
      Kle $
        \ x -> do
          r <- k1 x
          rKe (f r) x
