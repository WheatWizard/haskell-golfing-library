{-# Language FlexibleContexts #-}
{-|
Module :
  P.Arrow.ArrowM
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

The Arrow type class requires that a type be an arrow on the last two holes in its type.
Arrows must have kind @(* -> * -> *)@ with a category forming over the first two @*@s to be instances of the class.
However plenty of types can in the mathematical sense be arrows without having their arguments in the correct places for the Arrow type class.

This module deals with types of kind @(* -> * -> * -> *)@ which are arrows on the first two @*@.
That is they match the Arrow type class but with an additional parameter at the end blocking them from being implemented.

Typically we expect that these types be a monad on the last parameter thus we informally call them \"ArrowM\" or "monadic arrows".

The 'Parser' type is an example of a type which is ArrowM.
-}
module P.Arrow.ArrowM
  ( -- * Basic functions
    -- ** Loose arrow
    mSM
  , mnM
  , (=:@)
  , prM
  , fPM
  , syM
  , (><@)
  , cxM
  , fXM
  -- ** Arrow
  , (-<@)
  , otM
  , foM
  , arM
  -- * Additional combinators
  , (=:%)
  , prv
  , fpv
  , (==@)
  , p2M
  , f2M
  , (^=:)
  , lpr
  , fLP
  , (=:^)
  , rpr
  , fRP
  -- * Parser variants
  , (=:>)
  , prR
  , fpR
  , (=:<)
  , prL
  , fpL
  )
  where


import qualified Prelude


import P.Applicative
import P.Arrow
import P.Bifunctor
import P.Bifunctor.Flip
import P.Bifunctor.Profunctor
import P.Category
import P.Function.Flip
import P.Function.Compose
import P.Function.Curry
import P.Functor
import P.Parse


-- | Monadic version of 'mSt'.
mSM ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p (a, c) (b, c) x
mSM =
  mF2 mSt


-- | Monadic version of 'mNd'.
mnM ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p (c, a) (c, b) x
mnM =
  mF2 mNd


-- | Combine two monadic arrows in parallel.
(=:@) ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p c d x -> p (a, c) (b, d) x
(=:@) =
  m22 (=:)


-- | Prefix version of '(=:@)'.
prM ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p c d x -> p (a, c) (b, d) x
prM =
  (=:@)


-- | Flip of 'prM'.
fPM ::
  ( LooseArrow (Flip2 p x)
  )
    => p c d x -> p a b x -> p (a, c) (b, d) x
fPM =
  f' prM


-- | Voids both arrows before combining them.
(=:%) ::
  ( LooseArrow (Flip2 p ())
  , Functor (p a b)
  , Functor (p c d)
  )
    => p a b x -> p c d y -> p (a, c) (b, d) ()
f =:% g =
  vd f =:@ vd g


-- | Infix of '(=:%)'.
prv ::
  ( LooseArrow (Flip2 p ())
  , Functor (p a b)
  , Functor (p c d)
  )
    => p a b x -> p c d y -> p (a, c) (b, d) ()
prv =
  (=:%)


-- | Flip of 'prv'.
fpv ::
  ( LooseArrow (Flip2 p ())
  , Functor (p a b)
  , Functor (p c d)
  )
    => p c d y -> p a b x -> p (a, c) (b, d) ()
fpv =
  f' prv


-- | A variant of '(=:@)' specific to the 'Parser' type which ignores the result of the left monad and keeps the result of right monad.
(=:>) ::
  ( Applicative m
  )
    => Parser m a b x -> Parser m c d y -> Parser m (a, c) (b, d) y
P f =:> P g =
  P
    { uP' =
      \ (a, c) ->
        l2 ((,) ==: F p) (f a) (g c)
    }


-- | Prefix version of '(=:>)'.
prR ::
  ( Applicative m
  )
    => Parser m a b x -> Parser m c d y -> Parser m (a, c) (b, d) y
prR =
  (=:>)


-- | Flip of 'prR'.
fpR ::
  ( Applicative m
  )
    => Parser m c d y -> Parser m a b x -> Parser m (a, c) (b, d) y
fpR =
  f' prR


-- | A variant of '(=:@)' specific to the 'Parser' type which ignores the result of the right monad and keeps the result of the left monad.
(=:<) ::
  ( Applicative m
  )
    => Parser m a b x -> Parser m c d y -> Parser m (a, c) (b, d) x
P f =:< P g =
  P
    { uP' =
      \ (a, c) ->
        l2 ((,) ==: p) (f a) (g c)
    }


-- | Prefix version of '(=:<)'.
prL ::
  ( Applicative m
  )
    => Parser m a b x -> Parser m c d y -> Parser m (a, c) (b, d) x
prL =
  (=:<)


-- | Flip of 'prL'
fpL ::
  ( Applicative m
  )
    => Parser m c d y -> Parser m a b x -> Parser m (a, c) (b, d) x
fpL =
  f' prL


-- | Parallelize a monadic arrow with itself.
syM ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p (a, a) (b, b) x
syM =
  mF2 syr


-- | Parallelize across two layers of monadic arrows.
(==@) ::
  ( LooseArrow (Flip2 p x)
  , LooseArrow (Flip2 q y)
  , Functor (Flip (p (a, a')) x)
  )
    => p a (q b c y) x -> p a' (q b' c' y) x -> p (a, a') (q (b, b') (c, c') y) x
(==@) =
  mst (U (=:@)) << (=:@)


-- | Prefix version of '(==@)'.
p2M ::
  ( LooseArrow (Flip2 p x)
  , LooseArrow (Flip2 q y)
  , Functor (Flip (p (a, a')) x)
  )
    => p a (q b c y) x -> p a' (q b' c' y) x -> p (a, a') (q (b, b') (c, c') y) x
p2M =
  (==@)


-- | Flip of 'p2M'.
f2M ::
  ( LooseArrow (Flip2 p x)
  , LooseArrow (Flip2 q y)
  , Functor (Flip (p (a, a')) x)
  )
    => p a' (q b' c' y) x -> p a (q b c y) x -> p (a, a') (q (b, b') (c, c') y) x
f2M =
  f' p2M


-- | A monadic version of '(><:)'.
--
-- Crosses two morphisms.
(><@) ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p c d x -> p (a, c) (d, b) x
(><@) =
  m22 (><:)


-- | Prefix version of '(><@)'.
cxM ::
  ( LooseArrow (Flip2 p x)
  )
    => p a b x -> p c d x -> p (a, c) (d, b) x
cxM =
  (><@)


-- | Flip of 'cxM'.
fXM ::
  ( LooseArrow (Flip2 p x)
  )
    => p c d x -> p a b x -> p (a, c) (d, b) x
fXM =
  f' cxM


-- | A monadic version of the fanout '(-<)'.
(-<@) ::
  ( Arrow (Flip2 p x)
  )
    => p a b x -> p a c x -> p a (b, c) x
(-<@) =
  m22 (-<)


-- | Prefix version of '(-<@)'.
-- Flip of 'foM'.
otM ::
  ( Arrow (Flip2 p x)
  )
    => p a b x -> p a c x -> p a (b, c) x
otM =
  (-<@)


-- | Prefix version of '(-<@)'.
-- Flip of 'otM'.
foM ::
  ( Arrow (Flip2 p x)
  )
    => p a c x -> p a b x -> p a (b, c) x
foM =
  f' otM


-- | A monadic version of 'arr'.
--
-- Converts a normal function to a monadic arrow.
-- For parsers this creates a parser that performs a function and gives the monoid identity as the result.
arM ::
  ( Arrow (Flip2 p x)
  )
    => (a -> b) -> p a b x
arM =
  uF2 < arr


-- | Run a function in parallel with a monadic arrow.
--
-- For parsers this runs a function on a state parallel to the parse.
(^=:) ::
  ( Arrow (Flip2 p x)
  )
    => (a -> b) -> p c d x -> p (a, c) (b, d) x
(^=:) =
  (=:@) < arM


-- | Prefix version of '(^=:)'.
lpr ::
  ( Arrow (Flip2 p x)
  )
    => (a -> b) -> p c d x -> p (a, c) (b, d) x
lpr =
  (^=:)


-- | Flip of 'lpr'.
fLP ::
  ( Arrow (Flip2 p x)
  )
    => p c d x -> (a -> b) -> p (a, c) (b, d) x
fLP =
  f' lpr


-- | Run a function in parallel with a monadic arrow.
--
-- For parsers this runs a function on a state parallel to the parse.
(=:^) ::
  ( Arrow (Flip2 p x)
  )
    => p a b x -> (c -> d) -> p (a, c) (b, d) x
(=:^) =
  (=:@) ^. arM


-- | Prefix version of '(=:^)'.
rpr ::
  ( Arrow (Flip2 p x)
  )
    => p a b x -> (c -> d) -> p (a, c) (b, d) x
rpr =
  (=:^)


-- | Flip of 'rpr'.
fRP ::
  ( Arrow (Flip2 p x)
  )
    => (c -> d) -> p a b x -> p (a, c) (b, d) x
fRP =
  f' rpr
