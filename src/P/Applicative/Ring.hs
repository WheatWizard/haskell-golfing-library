module P.Applicative.Ring
  where

import P.Applicative

-- | An applicative such that if
--
-- @
-- mul_id :: r
-- add_id :: r
-- add_inv :: r -> r
-- add :: r -> r -> r
-- mul :: r -> r -> r
-- @
--
-- is a ring then
--
-- @
-- p mul_id :: f r
-- p add_id :: f r
-- m add_inv :: f r -> f r
-- l2 add :: f r -> f r -> f r
-- ml2 mul :: f r -> f r -> f r
-- @
--
-- is also a ring.
class Applicative f => RingApplicative f where
  ml2 :: (a -> b -> c) -> (f a -> f b -> f c)
