{-# Language GADTs #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language ScopedTypeVariables #-}
{-# Language UndecidableInstances #-}
module P.Permutation
  ( Permutation (..)
  )
  where


import Prelude
  ( Integral
  , show
  , (==)
  )
import qualified Prelude


import P.Algebra.Group
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Magma
import P.Algebra.Monoid
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Ring
import P.Algebra.Semigroup
import P.Algebra.Shelf
import P.Arithmetic.Number
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function.Compose
import P.List.Index
import P.Ord
import P.Reverse
import P.Show
import P.Sort.Class
import P.Vector


{-# Deprecated UsP "Unsafe" #-}
newtype Permutation n a
  = UsP
    { uPr ::
      Vec n a
    }


instance
  ( Show a
  )
    => Show (Permutation n a)
  where
    show =
      sh < tL


instance Foldable (Permutation n)
  where
    foldr func accum =
      rF func accum < uPr
    foldMap func =
      Prelude.foldMap func < uPr


instance
  (
  )
    => Eq1 (Permutation n)
  where
    q1 _ (UsP Ev) (UsP Ev) =
      T
    q1 userEq (UsP (x :++ xs)) (UsP (y :++ ys)) =
      userEq x y <> q1 userEq (UsP xs) (UsP ys)


instance
  ( Eq a
  )
    => Eq (Permutation n a)
  where
    (==) =
      q1 eq


instance
  ( Integral i
  , Number i
  )
    => Magma (Permutation n i)
  where
    (%%%) =
      (<>)


instance
  ( Integral i
  , Number i
  )
    => Semigroup (Permutation n i)
  where
    UsP v1 <> UsP v2 =
      UsP $ m (v1 !) v2


instance
  ( Integral i
  , Number i
  )
    => Monoid (Permutation Zr i)
  where
    mempty =
      UsP Ev


instance
  ( Integral i
  , Number i
  , Monoid (Permutation n i)
  )
    => Monoid (Permutation (Nx n) i)
  where
    mempty =
      UsP $ 0 :++ m (+ 1) (uPr i)


instance
  (
  )
    => Reversable (Permutation n)
  where
    _rv (UsP xs) =
      UsP (_rv xs)



instance
  ( Ord i
  , Integral i
  , Number i
  )
    => Quasigroup (Permutation n i)
  where
    UsP v1 ~/~ UsP v2 =
      UsP $ (v1 !) < (xsr v2 :: Vec n i)
    UsP v1 ~\~ UsP v2 =
      UsP $ (xsr v1 !) < v2


instance
  ( Ord i
  , Integral i
  , Number i
  )
    => Loop (Permutation Zr i)
  where
    lem =
      UsP Ev


instance
  ( Loop (Permutation n i)
  , Ord i
  , Number i
  , Integral i
  )
    => Loop (Permutation (Nx n) i)
  where
    lem =
      UsP $ 0 :++ m (+ 1) (uPr lem)


instance
  ( Loop (Permutation n i)
  , Ord i
  , Integral i
  )
    => Moufang (Permutation n i)
  where
    _iv (UsP vec) =
      UsP $ xsr vec


instance
  ( Integral i
  , Number i
  , Ord i
  )
    => Shelf (Permutation n i)
  where
    x@(UsP xv) <! y =
      x <> y <> UsP (xsr xv)


instance
  ( Integral i
  , Number i
  , Ord i
  )
    => Rack (Permutation n i)
  where
    x !> y@(UsP yv) =
      UsP (xsr yv) <> x <> y


instance
  ( Integral i
  , Number i
  , Ord i
  )
    => Quandle (Permutation n i)


instance
  ( Integral i
  , Number i
  , Ord i
  , Monoid (Permutation n i)
  , Loop (Permutation n i)
  )
    => Group (Permutation n i)
