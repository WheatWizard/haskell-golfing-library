{-# Language PatternSynonyms #-}
{-|
Module :
  P.Deprecated
Description :
  Long form functions for the P library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library consisting of functions from P with longer more descriptive names.
All functions here are deprecated with text providing the shorter name to use.
-}
module P.Deprecated
  (
  -- | This module provides longer, more descriptive, and familiar names for certain functions in the P module.
  -- All the functions are deprecated since shorter versions exist which are preferable.
  --
  -- The intention of this library is to provide a way to check the shorter names of functions while writing code.
  -- Ideally you should be able to write your code using long names as placeholders and have the compiler provide you with the short names via deprecation warnings.
  --
  -- Unfortunately this library is missing many functions it should have, so it can't always be used this way.
  -- Hopefully in future updates this issue will become smaller and smaller.
  --
  -- No function here has a name under 4 bytes.
    sort
  , sortOn
  , sortBy
  , frsB
  -- , snoc
  , cons
  , reverse
  , drop
  , genericDrop
  , tail
  , head
  , init
  , last
  , splitAt
  , genericSplitAt
  , dropWhile
  , permutations
  , intersperse
  , intercalate
  , group
  , groupBy
  , groupWith
  , pattern True
  , pattern False
  , flip
  , find
  , foldr
  , foldl
  , foldr1
  , foldl1
  , foldMap
  , toList
  , liftEq
  , liftEq2
  , compare
  , liftCompare
  , show
  , fzdm
  , zipWith
  , fzd'
  , void
  , string
  , char
  , fxly
  , fxlm
  , fpOp
  , fpOn
  , fqOn
  , fkOn
  , fxOn
  , fyOn
  , print
  , lines
  , unlines
  , words
  , unwords
  , join
  , mapM
  , mapM_
  , liftM
  , liftM2
  , return
  , liftA
  , liftA2
  , liftA3
  , liftM3
  , pure
  , const
  , traverse
  , sequence
  , sequenceA
  , transpose
  , concat
  , mconcat
  , concatMap
  , assoc
  , unassoc
  , swap
  , either
  , isLeft
  , isRight
  , lefts
  , rights
  , length
  , null
  , notNull
  , liftF
  , retract
  , iter
  , foldFree
  , hoistFree
  , manyTill
  , someTill
  , optional
  , asum
  , empty
  , deltas
  , hoistFix
  , foldFix
  , unfoldFix
  , pattern Just
  , pattern Nothing
  , maximum
  , minimum
  , maximumBy
  , minimumBy
  , maximumWith
  , minimumWith
  , maximumBound
  , minimumBound
  , maximumDef
  , minimumDef
  , elem
  , notElem
  , bimap
  , first
  , second
  , hoistCofree
  , member
  , adjust
  , take
  , negate
  , subtract
  , mappend
  , count
  , isControl
  , isSpace
  , isLower
  , isUpper
  , isAlpha
  , isLetter
  , isAlphaNum
  , isPrint
  , isDigit
  , isSymbol
  , toLower
  , toUpper
  , pattern UppercaseLetter
  , pattern LowercaseLetter
  , pattern TitlecaseLetter
  , pattern ModifierLetter
  , pattern OtherLetter
  , pattern NonSpacingMark
  , pattern SpacingCombiningMark
  , pattern DecimalNumber
  , pattern LetterNumber
  , pattern OtherNumber
  , pattern ConnectorPunctuation
  , pattern DashPunctuation
  , pattern OpenPunctuation
  , pattern ClosePunctuation
  , pattern InitialQuote
  , pattern FinalQuote
  , pattern OtherPunctuation
  , pattern MathSymbol
  , pattern CurrencySymbol
  , pattern ModifierSymbol
  , pattern OtherSymbol
  , pattern Space
  , pattern LineSeparator
  , pattern ParagraphSeparator
  , pattern Control
  , pattern Format
  , pattern Surrogate
  , pattern PrivateUse
  , pattern NotAssigned
  , generalCategory
  , even
  , filter
  , unionWith
  , union
  , unions
  , fanout
  , liftParser2
  , lmap
  , rmap
  , dimap
  , cycle
  , putStr
  , putStrLn
  , putChar
  , interact
  , getChar
  , getLine
  , getContents
  , iterate
  , unfoldr
  , unfoldl
  , divMod
  , quotRem
  , fromMaybe
  , foldl'
  , xoW_
  , partition
  , scanl
  , scanr
  , range
  , index
  , inRange
  , rangeSize
  , minBound
  , maxBound
  , none
  , chunkList
  , holes
  , equalLength
  , ltLength
  , leLength
  , compareLength
  , exactLog2
  , atLeast
  , atMost
  , between
  , foldMapA
  , foldA
  , iterateM
  , map
  , mapFst
  , mapSnd
  , foldM
  , foldlM
  , foldrM
  , runState
  , evalState
  , execState
  , modify
  , modifyM
  , withState
  , elemIndices
  , findIndices
  , recip
  , all2
  , any2
  , partitionEithers
  , assocs
  , toAscList
  , indices
  , unzip
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Monoid.Free.Split
import P.Algebra.Ring
import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.AtLeast
import P.Alternative.AtMost
import P.Alternative.Between
import P.Alternative.Many
import P.Alternative.Possible
import P.Alternative.Some
import P.Applicative
import P.Arithmetic
import P.Arithmetic.Floating
import P.Arithmetic.Pattern
import P.Arrow
import P.Assoc
import P.Bifunctor
import P.Bifunctor.Either
import P.Bifunctor.Profunctor
import P.Bool
import P.Char
import P.Char.Category
import P.Comonad.Cofree
import P.Eq
import P.First
import P.Foldable
import P.Foldable.Count
import P.Foldable.Find
import P.Foldable.Length
import P.Foldable.MinMax
import P.Foldable.Unfold
import P.Function
import P.Function.Flip
import P.Functor
import P.Functor.Fix
import P.IO
import P.Ix
import P.Last
import P.List
import P.List.Expandable
import P.List.Group
import P.List.Intersperse
import P.List.Pairs
import P.List.Permutation
import P.List.Split
import P.Map
import P.Map.Class
import P.Maybe
import P.Monad
import P.Monad.Fold
import P.Monad.Free
import P.Monad.Plus.Filter
import P.Monad.State
import P.Ord
import P.Ord.Bounded
import P.Parse
import P.Parse.Combinator
import P.Reverse
import P.Show
import P.Sort.Class
import P.String
import P.Swap
import P.Traversable
import P.Zip
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign


{-# Deprecated transpose "Use tx instead. See also TP." #-}
-- | Long version of 'tx'.
--
-- See also 'P.Matrix.TP'.
transpose ::
  (
  )
    => List (List a) -> List (List a)
transpose =
  tx


{-# Deprecated concat "Use cx, fo or jn instead." #-}
-- | Concatenation.
-- Long version of 'cx', 'fo' or 'jn'.
concat ::
  (
  )
    => List (List a) -> List a
concat =
  jn


{-# Deprecated concatMap "Use cM, mF, vm or fb instead." #-}
-- | Maps and concats.
-- Long version of 'cM', 'mF', 'vm' or 'fb'.
concatMap ::
  (
  )
    => (a -> List b) -> List a -> List b
concatMap =
  cM


{-# Deprecated mconcat "Use fo instead." #-}
-- | Long version of 'fo'.
mconcat ::
  ( Monoid m
  )
    => List m -> m
mconcat =
  fo


{-# Deprecated map "Use m or (<) instead." #-}
-- | 'm' restricted to lists.
map ::
  (
  )
    => (a -> b) -> List a -> List b
map =
  Prelude.fmap


{-# Deprecated mapFst "Use mSt instead." #-}
-- | 'mSt' restricted to @(->)@.
mapFst ::
  (
  )
    => (a -> b) -> (a, c) -> (b, c)
mapFst =
  mSt


{-# Deprecated mapSnd "Use m, (<), or mNd instead." #-}
-- | 'm' restricted to tuples or 'mNd' restricted to @(->)@.
mapSnd ::
  (
  )
    => (a -> b) -> (c, a) -> (c, b)
mapSnd =
  Prelude.fmap
