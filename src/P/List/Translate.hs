{-# Language FlexibleContexts #-}
module P.List.Translate
  ( tr
  , ftr
  , trM
  , frM
  , tI
  , ftI
  , tIM
  , fIM
  , trB
  )
  where


import qualified Prelude
import Prelude
  ( otherwise
  , Integral
  , Int
  )


import P.Aliases
import P.Arithmetic
import P.Category
import P.Char
import P.Foldable.Length.Pattern
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Map.Class
import P.Ord


-- | Indexes a structure using a conversion function.
-- If the index is out of bounds it returns the original input.
trB ::
  ( Indexable f k
  )
    => (a -> k) -> f a -> a -> a
trB conv xs y =
  case
    xs !? conv y
  of
    Ø ->
      y
    [x] ->
      x


-- | Uses the codepoint of the input character to lookup the result in a string.
-- If the index is out of bounds it returns the input character.
--
-- This essentially performs a code page substitution.
--
-- ==== __Examples__
--
-- Get the next character in sequence with by using the codepage shifted by one.
--
-- >>> tr [Ch 1..] 'A'
-- 'B'
-- >>> tr [Ch 1..] '~'
-- '\DEL'
-- >>> tr [Ch 1..] '!'
-- '"'
tr ::
  ( Indexable f Int
  )
    => f Char -> Char -> Char
tr =
  trB (Or :: Char -> Int)


-- | Flip of 'tr'.
ftr ::
  ( Indexable f Int
  )
    => Char -> f Char -> Char
ftr =
  F tr


-- | Uses the same page to 'tr' across an entire string.
--
-- ==== __Examples__
--
-- >>> tr [Ch 1..] "Hello, world!"
-- "Ifmmp-~xpsme\""
trM ::
  ( Functor f
  , Indexable g Int
  )
    => g Char -> f Char -> f Char
trM =
  m < tr


-- | Flip of 'trM'.
frM ::
  ( Functor f
  , Indexable g Int
  )
    => f Char -> g Char -> f Char
frM =
  F trM


-- | Index a structure returning the index if it is out of bounds.
tI ::
  ( Indexable f k
  )
    => f k -> k -> k
tI =
  trB id


-- | Flip of 'tI'.
ftI ::
  ( Indexable f k
  )
    => k -> f k -> k
ftI =
  F tI


-- | Maps 'tI' across a functor using the same structure for every lookup.
tIM ::
  ( Indexable f k
  , Functor g
  )
    => f k -> g k -> g k
tIM =
  m < tI


-- | Flip of 'tIM'.
fIM ::
  ( Indexable f k
  , Functor g
  )
    => g k -> f k -> g k
fIM =
  F tIM
