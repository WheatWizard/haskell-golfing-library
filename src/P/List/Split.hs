{-# Language ViewPatterns #-}
module P.List.Split
  ( sp
  , bk
  , sy
  , sl
  , sY
  , (|/)
  , sY'
  , sYe
  , fYe
  , sYE
  , fYE
  , sYn
  , fYn
  , sYN
  , fYN
  , sL
  , (|\)
  , sYm
  , sYM
  , sLm
  , sLM
  , sA
  , (#=)
  , fsA
  , sps
  , spn
  , spe
  , spN
  , spM
  , snM
  , seM
  , sNM
  -- * Deprecated
  , splitAt
  , genericSplitAt
  )
  where


import Prelude
  ( Integral
  )
import qualified Prelude


import P.Aliases
import P.Arithmetic
import P.Bifunctor
import P.Bool
import P.Enum
import P.Eq
import P.First
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Foldable.Unfold
import P.Monad.Plus.Filter


-- | Takes a predicate and a list giving a tuple containing the longest prefix for which every element satisfies the predicate and the remainder of the list.
--
-- Equivalent to 'Prelude.span'
--
-- ==== __Examples__
--
-- >>> sp (> 3) [8,6,7,2,3,4]
-- ([8,6,7],[2,3,4])
--
sp ::
  ( Uncons f f
  )
    => Predicate a -> f a -> (List a, f a)
sp pred (_uC -> [(x, xs)])
  | pred x
  =
    mst (x :) (sp pred xs)
sp _ xs =
  ( []
  , xs
  )


-- | Takes a predicate and a list giving a tuple containing the longest prefix for which every element fails the predicate and the remainder of the list.
--
-- Equivalent to 'Prelude.break'.
--
-- ==== __Examples__
--
-- >>> bk (<= 3) [8,6,7,2,3,4]
-- ([8,6,7],[2,3,4])
--
bk ::
  ( Uncons f f
  )
    => Predicate a -> f a -> (List a, f a)
bk =
  sp < m n


-- | Similar to 'sp' except it drops the element that passes entirely.
-- This means it takes predicate and a list and returns the longest prefix of elements that don't satisfy the predicate and all the elements after the first element that satisfies the predicate.
--
-- ==== __Examples__
--
-- Combine with 'P.Char.iW' to split off the first word of a string.
--
-- >>> sy iW "Hello I'm a string!"
-- ("Hello", "I'm a string!")
--
sy ::
  ( Uncons f f
  )
    => Predicate a -> f a -> (List a, f a)
sy =
  dr1 <<< bk
  where
    dr1 (_uC -> [(_, xs)]) =
      xs
    dr1 xs =
      xs


-- | Like 'sy' except instead of a predicate it takes a value and finds the first element equal to the value to split at.
sl ::
  ( Eq a
  )
    => a -> List a -> (List a, List a)
sl =
  sy < eq


-- | Takes a predicate and a list and splits at all elements which satisfy the predicate making a list with divisions at the matches.
--
-- ==== __Examples__
--
-- With 'P.Char.iW' this splits a string into a list of words.
-- Analygous to 'P.String.Wr'.
--
-- >>> sY iW "Hello I'm a string!"
-- ["Hello", "I'm", "a", "string!"]
sY ::
  ( Uncons f f
  )
    => Predicate a -> f a -> List (List a)
sY pred =
  go []
  where
    go temp UD =
      [Prelude.reverse temp]
    go temp (_uC -> [(x, xs)])
      | pred x
      =
        Prelude.reverse temp : go [] xs
      | T
      =
        go (x : temp) xs


-- | Infix of 'sY'.
(|/) ::
  ( Uncons f f
  )
    => Predicate a -> f a -> List (List a)
(|/) =
  sY


-- | Takes a predicate and splits at all elements which satisfy the predicate and removing empty lists from the final partition.
--
-- Thus it treats runs of consecutive satisfying elements the same as a single satisfying element.
--
-- ==== __Examples__
--
-- With 'P.Char.iW' this splits a string into a list of words.
-- Analygous to 'P.String.wR'.
--
-- >>> sY' iW "Hello    world!"
-- ["Hello","world!"]
--
sY' ::
  ( Uncons f f
  )
    => Predicate a -> f a -> List (List a)
sY' pred =
  go []
  where
    go [] UD =
      []
    go [] (_uC -> [(x, xs)])
      | pred x
      =
        go [] xs
      | T
      =
        go [x] xs
    go temp UD =
      [Prelude.reverse temp]
    go temp (_uC -> [(x, xs)])
      | pred x
      =
        Prelude.reverse temp : go [] xs
      | T
      =
        go (x : temp) xs


-- | Splits a list along any of a finite list of values.
--
-- ==== __Examples__
--
-- >>> sYe "()" "aa(b)cc"
-- ["aa","b","cc"]
sYe ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => t a -> f a -> List (List a)
sYe =
  sY < fe


-- | Flip of 'sYe'.
fYe ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => f a -> t a -> List (List a)
fYe =
  f' sYe


-- | Splits a list along clusters of values belonging to some finite list.
--
-- ==== __Examples__
--
-- Split words along vowel clusters using 'P.Constants.Language.W5'.
--
-- >>> sYE W5 "comhghairdeas"
-- ["c","mhgh","rd","s"]
--
sYE ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => t a -> f a -> List (List a)
sYE =
  sY' < fe


-- | Flip of 'sYE'.
fYE ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => f a -> t a -> List (List a)
fYE =
  f' sYE


-- | Splits a list along any element not in a finite list of values.
--
-- ==== __Examples__
--
-- >>> sYn W5 "comhghairdeas"
-- ["","o","","","","ai","","ea",""]
--
sYn ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => t a -> f a -> List (List a)
sYn =
  sY < fE


-- | Flip of 'sYn'.
fYn ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => f a -> t a -> List (List a)
fYn =
  f' sYn


-- | Splits a list along clusters of values not belonging to some finite list.
--
-- ==== __Examples__
--
-- Split words along consonant clusters using 'P.Constants.Language.W5'.
--
-- >>> sYN W5 "comhghairdeas"
-- ["o","ai","ea"]
--
sYN ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => t a -> f a -> List (List a)
sYN =
  sY' < fE


-- | Flip of 'sYN'.
fYN ::
  ( Uncons f f
  , Eq a
  , Foldable t
  )
    => f a -> t a -> List (List a)
fYN =
  f' sYN


-- | Like 'sY' except it takes a value instead of a predicate and splits at elements that are equal to the given value.
sL ::
  ( Eq a
  , Uncons f f
  )
    => a -> f a -> List (List a)
sL =
  sY < eq


-- | Infix of 'sL'.
(|\) ::
  ( Eq a
  )
    => a -> List a -> List (List a)
(|\) =
  sL


-- | Split a list along elements matching a predicate, perform a map and then put the list back together.
--
-- ==== __Examples__
--
-- Reverse every word in a string in place.
--
-- >>> sYm rv iW "This is a string with several words"
-- "sihT si a gnirts htiw lareves sdrow"
--
sYm ::
  ( Firstable f f
  )
    => (List a -> List a) -> Predicate a -> f a -> List a
sYm conv pred =
  go []
  where
    go temp (K x xs)
      | pred x
      =
        conv (Prelude.reverse temp) Prelude.++ K x (go [] xs)
      | T
      =
        go (K x temp) xs
    go temp _ =
      conv (Prelude.reverse temp)


-- | Flip of 'sYm'.
sYM ::
  ( Firstable f f
  )
    => Predicate a -> (List a -> List a) -> f a -> List a
sYM =
  F sYm


-- | Split a list along a particular element, perform a map and then put the list back together.
sLm ::
  ( Eq a
  , Firstable f f
  )
    => (List a -> List a) -> a -> f a -> List a
sLm =
  sYm ^. eq


-- | Flip of 'sLm'.
sLM ::
  ( Eq a
  , Firstable f f
  )
    => a -> (List a -> List a) -> f a -> List a
sLM =
  F sLm


{-# Deprecated splitAt, genericSplitAt "Use sA instead" #-}
-- | Long version of 'sA'.
-- Splits a list at an index.
splitAt ::
  ( Integral i
  , Uncons f f
  )
    => i -> f a -> (List a, f a)
splitAt =
  sA


-- | Long version of 'sA'.
-- Splits a list at an index.
genericSplitAt ::
  ( Integral i
  , Uncons f f
  )
    => i -> f a -> (List a, f a)
genericSplitAt =
  sA


-- | Splits a list at an index.
--
-- Equivalent to 'Data.List.genericSplitAt'.
-- More general version of 'Data.List.splitAt'
sA ::
  ( Integral i
  , Uncons f f
  )
    => i -> f a -> (List a, f a)
sA n xs
  | n Prelude.<= 0
  =
    ( []
    , xs
    )
sA n (_uC -> [(x, xs)]) =
  mst (x :) (sA (Pv n) xs)
sA _ xs =
  ( []
  , xs
  )


-- | Infix version of 'sA'.
(#=) ::
  ( Integral i
  , Uncons f f
  )
    => i -> f a -> (List a, f a)
(#=) =
  sA


-- | Flip of 'sA'.
fsA ::
  ( Integral i
  , Uncons f f
  )
    => f a -> i -> (List a, f a)
fsA =
  F sA


-- | Gives all the ways to split a list into two pieces.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sPs'.
--
-- ==== __Examples__
--
-- >>> sps "Hello"
-- [("","Hello"),("H","ello"),("He","llo"),("Hel","lo"),("Hell","o"),("Hello","")]
--
sps ::
  ( Uncons f f
  )
    => f a -> List (List a, f a)
sps xs@(_uC -> [(x, xs')]) =
  ([], xs) : m2t (x:) (sps xs')
sps x =
  [ ([], x) ]


-- | Maps a function across all ways to split a list in two pieces.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.mPs'.
--
spM ::
  ( Uncons f f
  )
    => (List a -> f a -> b) -> f a -> List b
spM =
  (<<% sps)


-- | Maps a function across all ways to split a list with a non-empty prefix.
--
-- Ordered in ascending size of prefix.
--
-- alternative to 'p.list.mpn'.
--
snM ::
  ( Uncons f f
  )
    => (List a -> f a -> b) -> f a -> List b
snM =
  (<<% spn)


-- | Maps a function across all ways to split a list with a non-empty suffix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.mPe'.
--
seM ::
  ( Uncons f f
  )
    => (List a -> f a -> b) -> f a -> List b
seM =
  (<<% spe)


-- | Maps a function across all ways to split a list into two non-empty parts.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.mPN'.
--
sNM ::
  ( Uncons f f
  )
    => (List a -> f a -> b) -> f a -> List b
sNM =
  (<<% spN)


-- | Gives all the ways to split a list with a non-empty prefix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sPn'.
--
spn ::
  ( Uncons f f
  )
    => f a -> List (List a, f a)
spn (_uC -> [(x, xs)]) =
  m2t (x:) (([], xs) : spn xs)
spn _ =
  []


-- | Gives all the ways to split a list with a non-empty suffix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sPe'.
--
spe ::
  ( Uncons f f
  )
    => f a -> List (List a, f a)
spe xs@(_uC -> [(x, xs')]) =
  ([], xs) : m2t (x:) (spe xs')
spe _ =
  []


-- | Gives all the ways to split a list into two non-empty sections.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sPN'.
--
spN ::
  ( Uncons f f
  )
    => f a -> List (List a, f a)
spN (_uC -> [(x, xs@(_uC -> _:_))]) =
  m2t (x:) (([], xs) : spN xs)
spN _ =
  []
