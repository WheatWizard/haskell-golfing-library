{-|
Module :
  P.List.Extra
Description :
  Additional functions for the P.List library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.List library.
-}
module P.List.Extra
  ( ak3
  , ak4
  , ak5
  , ak6
  , ak7
  , bw1
  , bw2
  , bw3
  , bw4
  , bw5
  , bw6
  , bw7
  , bw8
  , bw9
  , bw0
  )
  where


import P.Aliases
import P.List


-- | Takes 3 lists and creates a new list alternating elements between the three.
ak3 ::
  (
  )
    => List a -> List a -> List a -> List a
ak3 [] ys zs =
  ak ys zs
ak3 (x : xs) ys zs =
  x : ak3 ys zs xs


-- | Takes 4 lists and creates a new list alternating elements between the four.
ak4 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a
ak4 [] ys zs ws =
  ak3 ys zs ws
ak4 (x : xs) ys zs ws =
  x : ak4 ys zs ws xs


-- | Takes 5 lists and creates a new list alternating elements between the five.
ak5 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a
ak5 [] ys zs ws vs =
  ak4 ys zs ws vs
ak5 (x : xs) ys zs ws vs =
  x : ak5 ys zs ws vs xs


-- | Takes 6 lists and creates a new list alternating elements between the six.
ak6 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a -> List a
ak6 [] ys zs ws vs us =
  ak5 ys zs ws vs us
ak6 (x : xs) ys zs ws vs us =
  x : ak6 ys zs ws vs us xs


-- | Takes 7 lists and creates a new list alternating elements between the six.
ak7 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a -> List a -> List a
ak7 [] ys zs ws vs us ts =
  ak6 ys zs ws vs us ts
ak7 (x : xs) ys zs ws vs us ts =
  x : ak7 ys zs ws vs us xs ts


-- | Insert a value between the 1st and 2nd values of a list.
bw1 ::
  (
  )
    => a -> List a -> List a
bw1 =
  iBw (1 :: Int)


-- | Insert a value between the 2nd and 3rd values of a list.
bw2 ::
  (
  )
    => a -> List a -> List a
bw2 =
  iBw (2 :: Int)


-- | Insert a value between the 3rd and 4th values of a list.
bw3 ::
  (
  )
    => a -> List a -> List a
bw3 =
  iBw (3 :: Int)


-- | Insert a value between the 4th and 5th values of a list.
bw4 ::
  (
  )
    => a -> List a -> List a
bw4 =
  iBw (4 :: Int)


-- | Insert a value between the 5th and 6th values of a list.
bw5 ::
  (
  )
    => a -> List a -> List a
bw5 =
  iBw (5 :: Int)


-- | Insert a value between the 6th and 7th values of a list.
bw6 ::
  (
  )
    => a -> List a -> List a
bw6 =
  iBw (6 :: Int)


-- | Insert a value between the 7th and 8th values of a list.
bw7 ::
  (
  )
    => a -> List a -> List a
bw7 =
  iBw (7 :: Int)


-- | Insert a value between the 8th and 9th values of a list.
bw8 ::
  (
  )
    => a -> List a -> List a
bw8 =
  iBw (8 :: Int)


-- | Insert a value between the 9th and 10th values of a list.
bw9 ::
  (
  )
    => a -> List a -> List a
bw9 =
  iBw (9 :: Int)


-- | Insert a value between the 10th and 11th values of a list.
bw0 ::
  (
  )
    => a -> List a -> List a
bw0 =
  iBw (10 :: Int)
