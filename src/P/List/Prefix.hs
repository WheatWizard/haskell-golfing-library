module P.List.Prefix
  ( lpw
  , lpb
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Monoid.Free.Prefix
import P.Bool
import P.Eq
import P.First
import P.Function
import P.Function.Compose


-- | Longest common prefix of two lists using a user defined comparison on the elements.
--
-- ==== __Examples__
--
-- >>> lpw (>=) [9..] [0,2..]
-- [9,10,11,12,13,14,15,16,17,18]
--
lpw ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> List a
lpw =
  lPw < fqO he


-- | Longest common prefix of two lists using a substitution function.
--
-- ==== __Examples__
--
-- Get the longest prefix with matching parity
--
-- >>> lpb od [1,8,9,7,6] [1..]
-- [1,8,9]
--
lpb ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> List a
lpb =
  lpw < qb
