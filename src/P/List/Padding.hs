module P.List.Padding
  ( lpW
  , rpW
  , lLW
  , rLW
  , lpp
  , rpp
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , Int
  , (-)
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Bool
import P.Category
import P.Enum
import P.Foldable
import P.Foldable.Length
import P.Foldable.MinMax
import P.Function.Compose
import P.Ord
import P.Zip.WeakAlign
import P.Zip


-- | Left pads a list to a certain size using a particular value.
--
-- Does nothing if the list is already larger than the size.
lpW ::
  ( Integral i
  , Ord i
  )
    => a -> i -> List a -> List a
lpW padding size list =
  rl (size - l list) padding <> list


-- | Right pads a list to a certain size using a particular value.
--
-- Does nothing if the list is already larger than the size.
rpW ::
  ( Integral i
  , Ord i
  )
    => a -> i -> List a -> List a
rpW padding =
  go
  where
    go size xs
      | 0 >= size
      =
        xs
    go size (x : xs) =
      x : go (Pv size) xs
    go size [] =
      rl size padding


-- | Left pads a list with a particular value to the size of another list.
--
-- Does nothing if the list is already larger than the size.
--
-- Unlike 'rLW' this is not lazy on the size of the list and requires fully evaluating both lists.
lLW ::
  (
  )
    => a -> List b -> List a -> List a
lLW =
  fm lg < lpW


-- | Right pads a list with a particular value to the size of another list.
--
-- Does nothing if the list is already larger than the size.
--
-- Unlike 'lLW' this is lazy on the size of the lists.
rLW ::
  (
  )
    => a -> List b -> List a -> List a
rLW v ys [] =
  v Prelude.<$ ys
rLW v (_ : ys) (x : xs) =
  x : rLW v ys xs


-- | Left pads all elements of a list to be the same length using a custom padding value.
--
-- Not lazy on elements of the list.
lpp ::
  (
  )
    => a -> List (List a) -> List (List a)
lpp v xs =
  lpW v (xMl xs :: Int) < xs


-- | Right pads all elements of a list to be the same length using a custom padding value.
--
-- Lazy on elements of the list.
rpp ::
  (
  )
    => a -> List (List a) -> List (List a)
rpp v xs
  | mF ø xs
  =
    xs
  | mF nø xs
  =
    zW (:) (Prelude.head < xs) $ rpp v $ Prelude.tail < xs
  | T
  =
    rpp v $ cnR [v] < xs

