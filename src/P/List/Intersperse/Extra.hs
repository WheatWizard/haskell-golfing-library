{-|
Module :
  P.List.Intersperse.Extra
Description :
  Additional functions for interspersing lists
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.List.Interspserse library.
-}
{-# Language PatternSynonyms #-}
module P.List.Intersperse.Extra
  where


import qualified Prelude
import Prelude
  ( Integral
  , String
  )


import P.Aliases
import P.Eq
import P.List.Intersperse


-- | Intersperse a list with 0s.
pattern Is0 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is0 xs =
  Is 0 xs


-- | Intersperse a list with 1s.
pattern Is1 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is1 xs =
  Is 1 xs


-- | Intersperse a list with 2s.
pattern Is2 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is2 xs =
  Is 2 xs


-- | Intersperse a list with 3s.
pattern Is3 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is3 xs =
  Is 3 xs


-- | Intersperse a list with 4s.
pattern Is4 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is4 xs =
  Is 4 xs


-- | Intersperse a list with 5s.
pattern Is5 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is5 xs =
  Is 5 xs


-- | Intersperse a list with 6s.
pattern Is6 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is6 xs =
  Is 6 xs


-- | Intersperse a list with 7s.
pattern Is7 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is7 xs =
  Is 7 xs


-- | Intersperse a list with 8s.
pattern Is8 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is8 xs =
  Is 8 xs


-- | Intersperse a list with 9s.
pattern Is9 ::
  ( Eq i
  , Integral i
  )
    => List i -> List i
pattern Is9 xs =
  Is 9 xs


-- | Intersperse a string with @a@s.
pattern ISa ::
  (
  )
    => String -> String
pattern ISa xs =
  Is 'a' xs


-- | Intersperse a string with @b@s.
pattern ISb ::
  (
  )
    => String -> String
pattern ISb xs =
  Is 'b' xs


-- | Intersperse a string with @c@s.
pattern ISc ::
  (
  )
    => String -> String
pattern ISc xs =
  Is 'c' xs


-- | Intersperse a string with @d@s.
pattern ISd ::
  (
  )
    => String -> String
pattern ISd xs =
  Is 'd' xs


-- | Intersperse a string with @e@s.
pattern ISe ::
  (
  )
    => String -> String
pattern ISe xs =
  Is 'e' xs


-- | Intersperse a string with @f@s.
pattern ISf ::
  (
  )
    => String -> String
pattern ISf xs =
  Is 'f' xs


-- | Intersperse a string with @g@s.
pattern ISg ::
  (
  )
    => String -> String
pattern ISg xs =
  Is 'g' xs


-- | Intersperse a string with @h@s.
pattern ISh ::
  (
  )
    => String -> String
pattern ISh xs =
  Is 'h' xs


-- | Intersperse a string with @i@s.
pattern ISi ::
  (
  )
    => String -> String
pattern ISi xs =
  Is 'i' xs


-- | Intersperse a string with @j@s.
pattern ISj ::
  (
  )
    => String -> String
pattern ISj xs =
  Is 'j' xs


-- | Intersperse a string with @k@s.
pattern ISk ::
  (
  )
    => String -> String
pattern ISk xs =
  Is 'k' xs


-- | Intersperse a string with @l@s.
pattern ISl ::
  (
  )
    => String -> String
pattern ISl xs =
  Is 'l' xs


-- | Intersperse a string with @m@s.
pattern ISm ::
  (
  )
    => String -> String
pattern ISm xs =
  Is 'm' xs


-- | Intersperse a string with @n@s.
pattern ISn ::
  (
  )
    => String -> String
pattern ISn xs =
  Is 'n' xs


-- | Intersperse a string with @o@s.
pattern ISo ::
  (
  )
    => String -> String
pattern ISo xs =
  Is 'o' xs


-- | Intersperse a string with @p@s.
pattern ISp ::
  (
  )
    => String -> String
pattern ISp xs =
  Is 'p' xs


-- | Intersperse a string with @q@s.
pattern ISq ::
  (
  )
    => String -> String
pattern ISq xs =
  Is 'q' xs


-- | Intersperse a string with @r@s.
pattern ISr ::
  (
  )
    => String -> String
pattern ISr xs =
  Is 'r' xs


-- | Intersperse a string with @s@s.
pattern ISs ::
  (
  )
    => String -> String
pattern ISs xs =
  Is 's' xs


-- | Intersperse a string with @t@s.
pattern ISt ::
  (
  )
    => String -> String
pattern ISt xs =
  Is 't' xs


-- | Intersperse a string with @u@s.
pattern ISu ::
  (
  )
    => String -> String
pattern ISu xs =
  Is 'u' xs


-- | Intersperse a string with @v@s.
pattern ISv ::
  (
  )
    => String -> String
pattern ISv xs =
  Is 'v' xs


-- | Intersperse a string with @w@s.
pattern ISw ::
  (
  )
    => String -> String
pattern ISw xs =
  Is 'w' xs


-- | Intersperse a string with @x@s.
pattern ISx ::
  (
  )
    => String -> String
pattern ISx xs =
  Is 'x' xs


-- | Intersperse a string with @y@s.
pattern ISy ::
  (
  )
    => String -> String
pattern ISy xs =
  Is 'y' xs


-- | Intersperse a string with @z@s.
pattern ISz ::
  (
  )
    => String -> String
pattern ISz xs =
  Is 'z' xs


-- | Intersperse a string with @A@s.
pattern ISA ::
  (
  )
    => String -> String
pattern ISA xs =
  Is 'A' xs


-- | Intersperse a string with @B@s.
pattern ISB ::
  (
  )
    => String -> String
pattern ISB xs =
  Is 'B' xs


-- | Intersperse a string with @C@s.
pattern ISC ::
  (
  )
    => String -> String
pattern ISC xs =
  Is 'C' xs


-- | Intersperse a string with @D@s.
pattern ISD ::
  (
  )
    => String -> String
pattern ISD xs =
  Is 'D' xs


-- | Intersperse a string with @E@s.
pattern ISE ::
  (
  )
    => String -> String
pattern ISE xs =
  Is 'E' xs


-- | Intersperse a string with @F@s.
pattern ISF ::
  (
  )
    => String -> String
pattern ISF xs =
  Is 'F' xs


-- | Intersperse a string with @G@s.
pattern ISG ::
  (
  )
    => String -> String
pattern ISG xs =
  Is 'G' xs


-- | Intersperse a string with @H@s.
pattern ISH ::
  (
  )
    => String -> String
pattern ISH xs =
  Is 'H' xs


-- | Intersperse a string with @I@s.
pattern ISI ::
  (
  )
    => String -> String
pattern ISI xs =
  Is 'I' xs


-- | Intersperse a string with @J@s.
pattern ISJ ::
  (
  )
    => String -> String
pattern ISJ xs =
  Is 'J' xs


-- | Intersperse a string with @K@s.
pattern ISK ::
  (
  )
    => String -> String
pattern ISK xs =
  Is 'K' xs


-- | Intersperse a string with @L@s.
pattern ISL ::
  (
  )
    => String -> String
pattern ISL xs =
  Is 'L' xs


-- | Intersperse a string with @M@s.
pattern ISM ::
  (
  )
    => String -> String
pattern ISM xs =
  Is 'M' xs


-- | Intersperse a string with @N@s.
pattern ISN ::
  (
  )
    => String -> String
pattern ISN xs =
  Is 'N' xs


-- | Intersperse a string with @O@s.
pattern ISO ::
  (
  )
    => String -> String
pattern ISO xs =
  Is 'O' xs


-- | Intersperse a string with @P@s.
pattern ISP ::
  (
  )
    => String -> String
pattern ISP xs =
  Is 'P' xs


-- | Intersperse a string with @Q@s.
pattern ISQ ::
  (
  )
    => String -> String
pattern ISQ xs =
  Is 'Q' xs


-- | Intersperse a string with @R@s.
pattern ISR ::
  (
  )
    => String -> String
pattern ISR xs =
  Is 'R' xs


-- | Intersperse a string with @S@s.
pattern ISS ::
  (
  )
    => String -> String
pattern ISS xs =
  Is 'S' xs


-- | Intersperse a string with @T@s.
pattern IST ::
  (
  )
    => String -> String
pattern IST xs =
  Is 'T' xs


-- | Intersperse a string with @U@s.
pattern ISU ::
  (
  )
    => String -> String
pattern ISU xs =
  Is 'U' xs


-- | Intersperse a string with @V@s.
pattern ISV ::
  (
  )
    => String -> String
pattern ISV xs =
  Is 'V' xs


-- | Intersperse a string with @W@s.
pattern ISW ::
  (
  )
    => String -> String
pattern ISW xs =
  Is 'W' xs


-- | Intersperse a string with @X@s.
pattern ISX ::
  (
  )
    => String -> String
pattern ISX xs =
  Is 'X' xs


-- | Intersperse a string with @Y@s.
pattern ISY ::
  (
  )
    => String -> String
pattern ISY xs =
  Is 'Y' xs


-- | Intersperse a string with @Z@s.
pattern ISZ ::
  (
  )
    => String -> String
pattern ISZ xs =
  Is 'Z' xs


-- | Intersperse a string with @_@s.
pattern IS_ ::
  (
  )
    => String -> String
pattern IS_ xs =
  Is '_' xs


-- | Intersperse a string with @'@s.
pattern IS' ::
  (
  )
    => String -> String
pattern IS' xs =
  Is '\'' xs
