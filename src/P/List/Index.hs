module P.List.Index
  ( (!)
  , ix_
  , im
  , fim
  , (>!)
  , ri
  , fri
  , nim
  , fnm
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , String
  )


import P.Aliases
import P.Arithmetic
import P.Arithmetic.Number
import P.Arithmetic.Pattern
import P.Category
import P.Enum
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Functor.Compose
import P.Monad


-- | Indexes a list.
--
-- Takes the modulo of the index by the length of the list,
-- so that indices that would be out of bounds still give results.
--
-- It tries to index once and only tries the modulo if that fails,
-- so this will work if you try to index an infinite list with a non-negative number.
--
-- Indexing an empty list causes an error.
(!) ::
  ( Integral i
  , Number i
  , Foldable t
  )
    => t a -> i -> a
(!) =
  ix_ "(!) called on an empty structure"


-- | Index a list.
--
-- Gives a custom error message when trying to index an empty list.
--
-- For internal use.
ix_ ::
  ( Integral i
  , Number i
  , Foldable t
  )
    => String -> t a -> i -> a
ix_ errorM =
  go
  where
    go list index@N =
      go list (index % l list)
    go list index =
      case
        list !! index
      of
        [ result ] ->
          result
        _ ->
          if
            l list == 0
          then
            Prelude.error errorM
          else
            (Prelude.!!) (tL list) $
            Prelude.fromInteger $
            Prelude.toInteger $
            index % l list


-- | Takes a list of indexes and a list to index and produces a list of results.
--
-- Uses '(!)' for indexing so out of bounds indices will wrap around.
--
-- Errors when the list to be indexed is empty.
--
-- ==== __Examples__
--
-- >>> im [0,1,-2,0,4] [1,2,3]
-- [1,2,2,1,1]
im ::
  ( Integral i
  , Number i
  )
    => List i -> List a -> List a
im =
  f' fim


-- | Flip of 'im'.
fim ::
  ( Integral i
  , Number i
  )
    => List a -> List i -> List a
fim =
  m < (!)


-- | Infix of 'im' and 'fim'.
(>!) ::
  ( Integral i
  , Number i
  )
    => List a -> List i -> List a
(>!) =
  fim


-- | Removes the value at a particular index.
ri ::
  ( Integral i
  , Number i
  )
    => i -> List a -> List a
ri _ [] =
  []
ri 0 (_ : xs) =
  xs
ri n (x : xs) =
  x : ri (Pv n) xs


-- | Flip of 'ri'.
fri ::
  ( Integral i
  , Number i
  )
    => List a -> i -> List a
fri =
  f' ri


-- | Return all elements of a list not present at the indexes provided in a second list.
--
-- Effectively removes all elements at the given indexes.
nim ::
  ( Integral i
  , Number i
  , Foldable f
  )
    => List a -> f i -> List a
nim =
  lF' fri


-- | Flip of 'nim'.
--
-- To remove 2 or fewer fixed indexes it is often cheaper to use the pre-made functions from @P.Foldable.Index.Extra'.
--
-- @
-- fnm[1,5]
-- ri1<ri5
-- @
--
-- @
-- fnm[0,9,10]
-- tl<ri9<ri0
-- @
--
fnm ::
  ( Integral i
  , Number i
  , Foldable f
  )
    => f i -> List a -> List a
fnm =
  f' nim
