{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.List.Chop.Extra
Description :
  Additional functions for the P.List.Chop library
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for parsing that have been moved to this file to reduce clutter in the main P.List.Chop library.
-}
module P.List.Chop.Extra
  ( pattern Wx1
  , pattern Wx2
  , pattern WX2
  , pattern Wx3
  , pattern WX3
  , pattern Wx4
  , pattern WX4
  , pattern Wx5
  , pattern WX5
  , pattern Wx6
  , pattern WX6
  , pattern Wx7
  , pattern WX7
  , pattern Wx8
  , pattern WX8
  , pattern Wx9
  , pattern WX9
  , pattern Wx0
  , pattern WX0
  , pattern WX1
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , Int
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Foldable.Length
import P.Function.Compose
import P.List.Chop


unWx ::
  (
  )
    => Int -> (List (List a)) -> Maybe' (List a)
unWx n [] =
  [[]]
unWx n [x]
  | leL n x
  =
    [x]
unWx n (x : xs)
  | eL n x
  =
    (x <>) < unWx n xs
unWx _ _ =
  []


unWX ::
  (
  )
    => Int -> (List (List a)) -> Maybe' (List a)
unWX n [] =
  [[]]
unWX n (x : xs)
  | eL n x
  =
    (x <>) < unWX n xs
unWX _ _ =
  []


-- | 'wx' 1
pattern Wx1 ::
  (
  )
    => List a -> List (List a)
pattern Wx1 x <- (unWx 1 -> [x]) where
  Wx1 =
    wx (1 :: Int)


-- | 'wx' 2
pattern Wx2 ::
  (
  )
    => List a -> List (List a)
pattern Wx2 x <- (unWx 2 -> [x]) where
  Wx2 =
    wx (2 :: Int)


-- | 'wX' 2
pattern WX2 ::
  (
  )
    => List a -> List (List a)
pattern WX2 x <- (unWX 2 -> [x]) where
  WX2 =
    wX (2 :: Int)


-- | 'wx' 3
pattern Wx3 ::
  (
  )
    => List a -> List (List a)
pattern Wx3 x <- (unWx 3 -> [x]) where
  Wx3 =
    wx (3 :: Int)


-- | 'wX' 3
pattern WX3 ::
  (
  )
    => List a -> List (List a)
pattern WX3 x <- (unWX 3 -> [x]) where
  WX3 =
    wX (3 :: Int)


-- | 'wx' 4
pattern Wx4 ::
  (
  )
    => List a -> List (List a)
pattern Wx4 x <- (unWx 4 -> [x]) where
  Wx4 =
    wx (4 :: Int)


-- | 'wX' 4
pattern WX4 ::
  (
  )
    => List a -> List (List a)
pattern WX4 x <- (unWX 4 -> [x]) where
  WX4 =
    wX (4 :: Int)


-- | 'wx' 5
pattern Wx5 ::
  (
  )
    => List a -> List (List a)
pattern Wx5 x <- (unWx 5 -> [x]) where
  Wx5 =
    wx (5 :: Int)


-- | 'wX' 5
pattern WX5 ::
  (
  )
    => List a -> List (List a)
pattern WX5 x <- (unWX 5 -> [x]) where
  WX5 =
    wX (5 :: Int)


-- | 'wx' 6
pattern Wx6 ::
  (
  )
    => List a -> List (List a)
pattern Wx6 x <- (unWx 6 -> [x]) where
  Wx6 =
    wx (6 :: Int)


-- | 'wX' 6
pattern WX6 ::
  (
  )
    => List a -> List (List a)
pattern WX6 x <- (unWX 6 -> [x]) where
  WX6 =
    wX (6 :: Int)


-- | 'wx' 7
pattern Wx7 ::
  (
  )
    => List a -> List (List a)
pattern Wx7 x <- (unWx 7 -> [x]) where
  Wx7 =
    wx (7 :: Int)


-- | 'wX' 7
pattern WX7 ::
  (
  )
    => List a -> List (List a)
pattern WX7 x <- (unWX 7 -> [x]) where
  WX7 =
    wX (7 :: Int)


-- | 'wx' 8
pattern Wx8 ::
  (
  )
    => List a -> List (List a)
pattern Wx8 x <- (unWx 8 -> [x]) where
  Wx8 =
    wx (8 :: Int)


-- | 'wX' 8
pattern WX8 ::
  (
  )
    => List a -> List (List a)
pattern WX8 x <- (unWX 8 -> [x]) where
  WX8 =
    wX (8 :: Int)


-- | 'wx' 9
pattern Wx9 ::
  (
  )
    => List a -> List (List a)
pattern Wx9 x <- (unWx 9 -> [x]) where
  Wx9 =
    wx (9 :: Int)


-- | 'wX' 9
pattern WX9 ::
  (
  )
    => List a -> List (List a)
pattern WX9 x <- (unWX 9 -> [x]) where
  WX9 =
    wX (9 :: Int)


-- | 'wx' 10
pattern Wx0 ::
  (
  )
    => List a -> List (List a)
pattern Wx0 x <- (unWx 10 -> [x]) where
  Wx0 =
    wx (10 :: Int)


-- | 'wX' 10
pattern WX0 ::
  (
  )
    => List a -> List (List a)
pattern WX0 x <- (unWX 10 -> [x]) where
  WX0 =
    wX (10 :: Int)


-- | 'wX' 11
pattern WX1 ::
  (
  )
    => List a -> List (List a)
pattern WX1 x <- (unWX 11 -> [x]) where
  WX1 =
    wX (11 :: Int)

