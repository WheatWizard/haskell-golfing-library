{-# Language FlexibleContexts #-}
module P.List.Expandable
  ( Expandable (..)
  , fis
  , fic
  , rs'
  , ls'
  , pE'
  -- * Deprecated
  , intersperse
  , intercalate
  )
  where


import qualified Prelude


import qualified Data.List


import P.Aliases
import P.Alternative
import P.Applicative
import P.Applicative.Unpure
import P.Arrow
import P.Bifunctor.Either
import P.Bifunctor.Flip
import P.Category
import P.Function.Compose
import P.Function.Flip
import P.Functor.Compose


-- | A structure which is locally like a list and can be expanded like a concat map.
class
  ( Functor f
  )
    => Expandable f
  where
    {-# MINIMAL (vx | vm), (is | ic) #-}
    -- | Expand a list in place with the element at each location.
    --
    -- Generalized version of 'Prelude.concat'.
    vx :: f (List a) -> f a
    vx =
      vm id

    -- | Expand a list in place applying a function to each element.
    --
    -- Generalized version of 'Prelude.concatMap'
    vm :: (a -> List b) -> f a -> f b
    vm =
      vx << m


    -- | Take an element and add it between consecutive elements of a structure.
    --
    -- More general version of 'Data.Listintersperse'.
    --
    -- ==== __Examples__
    --
    -- >>> is ','"Hello"
    -- "H,e,l,l,o"
    -- >>> is 1 [3,4,5]
    -- [3,1,4,1,5]
    is :: a -> f a -> f a
    is =
      (ic ^. m (:[])) < (:[])

    -- | Intersperse and then concat.
    --
    -- More general version of to 'Data.List.intercalate'.
    ic :: List a -> f (List a) -> f a
    ic =
      vx << is


{-# Deprecated intersperse "Use is instead" #-}
-- | Long version of 'is'.
intersperse ::
  (
  )
    => a -> List a -> List a
intersperse =
  is


{-# Deprecated intercalate "Use ic instead" #-}
-- | Long version of 'ic'.
intercalate ::
  (
  )
    => List a -> List (List a) -> List a
intercalate =
  ic


-- | Flip of 'is'.
fis ::
  ( Expandable f
  )
    => f a -> a -> f a
fis =
  f' is

-- | Flip of 'ic'.
fic ::
  ( Expandable f
  )
    => f (List a) -> List a -> f a
fic =
  f' ic


-- | A version of 'rts' which operates on 'Expandable's.
--
-- ==== __Examples__
--
-- On 'P.Bifunctor.Either':
-- >>> rs' [p "s", Lef (), Le (), p "x"]
-- ["s","x"]
--
-- On 'P.Bifunctor.These':
-- >>> rs' [Te () "s", Ti (), Ta "x"]
-- ["x"]
rs' ::
  ( Expandable f
  , Unpure g
  )
    => f (g a) -> f a
rs' =
  vm unp


-- | A version of 'lfs' which operates on 'Expandable's.
--
-- ==== __Examples__
--
-- On 'P.Bifunctor.Either':
-- >>> ls' [Lef "s", p (), p (), Le "x"]
-- ["s","x"]
--
-- On 'P.Bifunctor.These':
-- >>> ls' [Te "s" (), Ta (), Ti "x"]
-- ["x"]
ls' ::
  ( Expandable f
  , Unpure (Flip g b)
  )
    => f (g a b) -> f a
ls' =
  vm upF


-- | A version of 'pEi' which operates on 'Expandable's.
pE' ::
  ( Expandable f
  , Unpure (g a)
  , Unpure (Flip g b)
  )
    => f (g a b) -> (f a, f b)
pE' =
  ls' -< rs'


instance
  (
  )
    => Expandable []
  where
    vx =
      Prelude.concat

    vm =
      Prelude.concatMap

    is =
      Data.List.intersperse


instance
  ( Functor f
  , Expandable g
  )
    => Expandable (Comp f g)
  where
    vx (Co s) =
      Co (vx < s)

    vm f (Co s) =
      Co (vm f < s)

    is i (Co s) =
      Co (is i < s)

    ic i (Co s) =
      Co (ic i < s)
