{-# Language BangPatterns #-}
{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
{-# Language FlexibleContexts #-}
module P.List.Fenceposts
  ( Fenceposts (..)
  , azp
  , faz
  , AltList (..)
  , dep
  , dhp
  , ep
  , hp
  , po
  )
  where


import qualified Prelude
import Prelude
  ( Show
  )


import Data.Foldable
import Control.Applicative


import P.Algebra.Monoid
import P.Aliases
import P.Bifunctor
import P.Bifunctor.Either
import P.Bifunctor.Flip
import P.Bifunctor.These
import P.Bool
import P.Category
import P.Eq
import P.First
import P.Foldable hiding
  ( foldMap
  , foldl'
  )
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Ord
import P.Reverse
import P.Scan
import P.Zip
import P.Zip.Bi.WeakAlign
import P.Zip.Bi.SemiAlign
import P.Zip.Bi.Align
import P.Zip.Bi.Zip
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A list alternating between two types of values.
-- Used internally for implementing 'Fenceposts', use 'Comp List ((,) a)' for practical purposes.
data AltList a b
  = Ending
  | Fence b (Fenceposts a b)
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


-- | Zips two lists together into an alternating list.
azp ::
  (
  )
    => List a -> List b -> AltList b a
azp [] _ =
  Ending
azp _ [] =
  Ending
azp (x : xs) (y : ys) =
  Fence x $ Post y $ azp xs ys


-- | Flip of 'azp'.
faz ::
  (
  )
    => List a -> List b -> AltList a b
faz =
  f' azp


-- | Fencepost lists are like lists with "post" values tagging the spaces in between the "fence" values.
-- There is always one more of the post values than the fence values.
data Fenceposts a b
  = Post a (AltList a b)
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


-- | Convert a fencepost list to a list of tuples by removing the last post.
dep ::
  (
  )
    => Fenceposts a b -> [(a, b)]
dep (Post _ Ending) =
  []
dep (Post a (Fence x xs)) =
  (a, x) : dep xs


-- | Converts a fencepost list to a list of tuples by removing the head post.
dhp ::
  (
  )
    => Fenceposts a b -> [(b, a)]
dhp (Post _ Ending) =
  []
dhp (Post _ (Fence x xs@(Post b _))) =
  (x, b) : dhp xs


-- | Get the end post of a fencepost list.
ep ::
  (
  )
    => Fenceposts a b -> a
ep (Post a Ending) =
  a
ep (Post _ (Fence _ xs)) =
  ep xs


-- | Get the head post of a fencepost list.
hp ::
  ( Uncons (Flip f b) g
  )
    => f a b -> a
hp =
  he < Flp


-- | Get the posts of a fencepost list as a list.
--
-- To get the fence values of a fencepost list use 'tL'.
po ::
  ( Foldable (Flip f b)
  )
    => f a b -> List a
po =
  tL < Flp


instance
  (
  )
    => Functor (AltList a)
  where
    fmap f (Fence x xs) =
      Fence (f x) $ f < xs
    fmap f Ending =
      Ending


instance
  (
  )
    => Functor (Fenceposts a)
  where
    fmap f (Post a xs) =
      Post a $ f < xs


instance
  (
  )
    => Functor (Flip AltList a)
  where
    fmap f (Flp (Fence x xs)) =
      Flp $ Fence x $ mst f xs
    fmap f (Flp Ending) =
      Flp Ending


instance
  (
  )
    => Functor (Flip Fenceposts a)
  where
    fmap f (Flp (Post a xs)) =
      Flp (Post (f a) (mst f xs))


instance
  ( Monoid a
  )
    => WeakAlign (AltList a)
  where
    alL Ending ys =
      Rit < ys
    alL xs Ending =
      Lef < xs
    alL (Fence x xs) (Fence _ ys) =
      Fence (Lef x) $ alL xs ys


instance
  ( Monoid a
  )
    => WeakAlign (Fenceposts a)
  where
    alL (Post a xs) (Post b ys) =
      Post (a <> b) $ alL xs ys


instance
  ( Monoid a
  )
    => SemiAlign (AltList a)
  where
    aln Ending ys =
      Ta < ys
    aln xs Ending =
      Ti < xs
    aln (Fence x xs) (Fence y ys) =
      Fence (Te x y) $ aln xs ys


instance
  ( Monoid a
  )
    => SemiAlign (Fenceposts a)
  where
    aln (Post a xs) (Post b ys) =
      Post (a <> b) $ aln xs ys


instance
  (
  )
    => WeakBialign AltList
  where
    cLL Ending ys =
      ys
    cLL xs Ending =
      xs
    cLL (Fence x xs) (Fence y ys) =
      Fence x (cLL xs ys)

    cRR Ending ys =
      ys
    cRR xs Ending =
      xs
    cRR (Fence x xs) (Fence y ys) =
      Fence y (cRR xs ys)

    cLR Ending ys =
      ys
    cLR xs Ending =
      xs
    cLR (Fence x xs) (Fence y ys) =
      Fence x (cLR xs ys)

    cRL Ending ys =
      ys
    cRL xs Ending =
      xs
    cRL (Fence x xs) (Fence y ys) =
      Fence y (cRL xs ys)


instance
  (
  )
    => WeakBialign Fenceposts
  where
    cLL (Post a xs) (Post _ ys) =
      Post a (cLL xs ys)

    cRR (Post _ xs) (Post b ys) =
      Post b (cRR xs ys)

    cLR (Post _ xs) (Post b ys) =
      Post b (cLR xs ys)

    cRL (Post a xs) (Post _ ys) =
      Post a (cRL xs ys)


instance
  (
  )
    => SemiBialign AltList
  where
    bal Ending ys =
      bm Ta Ta ys
    bal xs Ending =
      bm Ti Ti xs
    bal (Fence x xs) (Fence y ys) =
      Fence (Te x y) $ bal xs ys


instance
  (
  )
    => SemiBialign Fenceposts
  where
    bal (Post a xs) (Post b ys) =
      Post (Te a b) $ bal xs ys


instance
  (
  )
    => Bizip AltList
  where
    zW2 _ _ Ending _ =
      Ending
    zW2 _ _ _ Ending =
      Ending
    zW2 f g (Fence x xs) (Fence y ys) =
      Fence (g x y) $ zW2 f g xs ys


instance
  (
  )
    => Bizip Fenceposts
  where
    zW2 f g (Post a xs) (Post b ys) =
      Post (f a b) $ zW2 f g xs ys


instance
  ( Monoid a
  )
    => Zip (AltList a)
  where
    zW _ Ending _ =
      Ending
    zW _ _ Ending =
      Ending
    zW f (Fence x xs) (Fence y ys) =
      Fence (f x y) $ zW f xs ys


instance
  ( Monoid a
  )
    => Zip (Fenceposts a)
  where
    zW f (Post a xs) (Post b ys) =
      Post (a <> b) $ zW f xs ys


instance
  ( Monoid a
  )
    => UnitalZip (Fenceposts a)
  where
    aId =
      Post i Ending


instance
  (
  )
    => Foldable (AltList a)
  where
    foldMap _ Ending =
      i
    foldMap f (Fence x xs) =
      f x <> (foldMap f xs)

    foldl' f !s Ending =
      s
    foldl' f !s (Fence x xs) =
      foldl' f (f s x) xs


instance
  (
  )
    => Foldable (Fenceposts a)
  where
    foldMap f (Post _ xs) =
      foldMap f xs

    foldl' f !s (Post _ xs) =
      foldl' f s xs


instance
  (
  )
    => Foldable (Flip AltList a)
  where
    foldMap _ (Flp Ending) =
      i
    foldMap f (Flp (Fence _ xs)) =
      foldMap f $ Flp xs

    foldl' f !s (Flp Ending) =
      s
    foldl' f !s (Flp (Fence _ xs)) =
      foldl' f s $ Flp xs


instance
  (
  )
    => Foldable (Flip Fenceposts a)
  where
    foldMap f (Flp (Post a xs)) =
      f a <> (foldMap f $ Flp xs)

    foldl' f !s (Flp (Post a xs)) =
      foldl' f (f s a) $ Flp xs


instance
  ( Eq a
  )
    => Eq1 (Fenceposts a)
  where
    q1 =
      q2 eq


instance
  ( Eq a
  )
    => Eq1 (Flip Fenceposts a)
  where
    q1 =
      q2 eq


instance
  (
  )
    => Eq2 AltList
  where
    q2 _ _ Ending Ending =
      T
    q2 f g (Fence x xs) (Fence y ys) =
      g x y <> q2 f g xs ys
    q2 _ _ _ _ =
      B


instance
  (
  )
    => Eq2 Fenceposts
  where
    q2 f g (Post a xs) (Post b ys) =
      f a b <> q2 f g xs ys


instance
  (
  )
    => Scan (AltList a)
  where
    sc _ _ Ending =
      Ending
    sc f s (Fence x xs) =
      let
        s' =
          f s x
      in
        Fence s' $ sc f s' xs


instance
  (
  )
    => Scan (Fenceposts a)
  where
    sc f s (Post a xs) =
      Post a $ sc f s xs


instance
  (
  )
    => Scan (Flip AltList a)
  where
    sc _ _ (Flp Ending) =
      Flp Ending
    sc f s (Flp (Fence b xs)) =
      Flp $ Fence b $ uFl $ sc f s $ Flp xs


instance
  (
  )
    => Scan (Flip Fenceposts a)
  where
    sc f s (Flp (Post a xs)) =
      let
        s' =
          f s a
      in
        Flp $ Post s' $ uFl $ sc f s' $ Flp xs


instance
  (
  )
    => Uncons (Fenceposts a) (Fenceposts a)
  where
    _uC (Post _ Ending) =
      []
    _uC (Post _ (Fence x xs)) =
      [(x, xs)]

    tl (Post _ (Fence _ xs)) =
      xs
    tl xs =
      xs


instance
  (
  )
    => Uncons (Flip Fenceposts a) (Flip Fenceposts a)
  where
    _uC (Flp (Post _ Ending)) =
      []
    _uC (Flp (Post x (Fence _ xs))) =
      [(x, Flp xs)]

    tl (Flp (Post _ (Fence _ xs))) =
      Flp xs
    tl xs =
      xs

    he (Flp (Post x _)) =
      x


-- | Fails the law because reversing an infinite fencepost list does not halt.
instance
  (
  )
    => Reversable (Fenceposts a)
  where
    _rv (Post a xs)=
      go (Post a Ending) xs
      where
        go !ys Ending =
          ys
        go !ys (Fence z (Post b zs)) =
          go (Post b (Fence z ys)) zs


-- | Fails the law because reversing an infinite fencepost list does not halt.
instance
  (
  )
    => Reversable (Flip Fenceposts a)
  where
    _rv =
      Flp < _rv < uFl
