module P.List.Infix
  where


import qualified Prelude
import Prelude
  ( Integer
  )
import qualified Data.Function


import P.Aliases
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Prefix
import P.Bool
import P.Category
import P.Eq
import P.First
import P.Function.Compose
import P.Function.Flip
import P.Functor.Compose
import P.Foldable.Length
import P.Foldable.MinMax
import P.List
import P.List.Find
import P.List.Group
import P.List.Suffix
import P.Monad
import P.Monad.Plus.Filter


-- | Gives all non-empty contiguous substrings of a given string.
--
-- === __Examples__
--
-- >>> cg $ 1 ## 3
-- [[1],[2],[1,2],[3],[2,3],[1,2,3]]
cg ::
  (
  )
    => List a -> List (List a)
cg =
  (tl < px) +> (tl < sX)


-- | Gives all contiguous substrings of a given string.
--
-- === __Examples__
--
-- >>> cG $ 1 ## 3
-- [[],[1],[2],[1,2],[3],[2,3],[1,2,3]]
cG ::
  (
  )
    => List a -> List (List a)
cG =
  (i :) < cg


-- | Get all contiguous substrings satisfying a predicate.
cSt ::
  (
  )
    => Predicate (List a) -> List a -> List (List a)
cSt pred =
  fl pred < cG


-- | Flip of 'cSt'.
fcS ::
  (
  )
    => List a -> Predicate (List a) -> List (List a)
fcS =
  f' cSt


-- | Takes two lists and determines whether the first list is an infix of the second.
--
-- Equivalent to 'Data.List.isInfixOf'.
iw ::
  ( Eq a
  )
    => List a -> List a -> Bool
iw =
  iwW (==)


-- | Infix version of 'iw'.
(%=) ::
  ( Eq a
  )
    => List a -> List a -> Bool
(%=) =
  iw


-- | Flip of 'iw'.
fiw ::
  ( Eq a
  )
    => List a -> List a -> Bool
fiw =
  F iw

-- | Negation of 'iw'.
niw ::
  ( Eq a
  )
    => List a -> List a -> Bool
niw =
  mm Prelude.not iw

-- | Works like 'iw' but with a user defined equality function.
--
-- ==== __Order Notation__
-- Over the two strings separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
iwW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> Bool
iwW _ [] _ =
  T
iwW userComp ifx list =
  nø $ faw userComp ifx list


-- | Flip of 'iwW'.
fIW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a -> Bool
fIW =
  F iwW


-- | Works like 'iwW' but with a slightly more general type but worse performance on large strings.
--
-- ==== __Order Notation __
-- Over the two strings separately it is \(O(\lambda m n.mn)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n^2)\) time.
iWW ::
  (
  )
    => (a -> b -> Bool) -> List a -> List b -> Bool
iWW _ [] [] =
  T
iWW _ (_ : _) [] =
  B
iWW userComp ifx (x : xs) =
  swW (q1 $ f' userComp) (x : xs) ifx || iWW userComp ifx xs


-- | Flip of 'iWW'.
fWW ::
  (
  )
    => List a -> (a -> b -> Bool) -> List b -> Bool
fWW =
  f' iWW


-- | Works like 'iw' but with a substitution function mapped across both inputs.
iWB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> Bool
iWB =
  Data.Function.on iw < m

-- | Flip of 'iWB'.
fIB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a -> Bool
fIB =
  F iWB

-- | Gives the largest infix where all elements satisfy a certain predicate.
--
-- When two infixes are of the same length it gives the earlier one.
ih ::
  (
  )
    => Predicate a -> List a -> List a
ih =
  xBL << gF

-- | Flip of 'ih'.
fih ::
  (
  )
    => List a -> Predicate a -> List a
fih =
  F ih


-- | Gives the longest infix satisfying a predicate.
--
-- When two infixes are of the same length it gives the earlier one.
-- Produces an error when no infix satisfies the predicate.
liS ::
  (
  )
    => Predicate (List a) -> List a -> List a
liS =
  xBl << cSt


-- | Flip of 'liS'
fLS ::
  (
  )
    => List a -> Predicate (List a) -> List a
fLS =
  f' liS
