{-|
Module :
  P.List.Pairs
Description :
  Functions operating on pairs of elements drawn from a list
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library for functions operating on pairs of elements drawn from a list.
Most of these operate on all the pairs of consecutive elements.
-}
module P.List.Pairs
  ( pa
  , (%%)
  , fpa
  , paf
  , paF
  , pA
  , pac
  , pax
  , fpx
  , apa
  , fap
  , nap
  , fna
  , epa
  , fep
  , nep
  , fne
  , δ
  , δ'
  , paq
  , pnq
  , pam
  , pmx
  , pmn
  , xQ
  , xX
  , paS
  , fPs
  , pAS
  , fPS
  -- * Deprecated
  , deltas
  )
  where


import qualified Prelude


import P.Algebra.Ring
import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.List
import P.Ord
import P.Ord.Bounded


-- | Applies a function to consecutive elements of a list.
--
-- ==== __Examples__
--
-- @pa (-)@ calculates the descending differences or the difference between each element and the previous.
--
-- >>> pa (-) [1,2,4,2,1]
-- [-1,-2,2,1]
--
-- @pa (,)@ gives all pairs of consecutive elements.
-- This is the same as 'pA'.
--
-- >>> pa (,) [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pa ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
pa func =
  go < tL
  where
    go (x1 : x2 : xs) =
      func x1 x2 : go (x2 : xs)
    go _ =
      []


-- | Infix of 'pa'.
(%%) ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
(%%) =
  pa


-- | Flip of 'pa'.
fpa ::
  ( Foldable f
  )
    => f a -> (a -> a -> b) -> List b
fpa =
  f' pa


-- | 'pa' but using a flipped version of the given function.
--
-- ==== __Examples__
--
-- When calculating the consecutive difference between elements 'pa' will give the descending difference:
--
-- >>> pa (-) [1,3..9]
-- [-2,-2,-2,-2]
--
-- If we want the ascending difference then we need to flip the @(-)@.
--
-- >> paf (-) [1,3..9]
-- [2,2,2,2]
--
paf ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
paf =
  pa < f'


-- | Flip of 'paf'.
paF ::
  ( Foldable f
  )
    => f a -> (a -> a -> b) -> List b
paF =
  f' paf


-- | Gives all pairs of consecutive elements in a list.
--
-- ==== __Examples__
--
-- >>> pA [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pA ::
  (
  )
    => List a -> List (a, a)
pA =
  pa (,)


-- | Gives the contour of a list.
--
-- Compares consecutive elements of a list with 'cp'.
pac ::
  ( Ord a
  , Foldable f
  )
    => f a -> List Ordering
pac =
  pa cp


-- | Map over pairs of consecutive elements of a list and concat.
pax ::
  ( Foldable f
  )
    => (a -> a -> List a) -> f a -> List a
pax =
  fo << pa


-- | Flip of 'pax'.
fpx ::
  ( Foldable f
  )
    => f a -> (a -> a -> List a) -> List a
fpx =
  f' pax


-- | Determine if all pairs of consecutive elements of a list satisfy a predicate.
--
-- More generally performs a pair map and then folds the results with a monoid action.
--
-- ==== __Examples__
--
-- Determine if each element starts with the previous element as a prefix:
--
-- >>> apa fsw ["a", "abc", "abcxy"]
-- True
-- >>> apa fsw ["a", "abc", "abxy"]
-- False
--
-- Determine if the length of each element is double the previous:
--
-- >>> apa (eq<db.*l) ["a", "abc", "abxy"]
-- False
-- >>> apa (eq<db.*l) ["a", "ab", "abxy"]
-- True
--
apa ::
  ( Foldable f
  , Monoid m
  )
    => (a -> a -> m) -> f a -> m
apa =
  fo << pa


-- | Flip of 'apa'.
fap ::
  ( Foldable f
  , Monoid m
  )
    => f a -> (a -> a -> m) -> m
fap =
  f' apa


-- | Negation of 'apa'.
nap ::
  ( Foldable f
  )
    => (a -> a -> Bool) -> f a -> Bool
nap =
  n << apa


-- | Flip of 'nap'.
fna ::
  ( Foldable f
  )
    => f a -> (a -> a -> Bool) -> Bool
fna =
  f' nap


-- | Determine if any pair of consecutive elements satisfies a predicate.
--
-- More generally, maps a function over all pairs of consecutive elements and gives the maximum result.
--
-- ==== __Examples__
--
-- Determine if any element starts with the previous element as a prefix:
--
-- >>> epa fsw ["a", "abc", "abxy"]
-- True
-- >>> epa fsw ["ax", "abcx", "abxy"]
-- False
--
-- Determine if the length of any element is double the previous:
--
-- >>> epa (eq<db.*l) ["a", "ab", "abxy"]
-- True
-- >>> epa (eq<db.*l) ["ae", "a", "abxy"]
-- False
--
epa ::
  ( Foldable f
  , MinBounded b
  )
    => (a -> a -> b) -> f a -> b
epa =
  or << pa


-- | Flip of 'epa'.
fep ::
  ( Foldable f
  , MinBounded b
  )
    => f a -> (a -> a -> b) -> b
fep =
  f' epa


-- | Negation of 'epa'.
nep ::
  ( Foldable f
  )
    => (a -> a -> Bool) -> f a -> Bool
nep =
  n << epa


-- | Flip of 'nep'.
fne ::
  ( Foldable f
  )
    => f a -> (a -> a -> Bool) -> Bool
fne =
  f' nep


{-# Deprecated deltas "Use δ instead" #-}
-- | Gives the deltas of a list.
-- Long version of δ.
deltas ::
  ( Ring i
  )
    => List i -> List i
deltas =
  δ


-- | Gives the deltas of a list.
δ ::
  ( Ring i
  , Foldable f
  )
    => f i -> List i
δ =
  paf (Prelude.-)


-- | Gives the absolute difference between consecutive elements of a list.
δ' ::
  ( Ring i
  , Foldable f
  )
    => f i -> List i
δ' =
  paf (mm Prelude.abs (Prelude.-))


-- | Compares consecutive elements of a list with 'eq'.
paq ::
  ( Eq a
  , Foldable f
  )
    => f a -> List Bool
paq =
  pa eq


-- | Compares consecutive elements of a list with 'nq'.
pnq ::
  ( Eq a
  , Foldable f
  )
    => f a -> List Bool
pnq =
  pa nq


-- | Combines consecutive elements with the 'Semigroup' action.
pam ::
  ( Semigroup a
  , Foldable f
  )
    => f a -> List a
pam =
  pa mp


-- | Gets the maximum of each pair of consecutive elements.
--
-- For booleans this gets the logical or of consecutive elements.
pmx ::
  ( Ord a
  , Foldable f
  )
    => f a -> List a
pmx =
  pa ma


-- | Gets the minimum of each pair of consecutive elements.
--
-- For booleans this gets the logical and of consecutive elements.
pmn ::
  ( Ord a
  , Foldable f
  )
    => f a -> List a
pmn =
  pa mN


-- | Takes a function and applies to all pairs in which the first element occurs before the second.
--
-- For a commutative function this prevents duplication.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a version that also includes applying the function to every element and itself, see 'xX'.
--
-- For a version that just applies to all pairs see 'P.Applicative.l2'.
--
-- ==== __Examples__
--
-- Use it to get all numbers with exactly two 1s in their binary representation.
--
-- >>> xQ pl $ m (2^) [0..]
-- [3,6,5,12,9,10,17,24,33,18,65,20,129,34,257,48,513,66,1025,36...
--
xQ ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
xQ func =
  go < tL
  where
    go [] =
      []
    go (x : xs) =
      m (func x) xs :% go xs


-- | Takes a function and applies to all pairs in which the first element occurs at or before the second.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a commutative function this prevents duplication.
--
-- For a version which does not apply a function to each element and itself see 'xQ'.
xX ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
xX func =
  go < tL
  where
    go [] =
      []
    go (x : xs) =
      m (func x) (x : xs) :% go xs


-- | Apply a function to all pairs of consecutive elements satisfying a predicate.
paS ::
  ( Foldable f
  )
    => (a -> a -> Bool) -> (a -> a -> b) -> f a -> List b
paS pred func =
  go < tL
  where
    go (x1 : x2 : xs)
      | pred x1 x2
      =
        func x1 x2 : go (x2 : xs)
      | Prelude.otherwise
      =
        go (x2 : xs)
    go _ =
      []


-- | Flip of 'paS'.
fPs ::
  ( Foldable f
  )
    => (a -> a -> b) -> (a -> a -> Bool) -> f a -> List b
fPs =
  f' paS


-- | Get all pairs satisfying some predicate.
pAS ::
  (
  )
    => (a -> a -> Bool) -> List a -> List (a, a)
pAS =
  fPs (,)


-- | Flip of 'pAS'
fPS ::
  (
  )
    => List a -> (a -> a -> Bool) -> List (a, a)
fPS =
  f' pAS
