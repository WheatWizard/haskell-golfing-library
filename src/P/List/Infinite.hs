{-|
Module :
  P.List.Infinite
Description :
  Explicitly infinite lists
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Infinite lists and helper functions.
In general regular lists are prefered over explicitly infinite lists since they are more flexible.
However there are some uses for an explicitly infinite structure and the existence of Cofree already creates them so they are supported here.
-}
{-# Language PatternSynonyms #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language TypeSynonymInstances #-}
{-# Language MultiParamTypeClasses #-}
module P.List.Infinite
  ( IList
  , pattern (:..)
  )
  where


import qualified Prelude
import Prelude
  ( otherwise
  , (-)
  , Integral
  )


import P.Applicative.Unpure
import P.Comonad.Cofree
import P.Enum
import P.First
import P.Function.Compose
import P.Function.Flip
import P.Functor.Identity
import P.List.Expandable
import P.Map.Class
import P.Ord
import P.Show


infixr 5 :..


-- | An infinite list type made from 'Cofree'.
--
-- Since bottom exists it is not technically necessary that an 'IList' actually contain an infinite number of elements.
-- It might be more accurate to say that they are "endless" lists.
type IList =
  Cofree Ident


instance
  ( Show a
  )
    => Show (IList a)
  where
    show =
      F Prelude.showList "" < go
      where
        go (x :.. xs) =
          x : go xs


instance
  (
  )
    => Uncons IList IList
  where
    _uC (x :.. xs) =
      [(x, xs)]


instance
  (
  )
    => Firstable IList IList
  where
    cons =
      (:..)


instance
  ( Integral i
  , Ord i
  )
    => Indexable IList i
  where
    mr key _ =
      0 <= key

    iList !? key
      | nmr key iList
      =
        []
      | otherwise
      =
        go iList key
      where
        go (x :.. _) 0 =
          [x]
        go (_ :.. xs) n =
          go xs (Pv n)

    ajt key conv iList
      | nmr key iList
      =
        iList
      | otherwise
      =
        go key iList
      where
        go 0 (x :.. xs) =
          conv x :.. xs
        go n (x :.. xs) =
          x :.. go (Pv n) xs


-- | A utility pattern for infinite lists.
-- Allows us to avoid pattern matching on the identity functor.
--
-- As a function it adds an element to the front of an infinite list.
pattern (:..) ::
  (
  )
    => a -> IList a -> IList a
pattern x :.. xs =
    x :>> Pu xs

