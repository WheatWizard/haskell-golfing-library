{-|
Module :
  P.List.Intersperse
Description :
  Functions for interspersing elements in a list
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.List.Intersperse
  ( pattern Is
  , is1
  , fi1
  , is2
  , fi2
  , is3
  , fi3
  )
  where


import qualified Prelude


import qualified Data.List


import P.Aliases
import P.Alternative
import P.Applicative
import P.Bool
import P.Eq
import P.First
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.List
import P.List.Comparison
import P.List.Expandable
import P.Monad
import P.Ord


-- | Internal helper function for the 'Is' pattern.
_unIs ::
  ( Eq a
  )
    => List a -> Maybe' (a, List a)
_unIs xs =
  let
    (ys, zs) =
      uak xs
  in
    if
      lq zs || zs |=| ys
    then
      []
    else
      [(he zs, ys)]


-- | 'is' as a pattern.
pattern Is ::
  ( Eq a
  )
    => a -> List a -> List a
pattern Is x xs <- (_unIs -> [(x, xs)]) where
  Is =
    is


-- | Take an element and a structure and place a copy of that element before each element in the structure.
--
-- ===== __Examples__
--
-- >>> is1 1 [3,4,5]
-- [1,3,1,4,1,5]
is1 ::
  ( Alternative m
  , Monad m
  )
    => a -> m a -> m a
is1 =
  fMB μ < cs


-- | Flip of 'is1'.
fi1 ::
  ( Alternative m
  , Monad m
  )
    => m a -> a -> m a
fi1 =
  f' is1


-- | Take an element and a structure and place a copy of that element after each element in the structure
--
-- ===== __Examples__
--
-- >>> is1 1 [3,4,5]
-- [3,1,4,1,5,1]
is2 ::
  ( Alternative m
  , Monad m
  )
    => a -> m a -> m a
is2 =
  fMB μ < fec


-- | Flip of 'is2'.
fi2 ::
  ( Alternative m
  , Monad m
  )
    => m a -> a -> m a
fi2 =
  f' is2


-- | Take an element and a structure and place copies before and after the structure as well as in-between elements in the structure.
--
-- ===== __Examples__
--
-- >>> is3 1 [3,4,5]
-- [1,3,1,4,1,5,1]
--
is3 ::
  ( Alternative m
  , Monad m
  )
    => a -> m a -> m a
is3 =
  cs **< is2


-- | Flip of 'is3'.
fi3 ::
  ( Alternative m
  , Monad m
  )
    => m a -> a -> m a
fi3 =
  f' is3
