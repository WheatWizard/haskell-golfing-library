{-|
Module :
  P.List.Split.Extra
Description :
  Additional functions for the P.List.Split library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for parsing that have been moved to this file to reduce clutter in the main P.List.Split library.
-}
module P.List.Split.Extra where


import P.Aliases
import P.First
import P.List.Split


-- | Shortcut for 'sA 1'.
sA1 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA1 =
  sA (1 :: Int)


-- | Shortcut for 'sA 2'.
sA2 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA2 =
  sA (2 :: Int)


-- | Shortcut for 'sA 3'.
sA3 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA3 =
  sA (3 :: Int)


-- | Shortcut for 'sA 4'.
sA4 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA4 =
  sA (4 :: Int)


-- | Shortcut for 'sA 5'.
sA5 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA5 =
  sA (5 :: Int)


-- | Shortcut for 'sA 6'.
sA6 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA6 =
  sA (6 :: Int)


-- | Shortcut for 'sA 7'.
sA7 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA7 =
  sA (7 :: Int)


-- | Shortcut for 'sA 8'.
sA8 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA8 =
  sA (8 :: Int)


-- | Shortcut for 'sA 9'.
sA9 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA9 =
  sA (9 :: Int)


-- | Shortcut for 'sA 10'.
sA0 ::
  ( Uncons f f
  )
    => f a -> (List a, f a)
sA0 =
  sA (10 :: Int)

