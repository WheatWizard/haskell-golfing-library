{-|
Module : P.Parse.Extra
Description :
  Additional functions for the P.Parse library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for parsing that have been moved to this file to reduce clutter in the main P.Parse library.
Mostly this is a large collection of parsers for a specific character.
-}
module P.Parse.Extra
  where


import qualified Prelude
import Prelude
  ( Char
  , String
  , Integral
  )


import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Alternative.Some
import P.Applicative
import P.Function.Compose
import P.Monad
import P.Parse
import P.Parse.Combinator


-- | Parses a single digit and returns it as a string.
dgS ::
  (
  )
    => Parser List String String String
dgS =
  p < dg


-- | Parses at least one digit.
dg' ::
  (
  )
    => Parser List String String String
dg' =
  so dg


-- | Parses any number of digits.
dg_ ::
  (
  )
    => Parser List String String String
dg_ =
  my dg


-- | Parses a single alphabetic character and returns it as a string.
afS ::
  (
  )
    => Parser List String String String
afS =
  p < af


-- | Parses at least one alphabetic character.
af' ::
  (
  )
    => Parser List String String String
af' =
  so af


-- | Parses any number of alphabetic characters.
af_ ::
  (
  )
    => Parser List String String String
af_ =
  my af


-- | Parses a single whitespace character and returns it as a string.
p_S ::
  (
  )
    => Parser List String String String
p_S =
  p < p_


-- | Parses at least one whitespace character.
p_' ::
  (
  )
    => Parser List String String String
p_' =
  so p_


-- | Parses any number of whitespace characters.
p__ ::
  (
  )
    => Parser List String String String
p__ =
  my p_


-- | Parses a single control character and returns it as a string.
pcS ::
  (
  )
    => Parser List String String String
pcS =
  p < pc


-- | Parses at least one control character.
pc' ::
  (
  )
    => Parser List String String String
pc' =
  so pc


-- | Parses any number of control characters.
pc_ ::
  (
  )
    => Parser List String String String
pc_ =
  my pc


-- | Parses a single lowercase character and returns it as a string.
pLS ::
  (
  )
    => Parser List String String String
pLS =
  p < pL


-- | Parses at least one lowercase character.
pL' ::
  (
  )
    => Parser List String String String
pL' =
  so pL


-- | Parses any number of lowercase characters.
pL_ ::
  (
  )
    => Parser List String String String
pL_ =
  my pL


-- | Parses a single uppercase character and returns it as a string.
pUS ::
  (
  )
    => Parser List String String String
pUS =
  p < pU


-- | Parses at least one uppercase character.
pU' ::
  (
  )
    => Parser List String String String
pU' =
  so pU


-- | Parses any number of uppercase characters.
pU_ ::
  (
  )
    => Parser List String String String
pU_ =
  my pU


-- | Parses a single alpha-numeric character and returns it as a string.
aNS ::
  (
  )
    => Parser List String String String
aNS =
  p < aN


-- | Parses at least one alpha-numeric character.
aN' ::
  (
  )
    => Parser List String String String
aN' =
  so aN


-- | Parses any number of alpha-numeric characters.
aN_ ::
  (
  )
    => Parser List String String String
aN_ =
  my aN


-- | Parses a single printable character and returns it as a string.
pRS ::
  (
  )
    => Parser List String String String
pRS =
  p < pR


-- | Parses at least one printable character.
pR' ::
  (
  )
    => Parser List String String String
pR' =
  so pR


-- | Parses any number of printable characters.
pR_ ::
  (
  )
    => Parser List String String String
pR_ =
  my pR


-- | Parses a single @a@ character.
--
-- Shorthand for @'χ' \'a\'@.
χa ::
  (
  )
    => Parser List String String Char
χa =
  χ 'a'

-- | Parses a single @a@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"a\"@.
ʃa ::
  (
  )
    => Parser List String String String
ʃa =
  ʃ "a"


-- | Parses a single @b@ character.
--
-- Shorthand for @'χ' \'b\'@.
χb ::
  (
  )
    => Parser List String String Char
χb =
  χ 'b'

-- | Parses a single @b@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"b\"@.
ʃb ::
  (
  )
    => Parser List String String String
ʃb =
  ʃ "b"


-- | Parses a single @c@ character.
--
-- Shorthand for @'χ' \'c\'@.
χc ::
  (
  )
    => Parser List String String Char
χc =
  χ 'c'

-- | Parses a single @c@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"c\"@.
ʃc ::
  (
  )
    => Parser List String String String
ʃc =
  ʃ "c"


-- | Parses a single @d@ character.
--
-- Shorthand for @'χ' \'d\'@.
χd ::
  (
  )
    => Parser List String String Char
χd =
  χ 'd'

-- | Parses a single @d@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"d\"@.
ʃd ::
  (
  )
    => Parser List String String String
ʃd =
  ʃ "d"


-- | Parses a single @e@ character.
--
-- Shorthand for @'χ' \'e\'@.
χe ::
  (
  )
    => Parser List String String Char
χe =
  χ 'e'

-- | Parses a single @e@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"e\"@.
ʃe ::
  (
  )
    => Parser List String String String
ʃe =
  ʃ "e"


-- | Parses a single @f@ character.
--
-- Shorthand for @'χ' \'f\'@.
χf ::
  (
  )
    => Parser List String String Char
χf =
  χ 'f'

-- | Parses a single @f@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"f\"@.
ʃf ::
  (
  )
    => Parser List String String String
ʃf =
  ʃ "f"


-- | Parses a single @g@ character.
--
-- Shorthand for @'χ' \'g\'@.
χg ::
  (
  )
    => Parser List String String Char
χg =
  χ 'g'

-- | Parses a single @g@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"g\"@.
ʃg ::
  (
  )
    => Parser List String String String
ʃg =
  ʃ "g"


-- | Parses a single @h@ character.
--
-- Shorthand for @'χ' \'h\'@.
χh ::
  (
  )
    => Parser List String String Char
χh =
  χ 'h'

-- | Parses a single @h@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"h\"@.
ʃh ::
  (
  )
    => Parser List String String String
ʃh =
  ʃ "h"


-- | Parses a single @i@ character.
--
-- Shorthand for @'χ' \'i\'@.
χi ::
  (
  )
    => Parser List String String Char
χi =
  χ 'i'

-- | Parses a single @i@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"i\"@.
ʃi ::
  (
  )
    => Parser List String String String
ʃi =
  ʃ "i"


-- | Parses a single @j@ character.
--
-- Shorthand for @'χ' \'j\'@.
χj ::
  (
  )
    => Parser List String String Char
χj =
  χ 'j'

-- | Parses a single @j@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"j\"@.
ʃj ::
  (
  )
    => Parser List String String String
ʃj =
  ʃ "j"


-- | Parses a single @k@ character.
--
-- Shorthand for @'χ' \'k\'@.
χk ::
  (
  )
    => Parser List String String Char
χk =
  χ 'k'

-- | Parses a single @k@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"k\"@.
ʃk ::
  (
  )
    => Parser List String String String
ʃk =
  ʃ "k"


-- | Parses a single @l@ character.
--
-- Shorthand for @'χ' \'l\'@.
χl ::
  (
  )
    => Parser List String String Char
χl =
  χ 'l'

-- | Parses a single @l@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"l\"@.
ʃl ::
  (
  )
    => Parser List String String String
ʃl =
  ʃ "l"


-- | Parses a single @m@ character.
--
-- Shorthand for @'χ' \'m\'@.
χm ::
  (
  )
    => Parser List String String Char
χm =
  χ 'm'

-- | Parses a single @m@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"m\"@.
ʃm ::
  (
  )
    => Parser List String String String
ʃm =
  ʃ "m"


-- | Parses a single @n@ character.
--
-- Shorthand for @'χ' \'n\'@.
χn ::
  (
  )
    => Parser List String String Char
χn =
  χ 'n'

-- | Parses a single @n@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"n\"@.
ʃn ::
  (
  )
    => Parser List String String String
ʃn =
  ʃ "n"


-- | Parses a single @o@ character.
--
-- Shorthand for @'χ' \'o\'@.
χo ::
  (
  )
    => Parser List String String Char
χo =
  χ 'o'

-- | Parses a single @o@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"o\"@.
ʃo ::
  (
  )
    => Parser List String String String
ʃo =
  ʃ "o"


-- | Parses a single @p@ character.
--
-- Shorthand for @'χ' \'p\'@.
χp ::
  (
  )
    => Parser List String String Char
χp =
  χ 'p'

-- | Parses a single @p@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"p\"@.
ʃp ::
  (
  )
    => Parser List String String String
ʃp =
  ʃ "p"


-- | Parses a single @q@ character.
--
-- Shorthand for @'χ' \'q\'@.
χq ::
  (
  )
    => Parser List String String Char
χq =
  χ 'q'

-- | Parses a single @q@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"q\"@.
ʃq ::
  (
  )
    => Parser List String String String
ʃq =
  ʃ "q"


-- | Parses a single @r@ character.
--
-- Shorthand for @'χ' \'r\'@.
χr ::
  (
  )
    => Parser List String String Char
χr =
  χ 'r'

-- | Parses a single @r@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"r\"@.
ʃr ::
  (
  )
    => Parser List String String String
ʃr =
  ʃ "r"


-- | Parses a single @s@ character.
--
-- Shorthand for @'χ' \'s\'@.
χs ::
  (
  )
    => Parser List String String Char
χs =
  χ 's'

-- | Parses a single @s@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"s\"@.
ʃs ::
  (
  )
    => Parser List String String String
ʃs =
  ʃ "s"


-- | Parses a single @t@ character.
--
-- Shorthand for @'χ' \'t\'@.
χt ::
  (
  )
    => Parser List String String Char
χt =
  χ 't'

-- | Parses a single @t@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"t\"@.
ʃt ::
  (
  )
    => Parser List String String String
ʃt =
  ʃ "t"


-- | Parses a single @u@ character.
--
-- Shorthand for @'χ' \'u\'@.
χu ::
  (
  )
    => Parser List String String Char
χu =
  χ 'u'

-- | Parses a single @u@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"u\"@.
ʃu ::
  (
  )
    => Parser List String String String
ʃu =
  ʃ "u"


-- | Parses a single @v@ character.
--
-- Shorthand for @'χ' \'v\'@.
χv ::
  (
  )
    => Parser List String String Char
χv =
  χ 'v'

-- | Parses a single @v@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"v\"@.
ʃv ::
  (
  )
    => Parser List String String String
ʃv =
  ʃ "v"


-- | Parses a single @w@ character.
--
-- Shorthand for @'χ' \'w\'@.
χw ::
  (
  )
    => Parser List String String Char
χw =
  χ 'w'

-- | Parses a single @w@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"w\"@.
ʃw ::
  (
  )
    => Parser List String String String
ʃw =
  ʃ "w"


-- | Parses a single @x@ character.
--
-- Shorthand for @'χ' \'x\'@.
χx ::
  (
  )
    => Parser List String String Char
χx =
  χ 'x'

-- | Parses a single @x@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"x\"@.
ʃx ::
  (
  )
    => Parser List String String String
ʃx =
  ʃ "x"


-- | Parses a single @y@ character.
--
-- Shorthand for @'χ' \'y\'@.
χy ::
  (
  )
    => Parser List String String Char
χy =
  χ 'y'

-- | Parses a single @y@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"y\"@.
ʃy ::
  (
  )
    => Parser List String String String
ʃy =
  ʃ "y"


-- | Parses a single @z@ character.
--
-- Shorthand for @'χ' \'z\'@.
χz ::
  (
  )
    => Parser List String String Char
χz =
  χ 'z'

-- | Parses a single @z@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"z\"@.
ʃz ::
  (
  )
    => Parser List String String String
ʃz =
  ʃ "z"


-- | Parses a single @A@ character.
--
-- Shorthand for @'χ' \'A\'@.
χA ::
  (
  )
    => Parser List String String Char
χA =
  χ 'A'

-- | Parses a single @A@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"A\"@.
ʃA ::
  (
  )
    => Parser List String String String
ʃA =
  ʃ "A"


-- | Parses a single @B@ character.
--
-- Shorthand for @'χ' \'B\'@.
χB ::
  (
  )
    => Parser List String String Char
χB =
  χ 'B'

-- | Parses a single @B@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"B\"@.
ʃB ::
  (
  )
    => Parser List String String String
ʃB =
  ʃ "B"


-- | Parses a single @C@ character.
--
-- Shorthand for @'χ' \'C\'@.
χC ::
  (
  )
    => Parser List String String Char
χC =
  χ 'C'

-- | Parses a single @C@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"C\"@.
ʃC ::
  (
  )
    => Parser List String String String
ʃC =
  ʃ "C"


-- | Parses a single @D@ character.
--
-- Shorthand for @'χ' \'D\'@.
χD ::
  (
  )
    => Parser List String String Char
χD =
  χ 'D'

-- | Parses a single @D@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"D\"@.
ʃD ::
  (
  )
    => Parser List String String String
ʃD =
  ʃ "D"


-- | Parses a single @E@ character.
--
-- Shorthand for @'χ' \'E\'@.
χE ::
  (
  )
    => Parser List String String Char
χE =
  χ 'E'

-- | Parses a single @E@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"E\"@.
ʃE ::
  (
  )
    => Parser List String String String
ʃE =
  ʃ "E"


-- | Parses a single @F@ character.
--
-- Shorthand for @'χ' \'F\'@.
χF ::
  (
  )
    => Parser List String String Char
χF =
  χ 'F'

-- | Parses a single @F@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"F\"@.
ʃF ::
  (
  )
    => Parser List String String String
ʃF =
  ʃ "F"


-- | Parses a single @G@ character.
--
-- Shorthand for @'χ' \'G\'@.
χG ::
  (
  )
    => Parser List String String Char
χG =
  χ 'G'

-- | Parses a single @G@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"G\"@.
ʃG ::
  (
  )
    => Parser List String String String
ʃG =
  ʃ "G"


-- | Parses a single @H@ character.
--
-- Shorthand for @'χ' \'H\'@.
χH ::
  (
  )
    => Parser List String String Char
χH =
  χ 'H'

-- | Parses a single @H@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"H\"@.
ʃH ::
  (
  )
    => Parser List String String String
ʃH =
  ʃ "H"


-- | Parses a single @I@ character.
--
-- Shorthand for @'χ' \'I\'@.
χI ::
  (
  )
    => Parser List String String Char
χI =
  χ 'I'

-- | Parses a single @I@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"I\"@.
ʃI ::
  (
  )
    => Parser List String String String
ʃI =
  ʃ "I"


-- | Parses a single @J@ character.
--
-- Shorthand for @'χ' \'J\'@.
χJ ::
  (
  )
    => Parser List String String Char
χJ =
  χ 'J'

-- | Parses a single @J@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"J\"@.
ʃJ ::
  (
  )
    => Parser List String String String
ʃJ =
  ʃ "J"


-- | Parses a single @K@ character.
--
-- Shorthand for @'χ' \'K\'@.
χK ::
  (
  )
    => Parser List String String Char
χK =
  χ 'K'

-- | Parses a single @K@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"K\"@.
ʃK ::
  (
  )
    => Parser List String String String
ʃK =
  ʃ "K"


-- | Parses a single @L@ character.
--
-- Shorthand for @'χ' \'L\'@.
χL ::
  (
  )
    => Parser List String String Char
χL =
  χ 'L'

-- | Parses a single @L@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"L\"@.
ʃL ::
  (
  )
    => Parser List String String String
ʃL =
  ʃ "L"


-- | Parses a single @M@ character.
--
-- Shorthand for @'χ' \'M\'@.
χM ::
  (
  )
    => Parser List String String Char
χM =
  χ 'M'

-- | Parses a single @M@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"M\"@.
ʃM ::
  (
  )
    => Parser List String String String
ʃM =
  ʃ "M"


-- | Parses a single @N@ character.
--
-- Shorthand for @'χ' \'N\'@.
χN ::
  (
  )
    => Parser List String String Char
χN =
  χ 'N'

-- | Parses a single @N@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"N\"@.
ʃN ::
  (
  )
    => Parser List String String String
ʃN =
  ʃ "N"


-- | Parses a single @O@ character.
--
-- Shorthand for @'χ' \'O\'@.
χO ::
  (
  )
    => Parser List String String Char
χO =
  χ 'O'

-- | Parses a single @O@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"O\"@.
ʃO ::
  (
  )
    => Parser List String String String
ʃO =
  ʃ "O"


-- | Parses a single @P@ character.
--
-- Shorthand for @'χ' \'P\'@.
χP ::
  (
  )
    => Parser List String String Char
χP =
  χ 'P'

-- | Parses a single @P@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"P\"@.
ʃP ::
  (
  )
    => Parser List String String String
ʃP =
  ʃ "P"


-- | Parses a single @Q@ character.
--
-- Shorthand for @'χ' \'Q\'@.
χQ ::
  (
  )
    => Parser List String String Char
χQ =
  χ 'Q'

-- | Parses a single @Q@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Q\"@.
ʃQ ::
  (
  )
    => Parser List String String String
ʃQ =
  ʃ "Q"


-- | Parses a single @R@ character.
--
-- Shorthand for @'χ' \'R\'@.
χR ::
  (
  )
    => Parser List String String Char
χR =
  χ 'R'

-- | Parses a single @R@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"R\"@.
ʃR ::
  (
  )
    => Parser List String String String
ʃR =
  ʃ "R"


-- | Parses a single @S@ character.
--
-- Shorthand for @'χ' \'S\'@.
χS ::
  (
  )
    => Parser List String String Char
χS =
  χ 'S'

-- | Parses a single @S@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"S\"@.
ʃS ::
  (
  )
    => Parser List String String String
ʃS =
  ʃ "S"


-- | Parses a single @T@ character.
--
-- Shorthand for @'χ' \'T\'@.
χT ::
  (
  )
    => Parser List String String Char
χT =
  χ 'T'

-- | Parses a single @T@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"T\"@.
ʃT ::
  (
  )
    => Parser List String String String
ʃT =
  ʃ "T"


-- | Parses a single @U@ character.
--
-- Shorthand for @'χ' \'U\'@.
χU ::
  (
  )
    => Parser List String String Char
χU =
  χ 'U'

-- | Parses a single @U@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"U\"@.
ʃU ::
  (
  )
    => Parser List String String String
ʃU =
  ʃ "U"


-- | Parses a single @V@ character.
--
-- Shorthand for @'χ' \'V\'@.
χV ::
  (
  )
    => Parser List String String Char
χV =
  χ 'V'

-- | Parses a single @V@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"V\"@.
ʃV ::
  (
  )
    => Parser List String String String
ʃV =
  ʃ "V"


-- | Parses a single @W@ character.
--
-- Shorthand for @'χ' \'W\'@.
χW ::
  (
  )
    => Parser List String String Char
χW =
  χ 'W'

-- | Parses a single @W@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"W\"@.
ʃW ::
  (
  )
    => Parser List String String String
ʃW =
  ʃ "W"


-- | Parses a single @X@ character.
--
-- Shorthand for @'χ' \'X\'@.
χX ::
  (
  )
    => Parser List String String Char
χX =
  χ 'X'

-- | Parses a single @X@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"X\"@.
ʃX ::
  (
  )
    => Parser List String String String
ʃX =
  ʃ "X"


-- | Parses a single @Y@ character.
--
-- Shorthand for @'χ' \'Y\'@.
χY ::
  (
  )
    => Parser List String String Char
χY =
  χ 'Y'

-- | Parses a single @Y@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Y\"@.
ʃY ::
  (
  )
    => Parser List String String String
ʃY =
  ʃ "Y"


-- | Parses a single @Z@ character.
--
-- Shorthand for @'χ' \'Z\'@.
χZ ::
  (
  )
    => Parser List String String Char
χZ =
  χ 'Z'

-- | Parses a single @Z@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"Z\"@.
ʃZ ::
  (
  )
    => Parser List String String String
ʃZ =
  ʃ "Z"


-- | Parses a single @0@ character.
--
-- Shorthand for @'χ' \'0\'@.
χ0 ::
  (
  )
    => Parser List String String Char
χ0 =
  χ '0'

-- | Parses a single @0@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"0\"@.
ʃ0 ::
  (
  )
    => Parser List String String String
ʃ0 =
  ʃ "0"


-- | Parses a single @1@ character.
--
-- Shorthand for @'χ' \'1\'@.
χ1 ::
  (
  )
    => Parser List String String Char
χ1 =
  χ '1'

-- | Parses a single @1@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"1\"@.
ʃ1 ::
  (
  )
    => Parser List String String String
ʃ1 =
  ʃ "1"


-- | Parses a single @2@ character.
--
-- Shorthand for @'χ' \'2\'@.
χ2 ::
  (
  )
    => Parser List String String Char
χ2 =
  χ '2'

-- | Parses a single @2@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"2\"@.
ʃ2 ::
  (
  )
    => Parser List String String String
ʃ2 =
  ʃ "2"


-- | Parses a single @3@ character.
--
-- Shorthand for @'χ' \'3\'@.
χ3 ::
  (
  )
    => Parser List String String Char
χ3 =
  χ '3'

-- | Parses a single @3@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"3\"@.
ʃ3 ::
  (
  )
    => Parser List String String String
ʃ3 =
  ʃ "3"


-- | Parses a single @4@ character.
--
-- Shorthand for @'χ' \'4\'@.
χ4 ::
  (
  )
    => Parser List String String Char
χ4 =
  χ '4'

-- | Parses a single @4@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"4\"@.
ʃ4 ::
  (
  )
    => Parser List String String String
ʃ4 =
  ʃ "4"


-- | Parses a single @5@ character.
--
-- Shorthand for @'χ' \'5\'@.
χ5 ::
  (
  )
    => Parser List String String Char
χ5 =
  χ '5'

-- | Parses a single @5@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"5\"@.
ʃ5 ::
  (
  )
    => Parser List String String String
ʃ5 =
  ʃ "5"


-- | Parses a single @6@ character.
--
-- Shorthand for @'χ' \'6\'@.
χ6 ::
  (
  )
    => Parser List String String Char
χ6 =
  χ '6'

-- | Parses a single @6@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"6\"@.
ʃ6 ::
  (
  )
    => Parser List String String String
ʃ6 =
  ʃ "6"


-- | Parses a single @7@ character.
--
-- Shorthand for @'χ' \'7\'@.
χ7 ::
  (
  )
    => Parser List String String Char
χ7 =
  χ '7'

-- | Parses a single @7@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"7\"@.
ʃ7 ::
  (
  )
    => Parser List String String String
ʃ7 =
  ʃ "7"


-- | Parses a single @8@ character.
--
-- Shorthand for @'χ' \'8\'@.
χ8 ::
  (
  )
    => Parser List String String Char
χ8 =
  χ '8'

-- | Parses a single @8@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"8\"@.
ʃ8 ::
  (
  )
    => Parser List String String String
ʃ8 =
  ʃ "8"


-- | Parses a single @9@ character.
--
-- Shorthand for @'χ' \'9\'@.
χ9 ::
  (
  )
    => Parser List String String Char
χ9 =
  χ '9'

-- | Parses a single @9@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"9\"@.
ʃ9 ::
  (
  )
    => Parser List String String String
ʃ9 =
  ʃ "9"


-- | Parses a single @_@ character.
--
-- Shorthand for @'χ' \'_\'@.
χ_ ::
  (
  )
    => Parser List String String Char
χ_ =
  χ '_'

-- | Parses a single @_@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"_\"@.
ʃ_ ::
  (
  )
    => Parser List String String String
ʃ_ =
  ʃ "_"


-- | Parses a single @'@ character.
--
-- Shorthand for @'χ' \''\'@.
χ' ::
  (
  )
    => Parser List String String Char
χ' =
  χ '\''

-- | Parses a single @'@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"'\"@.
ʃ' ::
  (
  )
    => Parser List String String String
ʃ' =
  ʃ "'"


-- | Parses a single @a@ and evaluates to the input.
--
-- Shorthand for @'hh'"a"@.
hha ::
  (
  )
    => a -> Parser List String String a
hha =
  hh "a"


-- | Parses a single @b@ and evaluates to the input.
--
-- Shorthand for @'hh'"b"@.
hhb ::
  (
  )
    => a -> Parser List String String a
hhb =
  hh "b"


-- | Parses a single @c@ and evaluates to the input.
--
-- Shorthand for @'hh'"c"@.
hhc ::
  (
  )
    => a -> Parser List String String a
hhc =
  hh "c"


-- | Parses a single @d@ and evaluates to the input.
--
-- Shorthand for @'hh'"d"@.
hhd ::
  (
  )
    => a -> Parser List String String a
hhd =
  hh "d"


-- | Parses a single @e@ and evaluates to the input.
--
-- Shorthand for @'hh'"e"@.
hhe ::
  (
  )
    => a -> Parser List String String a
hhe =
  hh "e"


-- | Parses a single @f@ and evaluates to the input.
--
-- Shorthand for @'hh'"f"@.
hhf ::
  (
  )
    => a -> Parser List String String a
hhf =
  hh "f"


-- | Parses a single @g@ and evaluates to the input.
--
-- Shorthand for @'hh'"g"@.
hhg ::
  (
  )
    => a -> Parser List String String a
hhg =
  hh "g"


-- | Parses a single @h@ and evaluates to the input.
--
-- Shorthand for @'hh'"h"@.
hhh ::
  (
  )
    => a -> Parser List String String a
hhh =
  hh "h"


-- | Parses a single @i@ and evaluates to the input.
--
-- Shorthand for @'hh'"i"@.
hhi ::
  (
  )
    => a -> Parser List String String a
hhi =
  hh "i"


-- | Parses a single @j@ and evaluates to the input.
--
-- Shorthand for @'hh'"j"@.
hhj ::
  (
  )
    => a -> Parser List String String a
hhj =
  hh "j"


-- | Parses a single @k@ and evaluates to the input.
--
-- Shorthand for @'hh'"k"@.
hhk ::
  (
  )
    => a -> Parser List String String a
hhk =
  hh "k"


-- | Parses a single @l@ and evaluates to the input.
--
-- Shorthand for @'hh'"l"@.
hhl ::
  (
  )
    => a -> Parser List String String a
hhl =
  hh "l"


-- | Parses a single @m@ and evaluates to the input.
--
-- Shorthand for @'hh'"m"@.
hhm ::
  (
  )
    => a -> Parser List String String a
hhm =
  hh "m"


-- | Parses a single @n@ and evaluates to the input.
--
-- Shorthand for @'hh'"n"@.
hhn ::
  (
  )
    => a -> Parser List String String a
hhn =
  hh "n"


-- | Parses a single @o@ and evaluates to the input.
--
-- Shorthand for @'hh'"o"@.
hho ::
  (
  )
    => a -> Parser List String String a
hho =
  hh "o"


-- | Parses a single @p@ and evaluates to the input.
--
-- Shorthand for @'hh'"p"@.
hhp ::
  (
  )
    => a -> Parser List String String a
hhp =
  hh "p"


-- | Parses a single @q@ and evaluates to the input.
--
-- Shorthand for @'hh'"q"@.
hhq ::
  (
  )
    => a -> Parser List String String a
hhq =
  hh "q"


-- | Parses a single @r@ and evaluates to the input.
--
-- Shorthand for @'hh'"r"@.
hhr ::
  (
  )
    => a -> Parser List String String a
hhr =
  hh "r"


-- | Parses a single @s@ and evaluates to the input.
--
-- Shorthand for @'hh'"s"@.
hhs ::
  (
  )
    => a -> Parser List String String a
hhs =
  hh "s"


-- | Parses a single @t@ and evaluates to the input.
--
-- Shorthand for @'hh'"t"@.
hht ::
  (
  )
    => a -> Parser List String String a
hht =
  hh "t"


-- | Parses a single @u@ and evaluates to the input.
--
-- Shorthand for @'hh'"u"@.
hhu ::
  (
  )
    => a -> Parser List String String a
hhu =
  hh "u"


-- | Parses a single @v@ and evaluates to the input.
--
-- Shorthand for @'hh'"v"@.
hhv ::
  (
  )
    => a -> Parser List String String a
hhv =
  hh "v"


-- | Parses a single @w@ and evaluates to the input.
--
-- Shorthand for @'hh'"w"@.
hhw ::
  (
  )
    => a -> Parser List String String a
hhw =
  hh "w"


-- | Parses a single @x@ and evaluates to the input.
--
-- Shorthand for @'hh'"x"@.
hhx ::
  (
  )
    => a -> Parser List String String a
hhx =
  hh "x"


-- | Parses a single @y@ and evaluates to the input.
--
-- Shorthand for @'hh'"y"@.
hhy ::
  (
  )
    => a -> Parser List String String a
hhy =
  hh "y"


-- | Parses a single @z@ and evaluates to the input.
--
-- Shorthand for @'hh'"z"@.
hhz ::
  (
  )
    => a -> Parser List String String a
hhz =
  hh "z"


-- | Parses a single @A@ and evaluates to the input.
--
-- Shorthand for @'hh'"A"@.
hhA ::
  (
  )
    => a -> Parser List String String a
hhA =
  hh "A"


-- | Parses a single @B@ and evaluates to the input.
--
-- Shorthand for @'hh'"B"@.
hhB ::
  (
  )
    => a -> Parser List String String a
hhB =
  hh "B"


-- | Parses a single @C@ and evaluates to the input.
--
-- Shorthand for @'hh'"C"@.
hhC ::
  (
  )
    => a -> Parser List String String a
hhC =
  hh "C"


-- | Parses a single @D@ and evaluates to the input.
--
-- Shorthand for @'hh'"D"@.
hhD ::
  (
  )
    => a -> Parser List String String a
hhD =
  hh "D"


-- | Parses a single @E@ and evaluates to the input.
--
-- Shorthand for @'hh'"E"@.
hhE ::
  (
  )
    => a -> Parser List String String a
hhE =
  hh "E"


-- | Parses a single @F@ and evaluates to the input.
--
-- Shorthand for @'hh'"F"@.
hhF ::
  (
  )
    => a -> Parser List String String a
hhF =
  hh "F"


-- | Parses a single @G@ and evaluates to the input.
--
-- Shorthand for @'hh'"G"@.
hhG ::
  (
  )
    => a -> Parser List String String a
hhG =
  hh "G"


-- | Parses a single @H@ and evaluates to the input.
--
-- Shorthand for @'hh'"H"@.
hhH ::
  (
  )
    => a -> Parser List String String a
hhH =
  hh "H"


-- | Parses a single @I@ and evaluates to the input.
--
-- Shorthand for @'hh'"I"@.
hhI ::
  (
  )
    => a -> Parser List String String a
hhI =
  hh "I"


-- | Parses a single @J@ and evaluates to the input.
--
-- Shorthand for @'hh'"J"@.
hhJ ::
  (
  )
    => a -> Parser List String String a
hhJ =
  hh "J"


-- | Parses a single @K@ and evaluates to the input.
--
-- Shorthand for @'hh'"K"@.
hhK ::
  (
  )
    => a -> Parser List String String a
hhK =
  hh "K"


-- | Parses a single @L@ and evaluates to the input.
--
-- Shorthand for @'hh'"L"@.
hhL ::
  (
  )
    => a -> Parser List String String a
hhL =
  hh "L"


-- | Parses a single @M@ and evaluates to the input.
--
-- Shorthand for @'hh'"M"@.
hhM ::
  (
  )
    => a -> Parser List String String a
hhM =
  hh "M"


-- | Parses a single @N@ and evaluates to the input.
--
-- Shorthand for @'hh'"N"@.
hhN ::
  (
  )
    => a -> Parser List String String a
hhN =
  hh "N"


-- | Parses a single @O@ and evaluates to the input.
--
-- Shorthand for @'hh'"O"@.
hhO ::
  (
  )
    => a -> Parser List String String a
hhO =
  hh "O"


-- | Parses a single @P@ and evaluates to the input.
--
-- Shorthand for @'hh'"P"@.
hhP ::
  (
  )
    => a -> Parser List String String a
hhP =
  hh "P"


-- | Parses a single @Q@ and evaluates to the input.
--
-- Shorthand for @'hh'"Q"@.
hhQ ::
  (
  )
    => a -> Parser List String String a
hhQ =
  hh "Q"


-- | Parses a single @R@ and evaluates to the input.
--
-- Shorthand for @'hh'"R"@.
hhR ::
  (
  )
    => a -> Parser List String String a
hhR =
  hh "R"


-- | Parses a single @S@ and evaluates to the input.
--
-- Shorthand for @'hh'"S"@.
hhS ::
  (
  )
    => a -> Parser List String String a
hhS =
  hh "S"


-- | Parses a single @T@ and evaluates to the input.
--
-- Shorthand for @'hh'"T"@.
hhT ::
  (
  )
    => a -> Parser List String String a
hhT =
  hh "T"


-- | Parses a single @U@ and evaluates to the input.
--
-- Shorthand for @'hh'"U"@.
hhU ::
  (
  )
    => a -> Parser List String String a
hhU =
  hh "U"


-- | Parses a single @V@ and evaluates to the input.
--
-- Shorthand for @'hh'"V"@.
hhV ::
  (
  )
    => a -> Parser List String String a
hhV =
  hh "V"


-- | Parses a single @W@ and evaluates to the input.
--
-- Shorthand for @'hh'"W"@.
hhW ::
  (
  )
    => a -> Parser List String String a
hhW =
  hh "W"


-- | Parses a single @X@ and evaluates to the input.
--
-- Shorthand for @'hh'"X"@.
hhX ::
  (
  )
    => a -> Parser List String String a
hhX =
  hh "X"


-- | Parses a single @Y@ and evaluates to the input.
--
-- Shorthand for @'hh'"Y"@.
hhY ::
  (
  )
    => a -> Parser List String String a
hhY =
  hh "Y"


-- | Parses a single @Z@ and evaluates to the input.
--
-- Shorthand for @'hh'"Z"@.
hhZ ::
  (
  )
    => a -> Parser List String String a
hhZ =
  hh "Z"


-- | Parses a single @'@ and evaluates to the input.
--
-- Shorthand for @'hh'"'"@.
hh' ::
  (
  )
    => a -> Parser List String String a
hh' =
  hh "'"


-- | Parses a single @_@ and evaluates to the input.
--
-- Shorthand for @'hh'"_"@.
hh_ ::
  (
  )
    => a -> Parser List String String a
hh_ =
  hh "_"


-- | Parses a single @0@ and evaluates to the input.
--
-- Shorthand for @'hh'"0"@.
hh0 ::
  (
  )
    => a -> Parser List String String a
hh0 =
  hh "0"


-- | Parses a single @1@ and evaluates to the input.
--
-- Shorthand for @'hh'"1"@.
hh1 ::
  (
  )
    => a -> Parser List String String a
hh1 =
  hh "1"


-- | Parses a single @2@ and evaluates to the input.
--
-- Shorthand for @'hh'"2"@.
hh2 ::
  (
  )
    => a -> Parser List String String a
hh2 =
  hh "2"


-- | Parses a single @3@ and evaluates to the input.
--
-- Shorthand for @'hh'"3"@.
hh3 ::
  (
  )
    => a -> Parser List String String a
hh3 =
  hh "3"


-- | Parses a single @4@ and evaluates to the input.
--
-- Shorthand for @'hh'"4"@.
hh4 ::
  (
  )
    => a -> Parser List String String a
hh4 =
  hh "4"


-- | Parses a single @5@ and evaluates to the input.
--
-- Shorthand for @'hh'"5"@.
hh5 ::
  (
  )
    => a -> Parser List String String a
hh5 =
  hh "5"


-- | Parses a single @6@ and evaluates to the input.
--
-- Shorthand for @'hh'"6"@.
hh6 ::
  (
  )
    => a -> Parser List String String a
hh6 =
  hh "6"


-- | Parses a single @7@ and evaluates to the input.
--
-- Shorthand for @'hh'"7"@.
hh7 ::
  (
  )
    => a -> Parser List String String a
hh7 =
  hh "7"


-- | Parses a single @8@ and evaluates to the input.
--
-- Shorthand for @'hh'"8"@.
hh8 ::
  (
  )
    => a -> Parser List String String a
hh8 =
  hh "8"


-- | Parses a single @9@ and evaluates to the input.
--
-- Shorthand for @'hh'"9"@.
hh9 ::
  (
  )
    => a -> Parser List String String a
hh9 =
  hh "9"

-- | Parses a single @a@ character off the back of a string.
--
-- Shorthand for @'bx' \'a\'@.
bxa ::
  (
  )
    => Parser List String String Char
bxa =
  bx 'a'


-- | Parses a single @a@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "a"@.
bHa ::
  (
  )
    => Parser List String String String
bHa =
  bH "a"


-- | Parses a single @b@ character off the back of a string.
--
-- Shorthand for @'bx' \'b\'@.
bxb ::
  (
  )
    => Parser List String String Char
bxb =
  bx 'b'


-- | Parses a single @b@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "b"@.
bHb ::
  (
  )
    => Parser List String String String
bHb =
  bH "b"


-- | Parses a single @c@ character off the back of a string.
--
-- Shorthand for @'bx' \'c\'@.
bxc ::
  (
  )
    => Parser List String String Char
bxc =
  bx 'c'


-- | Parses a single @c@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "c"@.
bHc ::
  (
  )
    => Parser List String String String
bHc =
  bH "c"


-- | Parses a single @d@ character off the back of a string.
--
-- Shorthand for @'bx' \'d\'@.
bxd ::
  (
  )
    => Parser List String String Char
bxd =
  bx 'd'


-- | Parses a single @d@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "d"@.
bHd ::
  (
  )
    => Parser List String String String
bHd =
  bH "d"


-- | Parses a single @e@ character off the back of a string.
--
-- Shorthand for @'bx' \'e\'@.
bxe ::
  (
  )
    => Parser List String String Char
bxe =
  bx 'e'


-- | Parses a single @e@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "e"@.
bHe ::
  (
  )
    => Parser List String String String
bHe =
  bH "e"


-- | Parses a single @f@ character off the back of a string.
--
-- Shorthand for @'bx' \'f\'@.
bxf ::
  (
  )
    => Parser List String String Char
bxf =
  bx 'f'


-- | Parses a single @f@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "f"@.
bHf ::
  (
  )
    => Parser List String String String
bHf =
  bH "f"


-- | Parses a single @g@ character off the back of a string.
--
-- Shorthand for @'bx' \'g\'@.
bxg ::
  (
  )
    => Parser List String String Char
bxg =
  bx 'g'


-- | Parses a single @g@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "g"@.
bHg ::
  (
  )
    => Parser List String String String
bHg =
  bH "g"


-- | Parses a single @h@ character off the back of a string.
--
-- Shorthand for @'bx' \'h\'@.
bxh ::
  (
  )
    => Parser List String String Char
bxh =
  bx 'h'


-- | Parses a single @h@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "h"@.
bHh ::
  (
  )
    => Parser List String String String
bHh =
  bH "h"


-- | Parses a single @i@ character off the back of a string.
--
-- Shorthand for @'bx' \'i\'@.
bxi ::
  (
  )
    => Parser List String String Char
bxi =
  bx 'i'


-- | Parses a single @i@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "i"@.
bHi ::
  (
  )
    => Parser List String String String
bHi =
  bH "i"


-- | Parses a single @j@ character off the back of a string.
--
-- Shorthand for @'bx' \'j\'@.
bxj ::
  (
  )
    => Parser List String String Char
bxj =
  bx 'j'


-- | Parses a single @j@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "j"@.
bHj ::
  (
  )
    => Parser List String String String
bHj =
  bH "j"


-- | Parses a single @k@ character off the back of a string.
--
-- Shorthand for @'bx' \'k\'@.
bxk ::
  (
  )
    => Parser List String String Char
bxk =
  bx 'k'


-- | Parses a single @k@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "k"@.
bHk ::
  (
  )
    => Parser List String String String
bHk =
  bH "k"


-- | Parses a single @l@ character off the back of a string.
--
-- Shorthand for @'bx' \'l\'@.
bxl ::
  (
  )
    => Parser List String String Char
bxl =
  bx 'l'


-- | Parses a single @l@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "l"@.
bHl ::
  (
  )
    => Parser List String String String
bHl =
  bH "l"


-- | Parses a single @m@ character off the back of a string.
--
-- Shorthand for @'bx' \'m\'@.
bxm ::
  (
  )
    => Parser List String String Char
bxm =
  bx 'm'


-- | Parses a single @m@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "m"@.
bHm ::
  (
  )
    => Parser List String String String
bHm =
  bH "m"


-- | Parses a single @n@ character off the back of a string.
--
-- Shorthand for @'bx' \'n\'@.
bxn ::
  (
  )
    => Parser List String String Char
bxn =
  bx 'n'


-- | Parses a single @n@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "n"@.
bHn ::
  (
  )
    => Parser List String String String
bHn =
  bH "n"


-- | Parses a single @o@ character off the back of a string.
--
-- Shorthand for @'bx' \'o\'@.
bxo ::
  (
  )
    => Parser List String String Char
bxo =
  bx 'o'


-- | Parses a single @o@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "o"@.
bHo ::
  (
  )
    => Parser List String String String
bHo =
  bH "o"


-- | Parses a single @p@ character off the back of a string.
--
-- Shorthand for @'bx' \'p\'@.
bxp ::
  (
  )
    => Parser List String String Char
bxp =
  bx 'p'


-- | Parses a single @p@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "p"@.
bHp ::
  (
  )
    => Parser List String String String
bHp =
  bH "p"


-- | Parses a single @q@ character off the back of a string.
--
-- Shorthand for @'bx' \'q\'@.
bxq ::
  (
  )
    => Parser List String String Char
bxq =
  bx 'q'


-- | Parses a single @q@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "q"@.
bHq ::
  (
  )
    => Parser List String String String
bHq =
  bH "q"


-- | Parses a single @r@ character off the back of a string.
--
-- Shorthand for @'bx' \'r\'@.
bxr ::
  (
  )
    => Parser List String String Char
bxr =
  bx 'r'


-- | Parses a single @r@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "r"@.
bHr ::
  (
  )
    => Parser List String String String
bHr =
  bH "r"


-- | Parses a single @s@ character off the back of a string.
--
-- Shorthand for @'bx' \'s\'@.
bxs ::
  (
  )
    => Parser List String String Char
bxs =
  bx 's'


-- | Parses a single @s@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "s"@.
bHs ::
  (
  )
    => Parser List String String String
bHs =
  bH "s"


-- | Parses a single @t@ character off the back of a string.
--
-- Shorthand for @'bx' \'t\'@.
bxt ::
  (
  )
    => Parser List String String Char
bxt =
  bx 't'


-- | Parses a single @t@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "t"@.
bHt ::
  (
  )
    => Parser List String String String
bHt =
  bH "t"


-- | Parses a single @u@ character off the back of a string.
--
-- Shorthand for @'bx' \'u\'@.
bxu ::
  (
  )
    => Parser List String String Char
bxu =
  bx 'u'


-- | Parses a single @u@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "u"@.
bHu ::
  (
  )
    => Parser List String String String
bHu =
  bH "u"


-- | Parses a single @v@ character off the back of a string.
--
-- Shorthand for @'bx' \'v\'@.
bxv ::
  (
  )
    => Parser List String String Char
bxv =
  bx 'v'


-- | Parses a single @v@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "v"@.
bHv ::
  (
  )
    => Parser List String String String
bHv =
  bH "v"


-- | Parses a single @w@ character off the back of a string.
--
-- Shorthand for @'bx' \'w\'@.
bxw ::
  (
  )
    => Parser List String String Char
bxw =
  bx 'w'


-- | Parses a single @w@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "w"@.
bHw ::
  (
  )
    => Parser List String String String
bHw =
  bH "w"


-- | Parses a single @x@ character off the back of a string.
--
-- Shorthand for @'bx' \'x\'@.
bxx ::
  (
  )
    => Parser List String String Char
bxx =
  bx 'x'


-- | Parses a single @x@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "x"@.
bHx ::
  (
  )
    => Parser List String String String
bHx =
  bH "x"


-- | Parses a single @y@ character off the back of a string.
--
-- Shorthand for @'bx' \'y\'@.
bxy ::
  (
  )
    => Parser List String String Char
bxy =
  bx 'y'


-- | Parses a single @y@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "y"@.
bHy ::
  (
  )
    => Parser List String String String
bHy =
  bH "y"


-- | Parses a single @z@ character off the back of a string.
--
-- Shorthand for @'bx' \'z\'@.
bxz ::
  (
  )
    => Parser List String String Char
bxz =
  bx 'z'


-- | Parses a single @z@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "z"@.
bHz ::
  (
  )
    => Parser List String String String
bHz =
  bH "z"


-- | Parses a single @A@ character off the back of a string.
--
-- Shorthand for @'bx' \'A\'@.
bxA ::
  (
  )
    => Parser List String String Char
bxA =
  bx 'A'


-- | Parses a single @A@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "A"@.
bHA ::
  (
  )
    => Parser List String String String
bHA =
  bH "A"


-- | Parses a single @B@ character off the back of a string.
--
-- Shorthand for @'bx' \'B\'@.
bxB ::
  (
  )
    => Parser List String String Char
bxB =
  bx 'B'


-- | Parses a single @B@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "B"@.
bHB ::
  (
  )
    => Parser List String String String
bHB =
  bH "B"


-- | Parses a single @C@ character off the back of a string.
--
-- Shorthand for @'bx' \'C\'@.
bxC ::
  (
  )
    => Parser List String String Char
bxC =
  bx 'C'


-- | Parses a single @C@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "C"@.
bHC ::
  (
  )
    => Parser List String String String
bHC =
  bH "C"


-- | Parses a single @D@ character off the back of a string.
--
-- Shorthand for @'bx' \'D\'@.
bxD ::
  (
  )
    => Parser List String String Char
bxD =
  bx 'D'


-- | Parses a single @D@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "D"@.
bHD ::
  (
  )
    => Parser List String String String
bHD =
  bH "D"


-- | Parses a single @E@ character off the back of a string.
--
-- Shorthand for @'bx' \'E\'@.
bxE ::
  (
  )
    => Parser List String String Char
bxE =
  bx 'E'


-- | Parses a single @E@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "E"@.
bHE ::
  (
  )
    => Parser List String String String
bHE =
  bH "E"


-- | Parses a single @F@ character off the back of a string.
--
-- Shorthand for @'bx' \'F\'@.
bxF ::
  (
  )
    => Parser List String String Char
bxF =
  bx 'F'


-- | Parses a single @F@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "F"@.
bHF ::
  (
  )
    => Parser List String String String
bHF =
  bH "F"


-- | Parses a single @G@ character off the back of a string.
--
-- Shorthand for @'bx' \'G\'@.
bxG ::
  (
  )
    => Parser List String String Char
bxG =
  bx 'G'


-- | Parses a single @G@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "G"@.
bHG ::
  (
  )
    => Parser List String String String
bHG =
  bH "G"


-- | Parses a single @H@ character off the back of a string.
--
-- Shorthand for @'bx' \'H\'@.
bxH ::
  (
  )
    => Parser List String String Char
bxH =
  bx 'H'


-- | Parses a single @H@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "H"@.
bHH ::
  (
  )
    => Parser List String String String
bHH =
  bH "H"


-- | Parses a single @I@ character off the back of a string.
--
-- Shorthand for @'bx' \'I\'@.
bxI ::
  (
  )
    => Parser List String String Char
bxI =
  bx 'I'


-- | Parses a single @I@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "I"@.
bHI ::
  (
  )
    => Parser List String String String
bHI =
  bH "I"


-- | Parses a single @J@ character off the back of a string.
--
-- Shorthand for @'bx' \'J\'@.
bxJ ::
  (
  )
    => Parser List String String Char
bxJ =
  bx 'J'


-- | Parses a single @J@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "J"@.
bHJ ::
  (
  )
    => Parser List String String String
bHJ =
  bH "J"


-- | Parses a single @K@ character off the back of a string.
--
-- Shorthand for @'bx' \'K\'@.
bxK ::
  (
  )
    => Parser List String String Char
bxK =
  bx 'K'


-- | Parses a single @K@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "K"@.
bHK ::
  (
  )
    => Parser List String String String
bHK =
  bH "K"


-- | Parses a single @L@ character off the back of a string.
--
-- Shorthand for @'bx' \'L\'@.
bxL ::
  (
  )
    => Parser List String String Char
bxL =
  bx 'L'


-- | Parses a single @L@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "L"@.
bHL ::
  (
  )
    => Parser List String String String
bHL =
  bH "L"


-- | Parses a single @M@ character off the back of a string.
--
-- Shorthand for @'bx' \'M\'@.
bxM ::
  (
  )
    => Parser List String String Char
bxM =
  bx 'M'


-- | Parses a single @M@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "M"@.
bHM ::
  (
  )
    => Parser List String String String
bHM =
  bH "M"


-- | Parses a single @N@ character off the back of a string.
--
-- Shorthand for @'bx' \'N\'@.
bxN ::
  (
  )
    => Parser List String String Char
bxN =
  bx 'N'


-- | Parses a single @N@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "N"@.
bHN ::
  (
  )
    => Parser List String String String
bHN =
  bH "N"


-- | Parses a single @O@ character off the back of a string.
--
-- Shorthand for @'bx' \'O\'@.
bxO ::
  (
  )
    => Parser List String String Char
bxO =
  bx 'O'


-- | Parses a single @O@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "O"@.
bHO ::
  (
  )
    => Parser List String String String
bHO =
  bH "O"


-- | Parses a single @P@ character off the back of a string.
--
-- Shorthand for @'bx' \'P\'@.
bxP ::
  (
  )
    => Parser List String String Char
bxP =
  bx 'P'


-- | Parses a single @P@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "P"@.
bHP ::
  (
  )
    => Parser List String String String
bHP =
  bH "P"


-- | Parses a single @Q@ character off the back of a string.
--
-- Shorthand for @'bx' \'Q\'@.
bxQ ::
  (
  )
    => Parser List String String Char
bxQ =
  bx 'Q'


-- | Parses a single @Q@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "Q"@.
bHQ ::
  (
  )
    => Parser List String String String
bHQ =
  bH "Q"


-- | Parses a single @R@ character off the back of a string.
--
-- Shorthand for @'bx' \'R\'@.
bxR ::
  (
  )
    => Parser List String String Char
bxR =
  bx 'R'


-- | Parses a single @R@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "R"@.
bHR ::
  (
  )
    => Parser List String String String
bHR =
  bH "R"


-- | Parses a single @S@ character off the back of a string.
--
-- Shorthand for @'bx' \'S\'@.
bxS ::
  (
  )
    => Parser List String String Char
bxS =
  bx 'S'


-- | Parses a single @S@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "S"@.
bHS ::
  (
  )
    => Parser List String String String
bHS =
  bH "S"


-- | Parses a single @T@ character off the back of a string.
--
-- Shorthand for @'bx' \'T\'@.
bxT ::
  (
  )
    => Parser List String String Char
bxT =
  bx 'T'


-- | Parses a single @T@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "T"@.
bHT ::
  (
  )
    => Parser List String String String
bHT =
  bH "T"


-- | Parses a single @U@ character off the back of a string.
--
-- Shorthand for @'bx' \'U\'@.
bxU ::
  (
  )
    => Parser List String String Char
bxU =
  bx 'U'


-- | Parses a single @U@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "U"@.
bHU ::
  (
  )
    => Parser List String String String
bHU =
  bH "U"


-- | Parses a single @V@ character off the back of a string.
--
-- Shorthand for @'bx' \'V\'@.
bxV ::
  (
  )
    => Parser List String String Char
bxV =
  bx 'V'


-- | Parses a single @V@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "V"@.
bHV ::
  (
  )
    => Parser List String String String
bHV =
  bH "V"


-- | Parses a single @W@ character off the back of a string.
--
-- Shorthand for @'bx' \'W\'@.
bxW ::
  (
  )
    => Parser List String String Char
bxW =
  bx 'W'


-- | Parses a single @W@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "W"@.
bHW ::
  (
  )
    => Parser List String String String
bHW =
  bH "W"


-- | Parses a single @X@ character off the back of a string.
--
-- Shorthand for @'bx' \'X\'@.
bxX ::
  (
  )
    => Parser List String String Char
bxX =
  bx 'X'


-- | Parses a single @X@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "X"@.
bHX ::
  (
  )
    => Parser List String String String
bHX =
  bH "X"


-- | Parses a single @Y@ character off the back of a string.
--
-- Shorthand for @'bx' \'Y\'@.
bxY ::
  (
  )
    => Parser List String String Char
bxY =
  bx 'Y'


-- | Parses a single @Y@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "Y"@.
bHY ::
  (
  )
    => Parser List String String String
bHY =
  bH "Y"


-- | Parses a single @Z@ character off the back of a string.
--
-- Shorthand for @'bx' \'Z\'@.
bxZ ::
  (
  )
    => Parser List String String Char
bxZ =
  bx 'Z'


-- | Parses a single @Z@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "Z"@.
bHZ ::
  (
  )
    => Parser List String String String
bHZ =
  bH "Z"


-- | Parses a single @0@ character off the back of a string.
--
-- Shorthand for @'bx' \'0\'@.
bx0 ::
  (
  )
    => Parser List String String Char
bx0 =
  bx '0'


-- | Parses a single @0@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "0"@.
bH0 ::
  (
  )
    => Parser List String String String
bH0 =
  bH "0"


-- | Parses a single @1@ character off the back of a string.
--
-- Shorthand for @'bx' \'1\'@.
bx1 ::
  (
  )
    => Parser List String String Char
bx1 =
  bx '1'


-- | Parses a single @1@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "1"@.
bH1 ::
  (
  )
    => Parser List String String String
bH1 =
  bH "1"


-- | Parses a single @2@ character off the back of a string.
--
-- Shorthand for @'bx' \'2\'@.
bx2 ::
  (
  )
    => Parser List String String Char
bx2 =
  bx '2'


-- | Parses a single @2@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "2"@.
bH2 ::
  (
  )
    => Parser List String String String
bH2 =
  bH "2"


-- | Parses a single @3@ character off the back of a string.
--
-- Shorthand for @'bx' \'3\'@.
bx3 ::
  (
  )
    => Parser List String String Char
bx3 =
  bx '3'


-- | Parses a single @3@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "3"@.
bH3 ::
  (
  )
    => Parser List String String String
bH3 =
  bH "3"


-- | Parses a single @4@ character off the back of a string.
--
-- Shorthand for @'bx' \'4\'@.
bx4 ::
  (
  )
    => Parser List String String Char
bx4 =
  bx '4'


-- | Parses a single @4@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "4"@.
bH4 ::
  (
  )
    => Parser List String String String
bH4 =
  bH "4"


-- | Parses a single @5@ character off the back of a string.
--
-- Shorthand for @'bx' \'5\'@.
bx5 ::
  (
  )
    => Parser List String String Char
bx5 =
  bx '5'


-- | Parses a single @5@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "5"@.
bH5 ::
  (
  )
    => Parser List String String String
bH5 =
  bH "5"


-- | Parses a single @6@ character off the back of a string.
--
-- Shorthand for @'bx' \'6\'@.
bx6 ::
  (
  )
    => Parser List String String Char
bx6 =
  bx '6'


-- | Parses a single @6@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "6"@.
bH6 ::
  (
  )
    => Parser List String String String
bH6 =
  bH "6"


-- | Parses a single @7@ character off the back of a string.
--
-- Shorthand for @'bx' \'7\'@.
bx7 ::
  (
  )
    => Parser List String String Char
bx7 =
  bx '7'


-- | Parses a single @7@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "7"@.
bH7 ::
  (
  )
    => Parser List String String String
bH7 =
  bH "7"


-- | Parses a single @8@ character off the back of a string.
--
-- Shorthand for @'bx' \'8\'@.
bx8 ::
  (
  )
    => Parser List String String Char
bx8 =
  bx '8'


-- | Parses a single @8@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "8"@.
bH8 ::
  (
  )
    => Parser List String String String
bH8 =
  bH "8"


-- | Parses a single @9@ character off the back of a string.
--
-- Shorthand for @'bx' \'9\'@.
bx9 ::
  (
  )
    => Parser List String String Char
bx9 =
  bx '9'


-- | Parses a single @9@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "9"@.
bH9 ::
  (
  )
    => Parser List String String String
bH9 =
  bH "9"


-- | Parses a single @_@ character off the back of a string.
--
-- Shorthand for @'bx' \'_\'@.
bx_ ::
  (
  )
    => Parser List String String Char
bx_ =
  bx '_'


-- | Parses a single @_@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "_"@.
bH_ ::
  (
  )
    => Parser List String String String
bH_ =
  bH "_"


-- | Parses a single @'@ character off the back of a string.
--
-- Shorthand for @'bx' \'\\'\'@.
bx' ::
  (
  )
    => Parser List String String Char
bx' =
  bx '\''


-- | Parses a single @'@ character off the back of a string returning it as a 'String'.
--
-- Shorthand for @'bH' "'"@.
bH' ::
  (
  )
    => Parser List String String String
bH' =
  bH "'"


-- | Parses a single @)@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\")\"@.
cls ::
  (
  )
    => Parser List String String String
cls =
  ʃ ")"


-- | Parses a single @(@ character, returning a string of it.
--
-- Shorthand for @'ʃ'\"(\"@.
opn ::
  (
  )
    => Parser List String String String
opn =
  ʃ "("


-- | Parses a single newline character.
--
-- Shorthand for @'χ' 'P.Char.Nl'@.
pNl ::
  (
  )
    => Parser List String String Char
pNl =
  χ '\n'


-- | Parses a single newline character, returning it as a string.
--
-- Shorthand for @'ʃ' 'P.Char.NL'@.
pNL ::
  (
  )
    => Parser List String String String
pNL =
  ʃ "\n"


-- | Parses a single newline character, and evaluates to the input.
--
-- Shorthand for @'hh' "\n"@.
hNL ::
  (
  )
    => a -> Parser List String String a
hNL =
  hh "\n"


-- | Parses a single @"@ character.
--
-- Shorthand for @'χ' \'"\'@.
pQt ::
  (
  )
    => Parser List String String Char
pQt =
  χ '"'


-- | Parses a single @"@ character, returning it as a string.
--
-- Shorthand for @'ʃ'"\\""@.
pQT ::
  (
  )
    => Parser List String String String
pQT =
  ʃ "\""


-- | Parses a single @"@ character, and evaluates to the input.
--
-- Shorthand for @'hh' "\""@.
hQT ::
  (
  )
    => a -> Parser List String String a
hQT =
  hh "\""


-- | Parses a single @,@ character.
--
-- Shorthand for @'χ' \',\'@.
pCm ::
  (
  )
    => Parser List String String Char
pCm =
  χ ','


-- | Parses a single @,@ character, returning it as a string.
--
-- Shorthand for @'ʃ'","@.
pCM ::
  (
  )
    => Parser List String String String
pCM =
  ʃ ","


-- | Parses a single @,@ character, and evaluates to the input.
--
-- Shorthand for @'hh' ","@.
hCM ::
  (
  )
    => a -> Parser List String String a
hCM =
  hh ","


-- | Parses a single @;@ character.
--
-- Shorthand for @'χ' \';\'@.
pSc ::
  (
  )
    => Parser List String String Char
pSc =
  χ ';'


-- | Parses a single @;@ character, returning it as a string.
--
-- Shorthand for @'ʃ'";"@.
pSC ::
  (
  )
    => Parser List String String String
pSC =
  ʃ ";"


-- | Parses a single @;@ character, and evaluates to the input.
--
-- Shorthand for @'hh' ";"@.
hSC ::
  (
  )
    => a -> Parser List String String a
hSC =
  hh ";"


-- | Parses a single @|@ character.
--
-- Shorthand for @'χ' \'|\'@.
pPi ::
  (
  )
    => Parser List String String Char
pPi =
  χ '|'


-- | Parses a single @|@ character. returning it as a string.
--
-- Shorthand for @'ʃ'"|"@.
pPI ::
  (
  )
    => Parser List String String String
pPI =
  ʃ "|"


-- | Parses a single @|@ character, and evaluates to the input.
--
-- Shorthand for @'hh' "|"@.
hPI ::
  (
  )
    => a -> Parser List String String a
hPI =
  hh "|"


-- | Enclose a parser in @'@ characters.
enq ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enq =
  enk (ʃ "'")


-- | Enclose a parser in @"@ characters.
enQ ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enQ =
  enk (ʃ "\"")


-- | Enclose a parser in parentheses (@()@).
enP ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enP parser =
  ʃ "(" *> parser <* ʃ ")"


-- | Enclose a parser in square brackets (@[]@).
enS ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enS parser =
  ʃ "[" *> parser <* ʃ "]"


-- | Enclose a parser in curly braces (@{}@).
enC ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enC parser =
  ʃ "{" *> parser <* ʃ "}"


-- | Enclose a parser in angle brackets (@<>@).
enA ::
  ( Monad m
  , Alternative m
  )
    => Parser m String String a -> Parser m String String a
enA parser =
  ʃ "<" *> parser <* ʃ ">"
