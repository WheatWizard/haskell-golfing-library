{-|
Module :
  P.Parse.Combinator
Description :
  Parser m combinators
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Parser combinators.

Some useful combinators are implemented as a result of 'Parser' instances.

== Alternative

Alternative provides number of useful functions, some of the particularly geared towards parsing.

@
(++) :: Parser m a b c -> Parser m a b c -> Parser m a b c
@

@(++)@ runs takes two parsers and attempts to run both of them.
This can be thought of as an *or* operation on parsers

==== __Examples__

>>> uP (gsO (ʃa ++ ʃb)) "aab2bb2a"
[("2bb2a","aab"),("b2bb2a","aa"),("ab2bb2a","a")]

== Weakalign

==== __Examples__

>>> gc (so $ gSo dg #| hdS) "test1234mes"
[["t","e","s","t","1234","m","e","s"]]
-}
module P.Parse.Combinator
  (
  -- * Parser m combinators
    kB
  , bkB
  , nK
  , χ
  , sχ
  , x_
  , x'
  , lx_
  , lX_
  , lx'
  , lX'
  , bx
  , nχ
  , xx
  , zWx
  , xxh
  , bXx
  , ʃ
  , s_
  , s'
  , hʃ
  , yʃ
  , bH
  , hh
  , zWh
  , bhh
  , gy
  , bgy
  , lGk
  , gy'
  , bgY
  , bkw
  , ivP
  , gre
  , laz
  , bʃ
  , bSh
  , isP
  , ($|$)
  , fiP
  , pew
  , peW
  , pEw
  , νa
  , enk
  , fNk
  , yS
  , yχ
  , mQ
  , blj
  , h_t
  , h't
  , ah_
  , ah'
  , ar_
  , ar'
  , h_a
  , h'a
  , wh_
  , wh'
  , ayw
  , ayW
  -- ** Look-aheads
  , lA
  , nA
  , aχ
  , anx
  , akB
  , aʃ
  , ahd
  , ahS
  -- ** Parse from options
  -- Parses from a list of options.
  -- Similar to a character class in a regex.
  , xay
  , xys
  , nxy
  , nys
  , xyo
  , xyy
  , asy
  , syo
  , syO
  , syy
  , syY
  -- ** Composition
  , lp2
  , (#*>)
  , (#<*)
  , yju
  , fyj
  , (@<*)
  , yku
  , fyk
  , (@*>)
  , ymu
  , fym
  , (@^*)
  -- * Pre-made parsers
  -- For more see 'P.Parse.Extra'.
  , hd
  , h_
  , h'
  , rh_
  , rh'
  , hdS
  , phd
  , bhd
  , mhd
  , dg
  , af
  , p_
  , pc
  , pL
  , pU
  , aN
  , pR
  , wd
  , wr
  , en
  , ens
  , p16
  , lWs
  , tWs
  , ν
  , νw
  , νd
  -- * Deprecated
  , string
  , char
  , liftParser2
  )
  where


import qualified Prelude
import Prelude
  ( Read
  , Maybe (..)
  , Integral
  )


import qualified Text.Read


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Ring
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Alternative.Some
import P.Alternative.Possible
import P.Applicative
import P.Arithmetic.Nat
import P.Bifunctor
import P.Bool
import P.Category
import P.Char
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Functor
import P.Last
import P.Monad
import P.Monad.Plus.Filter
import P.Parse
import P.Reverse
import P.Show
import P.String
import P.Swap
import P.Zip
import P.Zip.WeakAlign


-- | Compose two parsers ignoring the result of the first parser.
--
-- More general version of '(*>)' for parsers.
(#*>) ::
  ( Monad m
  )
    => Parser m a b c -> Parser m b d e -> Parser m a d e
P p1 #*> P p2 =
  P $ \ v -> do
    (x, _) <- p1 v
    p2 x


-- | Compose two parsers ignoring the result of the second parser.
--
-- More general version of '(<*)' for parsers.
(#<*) ::
  ( Monad m
  )
    => Parser m a b c -> Parser m b d e -> Parser m a d c
P p1 #<* P p2 =
  P $ \ v -> do
    (x, y) <- p1 v
    (k, _) <- p2 x
    p (k, y)


-- | Adds any amount of leading whitespace to a parser.
lWs ::
  ( Alternative m
  , Monad m
  )
    => Parser m String a b -> Parser m String a b
lWs =
  (my p_ #*>)


-- | Adds any amount of trailing whitespace to a parser.
tWs ::
  ( Alternative m
  , Monad m
  )
    => Parser m a String b -> Parser m a String b
tWs =
  (#<* my p_)


-- | A look-ahead.
-- It takes a parser and produces a new parser which creates the same results without consuming the input.
--
-- Useful if you want to run two parsers on the same thing.
--
-- ==== __Examples__
--
-- Here's it being used to create a parser which consumes palindromes off the front of a list:
--
-- >>> f=lA h'>~ʃ<rv
-- >>> x1 f "abeba"
-- True
-- >>> x1 f "abba"
-- True
-- >>> x1 f "abeja"
-- False
--
lA ::
  ( Monad m
  )
    => Parser m k j a -> Parser m k k a
lA parser =
  P
    ( \ input ->
      do
        (_, res) <- uP' parser input
        p (input, res)
    )


-- | A negative look-ahead.
-- I takes a parser and produces a new parser which consumes no input and is successful if an only if the given parser was not.
--
nA ::
  ( Alternative m
  , Foldable m
  )
    => Parser m k j a -> Parser m k k ()
nA parser =
  P
    ( \ input ->
      if
        ø $ uP' parser input
      then
        p (input, ())
      else
        em
    )


-- | 'χ' with a look-ahead.
--
-- Warning depending on your font this may look like @ax@.
-- It is not.
-- The @x@ is the greek letter chi.
aχ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
aχ =
  lA < χ


-- | 'nχ' with a look-ahead.
anx ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
anx =
  lA < nχ


-- | 'kB' with a look-ahead.
akB ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => Predicate a -> Parser m (List a) (List a) a
akB =
  lA < kB


-- | 'ʃ' with a look-ahead.
aʃ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => List a -> Parser m (List a) (List a) (List a)
aʃ =
  lA < ʃ


-- | 'hd' with a look-ahead.
ahd ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) (List a) a
ahd =
  lA hd


-- | 'hdS' with a look-ahead.
ahS ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) (List a) (List a)
ahS =
  lA hdS


-- | Accepts and returns single character.
hd ::
  ( Alternative m
  )
    => Parser m (List a) (List a) a
hd =
  P run
  where
    run [] =
      em
    run (head : tail) =
      p (tail, head)


-- | Accepts a single character and returns it as a string.
hdS ::
  ( Alternative m
  )
    => Parser m (List a) (List a) (List a)
hdS =
  mhd p


-- | Accepts a single character and returns a constant value.
phd ::
  ( Alternative m
  )
    => a -> Parser m (List b) (List b) a
phd x =
  p x < hd


-- | Accepts and returns at least one character.
h_ ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) (List a) (List a)
h_ =
  so hd


-- | Accpets and returns any number of characters including none at all.
h' ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) (List a) (List a)
h' =
  my hd


-- | Accepts and returns at least one character.
-- Shorter parses have higher priority.
rh_ ::
  ( Alternative m
  , Monad m
  )
  => Parser m (List a) (List a) (List a)
rh_ =
  so' hd


-- | Accepts and returns any number of characters including none at all.
-- Shorter parses have a higher priority.
rh' ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) (List a) (List a)
rh' =
  my' hd


-- | Parse an arbitrary positive number of characters and feed the input into another parser.
--
-- More general version of @(h_ >~)@ or @bn h_@.
h_t ::
  ( Alternative m
  , Monad m
  )
    => (List a -> Parser m (List a) k b) -> Parser m (List a) k b
h_t func =
  P
    ( \ start ->
      do
        (rem1, res1) <- uP' h_ start
        uP' (func res1) rem1
    )


-- | Parse an arbitrary number (possibly zero) of characters and feed the input into another parser.
--
-- More general version of @(h' >~)@ or @bn h'@.
h't ::
  ( Alternative m
  , Monad m
  )
    => (List a -> Parser m (List a) k b) -> Parser m (List a) k b
h't func =
  P
    ( \ start ->
      do
        (rem1, res1) <- uP' h' start
        uP' (func res1) rem1
    )


-- | Parse an arbritrary positive number of characters and then perform the input parser.
--
-- Prioritizes parses that start later.
--
-- More general version of @(h_ *>)@ or @aT h_@ and @(hd +*>)@ or @(soA hd)@.
ah_ ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) k b -> Parser m (List a) k b
ah_ parser =
  P
    ( \ start ->
      do
        (rem1, _) <- uP' h_ start
        uP' parser rem1
    )



-- | Parse an arbritrary number (possibly zero) of characters and then perform the input parser.
--
-- Prioritizes parses that start later.
--
-- More general version of @(h' *>)@ or @aT h'@ and @(hd **>)@ or @(amy hd)@.
ah' ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) k b -> Parser m (List a) k b
ah' parser =
  P
    ( \ start ->
      do
        (rem1, _) <- uP' h' start
        uP' parser rem1
    )


-- | Parse an arbritrary positive number of characters and then perform the input parser.
--
-- Prioritizes parses that start earlier.
--
-- More general version of @(rh_ *>)@ or @aT rh_@.
ar_ ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) k b -> Parser m (List a) k b
ar_ parser =
  P
    ( \ start ->
      do
        (rem1, _) <- uP' rh_ start
        uP' parser rem1
    )


-- | Parse an arbitrary number (possibly zero) of characters and then perfom the inpur parser.
--
-- Prioritizes parses that start earlier.
--
-- More general version of @(rh' *>)@ or @aT rh'@.
ar' ::
  ( Alternative m
  , Monad m
  )
    => Parser m (List a) k b -> Parser m (List a) k b
ar' parser =
  P
    ( \ start ->
      do
        (rem1, _) <- uP' rh' start
        uP' parser rem1
    )


-- | Perform a parser and then parse an arbitrary positive number of characters.
-- Return the result of the parser ignoring the following characters entirely.
--
-- More general version of @(<* h_)@ or @faK h_@.
-- More general version of @(<* h_)@ or @faK h_@ and @(<*+ hd)@.
h_a ::
  ( Alternative m
  , Monad m
  )
    => Parser m k (List a) b -> Parser m k (List a) b
h_a parser =
  P
    ( \ start ->
      do
        (rem1, res1) <- uP' parser start
        (res1 <$) < uP' h' rem1
    )


-- | Perform a parser and then parse an arbitrary number (possibly zero) of characters.
-- Return the result of the parser ignoring the following characters entirely.
--
-- More general version of @(<* h')@ or @faK h'@ and @(<** hd)@ or @yma hd@.
h'a ::
  ( Alternative m
  , Monad m
  )
    => Parser m k (List a) b -> Parser m k (List a) b
h'a parser =
  P
    ( \ start ->
      do
        (rem1, res1) <- uP' parser start
        (res1 <$) < uP' h' rem1
    )


-- | Parse an arbitrary positive number of characters satisfying a predicate.
wh_ ::
  ( Alternative m
  , Monad m
  )
    => Predicate (List a) -> Parser m (List a) (List a) (List a)
wh_ =
  wh h_


-- | Parse an arbitrary number (possibly zero) of characters satisfying a predicate.
wh' ::
  ( Alternative m
  , Monad m
  )
    => Predicate (List a) -> Parser m (List a) (List a) (List a)
wh' =
  wh h'


-- | Accepts and returns a single character off the back of the input.
--
-- In general this is much slower than 'hd' because it needs to traverse to the end of the list.
--
-- ===== __Examples__
--
-- >>> uP bhd "Hello, world!"
-- [("Hello, world",'!')]
--
bhd ::
  ( Alternative m
  )
    => Parser m (List a) (List a) a
bhd =
  P run
  where
    run [] =
      em
    run (last :> init) =
      p (init, last)


-- | Accepts a single character and applies a function to it.
mhd ::
  ( Alternative m
  )
    => (a -> b) -> Parser m (List a) (List a) b
mhd =
  fm hd


-- | Parses a single character accepting if it satisfies some predicate.
kB ::
  ( Alternative m
  , Monad m
  )
    => Predicate a -> Parser m (List a) (List a) a
kB =
  wh hd


-- | Parses a single character off the back of a list accepting if it satisfies some predicate.
--
-- In general this is much slower than 'kB' because it needs to traverse to the end of a list.
--
-- ===== __Examples__
--
-- >>> uP (bkB α) "Toast"
-- [("Toas",'t')]
--
bkB ::
  ( Alternative m
  , Monad m
  )
    => Predicate a -> Parser m (List a) (List a) a
bkB =
  wh bhd


-- | Parses a single character accepting it if it doesn't satisfy the given predicate.
nK ::
  ( Alternative m
  , Monad m
  )
    => Predicate a -> Parser m (List a) (List a) a
nK =
  kB < m n


-- | Greedily consumes as many characters as possible matching a predicate.
gy ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Predicate a -> Parser m (List a) (List a) (List a)
gy =
  gMy < kB


-- | Greedily consibes as many characters as possible matching a predicate off the end of a list.
--
-- Significantly slower than consuming characters off the front of a list with 'gy'.
bgy ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Predicate a -> Parser m (List a) (List a) (List a)
bgy =
  rv << gMy < bkB


-- | Greedily consumes as many characters as possible matching a predicate and returns the number of characters consumed.
lGk ::
  ( Integral i
  , Monad m
  , Foldable m
  , Alternative m
  )
    => Predicate a -> Parser m (List a) (List a) i
lGk =
  l << gy


-- | Greedily consumes at least one character matching a predicate.
gy' ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Predicate a -> Parser m (List a) (List a) (List a)
gy' =
  gSo < kB


-- | Greedily consomes at least one character matching a predicate off the back of a list.
--
-- Significantly slower than consuming characters off the front of a list with 'gy''.
--
-- ===== __Examples__
--
-- >>> uP (bgy (eq '!')) "Hello, world!!!!"
-- [("Hello, world","!!!!")]
--
-- >>> uP (bgY α) "Hello, world"
-- [("Hello, ","world")]
--
bgY ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Predicate a -> Parser m (List a) (List a) (List a)
bgY =
  rv << gSo < bkB


{-# Deprecated char "Use χ instead" #-}
-- | Long version of 'χ'.
-- Parses a char accepting it if it is equal to the given char.
char ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
char =
  χ


-- | Parses a single character accepting it if it is equal to a particular character.
--
-- When parsing a particular character it is often shorter altogether to use 'ʃ'.
-- For example @ʃ\"*\"@ is shorter than @χ \'*\'@ (the space is necessary to split the tokens).
--
-- Warning depending on your font this may look like @x@.
-- It is not, it is the greek letter chi.
χ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
χ char =
  kB (Prelude.== char)


-- | Parses a single character returning it as a string.
sχ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) (List a)
sχ =
  p << χ


-- | Parses at least one of the given character.
x_ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) (List a)
x_ =
  so < χ


-- | Parses any number of the given character.
x' ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) (List a)
x' =
  my < χ


-- | Parses any number of the given character returning the number of parsed characters as a natural number.
lx' ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) Nat
lx' =
  lMy < χ


-- | Parses any number of the given character returning the number of parsed characters.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lx'' does not.
lX' ::
  ( Eq a
  , Alternative m
  , Monad m
  , Integral i
  )
    => a -> Parser m (List a) (List a) i
lX' =
  lMY < χ


-- | Parses at least one of the given character returning the number of parsed characters as a natural number.
lx_ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) Nat
lx_ =
  lSo < χ


-- | Parses at least one of the given character returning the number of parsed characters.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lx_' does not.
lX_ ::
  ( Eq a
  , Alternative m
  , Monad m
  , Integral i
  )
    => a -> Parser m (List a) (List a) i
lX_ =
  lSO < χ


-- | Parses a single character accepting it if it is not equal to a particular character.
--
-- Warning depending on your font this may look like @nx@.
-- It is not. The @x@ is the greek letter chi.
nχ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
nχ =
  kB < (/=)


-- | Parses a single element off the back of a list.
--
-- In general this is slower than 'χ' since it must traverse to the end of the list.
--
-- ===== __Examples__
--
-- >>> uP (bx '!') "Hello, world!"
-- [("Hello, world",'!')]
--
bx ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> Parser m (List a) (List a) a
bx =
  bkB < eq


-- | Parse a single character giving a particular result if it accepts.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (xx 's' "z") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSo $ xx 's' 'z' #| hd) "passenger"
-- [("","pazzenger")]
--
xx ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> b -> Parser m (List a) (List a) b
xx =
  fpM < χ


-- | Parse a single character off the back of a list giving a particular result if it accepts.
bXx ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => a -> b -> Parser m (List a) (List a) b
bXx =
  fpM < bx


-- | Uncons and apply 'xx' to create a parser.
--
-- If the input is empty this returns 'en'.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (xxh "sz") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSO $ xxh "sz" #| hdS) "passenger"
-- [("","pazzenger")]
--
xxh ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => List a -> Parser m (List a) (List a) (List a)
xxh [] =
  [] <$ en
xxh (x : xs) =
  xx x xs


-- | Zips with 'xx'.
zWx ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => List a -> List b -> List (Parser m (List a) (List a) b)
zWx =
  zW xx


-- | Parses a single digit character.
dg ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
dg =
  kB iD


-- | Parses a single alphabetic character.
af ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
af =
  kB α


-- | Parses a single whitespace character.
p_ ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
p_ =
  kB iW


-- | Parses a single control character.
pc ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
pc =
  kB iC


-- | Parses a single lowercase character.
pL ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
pL =
  kB iL


-- | Parses a single uppercase character.
pU ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
pU =
  kB iU


-- | Parses a single alpha-numeric character.
aN ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
aN =
  kB iA


-- | Parses a single printable character.
pR ::
  ( Alternative m
  , Monad m
  )
    => Parser m String String Char
pR =
  kB iP


-- | Parses a complete complete word.
-- That is it parses all alphabetic characters off the front of a string.
-- If there are none it fails.
wd ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Parser m String String String
wd =
  gSo af


-- | Parses a string into a list of whitespace separated chunks.
wr ::
  ( Monad m
  , Foldable m
  , Alternative m
  )
    => Parser m String String (List String)
wr =
  gMy $ gSo (nK iW) <* gMy p_


{-# Deprecated string "Use ʃ instead" #-}
-- | Long version of 'ʃ'.
-- Parses a string accepting it if it is equal to the given string
string ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List a -> Parser m (List a) (List a) (List a)
string =
  ʃ


-- | Parses a string accepting it if it is equal to the given string.
ʃ ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List a -> Parser m (List a) (List a) (List a)
ʃ =
  Prelude.traverse χ


-- | Parses at least one of the given string.
s_ ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => List a -> Parser m (List a) (List a) (List a)
s_ =
  sO < ʃ


-- | Parses any number of the given string.
s' ::
  ( Eq a
  , Alternative m
  , Monad m
  )
    => List a -> Parser m (List a) (List a) (List a)
s' =
  mY < ʃ


-- | Parses the representation of an object off the string.
-- It returns the consumed string.
hʃ ::
  ( Monad m
  , Alternative m
  , Show a
  )
    => a -> Parser m String String String
hʃ =
  ʃ < sh


-- | Parses the representation of an object off the string.
-- It returns the input.
yʃ ::
  ( Monad m
  , Alternative m
  , Show a
  )
    => a -> Parser m String String a
yʃ =
  pM *^ hʃ


-- | Parses a string off the back of a list.
--
-- In general this is much less efficient than 'ʃ'.
bH ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List a -> Parser m (List a) (List a) (List a)
bH =
  Prelude.traverse bx < Rv


-- | Parses a string giving a particular result if it accepts.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (hh "st" "z") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSo $ hh "ss" 'z' ++ hd) "passenger"
-- [("","pazenger")]
--
hh ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List a -> b -> Parser m (List a) (List a) b
hh =
  fpM < ʃ


-- | Parses a string off the back of a list giving a particular result if it accepts.
--
-- In general this is much less efficient than 'hh'.
bhh ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List a -> b -> Parser m (List a) (List a) b
bhh =
  fpM < bH


-- | Zips with 'hh'.
zWh ::
  ( Eq a
  , Monad m
  , Alternative m
  )
    => List (List a) -> List b -> List (Parser m (List a) (List a) b)
zWh =
  zW hh


-- | A parser which takes a list of options and matches any of them.
--
-- ==== __Examples__
--
-- A parser which parses any one vowel
--
-- >>> f=xay"aeiou"
-- >>> gP f "agf"
-- ['a']
-- >>> gP f "xs"
-- []
--
xay ::
  ( Eq a
  , Foldable f
  , Alternative m
  , Monad m
  )
    => f a -> Parser m (List a) (List a) a
xay =
  kB < fe


-- | A parser which takes a list of characters and matches any of them as a string.
--
-- Like 'xay' but @xay@ gives a character and @xys@ gives a string.
--
-- ==== __Examples__
--
-- A parser which parses any one vowel
--
-- >>> f=xys"aeiou"
-- >>> gP f "agf"
-- ["a"]
-- >>> gP f "xs"
-- []
--
xys ::
  ( Eq a
  , Foldable f
  , Monad m
  , Alternative m
  )
    => f a -> Parser m (List a) (List a) (List a)
xys =
  p << xay


-- | A parser which takes a list of options and matches one character not in the options.
--
-- ==== __Examples__
--
-- A parser which parses anything other than a vowel
--
-- >>> f=nxy"aeiou"
-- >>> gP f "agf"
-- []
-- >>> gP f "xs"
-- ['x']
--
nxy ::
  ( Eq a
  , Foldable f
  , Alternative m
  , Monad m
  )
    => f a -> Parser m (List a) (List a) a
nxy =
  kB < fE


-- | A parser which takes a list of characters and matches any of them as a string.
--
-- Like 'nxy' but @nxy@ gives a character and @nys@ gives a string.
--
nys ::
  ( Eq a
  , Foldable f
  , Alternative m
  , Monad m
  )
    => f a -> Parser m (List a) (List a) (List a)
nys =
  p << nxy


-- | Takes a list of options and parses at least one option.
--
-- Equivalent to @'so' < 'xay'@.
xyo ::
  ( Alternative m
  , Monad m
  , Eq a
  , Foldable f
  )
    => f a -> Parser m (List a) (List a) (List a)
xyo =
  so < xay


-- | Takes a list of options and parses zero or more options.
--
-- Equivalent to @'my' < 'xay'@.
xyy ::
  ( Alternative m
  , Monad m
  , Eq a
  , Foldable f
  )
    => f a -> Parser m (List a) (List a) (List a)
xyy =
  my < xay


-- | A parser which takes a list of strings and matches any one of them with 'ʃ'.
--
-- ==== __Examples__
--
-- >>> gP (asy ["axy","a","b"]) "axyl"
-- ["axy","a"]
asy ::
  ( Eq a
  , Foldable f
  , Functor f
  , Alternative m
  , Monad m
  )
    => f (List a) -> Parser m (List a) (List a) (List a)
asy =
  cM ʃ


-- | Takes a list of strings and matches at least one of them with 'ʃ'.
--
-- Equivlant to @'so' < 'asy'@.
syo ::
  ( Eq a
  , Foldable f
  , Functor f
  , Alternative m
  , Monad m
  )
    => f (List a) -> Parser m (List a) (List a) (List (List a))
syo =
  so < asy


-- | Takes a list of strings, matches at least one of them with 'ʃ', and combines the results with the monoidal action.
--
-- Equivlant to @'sO' < 'asy'@.
syO ::
  ( Eq a
  , Foldable f
  , Functor f
  , Alternative m
  , Monad m
  )
    => f (List a) -> Parser m (List a) (List a) (List a)
syO =
  sO < asy


-- | Takes a list of strings and matches zero or more of them with 'ʃ'.
--
-- Equivlant to @'my' < 'asy'@.
syy ::
  ( Eq a
  , Foldable f
  , Functor f
  , Alternative m
  , Monad m
  )
    => f (List a) -> Parser m (List a) (List a) (List (List a))
syy =
  my < asy


-- | Takes a list of strings, matches zero or more of them with 'ʃ', and combines the results with the monoidal action.
--
-- Equivlant to @'mY' < 'asy'@.
syY ::
  ( Eq a
  , Foldable f
  , Functor f
  , Alternative m
  , Monad m
  )
    => f (List a) -> Parser m (List a) (List a) (List a)
syY =
  mY < asy


-- | Parse a readable.
--
-- Warning depending on your font this may look like @v@.
-- It is not.
-- It is the greek letter nu.
ν ::
  ( Read a
  , Alternative m
  , Monad m
  )
    => Parser m String String a
ν =
  νa h'


-- | Parse a readable with a given witness type.
-- The witness is unused, but the output will have the same type.
--
-- Warning depending on your font this may look like @vw@.
-- It is not.
-- The first letter, @ν@, is the greek letter nu.
--
-- ==== __Examples__
--
-- >>> uP (νw ()) "()HELLO"
-- [("HELLO",())]
-- >>> uP (νw ()) "(((())))HELLO"
-- [("HELLO",())]
-- >>> uP (νw 1) "23HELLO"
-- [("HELLO",23)]
-- >>> uP (νw T) "False HELLO"
-- [(" HELLO",False)]
-- >>> uP (νw T) "True"
-- [("",True)]
-- >>> uP (νw B) "TrueHELLO"
-- []
-- >>> uP (νw T) "TrueHELLO"
-- []
νw ::
  ( Read a
  , Alternative m
  , Monad m
  )
    => a -> Parser m String String a
νw _ =
  ν


-- | Takes a parser and creates a new parser which reads the results.
-- If the read fails the parse is ignored.
--
-- Warning depending on your font this may look like @va@.
-- It is not.
-- The first letter, @ν@, is the greek letter nu.
νa ::
  ( Read b
  , Alternative m
  , Monad m
  )
    => Parser m a a String -> Parser m a a b
νa parser = do
  x <- parser
  case
      Text.Read.readMaybe x
    of
      Nothing ->
        em
      Just value ->
        p value


-- | Parses a single digit and returns it as a number.
--
-- Warning depending on your font this may look like @vd@.
-- It is not. @ν@ is the greek letter nu.
νd ::
  ( Integral i
  , Alternative m
  , Monad m
  )
    => Parser m String String i
νd =
  Prelude.fromInteger < νa (p < dg)


-- | Parse a hexadecimal integer case insensitive.
-- This parser is greedy.
--
-- ==== __Examples__
--
-- Will always give at least one parse, if it consumes nothing it will result in zero.
--
-- >>> gP p16 ""
-- [0]
-- >>> gP p16 "11"
-- [17]
-- >>> gP p16 "11m"
-- [17]
-- >>> gP p16 "1bAm"
-- [442]
--
p16 ::
  ( Foldable m
  , Monad m
  , Alternative m
  )
    => Parser m String String Prelude.Int
p16 =
  go 0
  where
    go n =
      ( (16*n+) < digit >~ go )
      #| p n
    digit =
      (Prelude.read < p < xay "0123456789")
      #| (xay "Aa" *> p 10)
      #| (xay "Bb" *> p 11)
      #| (xay "Cc" *> p 12)
      #| (xay "Dd" *> p 13)
      #| (xay "Ee" *> p 14)
      #| (xay "Ff" *> p 15)


-- | Runs a parser on the back of a list.
--
-- ==== __Examples__
--
-- Here's it being used to create a parser which matches palindromes:
--
-- >>> f=lA(gMy hd)>~bkw<ʃ
-- >>> x1 f "abeba"
-- True
-- >>> x1 f "abba"
-- True
-- >>> x1 f "abeja"
-- False
--
-- @gMy hd@ will parse the entire input.
-- @lA@ makes it a lookahead.
-- We use @bkw<ʃ@ to parse the same thing off the end of the list.
bkw ::
  ( Reversable r
  , Monoid (r a)
  , Functor m
  )
    => Parser m (r a) (r a) b -> Parser m (r a) (r a) b
bkw (P func) =
  P $ m (mst Rv) < func < Rv


-- | A parser which matches when the input has been completely consumed.
en ::
  ( Eq a
  , Monoid a
  , Alternative m
  )
    => Parser m a a ()
en =
  P go
  where
    go x
      | x == i
      =
        p (i, ())
    go _ =
      em


-- | A parser which matches when the input has been completely consumed.
-- Gives the empty list as a result.
ens ::
  ( Eq a
  , Monoid a
  , Alternative m
  )
    => Parser m a a (List b)
ens =
  [] <$ en


-- | Takes a value and creates a "parser" which unwrites that prepends that value to the working string.
-- The resulting parser always returns @()@.
-- For a version which returns the object given see 'bSh'.
--
-- You can think of this as doing the opposite of what 'ʃ' does.
--
-- Because @bʃ@ violates monotonicity there can be some real performance issues with solutions that use it.
--
-- ==== __Examples__
--
-- >>> uP (bʃ"Hello ") "world!"
-- [("Hello world!",())]
bʃ ::
  ( Semigroup m
  , Applicative f
  )
    => m -> Parser f m m ()
bʃ xs =
  P
    { uP' =
      ( \ unprocessed ->
        p (xs <> unprocessed, ())
      )
    }


-- | Takes a value and creates a "parser" which unwrites that prepends that value to the working string.
-- The resulting parser returns the object given.
-- For a version which always returns @()@ see @bʃ@.
--
-- You can think of this as doing the opposite of what 'ʃ' does.
--
-- Because @bSh@ violates monotonicity there can be some real performance issues with solutions that use it.
--
-- ==== __Examples__
--
-- >>> uP (bSh "Hello ") "world!"
-- [("Hello world!","Hello ")]
bSh ::
  ( Monoid m
  , Applicative f
  )
    => m -> Parser f m m m
bSh =
  (i <$) < bʃ


-- | Intersperses two parsers.
--
-- ==== __Examples__
--
-- >>> f=isP wd(ʃ",")
-- >>> gc f "hello,world,hi,word"
-- [["hello","world","hi","word"]]
--
isP ::
  ( Alternative m
  , Monad m
  )
    => Parser m a a c -> Parser m a a d -> Parser m a a (List c)
    -- => Parser m a b c -> Parser m b a d -> Parser m a b (List c)
isP parser1 parser2 =
  p [] ++ l2 (:) parser1 (my (parser2 #*> parser1))


-- | Flip of 'isP'.
fiP ::
  ( Alternative m
  , Monad m
  )
    => Parser m a a d -> Parser m a a c -> Parser m a a (List c)
    -- => Parser m b a d -> Parser m a b c -> Parser m a b (List c)
fiP =
  F isP


-- | Infix of 'isP'.
($|$) ::
  ( Alternative m
  , Monad m
  )
    => Parser m a a c -> Parser m a a d -> Parser m a a (List c)
    -- => Parser m a b c -> Parser m b a d -> Parser m a b (List c)
($|$) =
  isP


{-# Deprecated ivP "Use Rv instead" #-}
-- | Inverts the priority of parses.
--
-- ==== __Examples__
--
-- Here we have a two parsers each which consume two characters.
-- @h_@ will parse any number of characters giving with more characters having higher priority.
--
-- >>> gP (h_) "ab"
-- ["ab","a"]
--
-- @ivP@ takes this parser and switches the priority so fewer characters have higher priority.
--
-- >>> gP (ivP $ h_) "ab"
-- ["a","ab"]
--
ivP ::
  ( Reversable m
  )
    => Parser m a b c -> Parser m a b c
ivP parser =
  P
    { uP' =
      Rv < uP' parser
    }


-- | Removes all parses other than the one with the highest priority.
--
-- Can be used to retroactively make a parser greedy.
gre ::
  (
  )
    => Parser List a b c -> Parser List a b c
gre parser =
  P
    { uP' =
      Prelude.take 1 < uP' parser
    }


-- | Removes all parses ohter than the one with the lowest priority.
--
-- Can be used to retroactively make a parser lazy.
laz ::
  (
  )
    => Parser List a b c -> Parser List a b c
laz =
  gre < ivP


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result of @p@ is ignored and only the preceding string is returned.
--
-- For a version which returns only the result of @p@ see 'peW'.
--
-- ==== __Examples__
--
-- Parse for any string up to a @y@
--
-- >>> gP (pew $ χ 'y') "pollywoggy"
-- ["pollywogg","poll"]
pew ::
  ( Monad m
  , Alternative m
  )
    => Parser m (List a) (List a) b -> Parser m (List a) (List a) (List a)
pew =
  aK h'


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result is the result of the @p@ parse and discards the leading sequence.
--
-- For a version which returns only the prefix see 'pew'.
--
-- ==== __Examples__
--
-- Parse any capital letter.
--
-- >>> gP (peW $ pUS) "Sam the Dog"
-- ["D","S"]
peW ::
  ( Monad m
  , Alternative m
  )
    => Parser m (List a) (List a) b -> Parser m (List a) (List a) b
peW =
  aT h'


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result the preceding string combined with the result of the parse of @p@.
pEw ::
  ( Monad m
  , Alternative m
  )
    => Parser m (List a) (List a) (List a) -> Parser m (List a) (List a) (List a)
pEw =
  l2p h'


-- | Sandwiches a parser between two copies of another parser.
-- Returns the result of the middle parser.
--
-- ===== __Examples__
--
-- To parse a word surrounded in quotes you can use @enk (ʃ"\"") wd@.
--
enk ::
  ( Monad m
  )
    => Parser m a a b -> Parser m a a c -> Parser m a a c
enk parser1 parser2 =
  parser1 *> parser2 <* parser1


-- | Flip of 'enk'.
fNk ::
  ( Monad m
  )
    => Parser m a a b -> Parser m a a c -> Parser m a a b
fNk =
  f' enk


-- | Run a parser and then attempt to parse its result.
--
-- More general version of @'fb' 'ʃ'@.
--
-- ===== __Examples__
--
-- Parse a string of the form @a ++ a@:
--
-- >>> p=yS h_
-- >>> gP p "papa"
-- ["pa"]
-- >>> gP p "mapa"
-- []
yS ::
  ( Eq b
  , Monad m
  , Alternative m
  )
    => Parser m a (List b) (List b) -> Parser m a (List b) (List b)
yS parser =
  P (\ j ->
      uP' parser j >~ U (F $ uP' < ʃ)
    )


-- | Run a parser which produces an element and then attempt to parse its result.
--
-- More general version of @'fb' 'χ'@.
--
-- Warning depending on your font this may look like @yx@.
-- It is not, the second letter is the greek letter chi.
--
-- ===== __Examples__
--
-- Parse a string of two equal characters:
--
-- >>> p=yχ hd
-- >>> gP p "aa"
-- "a"
-- >>> gP p "xx"
-- "x"
-- >>> gP p "xy"
-- ""
yχ ::
  ( Eq b
  , Monad m
  , Alternative m
  )
    => Parser m a (List b) b -> Parser m a (List b) b
yχ parser =
  P (\ j ->
      uP' parser j >~ U (F $ uP' < χ)
    )


-- | Prefix version of '(@<*)'.
yju ::
  ( Monad m
  )
    => Parser m a b c -> Parser m c d e -> Parser m a b e
yju =
  (@<*)


-- | Flip of 'yju'.
fyj ::
  ( Monad m
  )
    => Parser m a b c -> Parser m d e a -> Parser m d e c
fyj =
  f' yju


-- | Run one parser on the result of another.
-- Gives the remainder of the first parser.
(@<*) ::
  ( Monad m
  )
    => Parser m a b c -> Parser m c d e -> Parser m a b e
p1 @<* p2 =
  Sw (Sw p1 #<* Sw p2)


-- | Prefix version of '(@*>)'.
yku ::
  ( Monad m
  )
    => Parser m a b c -> Parser m c d e -> Parser m a d e
yku =
  (@*>)


-- | Flip of 'yku'.
fyk ::
  ( Monad m
  )
    => Parser m a b c -> Parser m d e a -> Parser m d b c
fyk =
  f' yku


-- | Run one parser on the result of another.
-- Gives the remainder of the second parser.
(@*>) ::
  ( Monad m
  )
    => Parser m a b c -> Parser m c d e -> Parser m a d e
p1 @*> p2 =
  Sw (Sw p1 #*> Sw p2)


-- | Prefix version of '(@^*)'.
ymu ::
  ( Semigroup b
  , Monad m
  )
    => Parser m a b c -> Parser m c b d -> Parser m a b d
ymu =
  (@^*)


-- | Flip of 'ymu'.
fym ::
  ( Semigroup b
  , Monad m
  )
    => Parser m a b c -> Parser m d b a -> Parser m d b c
fym =
  f' ymu


-- | Run one parser on the result of another.
-- Combines the remainders with the semigroup action.
--
-- Often the first parser is used to restrict the domain the second parser acts on.
(@^*) ::
  ( Semigroup b
  , Monad m
  )
    => Parser m a b c -> Parser m c b d -> Parser m a b d
P p1 @^* P p2 =
  P $ \ i -> do
    (rem1, v1) <- p1 i
    (rem2, v2) <- p2 v1
    p (rem2 <> rem1, v2)


-- | Lift a monoid action to a parser.
--
-- The resulting parser consumes nothing returning the input state unchanged.
--
-- Analogous to 'Control.Monad.Trans.Class.lift'.
mQ ::
  ( Functor m
  )
    => m a -> Parser m b b a
mQ x =
  P
    { uP' =
      \ j ->
        (,) j < x
    }


-- | A particular recursive pattern useful for balanced strings.
--
-- Corresponds to the language
--
-- @
-- H := X H Y H | ε
-- @
--
-- Where @X@ and @Y@ are the given parsers.
--
-- ==== __Examples__
--
-- Determine if a string consists of balanced parentheses:
--
-- >>> f=blj opn cls
-- >>> x1 f "(())(()())()"
-- True
-- >>> x1 f "(())(("
-- False
-- >>> x1 f "))"
-- False
--
-- Determine if there is a way to replace @_@s with parentheses to lead to a balanced string:
--
-- >>> f=(blj.*xys)"(_"")_"
-- >>> cP f "((__"
-- True
-- >>> cP f "((_(_)"
-- True
-- >>> cP f "(_(_)_"
-- True
-- >>> cP f ")___"
-- False
-- >>> cP f "((_)(_()"
-- True
blj ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
blj x y =
  pY $ x *^* blj x y *^* y *^* blj x y


-- | Parse any number of symbol before and after a parser.
ayw ::
  ( Monad m
  , Alternative m
  )
    => Parser m (List a) (List a) (List a) -> Parser m (List a) (List a) (List a)
ayw =
  swg h'


-- | Parse at least one symbol before and after a parser.
ayW ::
  ( Monad m
  , Alternative m
  )
    => Parser m (List a) (List a) (List a) -> Parser m (List a) (List a) (List a)
ayW =
  swg h_
