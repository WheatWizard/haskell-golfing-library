{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
module P.Parse
  ( Parser (..)
  -- * Perform a parse
  , uP
  , gP
  , (-%)
  , gc
  , (-^)
  , gk
  , (-!)
  , glk
  , (-!$)
  , gpW
  , gpB
  , gpg
  , (->#)
  , gpl
  , (-<#)
  , ggL
  , (->|)
  , glL
  , (-<|)
  , gcy
  , (-^*)
  , gcY
  , gky
  , (-!*)
  , gkY
  , sre
  , sk
  , (-!%)
  , ysk
  , (-%%)
  -- ** Tests
  -- | Performs a parse in some way and returns a 'Bool'.
  , pP
  , (-@)
  , pH
  , cP
  , (-#)
  , x1
  , (-$)
  , cpy
  , x1y
  -- * Generalized versions
  -- | These give more general type signatures but usually require additional type information to type check.
  -- They are useful when using very specialized parser types.
  , gP'
  , gc'
  , gPW
  , gPB
  , gPg
  , gPl
  , gGL
  , gLL
  , gCy
  , gCY
  -- ** Tests
  -- | Performs a parse in some way and returns a 'Bool'.
  , pP'
  , pH'
  , cP'
  , x1'
  , cPy
  , x1Y
  -- * Misc
  , lp2
  -- * Deprecated
  , liftParser2
  )
  where


import qualified Prelude hiding (id)


import qualified Control.Applicative
import Control.Monad.Fail
  ( MonadFail
    ( fail
    )
  )


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Applicative
import P.Arrow
import P.Category
import P.Bifunctor.Flip
import P.Bifunctor.Profunctor
import P.Bool
import P.Eq
import P.Fix
import P.Foldable
import P.Foldable.Length
import P.Foldable.MinMax
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Monad
import P.Monad.Plus.Filter
import P.Ord
import P.Reverse
import P.Swap
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A parser type.
newtype Parser m j k a =
  P
    { uP' ::
      (j -> m (k, a))
    }


instance
  ( Functor m
  )
    => Functor (Parser m j k)
  where
    fmap func =
      P < m3 func < uP'


{-# Deprecated liftParser2 "Use lp2 instead" #-}
-- | A more general version of 'Control.Applicative.liftA2' for the 'Parser' type.
--
-- Long version of 'P.Parse.Combinator.lp2'.
liftParser2 ::
  ( Monad m
  )
    => (a -> b -> c) -> Parser m d e a -> Parser m e f b -> Parser m d f c
liftParser2 func (P parser1) (P parser2) =
  P
    ( \ start ->
      do
        (rem1, res1) <- parser1 start
        (rem2, res2) <- parser2 rem1
        p (rem2, func res1 res2)
    )


lp2 ::
  ( Monad m
  )
    => (a -> b -> c) -> Parser m d e a -> Parser m e f b -> Parser m d f c
lp2 =
  liftParser2


instance
  ( Monad f
  )
    => Applicative (Parser f k k)
  where
    pure =
      P < p << F (,)

    liftA2 =
      liftParser2


instance
  ( Alternative m
  , Monad m
  )
    => Alternative (Parser m k k)
  where
    empty =
      P $ p em

    P parser1 <|> P parser2 =
      P $ l2 (++) parser1 parser2


instance
  ( Monad m
  )
    => Monad (Parser m k k)
  where
    P parser1 >>= func =
      P
        ( \ start ->
          do
            (rem1, res1) <- parser1 start
            uP' (func res1) rem1
        )


instance
  ( MonadFail m
  , Monad m
  )
    => MonadFail (Parser m k k)
  where
    fail =
      P < p < fail


instance
  ( Semigroup m
  , Monad f
  )
    => Semigroup (Parser f k k m)
  where
    (<>) =
      l2 (<>)


instance
  ( Monoid m
  , Monad f
  )
    => Monoid (Parser f k k m)
  where
    mempty =
      p i


instance
  ( Foldable f
  , Functor f
  )
    => WeakAlign (Parser f j k)
  where
    cnL parser1 parser2 =
      P
        { uP' =
          ( \ start ->
              let
                parse1 =
                  uP' parser1 start
              in
                if
                  nø parse1
                then
                  parse1
                else
                  uP' parser2 start
          )
      }


instance
  ( Alternative m
  , Monad m
  , Foldable m
  )
    => UnitalZip (Parser m k k)
  where
    aId =
      em


instance
  ( Functor m
  )
    => Swap (Parser m k)
  where
    swap parser =
      P
        { uP' =
          Sw << uP' parser
        }


instance
  ( Reversable m
  )
    => Reversable (Parser m k r)
  where
    _rv parser =
      P
        { uP' =
          Rv < uP' parser
        }


instance
  ( ReversableFunctor m
  )
    => ReversableFunctor (Parser m k r)
  where
    rvW f parser =
      P
        { uP' =
          rvW (m f) < uP' parser
        }


instance
  ( Monad m
  , Semigroup a
  )
    => Semigroupoid (Flip2 (Parser m) a)
  where
    Fl2 f <@ Fl2 g =
      Fl2 $ P
        { uP' =
          \ i -> do
            (j, v1) <- uP' g i
            (k, v2) <- uP' f j
            p (k, v1 <> v2)
        }


instance
  ( Monad m
  , Monoid a
  )
    => Category (Flip2 (Parser m) a)
  where
    id =
      Fl2 $ P
        { uP' =
          \ x -> do
            p (x, i)
        }


instance
  ( Monad m
  , Monoid a
  )
    => LooseArrow (Flip2 (Parser m) a)
  where
    mSt (Fl2 f) =
      Fl2 $ P
        { uP' =
          \ (a, b) -> do
            (j, v) <- uP' f a
            p ((j, b), v)
        }

    mNd (Fl2 f) =
      Fl2 $ P
        { uP' =
          \ (a, b) -> do
            (j, v) <- uP' f b
            p ((a, j), v)
        }

    aSw =
      Fl2 $ P
        { uP' =
          \ (a, b) ->
            p ((b, a), i)
        }


instance
  ( Functor m
  )
    => Functor (Flip (Parser m a) b)
  where
    fmap f (Flp (P g)) =
      Flp $ P
        { uP' =
          mSt f << g
        }


instance
  ( Functor m
  )
    => Functor (Flip2 (Parser m) a b)
  where
    fmap f (Fl2 (P g)) =
      Fl2 $ P
        { uP' =
          mSt f << g
        }


instance
  ( Functor m
  )
    => Profunctor (Flip2 (Parser m) a)
  where
    dmp f g (Fl2 (P h)) =
      Fl2 $ P
        { uP' =
          mSt g << h < f
        }


instance
  ( Monad m
  , Monoid a
  )
    => Arrow (Flip2 (Parser m) a)
  where
    Fl2 (P f) -< Fl2 (P g) =
      Fl2 $ P
        { uP' =
          \ x -> do
            (o1, v1) <- f x
            (o2, v2) <- g x
            p ((o1, o2), v1 <> v2)
        }


-- | Specialized version of the deconstructor 'uP'.
uP ::
  (
  )
    => Parser List k j a -> k -> List (j, a)
uP =
  uP'


-- | Takes a parser and an input and returns @True@ if there is any parse of the input even an incomplete one.
pP ::
  (
  )
    => Parser List k j a -> k -> Bool
pP =
  pP'


-- | Generalized version of 'pP'.
pP' ::
  ( Foldable t
  )
    => Parser t k j a -> k -> Bool
pP' =
  Prelude.not << Prelude.null << uP'


-- | Infix of 'pP'.
(-@) ::
  (
  )
    => Parser List k j a -> k -> Bool
(-@) =
  pP


-- | Takes a parser and an input and returns @True@ if there is any contiguous substring of the input that can be parsed.
pH ::
  (
  )
    => Parser List (List a) b c -> List a -> Bool
pH =
  pH'


-- | Generalized version of 'pH'.
pH' ::
  ( Foldable t
  , Alternative t
  , Monad t
  )
    => Parser t (List a) b c -> List a -> Bool
pH' parser =
  pP' $ P
    ( \ start ->
      do
        (rem1, _) <- uP' (my $ P go) start
        uP' parser rem1
    )
  where
    go [] =
      em
    go (head : tail) =
      p (tail, head)


-- | Takes a parser and an input and returns @True@ if there is at least 1 complete parse.
--
-- A complete parse being a parse where the remainder is the identiy of the canonical monoid.
-- For most cases lists are being parsed so this is the empty list.
cP ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> Bool
cP =
  cP'


-- | Generalized version of 'cP'.
cP' ::
  ( Monoid j
  , Eq j
  , Alternative t
  , Monad t
  , Foldable t
  )
    => Parser t k j a -> k -> Bool
cP' =
  Prelude.not << Prelude.null << gc'


-- | Infix of 'cP'.
(-#) ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> Bool
(-#) =
  cP


-- | Takes a parser and an input and returns @True@ if there is exactly 1 complete parse.
x1 ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> Bool
x1 =
  x1'


-- | Generalized version of 'x1'.
x1' ::
  ( Monoid j
  , Eq j
  , Foldable t
  , Alternative t
  , Monad t
  )
    => Parser t k j a -> k -> Bool
x1' =
  eq 1 << Prelude.length << gc'


-- | Infix of 'x1'.
(-$) ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> Bool
(-$) =
  x1


-- | Gets the results of all parses.
gP ::
  (
  )
    => Parser List k j a -> k -> List a
gP =
  gP'


-- | Generalized version of 'gP'.
gP' ::
  ( Functor t
  )
    => Parser t k j a -> k -> t a
gP' =
  m Prelude.snd << uP'


-- | Infix of 'gP'.
(-%) ::
  (
  )
    => Parser List k j a -> k -> List a
(-%) =
  gP


-- | Gets all complete parses.
gc ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> List a
gc =
  gc'


-- | Generalized version of 'gc'
gc' ::
  ( Monoid j
  , Eq j
  , Alternative t
  , Monad t
  )
    => Parser t k j a -> k -> t a
gc' =
  Prelude.snd <<< fl (eq i < Prelude.fst) << uP'


-- | Infix of 'gc'.
(-^) ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> List a
(-^) =
  gc


-- | Gets the first complete parse, throws an error if there are no parses.
gk ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> a
gk parser x =
  case
    gc parser x
  of
    x : _ ->
      x
    [] ->
      Prelude.error "gk with no parse"


-- | Infix of 'gk'.
(-!) ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> a
(-!) =
  gk


-- | Gets the last complete parse, throws an error if there are no parses.
glk ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> a
glk parser x =
  case
    Prelude.reverse $ gc parser x
  of
    x : _ ->
      x
    [] ->
      Prelude.error "glk with no parse"


-- | Infix of 'glk'.
(-!$) ::
  ( Monoid j
  , Eq j
  )
    => Parser List k j a -> k -> a
(-!$) =
  glk


-- | Gets the highest priority parse using a user defined comparison to determine priority.
-- This throws an error if there is no parse.
gpW ::
  (
  )
    => (a -> a -> Ordering) -> Parser List k j a -> k -> a
gpW comp =
  x_W "gpW with no parse" comp << gP


-- | Generalized version of 'gpW'.
gPW ::
  ( Foldable t
  , Functor t
  )
    => (a -> a -> Ordering) -> Parser t k j a -> k -> a
gPW comp =
  x_W "gPW with no parse" comp << gP'


-- | Gets the highest priority parse using a user defined conversion to determine priority.
-- This throws an error if there is no parse.
gpB ::
  ( Ord b
  )
    => (a -> b) -> Parser List k j a -> k -> a
gpB conv =
  x_W "gpB with no parse" (cb conv) << gP


-- | Generalized version of 'gpB'.
gPB ::
  ( Ord b
  , Foldable t
  , Functor t
  )
    => (a -> b) -> Parser t k j a -> k -> a
gPB conv =
  x_W "gPB with no parse" (cb conv) << gP'


-- | Gets the greatest parse.
gpg ::
  ( Ord a
  )
    => Parser List k j a -> k -> a
gpg =
  x_W "gpg with no parse" cp << gP


-- | Generalized version of 'gpg'.
gPg ::
  ( Ord a
  , Foldable t
  , Functor t
  )
    => Parser t k j a -> k -> a
gPg =
  x_W "gPg with no parse" cp << gP'


-- | Infix of 'gpg'.
(->#) ::
  ( Ord a
  )
    => Parser List k j a -> k -> a
(->#) =
  x_W "(->#) with no parse" cp << gP


-- | Gets the least parse.
gpl ::
  ( Ord a
  )
    => Parser List k j a -> k -> a
gpl =
  x_W "gpl with no parse" fcp << gP


-- | Generalized version of 'gpl'.
gPl ::
  ( Ord a
  , Foldable t
  , Functor t
  )
    => Parser t k j a -> k -> a
gPl =
  x_W "gPl with no parse" fcp << gP'


-- | Infix of 'gpl'.
(-<#) ::
  ( Ord a
  )
    => Parser List k j a -> k -> a
(-<#) =
  x_W "(-<#) with no parse" fcp << gP


-- | Gets the longest parse.
ggL ::
  ( Foldable f
  )
    => Parser List k j (f a) -> k -> f a
ggL =
  x_W "ggL with no parse" lCp << gP


-- | Generalized version of 'ggL'.
gGL ::
  ( Foldable f
  , Foldable t
  , Functor t
  )
    => Parser t k j (f a) -> k -> f a
gGL =
  x_W "gGL with no parse" lCp << gP'


-- | Infix of 'ggL'.
(->|) ::
   ( Foldable f
   )
     => Parser List k j (f a) -> k -> f a
(->|) =
  x_W "(->|) with no parse" lCp << gP


-- | Gets the shortest parse.
glL ::
  ( Foldable f
  )
    => Parser List k j (f a) -> k -> f a
glL =
  x_W "glL with no parse" (f' lCp) << gP


-- | Generalized version of 'glL'
gLL ::
  ( Foldable f
  , Foldable t
  , Functor t
  )
    => Parser t k j (f a) -> k -> f a
gLL =
  x_W "gLL with no parse" (f' lCp) << gP'


-- | Infix of 'glL'.
(-<|) ::
  ( Foldable f
  )
    => Parser List k j (f a) -> k -> f a
(-<|) =
  x_W "(-<|) with no parse" (f' lCp) << gP


-- | Apply a parser as many times as is required to consume the input and return complete parses.
gcy ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> List (List a)
gcy =
  gCy


-- | Generalized version of 'gcy'.
gCy ::
  ( Monoid j
  , Eq j
  , Monad t
  , Alternative t
  , Alternative (Parser t k j)
  )
    => Parser t k j a -> k -> t (List a)
gCy =
  gc' < my


-- | Infix of 'gcy'.
(-^*) ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> List (List a)
(-^*) =
  gcy


-- | 'gcy' and concat the results with the monoid action.
gcY ::
  ( Monoid j
  , Eq j
  , Monoid a
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> List a
gcY =
  gc < mY


-- | Generalized version of 'gcY'.
gCY ::
  ( Monoid j
  , Eq j
  , Monoid a
  , Monad t
  , Alternative t
  , Alternative (Parser t k j)
  )
    => Parser t k j a -> k -> t a
gCY =
  gc' < mY


-- | Apply a parser as many times as is required to consume the input and return the first result.
--
-- Throws an error if there is no complete parse.
gky ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> List a
gky =
  gk < my


-- | Infix of 'gky'.
(-!*) ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> List a
(-!*) =
  gky


-- | 'gky' and concat the results with the monoid action.
--
-- Throws an error if there is no complete parse.
gkY ::
  ( Monoid j
  , Eq j
  , Monoid a
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> a
gkY =
  gk < mY


-- | Determines if a parser can be applied multiple times to fully consume the input.
cpy ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> Bool
cpy =
  cPy


-- | Generalized version of 'cpy'.
cPy ::
  ( Monoid j
  , Eq j
  , Alternative (Parser t k j)
  , Alternative t
  , Monad t
  , Foldable t
  )
    => Parser t k j a -> k -> Bool
cPy =
  cP' < my


-- | Determines if there is exactly one complete parse by applying a parser multiple times.
x1y ::
  ( Monoid j
  , Eq j
  , Alternative (Parser List k j)
  )
    => Parser List k j a -> k -> Bool
x1y =
  x1Y


-- | Generalized version of 'x1y'
x1Y ::
  ( Monoid j
  , Eq j
  , Alternative (Parser t k j)
  , Foldable t
  , Alternative t
  , Monad t
  )
    => Parser t k j a -> k -> Bool
x1Y =
  x1' < my


-- | Find and replace using parser.
--
-- Finds every way to apply the parser in the input and applies it replacing the parsed input with the result of the parse.
--
-- Output will always include the option of the input with no substitutions and thus you can assume the output is non-empty.
--
-- ==== __Examples__
--
-- All the ways to replace @a@ with @i@:
--
-- >>> sre (xxh"ai") "banana"
-- ["binini","binina","binani","binana","banini","banina","banani","banana"]
--
-- All the ways to replace @aa@ with @oo@:
--
-- >>> sre (hh"aa""oo") "aaaaa"
-- ["ooooa","ooaoo","ooaaa","aoooo","aooaa","aaooa","aaaoo","aaaaa"]
sre ::
  (
  )
    => Parser List (List a) (List a) (List a) -> List a -> List (List a)
sre parser =
  -- gcY (parser ++ hdS)
  gP $ mY (my' (P run) <> parser) <> (P < Prelude.take 1 << uP) (my (P run))
  where
    run [] =
      em
    run (x : xs) =
      [(xs, x)]


-- | Find and replace greedily using a parser.
--
-- Scans through a list applying a parser when it can and replacing the consumed input with the result of the parse.
--
-- If the parser cannot be applied anywhere in the input the input will be returned unchanged.
--
-- ==== __Examples__
--
-- Replace @a@ with @i@:
--
-- >>> sk (xxh"ai") "banana"
-- "binini"
--
-- Greedily replae @aa@ with @oo@:
--
-- >>> sk (hh"aa""oo") "aaaaa"
-- "ooooa"
sk ::
  (
  )
    => Parser List (List a) (List a) (List a) -> List a -> List a
sk parser input =
  case
    sre parser input
  of
    [] ->
      -- Should never happen! Return the input anyway.
      input
    (x : _) ->
      x


-- | Infix version of 'sk'.
(-!%) ::
  (
  )
    => Parser List (List a) (List a) (List a) -> List a -> List a
(-!%) =
  sk


-- | Take a parser and repeatedly replace matching substrings with their result until reaching a fixed point.
--
-- Prefers applying the parser leftwards.
--
-- ==== __Examples__
--
-- Reduce a string by removing consecutive repeated substrings until there are none.
--
-- >>> ysk (h_>~ʃ) "banana"
-- "bana"
-- >>> ysk (h_>~ʃ) "pooaqpoaaq"
-- "poaq"
--
ysk ::
  ( Eq a
  )
    => Parser List (List a) (List a) (List a) -> List a -> List a
ysk =
  yyc < sk


-- | Infix version of 'ysk'.
(-%%) ::
  ( Eq a
  )
    => Parser List (List a) (List a) (List a) -> List a -> List a
(-%%) =
  ysk
