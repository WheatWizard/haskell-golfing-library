{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
module P.Eq
  ( Eq
  , (==)
  , eq
  , (/=)
  , nq
  , qb
  , nqb
  , Eq1 (..)
  , fq1
  , Eq2 (..)
  , fq2
  , q1'
  , q1x
  , rw
  , frw
  -- * Deprecated
  , liftEq
  , liftEq2
  )
  where


import qualified Prelude
import Prelude
  ( Eq
  , Bool
  , (&&)
  )


import P.Aliases
import P.Bifunctor.Flip
import P.Bool
import P.Category.Iso
import P.Function
import P.Function.Flip
import P.Functor


infixr 4  ==, /=


-- | Equality.
--
-- Equivalent to '(Prelude.==)'.
(==) ::
  ( Eq a
  )
    => a -> a -> Bool
(==) =
  (Prelude.==)

-- | Equality.
eq ::
  ( Eq a
  )
    => a -> a -> Bool
eq =
  (==)

-- | Inequality.
--
-- Equivalent to '(Prelude./=)'
(/=) ::
  ( Eq a
  )
    => a -> a -> Bool
(/=) =
  (Prelude./=)


-- | Inequality.
nq ::
  ( Eq a
  )
    => a -> a -> Bool
nq =
  (/=)


-- | Checks equality after applying a substitution function.
--
-- ==== __Examples__
--
-- @qb(%2)@ checks equality mod 2
--
-- >>> qb (% 2) 11 95
-- True
--
-- >>> qb (% 2) 54 17
-- False
--
qb ::
  ( Eq b
  )
    => (a -> b) -> a -> a -> Bool
qb =
  on eq


-- | Checks inequality after applying a substitution function.
--
-- Negation of 'qb'.
--
-- ==== __Examples__
--
-- @nqb(%2)@ checks inequality mod 2
--
-- >>> nqb (% 2) 11 95
-- False
--
-- >>> nqb (% 2) 54 17
-- True
--
nqb ::
  ( Eq b
  )
    => (a -> b) -> a -> a -> Bool
nqb =
  on nq


-- | Expresses a type which can lift equality onto it.
class
  (
  )
    => Eq1 f
  where
    -- | Equivalent to 'liftEq'.
    q1 :: (a -> b -> Bool) -> f a -> f b -> Bool


instance
  (
  )
    => Eq1 List
  where
    q1 userEq (x : xs) (y : ys) =
      userEq x y && q1 userEq xs ys
    q1 _ [] [] =
      T
    q1 _ _ _ =
      B


instance
  ( Eq (p a b)
  , Eq (p b a)
  )
    => Eq (Iso p a b)
  where
    Iso f1 b1 == Iso f2 b2 =
      f1 == f2 Prelude.&& b1 == b2


instance
  ( Eq a
  )
    => Eq1 ((,) a)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  )
    => Eq1 ((,,) a b)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  , Eq c
  )
    => Eq1 ((,,,) a b c)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  , Eq c
  , Eq d
  )
    => Eq1 ((,,,,) a b c d)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  , Eq c
  , Eq d
  , Eq e
  )
    => Eq1 ((,,,,,) a b c d e)
  where
    q1 =
      q1'


instance
  ( Eq a
  )
    => Eq1 (Flip (,) a)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  )
    => Eq1 (Flip ((,,) a) b)
  where
    q1 =
      q1'


instance
  ( Eq a
  , Eq b
  , Eq c
  )
    => Eq1 (Flip ((,,,) a b) c)
  where
    q1 =
      q1'


{-# Deprecated liftEq "Use q1 instead" #-}
-- | long version of 'q1'.
liftEq ::
  ( Eq1 f
  )
    => (a -> b -> Bool) -> f a -> f b -> Bool
liftEq =
  q1


-- | Flip of 'q1'.
fq1 ::
  ( Eq1 f
  )
    => f a -> (a -> b -> Bool) -> f b -> Bool
fq1 =
  F q1


class
  (
  )
    => Eq2 p
  where
    q2 :: (a -> b -> Bool) -> (c -> d -> Bool) -> p a c -> p b d -> Bool


instance
  (
  )
    => Eq2 (,)
  where
    q2 userEq1 userEq2 (x1, y1) (x2, y2) =
      userEq1 x1 x2 && userEq2 y1 y2


instance
  ( Eq a
  )
    => Eq2 ((,,) a)
  where
    q2 userEq1 userEq2 (x1, y1, z1) (x2, y2, z2) =
      eq x1 x2 && userEq1 y1 y2 && userEq2 z1 z2


instance
  ( Eq a
  , Eq b
  )
    => Eq2 ((,,,) a b)
  where
    q2 userEq1 userEq2 (w1, x1, y1, z1) (w2, x2, y2, z2) =
      eq w1 w2 && eq x1 x2 && userEq1 y1 y2 && userEq2 z1 z2


instance
  ( Eq a
  , Eq b
  , Eq c
  )
    => Eq2 ((,,,,) a b c)
  where
    q2 userEq1 userEq2 (v1, w1, x1, y1, z1) (v2, w2, x2, y2, z2) =
      eq v1 v2 && eq w1 w2 && eq x1 x2 && userEq1 y1 y2 && userEq2 z1 z2


instance
  ( Eq a
  , Eq b
  , Eq c
  , Eq d
  )
    => Eq2 ((,,,,,) a b c d)
  where
    q2 userEq1 userEq2 (u1, v1, w1, x1, y1, z1) (u2, v2, w2, x2, y2, z2) =
      eq u1 u2 && eq v1 v2 && eq w1 w2 && eq x1 x2 && userEq1 y1 y2 && userEq2 z1 z2


instance
  ( Eq2 p
  )
    => Eq2 (Flip p)
  where
    q2 userEq2 userEq1 (Flp x) (Flp y) =
      q2 userEq1 userEq2 x y


{-# Deprecated liftEq2 "Use q2 instead" #-}
-- | long version of 'q2'.
liftEq2 ::
  ( Eq2 p
  )
    => (a -> b -> Bool) -> (c -> d -> Bool) -> p a c -> p b d -> Bool
liftEq2 =
  q2


-- | A version of 'q1' defined for members of 'Eq2'.
-- Useful to reduce duplication between instance delcarations.
q1' ::
  ( Eq2 p
  , Eq c
  )
    => (a -> b -> Bool) -> p c a -> p c b -> Bool
q1' =
  q2 eq


-- | Flip of 'q2'.
fq2 ::
  ( Eq2 p
  )
    => (a -> b -> Bool) -> (c -> d -> Bool) -> p c a -> p d b -> Bool
fq2 =
  F q2


-- | Like 'q1'' but it 'eq' on the second argument and a user defined equality on the first.
q1x ::
  ( Eq2 p
  , Eq c
  )
    => (a -> b -> Bool) -> p a c -> p b c -> Bool
q1x =
  fq2 eq


-- | Replace all occurences of an element with another.
--
-- ===== __Examples__
--
-- Replace all @%@s with @A@s:
--
-- >>> rw '%' 'A' "{%%}"
-- "{AA}"
rw ::
  ( Functor f
  , Eq a
  )
    => a -> a -> f a -> f a
rw x y =
  mwc (eq x) y


-- | Flip of 'rw'.
frw ::
  ( Functor f
  , Eq a
  )
    => a -> a -> f a -> f a
frw =
  f' rw
