module P.Distributive
  ( Distributive (..)
  , fds
  , fcl
  )
  where


import qualified Prelude


import P.Category
import P.Function.Compose
import P.Function.Flip


class
  ( Functor f
  )
    => Distributive f
  where
    dis ::
      ( Functor g
      )
        => g (f a) -> f (g a)
    dis =
      cll id

    cll ::
      ( Functor g
      )
        => (a -> f b) -> g a -> f (g b)
    cll =
      dis << m


instance Distributive ((->) a) where
  dis x y =
    ($ y) < x
  cll func x y =
    (`func` y) < x


-- | Flip of 'fds'.
fds ::
  ( Functor f
  )
    => a -> f (a -> b) -> f b
fds =
  F dis


-- | Flip of 'cll'.
fcl ::
  ( Functor f
  , Distributive g
  )
    => f a -> (a -> g b) -> g (f b)
fcl =
  F cll
