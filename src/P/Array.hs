{-# Language GeneralizedNewtypeDeriving #-}
{-# Language MultiParamTypeClasses #-}
{-# Language FlexibleInstances #-}
module P.Array
  ( Array
  , lAr
  -- * Deprecated
  , listArray
  , elems
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , (+)
  , Show
  , Read
  , compare
  )
import qualified Data.Foldable as F


import qualified Data.Array as Array


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Arrow
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Ix
import P.Map.Class
import P.Ord hiding
    ( compare
    )


-- | A wrapper that replaces the 'Array.Ix' instance with the 'P.Ix.Ix' instance.
--
-- It is an internal type used for the implementation of 'Array'.
newtype IxWrap a =
  IxWrap
    { ixUnwrap ::
      a
    }
  deriving
    ( Eq
    , Ord
    , Show
    , Read
    )


instance
  ( Ord a
  )
    => Prelude.Ord (IxWrap a)
  where
    compare =
      otp << cp


instance
  ( Ix a
  )
    => Array.Ix (IxWrap a)
  where
    range (IxWrap min, IxWrap max) =
      IxWrap < uRg (min, max)
    index (IxWrap min, IxWrap max) (IxWrap val) =
      uDx (min, max) val
    rangeSize (IxWrap min, IxWrap max) =
      uGz (min, max)
    inRange (IxWrap min, IxWrap max) (IxWrap val) =
      uIr (min, max) val


-- | An array type.
newtype Array i a =
  Array (Array.Array (IxWrap i) a)
  deriving
    ( Eq
    , Show
    , Read
    )


instance
  ( Ord a
  , Ix i
  )
    => Ord (Array i a)
  where
    cp (Array x) (Array y) =
      cp (Array.assocs x) (Array.assocs y)


instance
  (
  )
    => Functor (Array i)
  where
    fmap f (Array xs) =
      Array (f < xs)


instance
  ( Ix i
  )
    => Indexable (Array i) i
  where
    Array xs !? k
      | Array.inRange (Array.bounds xs) (IxWrap k)
      =
        [xs Array.! IxWrap k]
      | Prelude.otherwise
      =
        []

    mr k (Array xs) =
      Array.inRange (Array.bounds xs) (IxWrap k)


instance
  ( Ix i
  )
    => ReverseIndexable (Array i) i
  where
    tAL (Array xs) =
      mSt ixUnwrap < Array.assocs xs

    aix (Array xs) =
      ixUnwrap < Array.indices xs


instance
  (
  )
    => Foldable (Array i)
  where
    fold (Array xs) =
      F.fold xs
    foldMap f (Array xs) =
      F.foldMap f xs
    foldMap' f (Array xs) =
      F.foldMap' f xs
    foldr f x (Array xs) =
      F.foldr f x xs
    foldr' f x (Array xs) =
      F.foldr' f x xs
    foldl f x (Array xs) =
      F.foldl f x xs
    foldl' f x (Array xs) =
      F.foldl' f x xs
    foldr1 f (Array xs) =
      F.foldr1 f xs
    foldl1 f (Array xs) =
      F.foldl1 f xs
    toList (Array xs) =
      F.toList xs
    null (Array xs) =
      F.null xs
    length (Array xs) =
      F.length xs
    elem x (Array xs) =
      F.elem x xs
    maximum (Array xs) =
      F.maximum xs
    minimum (Array xs) =
      F.minimum xs
    sum (Array xs) =
      F.sum xs
    product (Array xs) =
      F.product xs


instance
  ( Ix i
  , Integral i
  )
    => Semigroup (Array i a)
  where
    ar1 <> ar2 =
      listArray (0, l ar1 + l ar2) (elems ar1 <> elems ar2)


instance
  ( Ix i
  , Integral i
  )
    => Monoid (Array i a)
  where
    mempty =
      listArray (0, 0) []


lAr ::
  ( Ix i
  )
    => Range i -> List a -> Array i a
lAr (min, max) =
  Array < Array.listArray (IxWrap min, IxWrap max)


{-# Deprecated listArray "Use lAr instead" #-}
-- | Long version of 'lAr'.
listArray ::
  ( Ix i
  )
    => Range i -> List a -> Array i a
listArray =
  lAr


{-# Deprecated elems "Use tL instead" #-}
-- | Long version of 'tL' for arrays.
elems ::
  (
  )
    => Array i a -> List a
elems =
  tL
