{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
module P.Map
  ( StrictMap
  , eSm
  , asc
  -- * Deprecated
  , assocs
  , toAscList
  )
  where


import qualified Prelude


import qualified Data.Functor.Classes
import qualified Data.Map.Strict
import qualified Data.Map.Merge.Strict


import P.Aliases
import P.Applicative
import P.Bifunctor.Flip
import P.Bifunctor.These
import P.Category
import P.Comonad
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Functor
import P.Map.Class
import P.Ord
import P.Tuple
import P.Zip
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A strict map type.
--
-- Uses the implementation from @Data.Map.Strict.@'Data.Map.Strict.Map'.
newtype StrictMap k a =
  StrictMap (Data.Map.Strict.Map (POrd k) a)


liftStrictMap ::
  (
  )
    => (Data.Map.Strict.Map (POrd k) a -> Data.Map.Strict.Map (POrd j) b) -> StrictMap k a -> StrictMap j b
liftStrictMap func (StrictMap xs) =
  StrictMap (func xs)


instance Functor (StrictMap k)
  where
    fmap =
      liftStrictMap < m


instance
  ( Ord k
  )
    => Indexable (StrictMap k) k
  where
    StrictMap xs !? key =
      tL $ xs Data.Map.Strict.!? (POrd key)

    mr key (StrictMap xs) =
      Data.Map.Strict.member (POrd key) xs

    ajt key transform (StrictMap xs) =
      StrictMap $ Data.Map.Strict.adjust transform (POrd key) xs


instance
  ( Ord k
  )
    => ReverseIndexable (StrictMap k) k
  where
    tAL =
      asc

    ixW pred (StrictMap s) =
      m (cr < st) $ Prelude.filter (pred < cr) $ Data.Map.Strict.assocs s

    aix (StrictMap s) =
      cr < Data.Map.Strict.keys s


instance
  ( Ord k
  )
    => Map (StrictMap k) k
  where
    nz key value (StrictMap xs) =
      StrictMap $ Data.Map.Strict.insert (POrd key) value xs

    dlt key (StrictMap xs) =
      StrictMap $ Data.Map.Strict.delete (POrd key) xs


instance
  ( Ord k
  )
    => WeakAlign (StrictMap k)
  where
    cnL (StrictMap xs) (StrictMap ys) =
      StrictMap $ Data.Map.Merge.Strict.merge
        ( Data.Map.Merge.Strict.mapMissing $ p $ id )
        ( Data.Map.Merge.Strict.mapMissing $ p $ id )
        ( Data.Map.Merge.Strict.zipWithMatched $ p $ p )
        xs
        ys


instance
  ( Ord k
  )
    => SemiAlign (StrictMap k)
  where
    aln (StrictMap xs) (StrictMap ys) =
      StrictMap $ Data.Map.Merge.Strict.merge
        ( Data.Map.Merge.Strict.mapMissing $ p $ Ti )
        ( Data.Map.Merge.Strict.mapMissing $ p $ Ta )
        ( Data.Map.Merge.Strict.zipWithMatched $ p $ Te )
        xs
        ys

    alW func (StrictMap xs) (StrictMap ys) =
      StrictMap $ Data.Map.Merge.Strict.merge
        ( Data.Map.Merge.Strict.mapMissing $ p $ func < Ti )
        ( Data.Map.Merge.Strict.mapMissing $ p $ func < Ta )
        ( Data.Map.Merge.Strict.zipWithMatched $ p $ func << Te )
        xs
        ys


instance
  ( Ord k
  )
    => UnitalZip (StrictMap k)
  where
    aId =
      StrictMap Data.Map.Strict.empty


instance
  ( Ord k
  )
    => Zip (StrictMap k)
  where
    zW func (StrictMap xs) (StrictMap ys) =
      StrictMap $ Data.Map.Strict.intersectionWith func xs ys


instance
  ( Eq k
  , Eq a
  )
    => Eq (StrictMap k a)
  where
    (==) =
      q1 eq


instance
  ( Eq k
  )
    => Eq1 (StrictMap k)
  where
    q1 =
      q1'


instance
  ( Eq a
  )
    => Eq1 (Flip StrictMap a)
  where
    q1 =
      q1'


instance
  (
  )
    => Eq2 StrictMap
  where
    q2 userEq1 userEq2 (StrictMap xs) (StrictMap ys) =
      Data.Functor.Classes.liftEq2 (q1 userEq1) userEq2 xs ys


-- | An empty strict map.
--
-- More specific version of 'aId'.
eSm ::
  ( Ord k
  )
    => StrictMap k a
eSm =
  aId


-- | Return all key value pairs in ascending order.
asc ::
  (
  )
    => StrictMap k a -> List (k, a)
asc (StrictMap s) =
  m (\(POrd k, v) -> (k, v)) $ Data.Map.Strict.assocs s


{-# Deprecated toAscList "Use asc or tAL instead" #-}
-- | Long version of 'asc'.
toAscList ::
  (
  )
    => StrictMap k a -> List (k, a)
toAscList =
  asc
