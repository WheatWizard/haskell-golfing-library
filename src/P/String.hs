{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.String
  ( String
  , pattern Ln
  , pattern Ul
  , wR
  , pattern Wr
  , pattern Uw
  , pattern Dl
  , pattern Ls
  , pattern Pn
  , pattern SB
  , pattern CB
  -- * Balanced strings
  , bld
  , bla
  , blc
  , blp
  , bls
  , blF
  -- * Padding
  , rP
  , lP
  , rPL
  , lPL
  , rPp
  , lPp
  -- * Deprecated
  , lines
  , unlines
  , words
  , unwords
  )
  where


import Prelude
  ( String
  , Integral
  , Int
  , (+)
  )


import qualified Data.List


import P.Algebra.Semigroup
import P.Aliases
import P.Applicative
import P.Bool
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Last
import P.List
import P.List.Expandable
import P.List.Padding
import P.List.Split
import P.Map.Class
import P.Ord
import P.Ord.Bounded


{-# Deprecated lines "Use Ln instead" #-}
-- | Long version of 'Ln'.
lines ::
  (
  )
    => String -> List String
lines =
  Ln


-- | Splits a string along newline characters.
--
-- Equivalent to 'Data.List.lines'.
pattern Ln ::
  (
  )
    => String -> List String
pattern Ln x <- (ic "\n" -> x) where
  Ln =
    sL '\n'


{-# Deprecated unlines "Use Ul instead" #-}
-- | Long version of 'Ul'.
unlines ::
  (
  )
    => List String -> String
unlines =
  Ul


-- | Takes a list of strings and concatenates them with separating newlines.
--
-- Equivalent to 'Data.List.unlines'.
pattern Ul ::
  (
  )
    => List String -> String
pattern Ul x <- (Ln -> x) where
  Ul =
    Data.List.unlines < tL


{-# Deprecated words "Use wR instead" #-}
-- | Long version of 'wR'.
words ::
  (
  )
    => String -> List String
words =
  wR

-- | Splits along spaces.
--
-- Similar to 'wR' except that while @wR@ will avoid empty strings in the output @Wr@ allows them.
-- @wR@ also splits any whitespace while @Wr@ only splits on spaces.
--
-- Because the input of @Wr@ can always be determined from the input @Wr@ is a pattern while @wR@ is not.
--
-- ==== __Examples__
--
-- >>> Wr " Hello  world"
-- ["","Hello","","world"]
-- >>> wR " Hello  world"
-- ["Hello","world"]
-- >>> Wr " Hello   world"
-- ["","Hello","","","world"]
-- >>> wR " Hello   world"
-- ["Hello","world"]
-- >>> wR " Hello   \nworld"
-- ["Hello","world"]
-- >>> Wr " Hello   \nworld"
-- ["","Hello","","","\nworld"]
--
pattern Wr ::
  (
  )
    => String -> List String
pattern Wr x <- (ic " " -> x) where
  Wr =
    sL ' '

-- | Splits strings along chunks of whitespace.
--
-- Equivalent to 'Data.List.words'.
--
-- ==== __Examples__
--
-- >>> Wr " Hello  world"
-- ["","Hello","","world"]
-- >>> wR " Hello  world"
-- ["Hello","world"]
-- >>> Wr " Hello   world"
-- ["","Hello","","","world"]
-- >>> wR " Hello   world"
-- ["Hello","world"]
-- >>> wR " Hello   \nworld"
-- ["Hello","world"]
-- >>> Wr " Hello   \nworld"
-- ["","Hello","","","\nworld"]
--
wR ::
  (
  )
    => String -> List String
wR =
  Data.List.words


{-# Deprecated unwords "Use Uw instead" #-}
-- | Long version of 'Uw'.
unwords ::
  (
  )
    => List String -> String
unwords =
  Uw


-- | Takes a list of strings and concatenates them with separating spaces.
--
-- More powerful version 'Data.List.unwords'.
pattern Uw ::
  (
  )
    => List String -> String
pattern Uw x <- (Wr -> x) where
  Uw =
    ic " "


-- | Split a string along commas.
pattern Dl ::
  (
  )
    => String -> List String
pattern Dl x <- (ic "," -> x) where
  Dl =
    sL ','


-- | Split a string along slashes (@/@).
pattern Ls ::
  (
  )
    => String -> List String
pattern Ls x <- (ic "/" -> x) where
  Ls =
    sL '/'


-- | Right pads with spaces to the specified size.
rP ::
  ( Integral n
  , Ord n
  )
    => n -> String -> String
rP =
  rpW ' '


-- | Left pads with spaces to the specified size.
lP ::
  ( Integral i
  , Ord i
  )
    => i -> String -> String
lP =
  lpW ' '


-- | Right pads a string with spaces to the size of another string.
rPL ::
  (
  )
    => List b -> String -> String
rPL =
  rLW ' '


-- | Left pads a string with spaces to the size of another string.
lPL ::
  (
  )
    => List b -> String -> String
lPL =
  lLW ' '


-- | Right pads all elements of a list to be the same length using spaces.
--
-- Lazy on elements of the list.
rPp ::
  (
  )
    => List String -> List String
rPp =
  rpp ' '


-- | Left pads all elements of a list to be the same length using spaces.
--
-- Not lazy on elements of the list.
lPp ::
  (
  )
    => List String -> List String
lPp =
  lpp ' '


-- | Encloses a string in parentheses: @()@.
--
-- ==== __Examples__
--
-- ===== As a function
--
-- >>> Pn "Test"
-- "(Test)"
--
-- ===== As a pattern
--
-- >>> f(Pn _)=T;f _=B
-- >>> f"(Water)"
-- True
-- >>> f"(Water)?"
-- False
-- >>> f"Car"
-- False
--
pattern Pn ::
  (
  )
    => String -> String
pattern Pn g =
  '(' : ')' :> g


-- | Encloses a string in square braces: @[]@.
--
-- ==== __Examples__
--
-- ===== As a function
--
-- >>> SB "Test"
-- "[Test]"
--
-- ===== As a pattern
--
-- >>> f(SB _)=T;f _=B
-- >>> f"[Water]"
-- True
-- >>> f"[Water]?"
-- False
-- >>> f"Car"
-- False
--
pattern SB ::
  (
  )
    => String -> String
pattern SB g =
  '[' : ']' :> g


-- | Encloses a string in curly braces: @{}@.
--
-- ==== __Examples__
--
-- ===== As a function
--
-- >>> CB "Test"
-- "{Test}"
--
-- ===== As a pattern
--
-- >>> f"{Water}"
-- True
-- >>> f"{Water}?"
-- False
-- >>> f"Car"
-- False
--
pattern CB ::
  (
  )
    => String -> String
pattern CB g =
  '{' : '}' :> g


-- | Takes a list of brace delimeters and determines if a string is balanced with the given delimeters.
--
-- Open braces are given at odd indices (starting with 0) and close braces at even indices.
-- It is expected that no symbol is both a open and close brace.
--
-- If there are more open braces than close braces (i.e. the provided list has odd length) then the final open brace cannot be matched and thus is an illegal symbol.
--
-- Items not in the given symbol list are ignored.
bld ::
  ( Eq a
  )
    => List a -> List a -> Bool
bld =
  go [] <% uak
  where
    go ::
      ( Eq a
      )
        => List Int -> List a -> List a -> List a -> Bool
    go stack _ _ [] =
      ø stack
    go (j : stack) opens closes (x : xs)
      | is@(_ : _) <- isx x closes
      =
        e j is <> go stack opens closes xs
    go stack opens closes (x : xs)
      | i : _ <- isx x opens
      =
        go (i : stack) opens closes xs
    go [] opens closes (x : xs)
      | _ : _ <- isx x closes :: List Int
      =
        B
    go stack opens closes (x : xs) =
      go stack opens closes xs


-- | Determine if a string contains balanced square brackets.
--
-- Ignores characters other than @[]@.
bls ::
  (
  )
    => String -> Bool
bls =
  bld "[]"


-- | Determine if a string contains balanced parentheses.
--
-- Ignores characters other than @()@.
blp ::
  (
  )
    => String -> Bool
blp =
  bld "()"


-- | Determine if a string contains balanced curly brackets.
--
-- Ignores characters other than @{}@.
blc ::
  (
  )
    => String -> Bool
blc =
  bld "{}"


-- | Determine if a string contains balanced angle brackets.
--
-- Ignores characters other than @<>@.
bla ::
  (
  )
    => String -> Bool
bla =
  bld "<>"


-- | Determine if a string contains balanced braces.
--
-- The braces considered are @[]@, @{}@, @<>@, and @()@.
-- Ignores characters other than the braces.
blF ::
  (
  )
    => String -> Bool
blF =
  bld "[]{}<>()"
