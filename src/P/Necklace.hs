{-|
Module :
  P.Necklace
Description :
  Necklaces
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

-}
module P.Necklace
  ( Necklace (..)
  )
  where


import qualified Prelude


import P.Algebra.Semigroup
import P.Aliases
import P.Category
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.List
import P.List.Infix
import P.List.Expandable
import P.Reverse


-- | Necklaces are finite cycles of elements.
--
-- Like lists that loop back on themselves.
newtype Necklace a
  = Nk
    -- ^ Convert a list to a necklace.
    { uNk :: -- ^ Extract a list from a necklace with an arbitrary starting point.
      List a
    }
  deriving
    ( Prelude.Show
    )


instance
  (
  )
    => Functor Necklace
  where
    fmap f (Nk x) =
      Nk (f < x)


instance
  ( Eq a
  )
    => Eq (Necklace a)
  where
    Nk n1 == Nk n2 =
      lEq n1 n2 <> iw n1 (n2 <> n2)


instance
  (
  )
    => Eq1 Necklace
  where
    q1 userEq (Nk n1) (Nk n2) =
      lEq n1 n2 <> iWW userEq n1 (n2 <> n2)


instance
  (
  )
    => Foldable Necklace
  where
    foldMap f (Nk x) =
      mF f x


instance
  (
  )
    => Expandable Necklace
  where
    vm f (Nk x) =
      Nk (vm f x)

    is x (Nk xs) =
      Nk (vm (x:$) xs)


instance
  (
  )
    => Reversable Necklace
  where
    _rv (Nk x) =
      Nk (_rv x)


instance
  (
  )
    => ReversableFunctor Necklace
  where
    rvW f (Nk x) =
      Nk (rvW f x)
