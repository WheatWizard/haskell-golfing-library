{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language TypeOperators #-}
module P.List
  ( p2
  , fp2
  , pattern (:$)
  , r1
  , fr1
  , ak
  , aku
  , fak
  , pattern (:%)
  , uak
  , ukw
  , fkw
  , ak'
  , rn
  , frn
  , tx
  , txi
  , txx
  , txm
  , fxm
  , xts
  , rp
  , rL1
  , rL2
  , rr1
  , rr2
  , ryl
  , ryr
  , dc
  , xdc
  , iBw
  , fBw
  , (|^>)
  , tlM
  , ftm
  , (~<~)
  -- * Deprecated
  , holes
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , Int
  , Num
  , otherwise
  , tail
  , head
  )
import qualified Data.List


import P.Algebra.Semigroup
import P.Algebra.Ring
import P.Aliases
import P.Alternative
import P.Category
import P.Category.Iso
import P.Eq
import P.Function.Compose
import P.Function.Flip
import P.Monad
import P.Ord


infixr 5 :%
infixr 9 ~<~


-- | Makes a list with two elements.
p2 ::
  ( Alternative f
  )
    => a -> a -> f a
p2 x1 x2 =
  Prelude.pure x1 ++ Prelude.pure x2


-- | Flip of p2.
fp2 ::
  ( Alternative f
  )
    => a -> a -> f a
fp2 =
  f' p2


-- | Infix of 'p2'.
pattern (:$) ::
  (
  )
    => a -> a -> List a
pattern x1 :$ x2 =
  [ x1
  , x2
  ]


-- | Removes the first element that matches a predicate.
r1 ::
  (
  )
    => Predicate a -> List a -> List a
r1 _ [] =
  []
r1 pred (x : xs)
  | pred x
  =
    xs
  | otherwise
  =
    x : r1 pred xs


-- | Flip of 'r1'.
fr1 ::
  (
  )
    => List a -> Predicate a -> List a
fr1 =
  F r1


-- | Takes two lists and makes a third list alternating elements between them.
ak ::
  (
  )
    => List a -> List a -> List a
ak =
  (:%)


-- | Uncurried version of 'ak'.
aku ::
  (
  )
    => (List a, List a) -> List a
aku (x, y) =
  ak x y


-- | Flip of 'ak'.
fak ::
  (
  )
    => List a -> List a -> List a
fak =
  F ak


{-# Complete (:%) #-}
-- | Infix of 'ak' that also allows pattern matching.
pattern (:%) :: List a -> List a -> List a
pattern xs :% ys <- (uak -> (xs, ys)) where
  [] :% ys =
    ys
  (x : xs) :% ys =
    x : (ys :% xs)


-- | Takes a list and produces two lists, one of elements at even indexes and one of elements at odd indexes.
uak ::
  (
  )
    => List a -> (List a, List a)
uak [] =
  ([], [])
uak (x : (uak -> (y1, y2))) =
  ( x : y2
  , y1
  )


-- | Perform 'uak' and combine the two resulting lists with a user provided function.
ukw ::
  (
  )
    => (List a -> List a -> c) -> List a -> c
ukw =
  (<% uak)


-- | Flip of 'ukw'.
fkw ::
  (
  )
    => List a -> (List a -> List a -> c) -> c
fkw =
  f' ukw


-- | Alternative version of 'ak' which truncates the output when one of the lists runs out.
ak' ::
  (
  )
    => List a -> List a -> List a
ak' [] _ =
  []
ak' (x : xs) ys =
  x : ak' ys xs


-- -- | Takes a list of indices and values and inserts them into a list with 'P.Map.Class.ns'.
-- nS ::
--   ( Integral i
--   , Foldable t
--   )
--     => t (i, a) -> List a -> List a
-- nS =
--   F fnS
--
-- -- | Flip of 'nS'.
-- fnS ::
--   ( Integral i
--   , Foldable t
--   )
--     => List a -> t (i, a) -> List a
-- fnS =
--   rF $ Uc ns


-- | Takes an integer @n@ and a list and removes every @n@th element from that list.
--
-- ==== __Examples__
--
-- @rn 2@ will remove every other element from a list so, to get a list of all the non-negative even integers you can do @rn 2 [0..]@.
--
-- >>> rn 2 [0..]
-- [0,2,4,6,8,10,12,14,16,18,20...
--
--
rn ::
  ( Integral i
  )
    => i -> List a -> List a
rn cycle =
  go 1
  where
    go _ [] =
      []
    go depth (x : xs)
      | depth == cycle
      =
        go 1 xs
      | otherwise
      =
        x : go (depth + 1) xs


-- | Flip of 'rn'.
frn ::
  ( Integral i
  )
    => List a -> i -> List a
frn =
  F rn


-- | Transposes a list of lists.
--
-- Equivalent to 'Data.List.transpose'.
tx ::
  (
  )
    => List (List a) -> List (List a)
tx =
  Data.List.transpose


-- | 'Iso' version of 'tx'.
--
-- Not technically an isomorphism in general, but it is assuming that the list is concave.
txi ::
  (
  )
    => List (List a) <-> List (List a)
txi =
  Iso
    { fwd =
      tx
    , bwd =
      tx
    }


-- | Transposes and concatenates.
txx ::
  (
  )
    => List (List a) -> List a
txx =
  cx < tx


-- | Maps and then transposes.
txm ::
  (
  )
    => (a -> List b) -> List a -> List (List b)
txm =
  tx << m


-- | Flip of 'txm'.
fxm ::
  (
  )
    => List a -> (a -> List b) -> List (List b)
fxm =
  f' txm


{-# Deprecated holes "Use xts instead." #-}
-- | Long version of 'xts'.
holes ::
  (
  )
    => List a -> List (a, List a)
holes =
  xts


-- | Gives all ways to extract a single element from a list.
--
-- Equivalent to 'GHC.Utils.Misc.holes'.
xts ::
  (
  )
    => List a -> List (a, List a)
xts [] =
  []
xts (x : xs) =
  (x, xs) : mm (x:) (xts xs)


-- | Takes a element and makes an infinite list consisting of only that element.
-- Equivalent to 'Data.List.repeat'.
rp ::
  (
  )
    => a -> List a
rp =
  Data.List.repeat


-- | Take the first element of a list and add it to the end.
--
-- This is "rotating" the list one to the left.
rL1 ::
  (
  )
    => List a -> List a
rL1 =
  ryl (1 :: Int)


-- | Apply 'rL1' twice, rotating a list twice to the left.
rL2 ::
  (
  )
    => List a -> List a
rL2 =
  ryl (1 :: Int)


-- | Take the last element of a list and add it to the front.
--
-- This is "rotating" the list one to the right.
rr1 ::
  (
  )
    => List a -> List a
rr1 =
  ryl (-1 :: Int)


-- | Apply 'rr1' twice, rotating a list twice to the right.
rr2 ::
  (
  )
    => List a -> List a
rr2 =
  ryl (-2 :: Int)


-- | Rotate a list to the left a certain amount.
-- The same as repeated applications of 'rL1'.
--
-- Giving a negative number of rotations results in rotations to the right.
ryl ::
  ( Integral i
  , Ord i
  )
    => i -> List a -> List a
ryl =
  go []
  where
    go ::
      ( Integral i
      , Ord i
      )
        => List a -> i -> List a -> List a
    go [] _ [] =
      []
    go ys 0 xs =
      xs <> Prelude.reverse ys
    go ys n [] =
      go [] n (Prelude.reverse ys)
    go ys n (x : xs)
      | n > 0
      =
        go (x : ys) (n-1) xs
      | 0 > n
      =
        Prelude.reverse $ go [] (-n) $ Prelude.reverse $ ys ++ x : xs


-- | Rotate a list to the right a certain amount.
-- The same as repeated applications of 'rr1'.
--
-- Giving a negative number of rotations results in rotations to the left.
ryr ::
  ( Integral i
  , Ord i
  )
    => i -> List a -> List a
ryr =
  ryl < Prelude.negate


-- | Generate all strings from a given finite alphabet.
--
-- ==== __Examples__
--
-- >>> tk 15 $ dc "ab"
-- ["","a","b","aa","ab","ba","bb","aaa","aab","aba","abb","baa","bab","bba","bbb"]
-- >>> tk 15 $ dc "ac"
-- ["","a","c","aa","ac","ca","cc","aaa","aac","aca","acc","caa","cac","cca","ccc"]
-- >>> tk 15 $ dc "abc"
-- ["","a","b","c","aa","ab","ac","ba","bb","bc","ca","cb","cc","aaa","aab"]
--
dc ::
  (
  )
    => List a -> List (List a)
dc =
  bn [0 :: Prelude.Integer ..] < rM


-- | Generate all strings from a given finite alphabet and then concat each string.
--
-- Build strings from chunks of symbols rather than individual symbols.
--
-- ==== __Examples__
--
-- >>> tk 15 $ xdc ["ko", "pa"]
-- ["","ko","pa","koko","kopa","pako","papa","kokoko","kokopa","kopako","kopapa","pakoko","pakopa","papako","papapa"]
xdc ::
  ( Alternative m
  )
    => List (m a) -> List (m a)
xdc =
  cx << dc



-- | Insert a value between existing values at a particular index.
--
-- ==== __Examples__
--
-- >>> iBw 6 'x' "abcdefghijklmop"
-- "abcdefxghijklmop"
-- >>> iBw 0 'x' "abcdefghijklmop"
-- "xabcdefghijklmop"
-- >>> iBw (-6) 'x' "abcdefghijklmop"
-- "abcdefghijklmop"
-- >>> iBw 200 'x' "abcdefghijklmop"
-- "abcdefghijklmop"
iBw ::
  ( Integral i
  , Eq i
  )
    => i -> a -> List a -> List a
iBw 0 a xs =
  a : xs
iBw n a [] =
  []
iBw n a (x : xs) =
  x : iBw (n-1) a xs


-- | Flip of 'iBw'.
fBw ::
  ( Integral i
  , Eq i
  )
    => a -> i -> List a -> List a
fBw =
  f' iBw


-- | Infix of 'iBw'.
(|^>) ::
  ( Integral i
  , Eq i
  )
    => i -> a -> List a -> List a
(|^>) =
  iBw


-- | Map a function across the tail of a list.
--
-- If the list is empty it returns the empty list.
tlM ::
  (
  )
    => (List a -> List a) -> List a -> List a
tlM f [] =
  []
tlM f (x : xs) =
  x : f xs


-- | Flip of 'tlM'
ftm ::
  (
  )
    => List a -> (List a -> List a) -> List a
ftm =
  f' tlM


-- | Infix of 'tlM'.
(~<~) ::
  (
  )
    => (List a -> List a) -> List a -> List a
(~<~) =
  tlM
