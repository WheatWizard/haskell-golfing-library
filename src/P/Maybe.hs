{-# Language PatternSynonyms #-}
{-# Language FlexibleInstances #-}
module P.Maybe
  ( Maybe
  -- * Deprecated
  , pattern Just
  , pattern Nothing
  , fromMaybe
  )
  where


import qualified Prelude


import P.Category
import P.Bifunctor.Either


-- | A maybe type defined in terms of 'Either'.
--
-- Since traditional Maybes are just lists with a limit on the number of elements they can have they are not of a lot of use in a golfing library.
-- Instead everywhere we would use Maybe we just use a list instead.
--
-- This type exists because can be useful as a building block for other structures.
type Maybe =
  Either ()


{-# Deprecated Just "Use Pu instead" #-}
-- | Long version of 'P.Unpure.Pu'.
-- Pure for 'Maybe'.
pattern Just ::
  (
  )
    => a -> Maybe a
pattern Just x =
  Rit x


{-# Deprecated Nothing "Use Ø or aId instead" #-}
-- | Deprecated pattern.
-- Use 'P.Foldable.Length.Pattern.Ø' for patterns and 'P.UnitalZip.aId' as a value.
pattern Nothing ::
  (
  )
    => Maybe a
pattern Nothing =
  Lef ()


{-# Deprecated fromMaybe "Use qd0 instead" #-}
-- | Extracts a value from a maybe using a default value for the nothing case.
--
-- Use 'P.Foldable.rFp', 'P.Foldable.lFp' or 'P.Map.Class.Extra.qd0' instead.
fromMaybe ::
  (
  )
    => a -> Maybe a -> a
fromMaybe x Nothing =
  x
fromMaybe _ (Just x) =
  x
