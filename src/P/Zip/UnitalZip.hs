{-# Language FlexibleInstances #-}
module P.Zip.UnitalZip
  ( UnitalZip (..)
  , cnF
  , cFM
  -- * Deprecated
  , unions
  )
  where


import Prelude
  (
  )


import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Zip.WeakAlign


-- | @UnitalZip@ functors are 'WeakAlign' functors with an identity.
--
-- === Laws
--
-- ==== Left Identity
--
-- prop> x #| aId = x
--
-- ==== Right Identity
--
-- prop> aId #| x = x
--
class
  ( WeakAlign f
  )
    => UnitalZip f
  where
    -- | Unit under 'cnL' and 'cnR'.
    aId :: f a


instance
  (
  )
    => UnitalZip []
  where
    aId =
      []


instance
  (
  )
    => UnitalZip (Either ())
  where
    aId =
      Lef ()


instance
  ( Eq k
  )
    => UnitalZip (Blackbird [] (,) k)
  where
    aId =
      MM []

-- | Right fold using '(#|)'.
--
-- More general version of 'Data.Map.Strict.unions'.
cnF ::
  ( UnitalZip f
  , Foldable t
  )
    => t (f a) -> f a
cnF =
  rF (#|) aId


-- | 'cnF' with a map.
cFM ::
  ( UnitalZip f
  , Foldable t
  )
    => (a -> f b) -> t a -> f b
cFM func =
  rF ((#|) < func) aId


{-# Deprecated unions "Use cnF instead" #-}
-- | Long version of 'cnF'.
unions ::
  ( UnitalZip f
  , Foldable t
  )
    => t (f a) -> f a
unions =
  cnF
