{-# Language InstanceSigs #-}
module P.Zip.Bi.SemiAlign
  ( SemiBialign (..)
  )
  where


import qualified Prelude


import P.Bifunctor
import P.Bifunctor.Blackbird
import P.Bifunctor.These
import P.Category
import P.Function.Compose
import P.Zip.Bi.WeakAlign
import P.Zip.SemiAlign


class
  ( WeakBialign p
  )
    => SemiBialign p
  where
    {-# Minimal bal | blW #-}
    bal :: p a b -> p c d -> p (These a c) (These b d)
    bal =
      blW id id

    blW :: (These a b -> c) -> (These d e -> f) -> p a d -> p b e -> p c f
    blW func1 func2 =
      bm func1 func2 << bal


instance
  (
  )
    => SemiBialign (,)
  where
    bal (a, b) (c, d) =
      ( Te a c
      , Te b d
      )


instance
  ( SemiAlign f
  , SemiBialign p
  )
    => SemiBialign (Blackbird f p)
  where
    bal :: Blackbird f p a b -> Blackbird f p c d -> Blackbird f p (These a c) (These b d)
    bal (MM xs) (MM ys) =
      MM $ alW go xs ys
      where
        go (Ti ws) =
          bm Ti Ti ws
        go (Ta zs) =
          bm Ta Ta zs
        go (Te ws zs) =
          bal ws zs
