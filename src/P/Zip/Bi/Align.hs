module P.Zip.Bi.Align
  ( Bialign (..)
  )
  where


import P.Zip.Bi.SemiAlign


class
  ( SemiBialign p
  )
    => Bialign p
  where
