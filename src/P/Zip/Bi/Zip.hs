module P.Zip.Bi.Zip
  ( Bizip (..)
  )
  where


import P.Bifunctor
import P.Bifunctor.Blackbird
import P.Function.Compose
import P.Function.Curry
import P.Zip
import P.Zip.Bi.SemiAlign


class
  ( SemiBialign p
  )
    => Bizip p
  where
    {-# Minimal zW2 | zp2 #-}
    zW2 :: (a -> b -> c) -> (d -> e -> f) -> p a d -> p b e -> p c f
    zW2 func1 func2 =
      bm (U func1) (U func2) << zp2

    -- | Takes two bifunctors and combines them pairwise across both arguments.
    zp2 :: p a b -> p c d -> p (a, c) (b, d)
    zp2 =
      zW2 (,) (,)


instance
  (
  )
    => Bizip (,)
  where
    zW2 f g (a, b) (c, d) =
      ( f a c
      , g b d
      )

    zp2 (a, b) (c, d) =
      ( (a, c)
      , (b, d)
      )


instance
  ( Zip f
  , Bizip p
  )
    => Bizip (Blackbird f p)
  where
    zW2 f g (MM xs) (MM ys) =
      MM $ zW (zW2 f g) xs ys

    zp2 (MM xs) (MM ys)=
      MM $ zW zp2 xs ys
