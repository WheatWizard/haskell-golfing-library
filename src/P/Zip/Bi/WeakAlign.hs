module P.Zip.Bi.WeakAlign
  ( WeakBialign (..)
  )
  where


import qualified Prelude


import P.Bifunctor
import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Bifunctor.These
import P.Category
import P.Function.Compose
import P.Function.Flip
import P.Swap
import P.Zip.SemiAlign


-- | A bifunctor which can can be simultaneously weak aligned on both of its arguments.
--
-- === Laws
--
-- ==== Idempotency
--
-- prop> jn cLL = id
-- prop> jn cLR = id
-- prop> jn cRL = id
-- prop> jn cRR = id
-- prop> jn aLL = bm Lef Lef
-- prop> jn aLR = bm Lef Rit
-- prop> jn aRL = bm Rit Lef
-- prop> jn aRR = bm Rit Rit
--
-- ==== Associativity
--
-- prop> cLL x (cLL y z) = cLL (cLL x y) z
-- prop> cLR x (cLR y z) = cLR (cLR x y) z
-- prop> cRL x (cRL y z) = cRL (cRL x y) z
-- prop> cRR x (cRR y z) = cRR (cRR x y) z
-- TODO
-- prop> alL x (alL y z) = Asc < alL (alL x y) z
-- prop> alR x (alR y z) = Asc < alR (alR x y) z
--
-- ==== Reflection
--
-- prop> cLL = F cRR
-- prop> cRR = F cLL
-- prop> cLR = F cRL
-- prop> cRL = F cLR
-- TODO
-- prop> alL = Sw <<< F alR
-- prop> alR = Sw <<< F alL
--
class
  ( Bifunctor p
  )
    => WeakBialign p
  where
    {-# Minimal (aLL | aRR | cLL | cRR), (aLR | aRL | cLR | cRL) #-}
    aLL :: p a b -> p c d -> p (Either a c) (Either b d)
    aLL x y =
      cLL (bm Lef Lef x) (bm Rit Rit y)

    aLR :: p a b -> p c d -> p (Either a c) (Either b d)
    aLR x y =
      cLR (bm Lef Lef x) (bm Rit Rit y)

    aRL :: p a b -> p c d -> p (Either a c) (Either b d)
    aRL =
      bm Sw Sw << F aLR

    aRR :: p a b -> p c d -> p (Either a c) (Either b d)
    aRR =
      bm Sw Sw << F aLL

    cLL :: p a b -> p a b -> p a b
    cLL =
      F cRR

    cLR :: p a b -> p a b -> p a b
    cLR =
      F cRL

    -- | Flip of 'cLR'.
    cRL :: p a b -> p a b -> p a b
    cRL =
      bm fI fI << aRL

    -- | Flip of 'cLL'.
    cRR :: p a b -> p a b -> p a b
    cRR =
      bm fI fI << aRR


instance
  (
  )
    => WeakBialign (,)
  where
    cLL x _ =
      x
    cRR _ y =
      y
    cLR (a, _) (_, d) =
      (a, d)
    cRL (_, b) (c, _) =
      (c, b)


instance
  ( SemiAlign f
  , WeakBialign p
  )
    => WeakBialign (Blackbird f p)
  where
    cLL (MM xs) (MM ys) =
      MM $ alW go xs ys
      where
        go (Ti ws) =
          ws
        go (Ta zs) =
          zs
        go (Te ws zs) =
          cLL ws zs

    cRL (MM xs) (MM ys) =
      MM $ alW go xs ys
      where
        go (Ti ws) =
          ws
        go (Ta zs) =
          zs
        go (Te ws zs) =
          cRL ws zs

    cLR (MM xs) (MM ys) =
      MM $ alW go xs ys
      where
        go (Ti ws) =
          ws
        go (Ta zs) =
          zs
        go (Te ws zs) =
          cLR ws zs

    cRR (MM xs) (MM ys) =
      MM $ alW go xs ys
      where
        go (Ti ws) =
          ws
        go (Ta zs) =
          zs
        go (Te ws zs) =
          cRR ws zs
