{-|
Module :
  P.Zip.Extra
Description :
  Additional functions for the P.Zip module
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Zip module.
-}
module P.Zip.Extra
  where


import P.Function.Compose
import P.Function.Curry
import P.Zip


-- | A 4 argument version of 'zW'.
z4 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f x
z4 =
  upZ z3


-- | A 5 argument version of 'zW'.
z5 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f x
z5 =
  upZ z4


-- | A 6 argument version of 'zW'.
z6 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f x
z6 =
  upZ z5


-- | A 7 argument version of 'zW'.
z7 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f a7 -> f x
z7 =
  upZ z6


-- | An 8 argument version of 'zW'.
z8 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a8 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f a7 -> f a8 -> f x
z8 =
  upZ z7


-- | A 9 argument version of 'zW'.
z9 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a8 -> a9 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f a7 -> f a8 -> f a9 -> f x
z9 =
  upZ z8


-- | A 10 argument version of 'zW'.
z0 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a8 -> a9 -> a10 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f a7 -> f a8 -> f a9 -> f a10 -> f x
z0 =
  upZ z9


-- | An 11 argument version of 'zW'.
z1 ::
  ( Zip f
  )
    => (a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a8 -> a9 -> a10 -> a11 -> x) -> f a1 -> f a2 -> f a3 -> f a4 -> f a5 -> f a6 -> f a7 -> f a8 -> f a9 -> f a10 -> f a11 -> f x
z1 =
  upZ z0
