{-|
Module :
  P.Monad.Plus
Description :
  Things for monads and alternatives
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Monad.Plus
  (
  -- * Many
    xmy
  , fxy
  , xmY
  , fxY
  , bmy
  , fBy
  , (>~*)
  , bmY
  , fBY
  -- * Some
  , xso
  , fxo
  , xsO
  , fXO
  , bso
  , fbo
  , (>~+)
  , bsO
  , fbO
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Alternative
import P.Applicative
import P.Category
import P.First
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Monad


infixl 1
    >~*
  , >~+


-- | Apply an alternative any number of times threading a result through.
--
-- ==== __Examples__
--
-- Parse strings of ascending integers starting from 1
--
-- >>> gc (xmy (yʃ < P1) 0) "1234567891011"
-- [[1,2,3,4,5,6,7,8,9,10,11]]
xmy ::
  ( Monad m
  , Alternative m
  )
    => (a -> m a) -> a -> m (List a)
xmy x v =
  tl < (p v >~* x)


-- | Flip of 'xmy'.
fxy ::
  ( Monad m
  , Alternative m
  )
    => a -> (a -> m a) -> m (List a)
fxy =
  f' xmy


-- | Like 'xmy' but combines values with '(P.Semigroup.<>)'
xmY ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => (a -> m a) -> a -> m a
xmY x v =
  p i ++ (x v >~ (mp v << xmY x))


-- | Flip of 'xmY'.
fxY ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => a -> (a -> m a) -> m a
fxY =
  f' xmY


-- | Like 'xmy' but it takes its starting value from the result of a monad.
--
-- ==== __Examples__
--
-- Parse runs of consecutive numbers
--
-- >>>  gc (ν >~* (yʃ < P1)) "6789"
-- [[6789],[6,7,8,9]]
-- >>>  gc (ν >~* (yʃ < P1)) "111213"
-- [[111213],[11,12,13]]
bmy ::
  ( Monad m
  , Alternative m
  )
    => (a -> m a) -> m a -> m (List a)
bmy x m =
  p < m ++ do
    { v1 <- m
    ; v2 <- x v1
    ; (v1 :) < bmy x (p v2)
    }


-- | Flip of 'bmy'.
fBy ::
  ( Monad m
  , Alternative m
  )
    => m a -> (a -> m a) -> m (List a)
fBy =
  f' bmy


-- | Infix of 'bmy' and 'fby'.
(>~*) ::
  ( Monad m
  , Alternative m
  )
    => m a -> (a -> m a) -> m (List a)
(>~*) =
  f' bmy


-- | Like 'bmy' but it combines the results with a monoidal action.
bmY ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => (a -> m a) -> m a -> m a
bmY =
  fo <<< bmy


-- | Flip of 'bmY'
fBY ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => m a -> (a -> m a) -> m a
fBY =
  f' bmY


-- | Apply an alternative at least once threading a result through.
--
-- ==== __Examples__
--
-- Parse strings of ascending integers starting from 1
--
-- >>> gc (xso (P1 << yʃ) 1) "1234567891011"
-- [[1,2,3,4,5,6,7,8,9,10,11]]
xso ::
  ( Monad m
  , Alternative m
  )
    => (a -> m a) -> a -> m (List a)
xso x v = do
  p [] ++ (x v >~ ((v :) << xmy x))
  x v >~* x


-- | Flip of 'xso'.
fxo ::
  ( Monad m
  , Alternative m
  )
    => a -> (a -> m a) -> m (List a)
fxo =
  f' xso


-- | Like 'xso' but combines the elements with a monoid action.
xsO ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => (a -> m a) -> a -> m a
xsO =
  fo <<< xso


-- | Flip of 'xsO'.
fXO ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => a -> (a -> m a) -> m a
fXO =
  f' xsO


-- | Like 'xmy' but it takes its starting value from the result of a monad.
--
-- ==== __Examples__
--
-- Parse runs of one or more consecutive numbers
--
-- >>>  gc (ν >~+ (yʃ < P1)) "6789"
-- [[6,7,8,9]]
-- >>>  gc (ν >~_ (yʃ < P1)) "111213"
-- [[11,12,13]]
bso ::
  ( Monad m
  , Alternative m
  )
    => (a -> m a) -> m a -> m (List a)
bso x v = do
  y1 <- v
  y2 <- x y1
  (y1 :) < bmy x (p y2)


-- | Flip of 'bso'.
fbo ::
  ( Monad m
  , Alternative m
  )
    => m a -> (a -> m a) -> m (List a)
fbo =
  f' bso


-- | Infix of 'bso' and 'fbo'.
(>~+) ::
  ( Monad m
  , Alternative m
  )
    => m a -> (a -> m a) -> m (List a)
(>~+) =
  f' bso


-- | Like 'bso' but combines the results with a monoid action.
bsO ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => (a -> m a) -> m a -> m a
bsO =
  fo <<< bso


-- | Flip of 'bsO'.
fbO ::
  ( Monad m
  , Alternative m
  , Monoid a
  )
    => m a -> (a -> m a) -> m a
fbO =
  f' bsO
