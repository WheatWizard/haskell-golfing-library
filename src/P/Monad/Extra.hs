{-|
Module :
  P.Monad.Extra
Description :
  Additional functions for the P.Monad library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Monad library.
-}
module P.Monad.Extra
  ( fb2
  , bn2
  , fb3
  , bn3
  , fb4
  , bn4
  , fb5
  , bn5
  )
  where


import qualified Prelude


import P.Category
import P.Function.Compose
import P.Function.Compose.Extra
import P.Function.Flip
import P.Function.Flip.Extra
import P.Monad


-- | 'bn2' with a different argument order.
fb2 ::
  ( Monad m
  )
    => (a -> b -> m c) -> m a -> m b -> m c
fb2 k m1 m2 =
  m1 >~ (bn m2 < k)


-- | Monadic bind over with a binary function over two monads.
bn2 ::
  ( Monad m
  )
    => m a -> m b -> (a -> b -> m c) -> m c
bn2 =
  f2a fb2


-- | 'bn3' with a different argument order.
fb3 ::
  ( Monad m
  )
    => (a -> b -> c -> m d) -> m a -> m b -> m c -> m d
fb3 k m1 m2 m3 =
  m1 >~ (bn2 m2 m3 < k)


-- | Monadic bind over with a ternary function over three monads.
bn3 ::
  ( Monad m
  )
    => m a -> m b -> m c -> (a -> b -> c -> m d) -> m d
bn3 =
  f3a fb3


-- | 'bn4' with a different argument order.
fb4 ::
  ( Monad m
  )
    => (a -> b -> c -> d -> m e) -> m a -> m b -> m c -> m d -> m e
fb4 k m1 m2 m3 m4 =
  m1 >~ (bn3 m2 m3 m4 < k)


-- | Monadic bind over with a quaternary function over four monads.
bn4 ::
  ( Monad m
  )
    => m a -> m b -> m c -> m d -> (a -> b -> c -> d -> m e) -> m e
bn4 =
  f4a fb4


-- | 'bn5' with a different argument order.
fb5 ::
  ( Monad m
  )
    => (a -> b -> c -> d -> e -> m f) -> m a -> m b -> m c -> m d -> m e -> m f
fb5 k m1 m2 m3 m4 m5 =
  m1 >~ (bn4 m2 m3 m4 m5 < k)


-- | Monadic bind over with a quintary function over five monads.
bn5 ::
  ( Monad m
  )
    => m a -> m b -> m c -> m d -> m e -> (a -> b -> c -> d -> e -> m f) -> m f
bn5 =
  f5a fb5
