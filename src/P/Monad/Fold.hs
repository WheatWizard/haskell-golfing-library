{-# Language ScopedTypeVariables #-}
{-|
Module :
  P.Monad.Fold
Description :
  Monadic folds
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Monad.Fold
  ( lfM
  , rfM
  , lM1
  , rM1
  , lsM
  , z1M
  -- * Deprecated
  , foldM
  , foldlM
  , foldrM
  )
  where


import qualified Prelude


import qualified Control.Monad
import qualified Data.Foldable


import P.Aliases
import P.Applicative
import P.Applicative.Unpure
import P.Category
import P.First
import P.Foldable
import P.Foldable.Length.Pattern
import P.Function.Compose
import P.Monad
import P.Traversable
import P.Zip.SemiAlign
import P.Zip.UnitalZip


{-# Deprecated foldM "Use lfM instead" #-}
-- | Long version of 'lfM'.
foldM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m a) -> a -> t b -> m a
foldM =
  lfM


{-# Deprecated foldlM "Use lfM instead" #-}
-- | Long version of 'lfM'.
foldlM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m a) -> a -> t b -> m a
foldlM =
  lfM


-- | A left fold with the result encapsulated in a monad.
lfM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m a) -> a -> t b -> m a
lfM =
  Control.Monad.foldM


{-# Deprecated foldrM "Use rfM instead" #-}
-- | Long version of 'rfM'.
foldrM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m b) -> b -> t a -> m b
foldrM =
  rfM


-- | A right fold with the result encapsulated in a monad.
rfM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m b) -> b -> t a -> m b
rfM =
  Data.Foldable.foldrM


-- | A left fold encapsulated in a monad using the first value of the structure as the starting value.
-- A combination of 'foldl1' and `foldM`.
--
-- Errors on an empty structure.
lM1 ::
  forall m t a.
  ( Monad m
  , Foldable t
  )
    => (a -> a -> m a) -> t a -> m a
lM1 f xs = do
  v <- lfM (sQ << zom f p ^. p) [] xs
  p $
    case
      v
    of
      Ø ->
        Prelude.error "lM1 with empty structure"
      [x] ->
        x


-- | A right fold encapsulated in a monad using the first value of the structure as the starting value.
-- A combination of 'foldr1' and `foldrM`.
--
-- Errors on an empty structure.
rM1 ::
  ( Monad m
  , Foldable t
  )
    => (a -> a -> m a) -> t a -> m a
rM1 f xs = do
  v <- rfM (sQ << zom f p < p) [] xs
  p $
    case
      v
    of
      Ø ->
        Prelude.error "rM1 with empty structure"
      [x] ->
        x


-- | Left scan with a monad.
lsM ::
  ( Monad m
  , Foldable t
  )
    => (a -> b -> m a) -> a -> t b -> m (List a)
lsM f start =
  go start < tL
  where
    go x [] =
      p [x]
    go x (y : ys) = do
      v <- f x y
      (x :) < go v ys


-- | Left scan with a monad starting at the first value in the structure.
z1M ::
  ( Monad m
  , Foldable t
  )
    => (a -> a -> m a) -> t a -> m (List a)
z1M f xs =
  fo < lsM (sQ << zom f p ^. p) [] xs
