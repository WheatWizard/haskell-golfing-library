{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Constants.Language
Description :
  Constants having to do with natural language
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Constants.Language
  ( pattern W5
  , pattern W5'
  , pattern W6
  , pattern W6'
  , pattern W7
  , pattern W7'
  , pattern WX
  , pattern WX'
  , pattern Wde
  , pattern WDe
  , pattern Wes
  , pattern WEs
  , pattern Wsv
  , pattern WSv
  )
  where


-- | The 5 vowel letters in lowercase, @"aeiou"@.
--
-- These are the IPA symbols for the commonly used 5 vowel system.
pattern W5 ::
  (
  )
    => String
pattern W5 <- "aeiou" where
  W5 =
    "aeiou"


-- | The 5 vowel letters in uppercase, @"AEIOU"@.
pattern W5' ::
  (
  )
    => String
pattern W5' <- "AEIOU" where
  W5' =
    "AEIOU"


-- | 6 vowel letters in lowercase, @"aeiouy"@.
-- Includes @y@.
pattern W6 ::
  (
  )
    => String
pattern W6 <- "aeiouy" where
  W6 =
    "aeiouy"


-- | 6 vowel letters in lowercase, @"AEIOUY"@.
-- Includes @Y@.
pattern W6' ::
  (
  )
    => String
pattern W6' <- "AEIOUY" where
  W6' =
    "AEIOUY"


-- | 7 vowel letters in lowercase, @"aeiouwy"@.
-- Includes @y@ and @w@.
-- @w@ is used as a vowel in English in obscure Welsh loan words.
pattern W7 ::
  (
  )
    => String
pattern W7 <- "aeiouwy" where
  W7 =
    "aeiouwy"


-- | 7 vowel letters in uppercase, @"AEIOUWY"@.
-- Includes @Y@ and @W@.
-- @W@ is used as a vowel in English in obscure Welsh loan words.
pattern W7' ::
  (
  )
    => String
pattern W7' <- "AEIOUWY" where
  W7' =
    "AEIOUWY"


-- | The 5 vowel letters in lowercase as well as the same five with an acute accent.
-- These are the vowels in languages such as Irish and Spanish (omitting marginal some marginal words).
pattern WX ::
  (
  )
    => String
pattern WX <- "aeiouáéíóú" where
  WX =
    "aeiouáéíóú"


-- | The 5 vowel letters in uppercase as well as the same five with an acute accent.
-- These are the vowels in languages such as Irish and Spanish (omitting marginal some marginal words).
pattern WX' ::
  (
  )
    => String
pattern WX' <- "AEIOUÁÉÍÓÚ" where
  WX' =
    "AEIOUÁÉÍÓÚ"


-- | The 8 lowercase vowel letters used in the German language.
pattern Wde ::
  (
  )
    => String
pattern Wde <- "aeiouäöü" where
  Wde =
    "aeiouäöü"


-- | The 8 uppercase vowel letters used in the German language.
pattern WDe ::
  (
  )
    => String
pattern WDe <- "AEIOUÄÖÜ" where
  WDe =
    "AEIOUÄÖÜ"


-- | The 11 lowercase vowel letters used in the Spanish language including (@ü@).
pattern Wes ::
  (
  )
    => String
pattern Wes <- "aeiouáéíóúü" where
  Wes =
    "aeiouáéíóúü"


-- | The 11 uppercase vowel letters used in the Spanish language including (@Ü@).
pattern WEs ::
  (
  )
    => String
pattern WEs <- "AEIOUÁÉÍÓÚÜ" where
  WEs =
    "AEIOUÁÉÍÓÚÜ"


-- | The 9 lowercase vowel letters used in the Swedish language.
pattern Wsv ::
  (
  )
    => String
pattern Wsv <- "aeiouyåäö" where
  Wsv =
    "aeiouyåäö"


-- | The 9 uppercase vowel letters used in the Swedish language.
pattern WSv ::
  (
  )
    => String
pattern WSv <- "AEIOUYÅÄÖ" where
  WSv =
    "AEIOUYÅÄÖ"
