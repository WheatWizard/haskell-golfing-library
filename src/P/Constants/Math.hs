{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Constants.Math
Description :
  Constants having to do with mathematics
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Constants.Math
  ( pattern C09
  , pattern C10
  , pattern C19
  , pattern C0F
  , pattern C0f
  )
  where


-- | The digits 0 to 9.
pattern C09 ::
  (
  )
    => String
pattern C09 =
  "0123456789"


-- | The digits in the order they appear on the keyboard (0 last).
pattern C10 ::
  (
  )
    => String
pattern C10 =
  "1234567890"


-- | The positive digits 1 to 9.
pattern C19 ::
  (
  )
    => String
pattern C19 =
  "123456789"


-- | The hexidecimal digits in upper case.
pattern C0F ::
  (
  )
    => String
pattern C0F =
  "0123456789ABCDEF"


-- | The hexidecimal digits in lower case.
pattern C0f ::
  (
  )
    => String
pattern C0f =
  "0123456789abcdef"
