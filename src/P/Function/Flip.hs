{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Function.Flip
Description :
  The flip and related functions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

The flip and related functions.
-}
module P.Function.Flip
  ( pattern F
  , f'
  , ff
  , (/$)
  , pattern F2
  , fF2
  -- * Deprecated
  , flip
  )
  where


import qualified Prelude
import Prelude
  ( Functor
  , ($)
  )


import P.Aliases


{-# Deprecated flip "Use F instead" #-}
-- | Long version of 'F'.
-- Takes a function and swaps the first two arguments.
flip ::
  (
  )
    => (a -> b -> c) -> (b -> a -> c)
flip =
  F


{-# Complete F #-}
-- | Takes a function and swaps the first two arguments.
--
-- Equivalent to 'Prelude.flip'.
pattern F ::
  (
  )
    => (a -> b -> c) -> (b -> a -> c)
pattern F x <- ((/$) -> x) where
  F =
    (/$)


-- | More general version of 'F' and 'Prelude.flip'.
--
-- Takes a value and a functor containing functions and applies that value to all of the functions in the functor using a map.
--
-- When the functor is '(a ->)' this swaps or "flips" the first two arguments of a function.
f' ::
  ( Functor f
  )
    => f (a -> b) -> a -> f b
f' =
  (/$)


-- | Flip of 'f''
ff ::
  ( Functor f
  )
    => a -> f (a -> b) -> f b
ff =
  F (/$)


-- | Infix of 'f''.
(/$) ::
  ( Functor f
  )
    => f (a -> b) -> a -> f b
f /$ x =
  Prelude.fmap ($ x) f


{-# Complete F2 :: List #-}
{-# Complete F2 :: (->) #-}
-- | A mapped flip.
--
-- Intended to be used to swap the second and third arguments of a function.
--
-- @
-- F2 :: (a -> b -> c -> d) -> (a -> c -> b -> d)
-- @
--
-- However it has a more general type signature.
pattern F2 ::
  ( Functor f
  )
    => f (a -> b -> c) -> f (b -> a -> c)
pattern F2 x <- (Prelude.fmap F -> x) where
  F2 =
    Prelude.fmap F


-- | The flip of 'F2'.
fF2 ::
  (
  )
    => a -> (a -> b -> c -> d) -> c -> b -> d
fF2 =
  F F2
