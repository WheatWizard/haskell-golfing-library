{-# Language RankNTypes #-}
{-|
Module :
  P.Function.Compose
Description :
  A set of functor combinators for the P library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental


== Cheat-sheet


@
f (g x)
(f < g) x
@
@
(f <) < g
f << g
@
@
(< g) < f
f ^. g
@
@
(< f) < g
f >< g
@
@
f < (g <)
f <!< g
@
@
((f <) <) < g
f <<< g
@
@
f < g < f
f <.< g
@
@
f x < g < f x
(f <.^ g) x
@
@
f < g < f < g
f <|< g
@
@
Uc f < g
f <% g
@
@
Uc3 f < g
f <%% g
@
@
(Uc f <) < g
f <<% g
@
@
(f +^$ g) x y =
@
@
f (g x y) (g y x)
@
-}
module P.Function.Compose
  ( Functor
  , m
  , fm
  , (<)
  , mm
  , (<<)
  , fM
  , mX
  , (^.) , fmX , m'
  , fm'
  , mof
  , m3
  , (<<<)
  , fm3
  , ma4
  , (<<&)
  , fM4
  , fMF
  , (><)
  , zz
  , (<.<)
  , fzz
  , z2
  , (<.^)
  , fz2
  , xc
  , fxc
  , pxc
  , qxc
  , ly
  , fly
  , (<|<)
  , mcm
  , fMM
  , mc2
  , (<!<)
  , fM2
  , dcp
  , fdC
  , dc2
  , fd2
  , knt
  , fKt
  , (+^$)
  -- * Curry composes
  , cqp
  , (<%)
  , fqp
  , ccq
  , (<<%)
  , fcq
  , cq3
  , (<%%)
  , fq3
  )
  where


-- Needed for documentation
import qualified Prelude
import qualified Control.Monad
import qualified Data.Functor
import Data.Functor
  ( Functor
  )


import P.Category
import P.Function.Flip


infixr 9 <, ><, <<, ^., <<<, <!<, <.<, <.^, <|<, <%, <<%, <%%


-- | Map across a functor.
--
-- Equivalent to 'Data.Functor.fmap'.
-- More general version of 'Prelude.map'
--
-- ==== __Examples__
--
-- Map a function across a list:
--
-- >>> m (+1) [3,2,4]
-- [4,3,5]
m ::
  ( Functor f
  )
    => (a -> b) -> f a -> f b
m =
  Data.Functor.fmap


-- | Flip of 'm'.
fm ::
  ( Functor f
  )
    => f a -> (a -> b) -> f b
fm =
  F m


-- | Infix version of 'm'.
--
-- Not named @(.)@ to avoid issues with module name parsing.
--
-- More general version of '(Prelude..)'.
-- Equivalent to '(Data.Functor.<$>)'
(<) ::
  ( Functor f
  )
    => (a -> b) -> f a -> f b
(<) =
  m


-- | A double map.
-- Maps two levels deep into a functor.
mm ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
mm =
  m' m


-- | Infix version of 'mm'.
--
-- ==== __Examples__
--
-- Can be used to compose functions over the second argument.
--
-- For example @mp@ takes two lists and concatenates them.
-- If we want a function that takes two lists, concats them and then takes the first 3 we can do @tk 3 << mp@.
--
-- >>> (tk 3 << mp) [1,2] [3,4]
-- [1,2,3]
--
(<<) ::
  ( Functor f
  , Functor g
  )
    => (a -> b) -> f (g a) -> f (g b)
(<<) =
  mm


-- | Flip of 'mm'.
fM ::
  ( Functor f
  , Functor g
  )
    => f (g a) -> (a -> b) -> f (g b)
fM =
  F mm


-- | Composes a function on the second argument of a binary function.
--
-- This has a more general type but can be used as:
--
-- @
-- mX :: (a -> b) -> (c -> b -> d) -> c -> a -> d
-- @
mX ::
  ( Functor f
  , Functor g
  )
    => g a -> f (a -> b) -> f (g b)
mX =
  m < fm


-- | Flip of 'mX'.
fmX ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> g a -> f (g b)
fmX =
  F mX


-- | Infix of 'mX' and 'fmX'.
(^.) ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> g a -> f (g b)
(^.) =
  F mX


-- | Maps map.
--
-- Takes a map n levels deep and upgrades it to a map n+1 levels deep.
m' ::
  ( Functor f
  , Functor g
  )
    => f (a -> b) -> f (g a -> g b)
m' =
  m m


-- | Flip of 'm''.
fm' ::
  ( Functor f
  )
    => a -> (a -> b -> c) -> f b -> f c
fm' =
  F m'


-- | Works like 'm'' works on functions, but with the first argument of a binary function instead of the second.
--
-- Compare the specialized type of 'm''
--
-- @
-- ( Functor f
-- )
--   => (a -> b -> c) -> a -> f a -> f b
-- @
--
-- With the type of 'mof'.
--
-- On functions this has the specialized type:
--
-- @
-- (a -> b -> c) -> (d -> a) -> b -> d -> c
-- @
mof ::
  ( Functor f
  )
    => (a -> b -> c) -> f a -> b -> f c
mof =
  F < m' < F


-- | Flip of 'mof'.
fMF ::
  ( Functor f
  )
    => f a -> (a -> b -> c) -> b -> f c
fMF =
  F mof


-- | A combinator for a common point-free idiom.
--
-- While this has a more general type signature this is most useful in the form:
--
-- @
-- (.\<) :: (a -> b) -> (c -> b -> d) -> (c -> a -> d)
-- x >< y =
--   (. x) . y
-- @
--
-- Here it allows you to compose a function on the second input of another function.
--
-- Flip of '(^.)'.
--
-- ==== __Examples__
--
-- If I want a function that adds two strings together separated by a @-@,
-- I can first use @(:)@ to add a comma to the second input and then combine the two.
--
-- >>> (('-' :) >< mp) "Hello" "world"
-- "Hello-world"
--
-- If I want the function:
--
-- @
-- \ x y -> x * (y + 1)
-- @
--
-- I can write it with @(><)@ as:
--
-- >>> ((+1) >< (*)) 5 1
-- 10
--
(><) ::
  ( Functor f
  , Functor g
  )
    => f a -> g (a -> b) -> g (f b)
(><) =
  m' fm


-- | "Map contramap", a weird functor combinator.
--
-- Broadly speaking if we consider @(-> a)@ to be a contravariant functor for all @a@, the type can be generalized to
--
-- @
-- mcm ::
--   ( Functor f
--   , Contravariant g
--   )
--     => g (f a -> f b) -> g (a -> b)
-- @
--
-- Which makes it analygous to 'mm' but with the outer map being a contramap.
--
-- If we have the more categorical functor class,
-- we could also further generalize the type to:
--
-- @
-- mcm ::
--   ( Functor' p1 q1
--   , Functor' (->) q2
--   )
--     => q2 (p1 a b) (q1 a b)
-- @
--
-- In this sense it's just the map of the first functor instance lifted into the @q2@ category.
--
-- This also acts as a generalization of 'mm'.
--
-- ==== __Examples__
--
-- If we have a @filter@ function we can make another filter which has the opposite behavior (keeps what it discards, discards what it keeps):
--
-- @
-- negFilter =
--   mcm mcm filter n
-- @
--
-- Note that @mcm mcm@ is just 'mc2', so in this case that is better to use.
mcm ::
  ( Functor f
  )
    => ((f a -> f b) -> c) -> (a -> b) -> c
mcm =
  fm m


-- | Flip of 'mcm'
fMM ::
  ( Functor f
  )
    => (a -> b) -> ((f a -> f b) -> c) -> c
fMM =
  F mcm


-- | Maps across the argument of a function.
--
-- On functions this has the type:
--
-- @
-- mc2 :: ((a -> b) -> c) -> (d -> b) -> (a -> d) -> c
-- @
--
-- And acts to compose across the second argument of a function.
--
-- ==== __Examples__
--
-- If we have a @filter@ function we can make another filter which has the opposite behavior (keeps what it discards, discards what it keeps):
--
-- @
-- negFilter =
--   mc2 filter n
-- @
mc2 ::
  ( Functor f
  )
    => (f b -> c) -> (a -> b) -> (f a -> c)
mc2 =
  mcm mcm


-- | Infix version of 'mc2'.
(<!<) ::
  ( Functor f
  )
    => (f b -> c) -> (a -> b) -> (f a -> c)
(<!<) =
  mc2


-- | Flip of 'mc2'.
fM2 ::
  ( Functor f
  )
    => (a -> b) -> (f b -> c) -> f a -> c
fM2 =
  F mc2


-- | A triple map.
-- Maps three levels deep into functors.
--
-- To multiply a number by three use @tp@.
m3 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => (a -> b) -> f (g (h a)) -> f (g (h b))
m3 =
  m' mm


-- | Infix of 'm3'
(<<<) ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => (a -> b) -> f (g (h a)) -> f (g (h b))
(<<<) =
  m3


-- | Flip of 'm3'.
fm3 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => f (g (h a)) -> (a -> b) -> f (g (h b))
fm3 =
  f' m3


-- | A quadruple map.
-- Maps four levels deep into functors.
ma4 ::
  ( Functor f
  , Functor g
  , Functor h
  , Functor k
  )
    => (a -> b) -> f (g (h (k a))) -> f (g (h (k b)))
ma4 =
  m' m3


-- | Infix of 'ma4'.
(<<&) ::
  ( Functor f
  , Functor g
  , Functor h
  , Functor k
  )
    => (a -> b) -> f (g (h (k a))) -> f (g (h (k b)))
(<<&) =
  ma4


-- | Flip of 'ma4'.
fM4 ::
  ( Functor f
  , Functor g
  , Functor h
  , Functor k
  )
    => f (g (h (k a))) -> (a -> b) -> f (g (h (k b)))
fM4 =
  f' ma4


-- | The "zigzag" or "sandwich" compose.
--
-- Takes a function and composes it on both ends of another.
zz ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p b a
zz =
  Control.Monad.ap og < og


-- | Flip of 'zz'.
fzz ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p a b
fzz =
  F zz


-- | Infix of 'zz' and 'fzz'.
--
-- Takes a function and composes it on both ends of another.
(<.<) ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p a b
(<.<) =
  fzz


-- | 'zz' on the second argument of a function
z2 ::
  ( Functor f
  , Semigroupoid p
  )
    => f (p a b) -> (p b a) -> f (p b a)
z2 =
  mof zz


-- | Flip of 'z2'.
fz2 ::
  ( Functor f
  , Semigroupoid p
  )
    => (p b a) -> f (p a b) -> f (p b a)
fz2 =
  F z2


-- | Infix of 'z2'.
(<.^) ::
  ( Functor f
  , Semigroupoid p
  )
    => f (p a b) -> (p b a) -> f (p b a)
(<.^) =
  z2


-- | Compose a function with itself.
xc ::
  ( Semigroupoid p
  )
    => p a a -> p a a
xc f =
  f <@ f


-- | Flip of 'xc'.
fxc ::
  (
  )
    => a -> (a -> a) -> a
fxc =
  F xc


-- | An alternative version of 'xc'.
--
-- If you define 'xc' as
--
-- @
-- xc func =
--   func <@ func
-- @
--
-- There is no single most general type in Haskell.
--
-- The tyep inferred by default is
--
-- @
-- xc :: Semigroupoid p => p a a -> p a a
-- @
--
-- This is the type given to 'xc'.
--
-- However we can notice that the input and the output do not need to be the same.
-- For example if we have @func :: a -> [a]@ then that can be composed with itself.
--
-- @pxc@ is one possible other type which can be given to 'xc'.
--
pxc ::
  ( Semigroupoid p
  )
    => (forall k. p k (f k)) -> (p a (f (f a)))
pxc f =
  f <@ f


-- | Flip of 'pxc'.
--
-- 'F' is not strong enough to flip 'pxc', but the expanded version passes the type checker.
fpX ::
  (
  )
    => a -> (forall k. k -> f k) -> f (f a)
fpX x f =
  f < f $ x


-- | @qxc@ is an alternative minimal type for the 'xc' function.
-- See 'pxc' for more information.
qxc ::
  ( Semigroupoid p
  )
    => (forall k. p (f k) k) -> (p (f (f a)) a)
qxc f =
  f <@ f


-- | Flip of 'qxc'.
fqX ::
  (
  )
    => (f (f a)) -> (forall k. f k -> k) -> a
fqX x f =
  f < f $ x


-- | Compose two functions and then compose the result with itself.
ly ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p b b
ly =
  xc << fog


-- | Flip of 'ly'.
fly ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p a a
fly =
  xc << og


-- | Infix of 'ly'.
(<|<) ::
  ( Semigroupoid p
  )
    => p a b -> p b a -> p b b
(<|<) =
  ly


-- | Takes a function on two arguments and two functions to compose on each of its arguments.
dcp ::
  ( Functor f
  , Functor g
  )
    => (a -> b -> c) -> f a -> g b -> f (g c)
dcp =
  f' < m' < f' mX


-- | Flip of dcp
fdC ::
  ( Functor f
  , Functor g
  )
    => f a -> (a -> b -> c) -> g b -> f (g c)
fdC =
  f' dcp


-- | Takes a function on three arguments and three functions to compose on each of its arguments.
dc2 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => (a -> b -> c -> d) -> f a -> g b -> h c -> f (g (h d))
dc2 func xs =
  fm (func < xs) << f' < fdC


-- | Flip of dc2.
fd2 ::
  ( Functor f
  , Functor g
  , Functor h
  )
    => f a -> (a -> b -> c -> d) -> g b -> h c -> f (g (h d))
fd2 =
  f' dc2


-- | Map a two argument function over a functor of tuples.
-- On functions this is a simultaneous uncurry and compose.
--
-- ==== __Examples__
--
-- Add pairs in a list:
--
-- >>> cqp (+) [(2,3),(5,6)]
-- [5,11]
--
cqp ::
  ( Functor f
  )
    => (a -> b -> c) -> f (a, b) -> f c
cqp f =
  m (\(x, y) -> f x y)


-- | Infix version of 'cqp'.
(<%) ::
  ( Functor f
  )
    => (a -> b -> c) -> f (a, b) -> f c
(<%) =
  cqp


-- | Flip of 'cqp'.
fqp ::
  ( Functor f
  )
    => f (a, b) -> (a -> b -> c) -> f c
fqp =
  F cqp


-- | Perform a two level deep map over a functor of tuples.
--
-- ==== __Examples__
--
-- >>> ccq (+) [[(x,y)|y<-[x..5]]|x<-[1..5]]
-- [[2,3,4,5,6],[4,5,6,7],[6,7,8],[8,9],[10]]
--
ccq ::
  ( Functor f
  , Functor g
  )
    => (a -> b -> c) -> f (g (a, b)) -> f (g c)
ccq f =
  mm (\(x, y) -> f x y)


-- | Infix of 'ccq'.
(<<%) ::
  ( Functor f
  , Functor g
  )
    => (a -> b -> c) -> f (g (a, b)) -> f (g c)
(<<%) =
  ccq


-- | Flip of 'ccq'
fcq ::
  ( Functor f
  , Functor g
  )
    => f (g (a, b)) -> (a -> b -> c) -> f (g c)
fcq =
  F ccq


-- | Map a two argument function over a functor of triples.
-- On functions this is a simultaneous uncurry and compose.
cq3 ::
  ( Functor f
  )
    => (a -> b -> c -> d) -> f (a, b, c) -> f d
cq3 f =
  m (\(x, y, z) -> f x y z)


-- | Flip of 'cq3'.
fq3 ::
  ( Functor f
  )
    => f (a, b, c) -> (a -> b -> c -> d) -> f d
fq3 =
  F cq3


-- | Infix of 'cq3'.
(<%%) ::
  ( Functor f
  )
    => (a -> b -> c -> d) -> f (a, b, c) -> f d
(<%%) =
  cq3


-- | Takes two binary functions and produces a third.
--
-- @@
-- knt f g x y =
--   f (g x y) (g y x)
-- @@
--
-- This is a sort of strange criss-crossing composition on binary functions.
--
-- ===== __Examples__
--
-- >>> knt mp mp "Hello " "World "
-- "Hello World World Hello "
-- >>> knt cp mp "abca" "abc"
-- LT
-- >>> knt cp mp "aaa" "aa"
-- EQ
--
knt ::
  (
  )
    => (a -> a -> b) -> (c -> c -> a) -> (c -> c -> b)
knt f g x y =
  f (g x y) (g y x)


-- | Flip of 'knt'.
fKt ::
  (
  )
    => (a -> a -> b) -> (b -> b -> c) -> (a -> a -> c)
fKt =
  f' knt


-- | Infix version of 'knt'.
(+^$) ::
  (
  )
    => (a -> a -> b) -> (c -> c -> a) -> (c -> c -> b)
(f +^$ g) x y =
  f (g x y) (g y x)
