{-|
Module :
  P.Function.Compose.Extra
Description :
  Additional functions for the P.Function.Compose module
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Function.Compose module.
-}
module P.Function.Compose.Extra
  where


import P.Function.Compose
import P.Function.Flip


-- | Map 4 levels deep into functors.
m4 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  )
    => (a -> b) -> f1 (f2 (f3 (f4 a))) -> f1 (f2 (f3 (f4 b)))
m4 =
  m' m3


-- | Flip of m4.
fm4 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  )
    => f1 (f2 (f3 (f4 a))) -> (a -> b) -> f1 (f2 (f3 (f4 b)))
fm4 =
  f' m4


-- | Map 5 levels deep into functors.
m5 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 a)))) -> f1 (f2 (f3 (f4 (f5 b))))
m5 =
  m' m4


-- | Flip of m5.
fm5 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  )
    => f1 (f2 (f3 (f4 (f5 a)))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 b))))
fm5 =
  f' m5


-- | Map 6 levels deep into functors.
m6 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 a))))) -> f1 (f2 (f3 (f4 (f5 (f6 b)))))
m6 =
  m' m5


-- | Flip of m6.
fm6 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  )
    => f1 (f2 (f3 (f4 (f5 (f6 a))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 b)))))
fm6 =
  f' m6


-- | Map 7 levels deep into functors.
m7 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 a)))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 b))))))
m7 =
  m' m6


-- | Flip of m7.
fm7 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 a)))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 b))))))
fm7 =
  f' m7


-- | Map 8 levels deep into functors.
m8 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 a))))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 b)))))))
m8 =
  m' m7


-- | Flip of m8.
fm8 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 a))))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 b)))))))
fm8 =
  f' m8


-- | Map 9 levels deep into functors.
m9 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 a)))))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 b))))))))
m9 =
  m' m8


-- | Flip of m9.
fm9 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 a)))))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 b))))))))
fm9 =
  f' m9


-- | Map 10 levels deep into functors.
m0 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 a))))))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 b)))))))))
m0 =
  m' m9


-- | Flip of m0.
fm0 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 a))))))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 b)))))))))
fm0 =
  f' m0


-- | Map 11 levels deep into functors.
m1 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  , Functor f11
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 a)))))))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 b))))))))))
m1 =
  m' m0


-- | Flip of m1.
fm1 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  , Functor f11
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 a)))))))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 b))))))))))
fm1 =
  f' m1


-- | Map 12 levels deep into functors.
m2 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  , Functor f11
  , Functor f12
  )
    => (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 (f12 a))))))))))) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 (f12 b)))))))))))
m2 =
  m' m1


-- | Flip of m2.
fm2 ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  , Functor f6
  , Functor f7
  , Functor f8
  , Functor f9
  , Functor f10
  , Functor f11
  , Functor f12
  )
    => f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 (f12 a))))))))))) -> (a -> b) -> f1 (f2 (f3 (f4 (f5 (f6 (f7 (f8 (f9 (f10 (f11 (f12 b)))))))))))
fm2 =
  f' m2
