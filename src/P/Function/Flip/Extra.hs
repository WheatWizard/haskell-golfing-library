{-|
Module :
  P.Function.Flip.Extra
Description :
  Additional functions for the P.Function.Flip library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Function.Flip library.
-}
module P.Function.Flip.Extra
  ( f2a
  , f2b
  , f3a
  , f3b
  , f4a
  , f4b
  , f5a
  , f5b
  )
  where


import qualified Prelude


import P.Category
import P.Function.Compose
import P.Function.Compose.Extra
import P.Function.Flip


-- | Move the front argument of a function to the third place.
f2a ::
  ( Functor f
  )
    => f (a -> b -> c) -> a -> b -> f c
f2a =
  f' << f'


-- | Move the third argument of a function to the front.
f2b ::
  ( Functor f1
  , Functor f2
  )
    => f1 (f2 (a -> b)) -> a -> f1 (f2 b)
f2b =
  f' (mm <@ f' ($))


-- | Move the front argument of a function to the fourth place.
f3a ::
  ( Functor f
  )
    => f (a -> b -> c -> d) -> a -> b -> c -> f d
f3a =
  f' <<< f2a


-- | Move the fourth argument of a function to the front.
f3b ::
  ( Functor f1
  , Functor f2
  , Functor f3
  )
    => f1 (f2 (f3 (a -> b))) -> a -> f1 (f2 (f3 b))
f3b =
  f' (m3 <@ f' ($))


-- | Move the front argument of a function to the fifth place.
f4a ::
  ( Functor f
  )
    => f (a -> b -> c -> d -> e) -> a -> b -> c -> d -> f e
f4a =
  m4 f' f3a


-- | Move the fifth argument of a function to the front.
f4b ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  )
    => f1 (f2 (f3 (f4 (a -> b)))) -> a -> f1 (f2 (f3 (f4 b)))
f4b =
  f' (m4 <@ f' ($))


-- | Move the front argument of a function to the sixth place.
f5a ::
  ( Functor f
  )
    => f (a1 -> a2 -> a3 -> a4 -> a5 -> a6) -> a1 -> a2 -> a3 -> a4 -> a5 -> f a6
f5a =
  m5 f' f4a


-- | Move the sixth argument of a function to the front.
f5b ::
  ( Functor f1
  , Functor f2
  , Functor f3
  , Functor f4
  , Functor f5
  )
    => f1 (f2 (f3 (f4 (f5 (a -> b))))) -> a -> f1 (f2 (f3 (f4 (f5 b))))
f5b =
  f' (m5 <@ f' ($))
