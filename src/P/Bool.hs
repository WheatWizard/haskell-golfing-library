{-# Language PatternSynonyms #-}
{-|
Module :
  P.Bool
Description :
  Function on booleans
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Some binary boolean functions are provided elsewhere with more general types.

* or: '(P.Ord.^^)'
* xor: '(P.Eq./=)'
* and: '(GHC.Base.<>)', '(P.Ord.!^)'
* xnor: '(P.Eq.==)'
* imply: '(P.Ord.>=)'
* nimply: '(P.Ord.>)'

-}
module P.Bool
  ( pattern T
  , pattern B
  , pattern True
  , pattern False
  , n
  , (||)
  , nO
  , (~|)
  , Bool
  -- * Conditional branching
  , iF
  , (?.)
  , fiF
  , liF
  , fli
  , lii
  , fIi
  , fiI
  , ffI
  , li2
  , fI2
  , li3
  , fI3
  )
  where


import qualified Prelude
import Prelude
  ( Bool
  )


import Control.Applicative as Applicative


import P.Category
import P.Function.Compose
import P.Function.Flip


infixr 2 ||


{-# Deprecated True "Use T instead" #-}
-- | A long version of 'T'.
pattern True :: Bool
pattern True <- Prelude.True where
  True =
    Prelude.True


{-# Deprecated False "Use B instead" #-}
-- | A long version of 'B'.
pattern False :: Bool
pattern False <- Prelude.False where
  False =
    Prelude.False


{-# Complete T, B #-}
{-# Complete T, False #-}
{-# Complete True, B #-}
-- | A shorthand for @True@.
pattern T :: Bool
pattern T <- Prelude.True where
  T =
    Prelude.True


-- | A shorthand for @False@.
pattern B :: Bool
pattern B <- Prelude.False where
  B =
    Prelude.False


-- | Logical not.
-- Takes a boolean and returns the opposite.
--
-- Equivalent to 'Prelude.not'.
n ::
  (
  )
    => Bool -> Bool
n =
  Prelude.not


-- | Logical or.
--
-- Equivalent to '(Prelude.||)'.
(||) ::
  (
  )
    => Bool -> Bool -> Bool
(||) =
  (Prelude.||)


-- | Logical nor.
nO ::
  (
  )
    => Bool -> Bool -> Bool
nO =
  n << (||)


-- | Logical nor.
(~|) ::
  (
  )
    => Bool -> Bool -> Bool
(~|) =
  nO


-- | Conditional branch.
-- Returns the first argument if true, and the second if false.
iF ::
  (
  )
    => a -> a -> Bool -> a
iF x _ T =
  x
iF _ y B =
  y


-- | Infix of 'iF'.
(?.) ::
  (
  )
    => a -> a -> Bool -> a
(?.) =
  iF


-- | Flip of 'iF'.
-- Returns the first argument if true, and the second if false.
fiF ::
  (
  )
    => a -> a -> Bool -> a
fiF =
  f' iF


-- | Applies a predicate to a value and then conditionally applies one of two functions.
-- Applies the first function if the predicate succeeds.
--
-- A more restricted but more expressive type is:
--
-- @
-- (a -> b) -> (a -> b) -> Predicate a -> a -> b
-- @
liF ::
  ( Applicative f
  )
    => f a -> f a -> f Bool -> f a
liF =
  Applicative.liftA3 iF


-- | Flip of 'liF'.
fli ::
  ( Applicative f
  )
    => f a -> f a -> f Bool -> f a
fli =
  f' liF


-- | Apply a function to a value if a predicate fails on that value, otherwise do nothing.
lii ::
  ( Applicative (p a)
  , Category p
  )
    => p a a -> p a Bool -> p a a
lii =
  liF id


-- | Flip of 'lii'.
fIi ::
  ( Applicative (p a)
  , Category p
  )
    => p a Bool -> p a a -> p a a
fIi =
  f' lii


-- | Apply a function to a value if a predictate passes on that value, otherwise do nothing.
fiI ::
  ( Applicative (p a)
  , Category p
  )
    => p a a -> p a Bool -> p a a
fiI =
  fli id


-- | Flip of 'fiI'.
ffI ::
  ( Applicative (p a)
  , Category p
  )
    => p a Bool -> p a a -> p a a
ffI =
  f' fiI


-- | Applies a binary predicate to two values and then conditionally applies one of two functions to those two values.
--
-- Applies the first function if the predicate succeeds.
--
-- A more restricted but more expressive type is:
--
-- @
-- (a -> b -> c) -> (a -> b -> c) -> (a -> b -> Bool) -> a -> b -> c
-- @
li2 ::
  ( Applicative f
  , Applicative g
  )
    => f (g a) -> f (g a) -> f (g Bool) -> f (g a)
li2 =
  Applicative.liftA3 < Applicative.liftA3 $ iF


-- | Flip of 'li2'.
fI2 ::
  ( Applicative f
  , Applicative g
  )
    => f (g a) -> f (g a) -> f (g Bool) -> f (g a)
fI2 =
  f' li2


-- | Applies a ternary predicate to three values and the conditionally applies one of two function to those three values.
--
-- Applies the first function if the predicate succeeds.
--
-- A more restricted but more expressive type is:
--
-- @
-- (a -> b -> c -> d) -> (a -> b -> c -> d) -> (a -> b -> c -> Bool) -> a -> b -> c -> d
-- @
li3 ::
  ( Applicative f
  , Applicative g
  , Applicative h
  )
    => f (g (h a)) -> f (g (h a)) -> f (g (h Bool)) -> f (g (h a))
li3 =
  Applicative.liftA3 < Applicative.liftA3 < Applicative.liftA3 $ iF


-- | Flip of 'li3'.
fI3 ::
  ( Applicative f
  , Applicative g
  , Applicative h
  )
    => f (g (h a)) -> f (g (h a)) -> f (g (h Bool)) -> f (g (h a))
fI3 =
  f' li3
