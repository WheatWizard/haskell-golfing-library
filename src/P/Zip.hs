{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Zip
  ( Zip (..)
  , (=#=)
  , zwp
  , zwK
  , uz
  , z3
  , fz3
  , ź
  , fZ3
  , fzW
  , fzp
  , zWq
  , zWX
  , zWn
  , nZx
  , xZn
  , upZ
  , fpZ
  -- * Zip and concat
  , jzW
  , (=#?)
  , fjz
  , ozW
  , (=#*)
  , foz
  , azW
  , (=#+)
  , faZ
  -- * Deprecated
  , zipWith
  , unzip
  )
  where


import Prelude
  (
  )
import qualified Prelude


import P.Aliases
import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Alternative
import P.Bool
import P.Eq
import P.Foldable
import P.Foldable.MinMax
import P.Function.Compose
import P.Function.Flip
import P.Function.Curry
import P.Monad
import P.Ord
import P.Zip.SemiAlign


-- | Functors with a zip operation that thakes the intersection of potentially non-uniform shapes.
--
class SemiAlign f => Zip f where
  {-# Minimal zW | zp #-}
  -- | When given two 'Zip's this combines them pairwise using a given binary function.
  -- The longer list is trimmed to the length of the shorter list.
  --
  -- In general this an alternative 'P.Applicative.l2', obeying all of the 'P.Applicative.Applicative' laws.
  -- However we privledge the normal implementation because it has a 'Prelude.Monad' instance while this does not.
  --
  -- More general version of 'Data.List.zipWith'.
  zW :: (a -> b -> c) -> f a -> f b -> f c
  zW func =
    U func <<< zp

  -- | Takes two lists and combines them pairwise into a list of tuples.
  --
  -- More general version of 'Data.List.zip'.
  zp :: f a -> f b -> f (a, b)
  zp =
    zW (,)


instance Zip [] where
  zW =
    Prelude.zipWith
  zp =
    Prelude.zip


infixr 9 =#=
-- | Infix of 'zW'.
(=#=) ::
  ( Zip f
  )
    => (a -> b -> c) -> f a -> f b -> f c
(=#=) =
  zW


{-# Deprecated zipWith "Use zW instead" #-}
-- | Long version of 'zW'.
zipWith ::
  ( Zip f
  )
    => (a -> b -> c) -> f a -> f b -> f c
zipWith =
  zW


-- | Flip of 'zW'.
fzW ::
  ( Zip f
  )
    => f a -> (a -> b -> c) -> f b -> f c
fzW =
  F zW


-- | Flip of 'zp'.
fzp ::
  ( Zip f
  )
    => f a -> f b -> f (b, a)
fzp =
  F zp


-- | Zips with semigroup action.
--
-- If you want to pad out to the length of the longer list use 'zdm'.
zwp ::
  ( Zip f
  , Semigroup a
  )
    => f a -> f a -> f a
zwp =
  zW mp


-- | Zips with cons ('K').
zwK ::
  ( Zip f
  )
    => f a -> f (List a) -> f (List a)
zwK =
  zW (:)


{-# Deprecated unzip "Use uz instead" #-}
-- | Long version of 'uz'.
unzip ::
  ( Functor f
  )
    => f (a, b) -> (f a, f b)
unzip =
  uz


-- | Takes a functor of pairs and gives a pair of functors.
-- The first one "contains" all the first elements.
-- And the second one "contains" all of the second elements.
--
-- More general version of 'Data.List.unzip'.
uz ::
  ( Functor f
  )
    => f (a, b) -> (f a, f b)
uz start =
  ( m Prelude.fst start
  , m Prelude.snd start
  )


-- | Zip with and join.
--
-- One of three generalizations of zip with and concat the others being 'ozW' and 'azW'.
jzW ::
  ( Monad m
  , Zip m
  )
    => (a -> b -> m c) -> m a -> m b -> m c
jzW =
  jn <<< zW


infixr 9 =#?
-- | Infix of 'jzW'.
(=#?) ::
  ( Monad m
  , Zip m
  )
    => (a -> b -> m c) -> m a -> m b -> m c
(=#?) =
  jzW


-- | Flip of 'jzW'.
fjz ::
  ( Monad m
  , Zip m
  )
    => m a -> (a -> b -> m c) -> m b -> m c
fjz =
  f' jzW


-- | Zip with and fold elements with the monoidal action.
--
-- One of three generalizations of zip with and concat the others being 'jzW' and 'azW'.
ozW ::
  ( Monoid m
  , Zip f
  , Foldable f
  )
    => (a -> b -> m) -> f a -> f b -> m
ozW =
  fo <<< zW


infixr 9 =#+
-- | Infix of 'ozW'.
(=#*) ::
  ( Monoid m
  , Zip f
  , Foldable f
  )
    => (a -> b -> m) -> f a -> f b -> m
(=#*) =
  ozW


-- | Flip of 'ozW'.
foz ::
  ( Monoid m
  , Zip f
  , Foldable f
  )
    => f a -> (a -> b -> m) -> f b -> m
foz =
  f' ozW


-- | Zip with and combine using '(++)'.
--
-- One of three generalizations of zip with and concat the others being 'jzW' and 'ozW'.
azW ::
  ( Alternative f
  , Zip t
  , Foldable t
  )
    => (a -> b -> f c) -> t a -> t b -> f c
azW =
  cx <<< zW


infixr 9 =#*
-- | Infix of 'azW'.
(=#+) ::
  ( Alternative f
  , Zip t
  , Foldable t
  )
    => (a -> b -> f c) -> t a -> t b -> f c
(=#+) =
  azW


-- | Flip of 'azW'.
faZ ::
  ( Alternative f
  , Zip t
  , Foldable t
  )
    => t a -> (a -> b -> f c) -> t b -> f c
faZ =
  f' azW


-- | A three argument version of 'zW'.
z3 ::
  ( Zip f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
z3 func =
  zW (Prelude.uncurry func) << zp


-- | The flip of 'z3'.
fz3 ::
  ( Zip f
  )
    => f a -> (a -> b -> c -> d) -> f b -> f c -> f d
fz3 =
  F z3


-- | A version of 'zp' which takes three arguments and makes a 3-tuple.
--
-- Equivalent to 'Data.List.zip3'.
ź ::
  ( Zip f
  )
    => f a -> f b -> f c -> f (a, b, c)
ź =
  z3 (,,)


-- | Flip of 'ź'.
fZ3 ::
  ( Zip f
  )
    => f b -> f a -> f c -> f (a, b, c)
fZ3 =
  F ź


-- | Zip using 'eq'.
--
-- Determines which pairwise elements of two lists are equal.
--
-- For a version which doesn't truncate to the intersection of the structures use 'P.Zip.SemiAlign.zdq'.
zWq ::
  ( Zip f
  , Eq a
  )
    => f a -> f a -> f Bool
zWq =
  zW eq


-- | Zip using 'ma'.
--
-- Determines the pairwise maxima of two structures.
--
-- For a version which doesn't truncate to the intersection of the structures use 'P.Zip.SemiAlign.zdx'.
zWX ::
  ( Zip f
  , Ord a
  )
    => f a -> f a -> f a
zWX =
  zW ma


-- | Zip using 'mn'.
--
-- Determines the pairwise minima of two structures.
--
-- For a version which doesn't truncate to the intersection of the structures use 'P.Zip.SemiAlign.zdn'.
zWn ::
  ( Zip f
  , Ord a
  )
    => f a -> f a -> f a
zWn =
  zW mN


-- | Zip using 'ma' then combine with 'mn'.
nZx ::
  ( Zip f
  , Foldable f
  , Ord a
  )
    => f a -> f a -> a
nZx =
  mn << zWX


-- | Zip using 'mN' the combine with 'mx'.
xZn ::
  ( Zip f
  , Foldable f
  , Ord a
  )
    => f a -> f a -> a
xZn =
  mx << zWn


-- | Take a zip with function (e.g. 'm', 'zW', 'z3') and upgrade it to operate on one more argument.
--
-- @
-- upZ m = zW
-- upZ zW = z3
-- upZ z3 = 'P.Zip.Extra.z4'
-- @
upZ ::
  ( Zip f
  )
    => (((a, b) -> c) -> f (d, e) -> g) -> (a -> b -> c) -> f d -> f e -> g
upZ zipper func =
  zipper (U func) << zp


-- | Flip of 'upZ'.
fpZ ::
  ( Zip f
  )
    => (a -> b -> c) -> (((a, b) -> c) -> f (d, e) -> g) -> f d -> f e -> g
fpZ =
  f' upZ
