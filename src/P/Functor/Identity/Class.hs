{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language FlexibleInstances #-}
{-# Language UndecidableInstances #-}
module P.Functor.Identity.Class
  ( Identity (..)
  , pattern UI
  , pattern Li2
  , pattern UL2
  , pattern La2
  , pattern L2'
  , pattern Ul2
  )
  where


import qualified Prelude
import Prelude
  ( Traversable
  , Bounded
  )


import P.Applicative
import P.Applicative.Unpure
import P.Category
import P.Comonad
import P.Function.Compose
import P.Monad


-- | A class for functors which are equivalent to the identity functor.
--
-- These occur commonly in newtype wrappers.
--
-- It has no new member functions but should satisfy the properties:
--
-- prop> p < cr = id
-- prop> cr < p = id
--
class
  ( Monad f
  , Unpure f
  , Comonad f
  )
    => Identity f


instance Identity ((->) ())


-- instance Identity ((,) ())
--
-- instance Identity ((,,) () ())
--
-- instance Identity ((,,,) () () ())


instance (Bounded a, Identity f) => Bounded (f a) where
  maxBound =
    p Prelude.maxBound
  minBound =
    p Prelude.minBound


-- | A pattern of copure.
--
-- For the more general copure use 'cr'.
pattern UI ::
  ( Identity f
  )
    => f a -> a
pattern UI x <- (p -> x) where
  UI =
    cr


-- | An internal function.
-- Use the exported 'UL2' instead.
_unLi2 ::
  ( Identity f
  , Identity g
  )
    => (f a -> g b -> c) -> a -> b -> c
_unLi2 func x y =
  func (p x) (p y)


{-# Complete Li2 #-}
-- | Lifts a function on two variables to act on identity functors.
pattern Li2 ::
  ( Identity f
  , Identity g
  )
    => (a -> b -> c) -> f a -> g b -> c
pattern Li2 x <- (_unLi2 -> x) where
  Li2 =
    fm UI < mX UI


{-# Complete UL2 #-}
-- | Takes a function on two Identity wrapped variables and converts it to one on unwrapped values.
pattern UL2 ::
  ( Identity f
  , Identity g
  )
    => (f a -> g b -> c) -> a -> b -> c
pattern UL2 x <- (Li2 -> x) where
  UL2 =
    _unLi2


-- | Specialized version of 'UL2'
pattern Di2 ::
  ( Identity f
  )
    => (f a -> f b -> f c) -> a -> b -> f c
pattern Di2 x <- (Li2 -> x) where
  Di2 func x y =
    func (p x) (p y)


-- | Internal function.
_unLa2 ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (f a -> g b -> h c) -> a -> b -> c
_unLa2 func x y=
  UI $ func (p x) (p y)


{-# Complete La2 #-}
-- | A version of 'l2' which functions as a pattern match when the functor is an identity.
pattern La2 ::
  ( Identity f
  )
    => (a -> b -> c) -> f a -> f b -> f c
pattern La2 x <- (_unLa2 -> x) where
  La2 =
    l2


{-# Complete L2' #-}
-- | A generalized version of 'La2' which allows the three identity functors to be different.
-- This may require some coaxing to get the right types.
pattern L2' ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (a -> b -> c) -> f a -> g b -> h c
pattern L2' x <- (_unLa2 -> x) where
  L2' func x y =
    p $ func (UI x) (UI y)


{-# Complete Ul2 #-}
-- | The reverse of both 'La2' and 'L2''.
--
-- Takes a function on wrapped values and creates a function on unwrapped values.
pattern Ul2 ::
  ( Identity f
  , Identity g
  , Identity h
  )
    => (f a -> g b -> h c) -> a -> b -> c
pattern Ul2 x <- (L2' -> x) where
  Ul2 =
    _unLa2
