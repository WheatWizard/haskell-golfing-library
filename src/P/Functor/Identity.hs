module P.Functor.Identity
  ( Ident (..)
  )
  where


import qualified Prelude
import Prelude
  ( Traversable
  , (++)
  )
import qualified Data.Functor.Classes


import P.Applicative
import P.Applicative.Unpure
import P.Bifunctor.These
import P.Category
import P.Comonad
import P.Eq
import P.Function.Compose
import P.Functor.Identity.Class
import P.Monad
import P.Ord
import P.Show
import P.Zip
import P.Zip.SemiAlign
import P.Zip.WeakAlign


-- | A simple identity functor.
newtype Ident i =
  Idn
    { uId ::
      i
    }


instance Functor Ident where
  fmap f =
    Idn < f < uId


instance Applicative Ident where
  pure =
    Idn
  Idn func <*> Idn v =
    Idn (func v)


instance Monad Ident where
  Idn m >>= func =
    func m


instance Comonad Ident where
  cr =
    uId
  dyp =
    p


instance Unpure Ident where
  unp =
    p < uId
  pure' =
    Idn


instance Identity Ident


instance
  ( Show a
  )
    => Show (Ident a)
  where
    showsPrec v (Idn a) b
      | 10 <= v
      =
        "(Idn " ++ Prelude.showsPrec 10 a (')' : b)
      | 10 > v
      =
        "Idn " ++ Prelude.showsPrec 10 a b


instance Eq a => Eq (Ident a) where
  (==) =
    Li2 eq


instance Ord a => Ord (Ident a) where
  cp =
    Li2 cp
  (<=) =
    Li2 (<=)


instance Eq1 Ident where
  q1 =
    Li2


instance Ord1 Ident where
  lcp =
    Li2


instance Zip Ident where
  zW func (Idn x) (Idn y) =
    Idn $ func x y


instance WeakAlign Ident where
  cnL =
    p


instance SemiAlign Ident where
  aln (Idn x) (Idn y) =
    Idn $ Te x y
  alW func (Idn x) (Idn y) =
    Idn $ func $ Te x y
