{-|
Module :
  P.Ix
Description :
  The Ix class
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

The 'Ix' class is used to map a contiguous subrange of values in type onto integers.
It is used primarily for array indexing.
'Ix' uses row-major order.
-}
module P.Ix
  ( Ix
  , Range
  , uRg
  , rg
  , frg
  , uDx
  , fux
  , dx
  , fdx
  , uIr
  , fur
  , ir
  , fir
  , uGz
  , gz
  , fgz
  -- * Deprecated
  , range
  , index
  , inRange
  , rangeSize
  )
  where


import qualified Prelude
import Prelude
  ( Char
  , Bool
  , Integer
  , Int
  , Word
  , (+)
  , (*)
  )


import qualified Data.Ix
import Data.Word
  ( Word8
  , Word16
  , Word32
  , Word64
  )
import Data.Char
  ( GeneralCategory
  )


import P.Algebra.Monoid
import P.Aliases
import P.Applicative
import P.Category
import P.Function.Curry
import P.Function.Flip
import P.Ord


-- | A range.
-- This is a type alias to allow more expressive type signatures.
-- It should be used when a tuple is intended to represent a range with the first value being the smaller end of the range.
type Range a =
  ( a
  , a
  )


class
  ( Ord a
  )
    => Ix a
  where
    -- | The list of values in the subrange defined by a bounding pair.
    --
    -- Equivalent to 'Data.Ix.range'.
    uRg :: Range a -> List a

    -- | Gives the position of a value in a range.
    --
    -- Produces an error if the value is not in the range.
    --
    -- Equivalent to 'Data.Ix.index'.
    uDx :: Range a -> a -> Int

    -- | Determine the size of a range.
    --
    -- Equivalent to 'Data.Ix.rangeSize'.
    uGz :: Range a -> Int
    uGz (x, y) =
      uDx (x, y) y

    -- | Determines if a value is in a range.
    --
    -- Equivalent to 'Data.Ix.inRange'.
    uIr :: Range a -> a -> Bool


{-# Deprecated range "Use uRg instead" #-}
-- | Long version of 'uRg'.
range ::
  ( Ix a
  )
    => Range a -> List a
range =
  uRg


-- | Lists the values between two items.
-- Curried version of 'uRg'.
--
-- Similar to 'P.Enum.ef'.
--
-- ==== __Examples__
--
-- >>> rg 0 (1,2)
-- [(0,0),(0,1),(0,2),(1,0),(1,1),(1,2)]
--
rg ::
  ( Ix a
  )
    => a -> a -> List a
rg =
  Cu uRg


-- | Flip of 'rg'.
frg ::
  ( Ix a
  )
    => a -> a -> List a
frg =
  f' rg


{-# Deprecated index "Use uDx instead" #-}
-- | Long version of 'uDx'.
index ::
  ( Ix a
  )
    => Range a -> a -> Int
index =
  uDx


-- | Flip of 'uDx'.
fux ::
  ( Ix a
  )
    => a -> Range a -> Int
fux =
  f' uDx


-- | Gives the position of a value between two other values.
--
-- Produces an error if the value is not in the range.
dx ::
  ( Ix a
  )
    => a -> a -> a -> Int
dx =
  Cu uDx


-- | Flip of 'dx'.
fdx ::
  ( Ix a
  )
    => a -> a -> a -> Int
fdx =
  f' dx


{-# Deprecated inRange "Use uIr instead" #-}
-- | Long version of 'uIr'.
inRange ::
  ( Ix a
  )
    => Range a -> a -> Bool
inRange =
  uIr


-- | Flip of 'uIr'.
fur ::
  ( Ix a
  )
    => a -> Range a -> Bool
fur =
  f' uIr


-- | Determines if a value is between two other values.
ir ::
  ( Ix a
  )
    => a -> a -> a -> Bool
ir =
  Cu uIr


-- | Flip of 'ir'.
fir ::
  ( Ix a
  )
    => a -> a -> a -> Bool
fir =
  f' ir


{-# Deprecated rangeSize "Use uGz instead" #-}
-- | Long version of 'uGz'.
rangeSize ::
  ( Ix a
  )
    => Range a -> Int
rangeSize =
  uGz


-- | Determine the number of elements between two values inclusive.
gz ::
  ( Ix a
  )
    => a -> a -> Int
gz =
  Cu uGz


-- | Flip of 'gz'.
fgz ::
  ( Ix a
  )
    => a -> a -> Int
fgz =
  f' gz


instance
  (
  )
    => Ix Word
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Word8
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Word16
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Word32
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Word64
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Int
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Integer
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Bool
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix Char
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix ()
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  (
  )
    => Ix GeneralCategory
  where
    uRg =
      Data.Ix.range
    uDx =
      Data.Ix.index
    uIr =
      Data.Ix.inRange
    uGz =
      Data.Ix.rangeSize


instance
  ( Ix a1
  , Ix a2
  )
    => Ix
      ( a1
      , a2
      )
  where
    uRg
      ( ( x1
        , x2
        )
      , ( y1
        , y2
        )
      )
      =
      ( \t1
        ( t2
        )
        ->
        ( t1
        , t2
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          )
          ( y2
          )
    uIr
      ( ( x1
        , x2
        )
      , ( y1
        , y2
        )
      )
      ( z1
      , z2
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              )
            , ( y2
              )
            )
            ( z2
            )
    uDx
      ( ( x1
        , x2
        )
      , ( y1
        , y2
        )
      )
      ( z1
      , z2
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            )
          , ( y2
            )
          )
          ( z2
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  )
    => Ix
      ( a1
      , a2
      , a3
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        )
      , ( y1
        , y2
        , y3
        )
      )
      =
      ( \t1
        ( t2
        , t3
        )
        ->
        ( t1
        , t2
        , t3
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          )
          ( y2
          , y3
          )
    uIr
      ( ( x1
        , x2
        , x3
        )
      , ( y1
        , y2
        , y3
        )
      )
      ( z1
      , z2
      , z3
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              )
            , ( y2
              , y3
              )
            )
            ( z2
            , z3
            )
    uDx
      ( ( x1
        , x2
        , x3
        )
      , ( y1
        , y2
        , y3
        )
      )
      ( z1
      , z2
      , z3
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            )
          , ( y2
            , y3
            )
          )
          ( z2
          , z3
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        )
      , ( y1
        , y2
        , y3
        , y4
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          )
          ( y2
          , y3
          , y4
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        )
      , ( y1
        , y2
        , y3
        , y4
        )
      )
      ( z1
      , z2
      , z3
      , z4
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              )
            , ( y2
              , y3
              , y4
              )
            )
            ( z2
            , z3
            , z4
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        )
      , ( y1
        , y2
        , y3
        , y4
        )
      )
      ( z1
      , z2
      , z3
      , z4
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            )
          , ( y2
            , y3
            , y4
            )
          )
          ( z2
          , z3
          , z4
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          )
          ( y2
          , y3
          , y4
          , y5
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              )
            , ( y2
              , y3
              , y4
              , y5
              )
            )
            ( z2
            , z3
            , z4
            , z5
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            )
          , ( y2
            , y3
            , y4
            , y5
            )
          )
          ( z2
          , z3
          , z4
          , z5
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  , Ix a11
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          , x11
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          , y11
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              , x11
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              , y11
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            , z11
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            , x11
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            , y11
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          , z11
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  , Ix a11
  , Ix a12
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          , x11
          , x12
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          , y11
          , y12
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              , x11
              , x12
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              , y11
              , y12
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            , z11
            , z12
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            , x11
            , x12
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            , y11
            , y12
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          , z11
          , z12
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  , Ix a11
  , Ix a12
  , Ix a13
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          , x11
          , x12
          , x13
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          , y11
          , y12
          , y13
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              , x11
              , x12
              , x13
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              , y11
              , y12
              , y13
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            , z11
            , z12
            , z13
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            , x11
            , x12
            , x13
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            , y11
            , y12
            , y13
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          , z11
          , z12
          , z13
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  , Ix a11
  , Ix a12
  , Ix a13
  , Ix a14
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      , a14
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        , t14
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        , t14
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          , x11
          , x12
          , x13
          , x14
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          , y11
          , y12
          , y13
          , y14
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      , z14
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              , x11
              , x12
              , x13
              , x14
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              , y11
              , y12
              , y13
              , y14
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            , z11
            , z12
            , z13
            , z14
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      , z14
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            , x11
            , x12
            , x13
            , x14
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            , y11
            , y12
            , y13
            , y14
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          , z11
          , z12
          , z13
          , z14
          )


instance
  ( Ix a1
  , Ix a2
  , Ix a3
  , Ix a4
  , Ix a5
  , Ix a6
  , Ix a7
  , Ix a8
  , Ix a9
  , Ix a10
  , Ix a11
  , Ix a12
  , Ix a13
  , Ix a14
  , Ix a15
  )
    => Ix
      ( a1
      , a2
      , a3
      , a4
      , a5
      , a6
      , a7
      , a8
      , a9
      , a10
      , a11
      , a12
      , a13
      , a14
      , a15
      )
  where
    uRg
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        , x15
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        , y15
        )
      )
      =
      ( \t1
        ( t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        , t14
        , t15
        )
        ->
        ( t1
        , t2
        , t3
        , t4
        , t5
        , t6
        , t7
        , t8
        , t9
        , t10
        , t11
        , t12
        , t13
        , t14
        , t15
        )
      ) ** (rg x1 y1) $
        rg
          ( x2
          , x3
          , x4
          , x5
          , x6
          , x7
          , x8
          , x9
          , x10
          , x11
          , x12
          , x13
          , x14
          , x15
          )
          ( y2
          , y3
          , y4
          , y5
          , y6
          , y7
          , y8
          , y9
          , y10
          , y11
          , y12
          , y13
          , y14
          , y15
          )
    uIr
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        , x15
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        , y15
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      , z14
      , z15
      )
      =
        uIr (x1, y1) z1 <>
          uIr
            ( ( x2
              , x3
              , x4
              , x5
              , x6
              , x7
              , x8
              , x9
              , x10
              , x11
              , x12
              , x13
              , x14
              , x15
              )
            , ( y2
              , y3
              , y4
              , y5
              , y6
              , y7
              , y8
              , y9
              , y10
              , y11
              , y12
              , y13
              , y14
              , y15
              )
            )
            ( z2
            , z3
            , z4
            , z5
            , z6
            , z7
            , z8
            , z9
            , z10
            , z11
            , z12
            , z13
            , z14
            , z15
            )
    uDx
      ( ( x1
        , x2
        , x3
        , x4
        , x5
        , x6
        , x7
        , x8
        , x9
        , x10
        , x11
        , x12
        , x13
        , x14
        , x15
        )
      , ( y1
        , y2
        , y3
        , y4
        , y5
        , y6
        , y7
        , y8
        , y9
        , y10
        , y11
        , y12
        , y13
        , y14
        , y15
        )
      )
      ( z1
      , z2
      , z3
      , z4
      , z5
      , z6
      , z7
      , z8
      , z9
      , z10
      , z11
      , z12
      , z13
      , z14
      , z15
      )
      =
        uDx (x1, y1) z1 + gz x1 y1 * uDx
          ( ( x2
            , x3
            , x4
            , x5
            , x6
            , x7
            , x8
            , x9
            , x10
            , x11
            , x12
            , x13
            , x14
            , x15
            )
          , ( y2
            , y3
            , y4
            , y5
            , y6
            , y7
            , y8
            , y9
            , y10
            , y11
            , y12
            , y13
            , y14
            , y15
            )
          )
          ( z2
          , z3
          , z4
          , z5
          , z6
          , z7
          , z8
          , z9
          , z10
          , z11
          , z12
          , z13
          , z14
          , z15
          )
