module P.Monad
  ( Monad
  , bn
  , (>~)
  , fb
  , (~<)
  , lbn
  , (<~)
  , lfb
  , (~~<)
  , bbn
  , fBn
  , (~<<)
  , mbn
  , fMB
  , bn_
  , (>~.)
  , fb_
  , (+>)
  , (<+)
  , jn
  , fjn
  , (>#<)
  , jj
  , fjj
  , mjn
  , mJn
  , mmj
  , mmJ
  , m3j
  , m3J
  , mM
  , fmM
  , m_
  , fm_
  , vl
  , vL
  , vr
  , vR
  , itM
  , ftM
  , iyM
  , fyM
  , (>~@)
  , fbM
  , fBM
  -- * Deprecated
  , join
  , mapM
  , mapM_
  , liftM
  , liftM2
  , return
  , iterateM
  )
  where


import qualified Prelude
import Prelude
  ( Monad
  )


import qualified Control.Monad


import P.Aliases
import P.Alternative
import P.Applicative
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Traversable


infixl 1 >~, >~., >~@, <~
infixr 1 ~<, ~~<, ~<<, +>, <+
infixr 9 >#<


{-# Deprecated join "Use jn instead" #-}
-- | Long version of 'jn'.
join ::
  ( Monad m
  )
    => m (m a) -> m a
join =
  jn


-- |
-- Equivalent to 'Control.Monad.join'.
-- More general version of 'Data.List.concat'.
jn ::
  ( Monad m
  )
    => m (m a) -> m a
jn =
  Control.Monad.join


-- | Flip of 'jn'.
fjn ::
  (
  )
    => a -> (a -> a -> b) -> b
fjn =
  F jn


-- | Infix of 'jn'.
(>#<) ::
  ( Monad m
  )
    => m (m a) -> m a
(>#<) =
  jn


-- | Double join.
-- Applies 'jn' twice.
--
-- ==== __Examples__
--
-- Can be used to concatenate triply nested lists:
--
-- >>> jj [[[1,2,3],[4,5,6]],[[1,2,3],[5],[6,7]]]
-- [1,2,3,4,5,6,1,2,3,5,6,7]
jj ::
  ( Monad m
  )
    => m (m (m a)) -> m a
jj =
  jn < jn


-- | Flip of 'jj'.
fjj ::
  (
  )
    => b -> (b -> b -> b -> c) -> c
fjj =
  F jj


-- | Join the second two layers in a stack.
-- The same as mapping 'jn' over a functor.
--
-- This is useful for dealing with the arguments of functions:
--
-- @
-- mjn :: (a -> b -> b -> c) -> (a -> b -> c)
-- @
--
mjn ::
  ( Monad m
  , Functor f
  )
    => f (m (m a)) -> f (m a)
mjn =
  m jn


-- | Flip of 'mjn'.
--
-- @
-- mJn :: a -> (a -> b -> b -> c) -> b -> c
-- @
mJn ::
  ( Monad m
  )
    => a -> (a -> m (m b)) -> m b
mJn =
  F mjn


-- | Join the third and fourth layers in a stack.
-- The same as mapping 'jn' two layers deep, or mapping 'mjn'.
--
-- This is useful for dealing with the arguments of functions:
--
-- @
-- mjn :: (a -> b -> c -> c -> d) -> (a -> b -> c -> d)
-- @
--
mmj ::
  ( Monad m
  , Functor f
  , Functor g
  )
    => f (g (m (m a))) -> f (g (m a))
mmj =
  mm jn


-- | Flip of 'mmj'.
--
-- @
-- mmJ :: a -> (a -> b -> c -> c -> d) -> b -> c -> d
-- @
mmJ ::
  ( Monad m
  , Functor f
  )
    => a -> (a -> f (m (m b))) -> f (m b)
mmJ =
  F mmj


-- | Join the fourth and fifth layers in a stack.
-- The same as mapping 'jn' three layers deep or mapping 'mmj'.
--
-- This is useful for dealing with the arguments of functions:
--
-- @
-- mjn :: (a -> b -> c -> d -> d -> e) -> (a -> b -> c -> d -> e)
-- @
--
m3j ::
  ( Monad m
  , Functor f
  , Functor g
  , Functor h
  )
    => f (g (h (m (m a)))) -> f (g (h (m a)))
m3j =
  m mmj


-- | Flip of 'm3j'.
--
-- @
-- mmJ :: a -> (a -> b -> c -> d -> d -> e) -> b -> c -> d -> e
-- @
m3J ::
  ( Monad m
  , Functor f
  , Functor g
  )
    => a -> (a -> f (g (m (m b)))) -> f (g (m b))
m3J =
  F m3j


-- | Monadic bind.
-- Equivalent to '(Prelude.>>=)'.
-- More general version of 'Prelude.concatMap'.
bn ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m b
bn =
  (Prelude.>>=)


-- | Monadic bind.
--
-- Equivalent to '(Prelude.>>=)'.
-- Infix version of 'bn'.
(>~) ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m b
(>~) =
  (Prelude.>>=)


-- | Flip of 'bn'.
--
-- Equivalent to '(Control.Monad.=<<)'
fb ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m b
fb =
  F bn


-- | Monadic bind.
--
-- Flip of '(>~)'
-- Equivalent to '(Contro.Monad.=<<)'.
-- Infix version of 'fb'.
(~<) ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m b
(~<) =
  (Control.Monad.=<<)


-- | Prefix of '(~<<)'.
mbn ::
  ( Monad m
  , Functor f
  )
    => (a -> m b) -> f (m a) -> f (m b)
mbn =
  (~<<)


-- | Flip of 'mbn'.
fMB ::
  ( Monad m
  , Functor f
  )
    => f (m a) -> (a -> m b) -> f (m b)
fMB =
  F mbn


-- | A monadic bind returning the result of the left hand.
--
-- ==== __Examples__
--
-- Replace each element of a list with that many of itself in place:
--
-- >>> f=lfb e1
-- >>> f[1,2,3]
-- [1,2,2,3,3,3]
-- >>> f[2,0,-3]
-- [2,2]
--
-- Parse some string followed by some number of characters from that string.
--
-- >>> f=gk $ my' hd <~ (my < xys)
-- >>> f"senselessness"
-- "sensel"
-- >>> f"banana"
-- "ban"
-- >>> f"nationalisation"
-- "nationalis"
--
lbn ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m a
lbn =
  (<~)


-- | Infix of 'lbn'.
(<~) ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m a
(<~) =
  f' lfb


-- | Flip of 'lbn'.
lfb ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m a
lfb =
  fb < ap pM


-- | Mapped bind.
-- A very strange sort of composition.
--
-- When both functors are functions this has the signature:
-- @
-- (a -> c -> b) -> (d -> c -> a) -> (d -> c -> b)
-- @
(~<<) ::
  ( Monad m
  , Functor f
  )
    => (a -> m b) -> f (m a) -> f (m b)
(~<<) =
  m m (~<)


-- | A version of '(~<)' which binds twice.
--
-- Can be used for complex function composition.
-- When @m@ is a function this has type:
--
-- @
-- (a -> c -> b) -> (c -> c -> a) -> (c -> b)
-- @
--
-- ==== __Examples__
--
-- Determine if a permutation has any fixed points:
--
-- >>> or<<zW eq~~<im$[0,1,3,2]
-- True
-- >>> or<<zW eq~~<im$[2,3,0,1]
-- False
--
(~~<) ::
  ( Monad m
  )
    => (a -> m b) -> m (m a) -> m b
(~~<) =
  jj << (<<)


-- | Prefix of '(~~<)'.
bbn ::
  ( Monad m
  )
    => (a -> m b) -> m (m a) -> m b
bbn =
  (~~<)


-- | Flip of 'bbn'.
fBn ::
  ( Monad m
  )
    => m (m a) -> (a -> m b) -> m b
fBn =
  F bbn


-- | Prefix of '(>~.)'.
bn_ ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m a
bn_ =
  F fb_


-- | A monadic bind which ignores the result of the second monad and preserves the result of the first.
--
-- Infix of 'bn_'.
--
-- ==== __Examples__
--
-- Used on a parser to create a parser which matches @a{n}b{n}c{n}@ giving back the @n@:
--
-- >>> (lSo ʃa >~. fXy ʃb >~. fXy ʃc) -% "aaaaabbbbbccccc"
-- [5]
--
(>~.) ::
  ( Monad m
  )
    => m a -> (a -> m b) -> m a
monad >~. func =
  monad >~ ( \ x -> func x $> x)


-- | Flip of 'bn_'.
fb_ ::
  ( Monad m
  )
    => (a -> m b) -> m a -> m a
fb_ =
  fb < fb ($>)


{-# Deprecated mapM "Use mM instead" #-}
-- | Long version of 'mM'.
mapM ::
  ( Monad m
  , Traversable t
  )
    => (a -> m b) -> t a -> m (t b)
mapM =
  mM


-- |
-- Equivalent to 'Prelude.mapM'.
mM ::
  ( Monad m
  , Traversable t
  )
    => (a -> m b) -> t a -> m (t b)
mM =
  Prelude.mapM


-- | Flip of 'mM'.
-- Equivalent to 'Prelude.forM'.
fmM ::
  ( Monad m
  , Traversable t
  )
    => t a -> (a -> m b) -> m (t b)
fmM =
  F mM


{-# Deprecated mapM_ "Use m_ instead" #-}
-- | Long version of 'm_'.
mapM_ ::
  ( Monad m
  , Traversable t
  )
    => (a -> m b) -> t a -> m ()
mapM_ =
  m_


-- | 'mM' ignoring the return value of the monad.
--
-- Equivalent to 'Prelude.mapM_'.
m_ ::
  ( Monad m
  , Foldable t
  )
    => (a -> m b) -> t a -> m ()
m_ =
  Prelude.mapM_


-- | Flip of 'm_'.
-- Equivalent to 'Prelude.forM_'.
fm_ ::
  ( Monad m
  , Foldable t
  )
    => t a -> (a -> m b) -> m ()
fm_ =
  F m_


-- | Kleisli composition.
--
-- Equivalent to '(Control.Monad.>=>)'.
-- Flip of '(<+)'.
(+>) ::
  ( Monad m
  )
     => (a -> m b) -> (b -> m c) -> a -> m c
(+>) =
   (Control.Monad.>=>)


-- | Kleisli composition.
--
-- Equivalent to '(Control.Monad.>=>)'.
-- Flip of '(+>)'.
(<+) ::
  ( Monad m
  )
     => (b -> m c) -> (a -> m b) -> a -> m c
(<+) =
   (Control.Monad.<=<)


-- | Sequences a kleisli arrow with a monad to make a kleisli arrow.
-- Returns the value of the monad.
--
-- Flip of 'vR'.
vr ::
  ( Monad m
  , Functor f
  )
    => f (m b) -> m c -> f (m c)
vr =
  F vR


-- | Flip of 'vr'.
vR ::
  ( Monad m
  , Functor f
  )
    => m c -> f (m b) -> f (m c)
vR =
  m' faT


-- | Sequences a monad with a kleisli arrow to make a kleisli arrow.
-- Returns the value of the kleisli arrow.
--
-- Flip of 'vl'.
vl ::
  ( Monad m
  , Functor f
  )
    => m c -> f (m b) -> f (m b)
vl =
  m' faK


-- | Flip of 'vL'.
vL ::
  ( Monad m
  , Functor f
  )
    => f (m b) -> m c -> f (m b)
vL =
  F vl


-- | A version of 'ix' on Kleisli arrows.
-- Iterates indefinitely with a monadic action.
itM ::
  ( Monad m
  )
    => (a -> m a) -> a -> m (List a)
itM x v =
  x v >~ ((v :) << itM x)


-- | Flip of 'itM'.
ftM ::
  ( Monad m
  )
    => a -> (a -> m a) -> m (List a)
ftM =
  f' itM


-- | Like 'itM' but it takes its starting value from a monad.
iyM ::
  ( Monad m
  )
    => (a -> m a) -> m a -> m (List a)
iyM =
  f' fyM


-- | Flip of 'iyM'.
fyM ::
  ( Monad m
  )
    => m a -> (a -> m a) -> m (List a)
fyM =
  (>~@)


-- | Infix of 'iyM' and 'fyM'.
(>~@) ::
  ( Monad m
  )
    => m a -> (a -> m a) -> m (List a)
m >~@ f =
  m >~ itM f


-- | Feed a function a value and map the result over that value.
--
-- ===== __Examples__
--
-- Get the number of times each element of a list appears in that list.
-- >>> fbM fce [1,3,1,2,2,4,2,2]
-- [2,1,2,4,4,1,4,4]
fbM ::
  ( Functor f
  )
    => (f a -> a -> b) -> f a -> f b
fbM =
  fb m


-- | Flip of 'fbM'
fBM ::
  ( Functor f
  )
    => f a -> (f a -> a -> b) -> f b
fBM =
  f' fbM


{-# Deprecated iterateM "Use itM instead" #-}
iterateM ::
  ( Monad m
  )
    => (a -> m a) -> a -> m (List a)
iterateM =
  itM


{-# Deprecated liftM "Use m or (<) instead" #-}
-- | Map a function over an arbitrary monad.
-- Long version of 'P.Function.Compose.m' and '(P.Function.Compose.<)'
liftM ::
  ( Monad m
  )
    => (a -> b) -> m a -> m b
liftM =
  Prelude.fmap


{-# Deprecated liftM2 "Use l2 or (**) instead" #-}
-- | Lift a binary function to a function on monads.
-- Long version of 'P.Applicative.l2' and '(P.Applicative.**)'
liftM2 ::
  ( Monad m
  )
    => (a -> b -> c) -> m a -> m b -> m c
liftM2 =
  l2


{-# Deprecated return "Use p or Pu instead" #-}
-- | Long version of 'p' or 'P.Functor.Identity.Pu'.
return ::
  ( Monad m
  )
    => a -> m a
return =
  p
