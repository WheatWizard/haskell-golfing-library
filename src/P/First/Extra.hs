{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.First.Extra
Description :
  Additional functions for the P.First library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.First library.
-}
module P.First.Extra
  (
  -- * Takes
    tk1
  , tk2
  , tk3
  , tk4
  , tk5
  , tk6
  , tk7
  , tk8
  , tk9
  , tk0
  -- * Drops
  , dr1
  , dr2
  , dr3
  , dr4
  , dr5
  , dr6
  , dr7
  , dr8
  , dr9
  , dr0
  -- * Cons
  , pattern Qi
  , pattern K0
  , pattern K1
  , pattern K2
  , pattern K3
  , pattern K4
  , pattern K5
  , pattern K6
  , pattern K7
  , pattern K8
  , pattern K9
  , pattern Ka
  , pattern Kb
  , pattern Kc
  , pattern Kd
  , pattern Ke
  , pattern Kf
  , pattern Kg
  , pattern Kh
  , pattern Ki
  , pattern Kj
  , pattern Kk
  , pattern Kl
  , pattern Km
  , pattern Kn
  , pattern Ko
  , pattern Kp
  , pattern Kq
  , pattern Kr
  , pattern Ks
  , pattern Kt
  , pattern Ku
  , pattern Kv
  , pattern Kw
  , pattern Kx
  , pattern Ky
  , pattern Kz
  , pattern KA
  , pattern KB
  , pattern KC
  , pattern KD
  , pattern KE
  , pattern KF
  , pattern KG
  , pattern KH
  , pattern KI
  , pattern KJ
  , pattern KK
  , pattern KL
  , pattern KM
  , pattern KN
  , pattern KO
  , pattern KP
  , pattern KQ
  , pattern KR
  , pattern KS
  , pattern KT
  , pattern KU
  , pattern KV
  , pattern KW
  , pattern KX
  , pattern KY
  , pattern KZ
  , pattern K_
  , pattern K'
  )
  where


import Prelude
  ( Int
  )
import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Ring
import P.Aliases
import P.Bool
import P.Char
import P.Eq
import P.First
import P.Function.Compose
import P.Reverse


-- | Takes 1 element from a list.
tk1 ::
  ( Firstable f g
  )
    => f a -> List a
tk1 (K x _) =
  [ x ]
tk1 _ =
  []


-- | Takes 2 elements from a list.
tk2 ::
  ( Firstable f g
  , Firstable g h
  )
    => f a -> List a
tk2 (K x xs) =
  x : tk1 xs
tk2 _ =
  []


-- | Takes 3 elements from a list.
tk3 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  )
    => f a -> List a
tk3 (K x xs) =
  x : tk2 xs
tk3 _ =
  []


-- | Takes 4 elements from a list.
tk4 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  )
    => f a -> List a
tk4 (K x xs) =
  x : tk3 xs
tk4 _ =
  []


-- | Takes 5 elements from a list.
tk5 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  )
    => f a -> List a
tk5 (K x xs) =
  x : tk4 xs
tk5 _ =
  []


-- | Takes 6 elements from a list.
tk6 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  )
    => f a -> List a
tk6 (K x xs) =
  x : tk5 xs
tk6 _ =
  []


-- | Takes 7 elements from a list.
tk7 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  )
    => f a -> List a
tk7 (K x xs) =
  x : tk6 xs
tk7 _ =
  []


-- | Takes 8 elements from a list.
tk8 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  )
    => f a -> List a
tk8 (K x xs) =
  x : tk7 xs
tk8 _ =
  []


-- | Takes 9 elements from a list.
tk9 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  , Firstable f8 f9
  )
    => f a -> List a
tk9 (K x xs) =
  x : tk8 xs
tk9 _ =
  []

-- | Takes 10 elements from a list.
tk0 ::
  ( Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  , Firstable f8 f9
  , Firstable f9 f10
  )
    => f a -> List a
tk0 (K x xs) =
  x : tk9 xs
tk0 _ =
  []


-- | Drops the first element from a list.
dr1 ::
  ( Firstable f f
  )
    => f a -> f a
dr1 =
  dr (1 :: Int)


-- | Drops the first 2 elements from a list.
-- Shorthand for @'dr' 2@.
dr2 ::
  ( Firstable f f
  )
    => f a -> f a
dr2 =
  dr (2 :: Int)


-- | Drops the first 3 elements from a list.
-- Shorthand for @'dr' 3@.
dr3 ::
  ( Firstable f f
  )
    => f a -> f a
dr3 =
  dr (3 :: Int)


-- | Drops the first 4 elements from a list.
-- Shorthand for @'dr' 4@.
dr4 ::
  ( Firstable f f
  )
    => f a -> f a
dr4 =
  dr (4 :: Int)


-- | Drops the first 5 elements from a list.
-- Shorthand for @'dr' 5@.
dr5 ::
  ( Firstable f f
  )
    => f a -> f a
dr5 =
  dr (5 :: Int)


-- | Drops the first 6 elements from a list.
-- Shorthand for @'dr' 6@.
dr6 ::
  ( Firstable f f
  )
    => f a -> f a
dr6 =
  dr (6 :: Int)


-- | Drops the first 7 elements from a list.
-- Shorthand for @'dr' 7@.
dr7 ::
  ( Firstable f f
  )
    => f a -> f a
dr7 =
  dr (7 :: Int)


-- | Drops the first 8 elements from a list.
-- Shorthand for @'dr' 8@.
dr8 ::
  ( Firstable f f
  )
    => f a -> f a
dr8 =
  dr (8 :: Int)

-- | Drops the first 9 elements from a list.
-- Shorthand for @'dr' 9@.
dr9 ::
  ( Firstable f f
  )
    => f a -> f a
dr9 =
  dr (9 :: Int)


-- | Drops the first 10 elements from a list.
-- To drop the first element of a list see 'P.Function.Identity.id'.
dr0 ::
  ( Firstable f f
  )
    => f a -> f a
dr0 =
  dr (10 :: Int)


-- | Gets the last 1 element from a list.
yv1 ::
  ( Reversable f
  , Firstable f g
  )
    => f a -> List a
yv1 =
  Rv < tk1 < Rv


-- | Gets the last 2 elements from a list.
yv2 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  )
    => f a -> List a
yv2 =
  Rv < tk2 < Rv


-- | Gets the last 3 elements from a list.
yv3 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  )
    => f a -> List a
yv3 =
  Rv < tk3 < Rv


-- | Gets the last 4 elements from a list.
yv4 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  )
    => f a -> List a
yv4 =
  Rv < tk4 < Rv


-- | Gets the last 5 elements from a list.
yv5 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  )
    => f a -> List a
yv5 =
  Rv < tk5 < Rv


-- | Gets the last 6 elements from a list.
yv6 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  )
    => f a -> List a
yv6 =
  Rv < tk6 < Rv


-- | Gets the last 7 elements from a list.
yv7 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  )
    => f a -> List a
yv7 =
  Rv < tk7 < Rv


-- | Gets the last 8 elements from a list.
yv8 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  )
    => f a -> List a
yv8 =
  Rv < tk8 < Rv


-- | Gets the last 9 elements from a list.
yv9 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  , Firstable f8 f9
  )
    => f a -> List a
yv9 =
  Rv < tk9 < Rv


-- | Gets the last 10 elements from a list.
yv0 ::
  ( Reversable f
  , Firstable f f1
  , Firstable f1 f2
  , Firstable f2 f3
  , Firstable f3 f4
  , Firstable f4 f5
  , Firstable f5 f6
  , Firstable f6 f7
  , Firstable f7 f8
  , Firstable f8 f9
  , Firstable f9 f10
  )
    => f a -> List a
yv0 =
  Rv < tk0 < Rv


-- | Add the monoid identity to the front of a list
pattern Qi ::
  ( Monoid m
  , Eq m
  )
    => List m -> List m
pattern Qi xs <- (K ((== i) -> T) xs) where
  Qi =
    K i


-- | Add 0 to the front of a list.
pattern K0 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K0 xs = K 0 xs


-- | Add 1 to the front of a list.
pattern K1 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K1 xs = K 1 xs


-- | Add 2 to the front of a list.
pattern K2 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K2 xs = K 2 xs


-- | Add 3 to the front of a list.
pattern K3 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K3 xs = K 3 xs


-- | Add 4 to the front of a list.
pattern K4 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K4 xs = K 4 xs


-- | Add 5 to the front of a list.
pattern K5 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K5 xs = K 5 xs


-- | Add 6 to the front of a list.
pattern K6 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K6 xs = K 6 xs


-- | Add 7 to the front of a list.
pattern K7 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K7 xs = K 7 xs


-- | Add 8 to the front of a list.
pattern K8 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K8 xs = K 8 xs


-- | Add 9 to the front of a list.
pattern K9 ::
  ( Ring a
  , Eq a
  )
    => List a -> List a
pattern K9 xs = K 9 xs


-- | Add a @A@ to the front of a string.
pattern KA ::
  (
  )
    => List Char -> List Char
pattern KA xs =
  K 'A' xs


-- | Add a @B@ to the front of a string.
pattern KB ::
  (
  )
    => List Char -> List Char
pattern KB xs =
  K 'B' xs


-- | Add a @C@ to the front of a string.
pattern KC ::
  (
  )
    => List Char -> List Char
pattern KC xs =
  K 'C' xs


-- | Add a @D@ to the front of a string.
pattern KD ::
  (
  )
    => List Char -> List Char
pattern KD xs =
  K 'D' xs


-- | Add a @E@ to the front of a string.
pattern KE ::
  (
  )
    => List Char -> List Char
pattern KE xs =
  K 'E' xs


-- | Add a @F@ to the front of a string.
pattern KF ::
  (
  )
    => List Char -> List Char
pattern KF xs =
  K 'F' xs


-- | Add a @G@ to the front of a string.
pattern KG ::
  (
  )
    => List Char -> List Char
pattern KG xs =
  K 'G' xs


-- | Add a @H@ to the front of a string.
pattern KH ::
  (
  )
    => List Char -> List Char
pattern KH xs =
  K 'H' xs


-- | Add a @I@ to the front of a string.
pattern KI ::
  (
  )
    => List Char -> List Char
pattern KI xs =
  K 'I' xs


-- | Add a @J@ to the front of a string.
pattern KJ ::
  (
  )
    => List Char -> List Char
pattern KJ xs =
  K 'J' xs


-- | Add a @K@ to the front of a string.
pattern KK ::
  (
  )
    => List Char -> List Char
pattern KK xs =
  K 'K' xs


-- | Add a @L@ to the front of a string.
pattern KL ::
  (
  )
    => List Char -> List Char
pattern KL xs =
  K 'L' xs


-- | Add a @M@ to the front of a string.
pattern KM ::
  (
  )
    => List Char -> List Char
pattern KM xs =
  K 'M' xs


-- | Add a @N@ to the front of a string.
pattern KN ::
  (
  )
    => List Char -> List Char
pattern KN xs =
  K 'N' xs


-- | Add a @O@ to the front of a string.
pattern KO ::
  (
  )
    => List Char -> List Char
pattern KO xs =
  K 'O' xs


-- | Add a @P@ to the front of a string.
pattern KP ::
  (
  )
    => List Char -> List Char
pattern KP xs =
  K 'P' xs


-- | Add a @Q@ to the front of a string.
pattern KQ ::
  (
  )
    => List Char -> List Char
pattern KQ xs =
  K 'Q' xs


-- | Add a @R@ to the front of a string.
pattern KR ::
  (
  )
    => List Char -> List Char
pattern KR xs =
  K 'R' xs


-- | Add a @S@ to the front of a string.
pattern KS ::
  (
  )
    => List Char -> List Char
pattern KS xs =
  K 'S' xs


-- | Add a @T@ to the front of a string.
pattern KT ::
  (
  )
    => List Char -> List Char
pattern KT xs =
  K 'T' xs


-- | Add a @U@ to the front of a string.
pattern KU ::
  (
  )
    => List Char -> List Char
pattern KU xs =
  K 'U' xs


-- | Add a @V@ to the front of a string.
pattern KV ::
  (
  )
    => List Char -> List Char
pattern KV xs =
  K 'V' xs


-- | Add a @W@ to the front of a string.
pattern KW ::
  (
  )
    => List Char -> List Char
pattern KW xs =
  K 'W' xs


-- | Add a @X@ to the front of a string.
pattern KX ::
  (
  )
    => List Char -> List Char
pattern KX xs =
  K 'X' xs


-- | Add a @Y@ to the front of a string.
pattern KY ::
  (
  )
    => List Char -> List Char
pattern KY xs =
  K 'Y' xs


-- | Add a @Z@ to the front of a string.
pattern KZ ::
  (
  )
    => List Char -> List Char
pattern KZ xs =
  K 'Z' xs


-- | Add a @a@ to the front of a string.
pattern Ka ::
  (
  )
    => List Char -> List Char
pattern Ka xs =
  K 'a' xs


-- | Add a @b@ to the front of a string.
pattern Kb ::
  (
  )
    => List Char -> List Char
pattern Kb xs =
  K 'b' xs


-- | Add a @c@ to the front of a string.
pattern Kc ::
  (
  )
    => List Char -> List Char
pattern Kc xs =
  K 'c' xs


-- | Add a @d@ to the front of a string.
pattern Kd ::
  (
  )
    => List Char -> List Char
pattern Kd xs =
  K 'd' xs


-- | Add a @e@ to the front of a string.
pattern Ke ::
  (
  )
    => List Char -> List Char
pattern Ke xs =
  K 'e' xs


-- | Add a @f@ to the front of a string.
pattern Kf ::
  (
  )
    => List Char -> List Char
pattern Kf xs =
  K 'f' xs


-- | Add a @g@ to the front of a string.
pattern Kg ::
  (
  )
    => List Char -> List Char
pattern Kg xs =
  K 'g' xs


-- | Add a @h@ to the front of a string.
pattern Kh ::
  (
  )
    => List Char -> List Char
pattern Kh xs =
  K 'h' xs


-- | Add a @i@ to the front of a string.
pattern Ki ::
  (
  )
    => List Char -> List Char
pattern Ki xs =
  K 'i' xs


-- | Add a @j@ to the front of a string.
pattern Kj ::
  (
  )
    => List Char -> List Char
pattern Kj xs =
  K 'j' xs


-- | Add a @k@ to the front of a string.
pattern Kk ::
  (
  )
    => List Char -> List Char
pattern Kk xs =
  K 'k' xs


-- | Add a @l@ to the front of a string.
pattern Kl ::
  (
  )
    => List Char -> List Char
pattern Kl xs =
  K 'l' xs


-- | Add a @m@ to the front of a string.
pattern Km ::
  (
  )
    => List Char -> List Char
pattern Km xs =
  K 'm' xs


-- | Add a @n@ to the front of a string.
pattern Kn ::
  (
  )
    => List Char -> List Char
pattern Kn xs =
  K 'n' xs


-- | Add a @o@ to the front of a string.
pattern Ko ::
  (
  )
    => List Char -> List Char
pattern Ko xs =
  K 'o' xs


-- | Add a @p@ to the front of a string.
pattern Kp ::
  (
  )
    => List Char -> List Char
pattern Kp xs =
  K 'p' xs


-- | Add a @q@ to the front of a string.
pattern Kq ::
  (
  )
    => List Char -> List Char
pattern Kq xs =
  K 'q' xs


-- | Add a @r@ to the front of a string.
pattern Kr ::
  (
  )
    => List Char -> List Char
pattern Kr xs =
  K 'r' xs


-- | Add a @s@ to the front of a string.
pattern Ks ::
  (
  )
    => List Char -> List Char
pattern Ks xs =
  K 's' xs


-- | Add a @t@ to the front of a string.
pattern Kt ::
  (
  )
    => List Char -> List Char
pattern Kt xs =
  K 't' xs


-- | Add a @u@ to the front of a string.
pattern Ku ::
  (
  )
    => List Char -> List Char
pattern Ku xs =
  K 'u' xs


-- | Add a @v@ to the front of a string.
pattern Kv ::
  (
  )
    => List Char -> List Char
pattern Kv xs =
  K 'v' xs


-- | Add a @w@ to the front of a string.
pattern Kw ::
  (
  )
    => List Char -> List Char
pattern Kw xs =
  K 'w' xs


-- | Add a @x@ to the front of a string.
pattern Kx ::
  (
  )
    => List Char -> List Char
pattern Kx xs =
  K 'x' xs


-- | Add a @y@ to the front of a string.
pattern Ky ::
  (
  )
    => List Char -> List Char
pattern Ky xs =
  K 'y' xs


-- | Add a @z@ to the front of a string.
pattern Kz ::
  (
  )
    => List Char -> List Char
pattern Kz xs =
  K 'z' xs


-- | Add a @_@ to the front of a string.
pattern K_ ::
  (
  )
    => List Char -> List Char
pattern K_ xs =
  K '_' xs


-- | Add a @'@ to the front of a string.
pattern K' ::
  (
  )
    => List Char -> List Char
pattern K' xs =
  K '\'' xs

