{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language QuantifiedConstraints #-}
{-# Language UndecidableInstances #-}
{-# Language FlexibleContexts #-}
{-# Language TypeOperators #-}
module P.Assoc
  ( Assoc (..)
  , pattern Asx
  , pattern Usx
  )
  where


import Prelude
  (
  )


import P.Bifunctor
import P.Bifunctor.Flip
import P.Category.Iso
import P.Function.Compose


-- |
--
-- prop> asx < usx = id
class Assoc p where
  {-# Minimal assoc, unassoc | asI #-}
  -- | Associate function used to define instances of the 'Assoc' class.
  -- Use 'Asx' instead.
  assoc :: p a (p b c) -> p (p a b) c
  assoc =
    fwd asI

  -- | Associate function used to define instances of the 'Assoc' class.
  -- Use 'Usx' instead.
  unassoc :: p (p a b) c -> p a (p b c)
  unassoc =
    bwd asI

  -- | 'assoc' and 'unassoc' as a single isomorphism.
  asI :: p a (p b c) <-> p (p a b) c
  asI =
    Iso assoc unassoc

{-# Deprecated assoc "Use Asx instead" #-}
{-# Deprecated unassoc "Use Usx instead" #-}


{-# Complete Asx, (,)#-}
-- | Associate a bifunctor to the left.
pattern Asx ::
  ( Assoc p
  )
    => p a (p b c) -> p (p a b) c
pattern Asx x <- (unassoc -> x) where
  Asx =
    assoc


{-# Complete Usx, (,)#-}
-- | Associate a bifunctor to the right.
pattern Usx ::
  ( Assoc p
  )
    => p (p a b) c -> p a (p b c)
pattern Usx x <- (assoc -> x) where
  Usx =
    unassoc


instance Assoc (,) where
  assoc (x, (y, z)) =
    ((x, y), z)
  unassoc ((x, y), z) =
    (x, (y, z))


instance
  ( Assoc p
  , forall a . Functor (p a)
  , forall a . Functor (Flip p a)
  )
    => Assoc (Flip p)
  where
    assoc =
      Flp < m Flp < Usx < mst UFl < UFl
    unassoc =
      Flp < mst Flp < Asx < m UFl < UFl
