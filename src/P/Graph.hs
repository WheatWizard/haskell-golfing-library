{-# Language FlexibleInstances #-}
{-# Language ScopedTypeVariables #-}
{-|
Module :
  P.Graph
Description :
  Graphs and graph theory
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Graph
  ( Graph
  )
  where


import Prelude
  ( Functor
    ( fmap
    )
  , Semigroup
  , mempty
  , Integral
  , (+)
  )


import Data.Foldable
import qualified GHC.Ix


import P.Algebra.Monoid
import P.Aliases
import P.Algebra.Monoid.Free.Split
import P.Alternative
import P.Array
import P.Bifunctor.Flip
import P.Category
import P.Foldable
  ( tL
  )
import P.Foldable.Length
  ( l
  )
import P.Ix
import P.Monad
import P.Ord


-- | The most general graph type.
data Graph i e v =
  Graph
    { _vertexLabels ::
      Array i v
    , _edgeLabels ::
      Array (i, i) e
    }


instance
  (
  )
    => Functor (Graph i e)
  where
    fmap f (Graph vL eL) =
      Graph (fmap f vL) eL


instance
  (
  )
    => Functor (Flip (Graph i) v)
  where
    fmap f (Flp (Graph vL eL)) =
      Flp $ Graph vL $ fmap f eL


instance
  (
  )
    => Foldable (Graph i e)
  where
    fold (Graph vL _) =
      fold vL
    foldMap f (Graph vL _) =
      foldMap f vL
    foldMap' f (Graph vL _) =
      foldMap' f vL
    foldr f x (Graph vL _) =
      foldr f x vL
    foldr' f x (Graph vL _) =
      foldr' f x vL
    foldl f x (Graph vL _) =
      foldl f x vL
    foldl' f x (Graph vL _) =
      foldl' f x vL
    foldr1 f (Graph vL _) =
      foldr1 f vL
    foldl1 f (Graph vL _) =
      foldl1 f vL
    toList (Graph vL _) =
      toList vL
    null (Graph vL _) =
      null vL
    length (Graph vL _) =
      length vL
    elem x (Graph vL _) =
      elem x vL
    maximum (Graph vL _) =
      maximum vL
    minimum (Graph vL _) =
      minimum vL
    sum (Graph vL _) =
      sum vL
    product (Graph vL _) =
      product vL


instance
  ( Ix i
  , Integral i
  , Monoid e
  )
    => Semigroup (Graph i e v)
  where
    Graph vL1 eL1 <> Graph vL2 eL2 =
      Graph
        { _vertexLabels =
          vL1 <> vL2
        , _edgeLabels =
          lAr ((0, 0), (len1 + len2, len1 + len2)) (part1 <> part2)
        }
      where
        len1 :: i
        len1 =
          l vL1
        len2 :: i
        len2 =
          l vL2
        part1 :: List e
        part1 =
          cxS len1 (tL eL1) >~ (<> rl len2 i)
        part2 :: List e
        part2 =
          cxS len2 (tL eL2) >~ (rl len1 i <>)


instance
  ( Ix i
  , Integral i
  , Monoid e
  )
    => Monoid (Graph i e v)
  where
    mempty =
      Graph
        { _vertexLabels =
          i
        , _edgeLabels =
          lAr ((0,0), (0,0)) []
        }
