{-# Language RankNTypes #-}
{-# Language TypeOperators #-}
{-|
Module :
  P.Category.Iso.Polymorphic
Description :
  Polymorphism variants of iso functions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Certain functions in P.Category.Iso have no most general type.
This module exports some variants of those functions, generally using RankNTypes.
-}
module P.Category.Iso.Polymorphic
  (
  -- * p polymorphism
    pwc
  , fpc
  , pwC
  , pWc
  , fPW
  , pWC
  -- * q polymorphism
  , qwc
  , qWc
  , fQW
  , qWC
  )
  where


import qualified Prelude

import P.Category
import P.Category.Iso
import P.Function.Compose
import P.Function.Flip
import P.Swap


-- | Variant of 'swc'.
pwc ::
  ( Semigroupoid p
  )
    => (forall x . Iso p (f x) (g x)) -> p (g a) (g b) -> p (f a) (f b)
pwc iso1 =
  fwd (pwC iso1)


-- | Flip of 'pwc'.
fpc ::
  ( Semigroupoid p
  )
    => p (g a) (g b) -> (forall x . Iso p (f x) (g x)) -> p (f a) (f b)
fpc g iso1 =
  pwc iso1 g


-- | Variant of 'swC'.
pwC ::
  ( Semigroupoid p
  )
    => (forall x . Iso p (f x) (g x)) -> (p (g a) (g b) <-> p (f a) (f b))
pwC iso1 =
  Iso
    { fwd =
      \ g -> bwd iso1 <@ g <@ fwd iso1
    , bwd =
      \ g -> fwd iso1 <@ g <@ bwd iso1
    }


-- | Variant of 'sWc'.
pWc ::
  ( Semigroupoid p
  , Swap p
  )
    => (forall x . p (f x) (g x)) -> p (g a) (g b) -> p (f a) (f b)
pWc iso1 iso2 =
  Sw iso1 <@ iso2 <@ iso1


-- | Flip of 'pWc'.
fPW ::
  ( Semigroupoid p
  , Swap p
  )
    => p (g a) (g b) -> (forall x . p (f x) (g x)) -> p (f a) (f b)
fPW iso1 iso2 =
  pWc iso2 iso1


-- | Variant of 'sWC'.
pWC ::
  ( Semigroupoid p
  , Swap p
  )
    => (forall x. p (f x) (g x)) -> (p (g a) (g b) <-> p (f a) (f b))
pWC iso1 =
  Iso
    { fwd =
      pWc iso1
    , bwd =
      pWc (Sw iso1)
    }


-- | Variant of 'swc'.
qwc ::
  ( Semigroupoid p
  )
    => (forall x y . Iso p (f x y) (g x y)) -> p (g a b) (g c d) -> p (f a b) (f c d)
qwc iso1 =
  \ g -> bwd iso1 <@ g <@ fwd iso1


-- | Variant of 'sWc'.
qWc ::
  ( Semigroupoid p
  , Swap p
  )
    => (forall x y . p (f x y) (g x y)) -> p (g a b) (g c d) -> p (f a b) (f c d)
qWc iso1 iso2 =
  Sw iso1 <@ iso2 <@ iso1


-- | Flip of 'pWc'.
fQW ::
  ( Semigroupoid p
  , Swap p
  )
    => p (g a b) (g c d) -> (forall x y . p (f x y) (g x y)) -> p (f a b) (f c d)
fQW iso1 iso2 =
  qWc iso2 iso1


-- | Variant of 'sWC'
qWC ::
  ( Semigroupoid p
  , Swap p
  )
    => (forall x y . p (f x y) (g x y)) -> p (g a b) (g c d) <-> p (f a b) (f c d)
qWC iso1 =
  Iso
    { fwd =
      qWc iso1
    , bwd =
      qWc (Sw iso1)
    }
