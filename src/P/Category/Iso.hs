{-# Language TypeOperators #-}
{-# Language FlexibleInstances #-}
{-|
Module :
  P.Category.Iso
Description :
  Isomorphisms
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Category.Iso
  ( Iso (..)
  , type (<->)
  , swc
  , (<#<)
  , fwc
  , swC
  , li
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Category
import P.Function.Compose
import P.Function.Flip


-- | Shorthand for the most common isomorphism type.
type a <-> b =
  Iso (->) a b


data Iso p a b =
  Iso
    {
    -- | The forward morphism
      fwd ::
      p a b
    -- | The backward morphism and inverse of the forward morphism.
    , bwd ::
      p b a
    }


-- | Lifts a morphism from one type to another using an 'Iso'.
--
-- This "sandwiches" the morphism between the two directions of the isomorphism.
swc ::
  ( Semigroupoid p
  )
    => Iso p a b -> p b b -> p a a
swc =
  fwd < swC


-- | Infix version of 'swc'.
(<#<) ::
  ( Semigroupoid p
  )
    => Iso p a b -> p b b -> p a a
(<#<) =
  swc


-- | Flip of 'swc'.
fwc ::
  ( Semigroupoid p
  )
    => p b b -> Iso p a b -> p a a
fwc =
  f' swc


-- | 'swc' as an 'Iso'.
swC ::
  ( Semigroupoid p
  )
    => Iso p a b -> (p b b <-> p a a)
swC iso1 =
  Iso
    { fwd =
      \ g -> bwd iso1 <@ g <@ fwd iso1
    , bwd =
      \ g -> fwd iso1 <@ g <@ bwd iso1
    }


-- | Lift an isomorphism over a functor.
--
-- Functoral maps with the isomorphism version instead.
li ::
  ( Functor f
  )
    => (a <-> b) -> (f a <-> f b)
li iso =
  Iso
    { fwd =
      m (fwd iso)
    , bwd =
      m (bwd iso)
    }


instance
  ( Semigroupoid p
  )
    => Semigroupoid (Iso p)
  where
    Iso f1 b1 <@ Iso f2 b2 =
      Iso (f1 <@ f2) (b2 <@ b1)


instance
  ( Category p
  )
    => Category (Iso p)
  where
    id =
      Iso id id


instance
  ( Semigroupoid p
  )
    => Semigroup (Iso p a a)
  where
    x <> y =
      x <@ y


instance
  ( Category p
  )
    => Monoid (Iso p a a)
  where
    mempty =
      id
