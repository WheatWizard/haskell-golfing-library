{-# Language IncoherentInstances #-}
{-# Language PatternSynonyms #-}
{-# Language ExplicitNamespaces #-}
{-|
Module :
  P
Description :
  A substitution for Prelude aimed at making programs as small as possible for code golf.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A substitution for Haskell's Prelude aimed at making programs as small as possible for code golf.

Please do not use this in real code.
-}
module P
  ( pattern (:=)
  , sh
  -- * Semigroupoids
  , Semigroupoid (..)
  , og
  , fog
  -- * Categories
  , Category (..)
  , ($)
  , fid
  , (&)
  , Endo (..)
  -- ** Isomorphisms
  , Iso (..)
  , type (<->)
  , swc
  , (<#<)
  , fwc
  , swC
  , sWc
  , (<%<)
  , fWc
  , sWC
  , pwc
  , fpc
  , pwC
  , pWc
  , fPW
  , pWC
  , qwc
  , qWc
  , fQW
  , qWC
  , li
  -- * Functions
  , yy
  , utl
  , fut
  , whl
  , fwh
  , fyy
  , yyc
  , fyc
  , ycW
  , fyW
  , ycB
  , fyB
  , yyC
  , fyC
  , myy
  , m2y
  , fbF
  , ffF
  -- ** On
  , on
  , fon
  , (.*)
  , on3
  , fO3
  -- *** Polymorphs
  , pOn
  , fpO
  , qOn
  , fqO
  , kOn
  , fkO
  , onp
  , xOn
  , fxO
  , yOn
  , fyO
  -- ** Flips
  , pattern F
  , f'
  , ff
  , (/$)
  , pattern F2
  , fF2
  , module P.Function.Flip.Extra
  -- * Booleans
  , Bool
  , pattern T
  , pattern B
  , n
  , (||)
  , nO
  , (~|)
  -- ** Conditional branching
  , iF
  , (?.)
  , fiF
  , liF
  , fli
  , lii
  , fIi
  , fiI
  , ffI
  , li2
  , fI2
  , li3
  , fI3
  -- * Swap
  , Swap (..)
  , pattern Sw
  -- * Assoc
  , Assoc (..)
  , pattern Asx
  , pattern Usx
  -- * Either
  , Either (..)
  -- ** Extract
  , fI
  , fR
  , ffR
  , fL
  , ffL
  , ei
  , il
  , iR
  , lfs
  , rts
  , pEi
  , ls'
  , rs'
  , pE'
  -- ** Pair conversion
  , e2p
  , p2e
  , pei
  , pattern EP
  , pattern (:$&)
  , pattern WEP
  , pattern (:$%)
  -- * These
  , These (..)
  -- * Maybe
  , Maybe
  -- * Tuples
  -- | Functions having to do with tuples.
  -- For an equivalent to 'Prelude.snd' use 'cr'.
  -- Among other things 'cr' returns the final element of tuples of varying sizes.
  , st
  , pattern Bp
  , pattern (:@)
  , pattern Pb
  , pattern Jbp
  -- * Currying
  , pattern Cu
  , fCu
  , pattern Cuf
  , fCf
  , pattern U
  , fUc
  , pattern Ucf
  , fUf
  , pattern Cu3
  , fC3
  , pattern U3
  , fU3
  , pattern Cu4
  , fC4
  , pattern U4
  , fU4
  , uid
  -- * Lists
  , List
  , dr
  , (#>)
  , fd
  , sA
  , (#=)
  , fsA
  , dW
  , fdW
  , sp
  , bk
  , sy
  , sl
  , sY
  , (|/)
  , sY'
  , sYe
  , fYe
  , sYE
  , fYE
  , sYn
  , fYn
  , sYN
  , fYN
  , sL
  , (|\)
  , sYm
  , sYM
  , sLm
  , sLM
  , sps
  , spn
  , spe
  , spN
  , spM
  , snM
  , seM
  , sNM
  , pm
  , pmN
  , ƪ
  , ƪN
  , pa
  , (%%)
  , fpa
  , paf
  , paF
  , pA
  , pac
  , pax
  , fpx
  , apa
  , fap
  , nap
  , fna
  , epa
  , fep
  , nep
  , fne
  , δ
  , δ'
  , paq
  , pnq
  , xQ
  , xX
  , paS
  , fPs
  , pAS
  , fPS
  , p2
  , fp2
  , pattern (:$)
  , r1
  , fr1
  , ak
  , fak
  , pattern (:%)
  , uak
  , ukw
  , fkw
  , ak'
  , rn
  , frn
  , tx
  , txi
  , txx
  , txm
  , fxm
  , xts
  , rp
  , rL1
  , rL2
  , rr1
  , rr2
  , ryl
  , ryr
  , dc
  , xdc
  , iBw
  , fBw
  , (|^>)
  , tlM
  , ftm
  , (~<~)
  , module P.List.Extra
  , module P.List.Split.Extra
  -- ** Interspersing
  , pattern Is
  , is1
  , fi1
  , is2
  , fi2
  , is3
  , fi3
  , module P.List.Intersperse.Extra
  -- ** Padding
  , lpW
  , rpW
  , lLW
  , rLW
  , lpp
  , rpp
  -- ** Grouping
  , gr
  , gW
  , fgW
  , gB
  , fgB
  , gF
  , fgF
  , gFn
  , gfn
  , gfq
  , fgq
  , gnq
  , gNq
  -- ** Chopping
  , wx
  , fwx
  , wX
  , fwX
  , module P.List.Chop.Extra
  -- ** Internal comparisons
  , lq
  , nlq
  , lqw
  , lqW
  , lqb
  , lqB
  , cδ
  , cΔ
  , uq
  , nuq
  , uqb
  , uqB
  , uqw
  , uqW
  , ulq
  , nqq
  , sMI
  , miw
  , miW
  , mnI
  , mIw
  , mIW
  , sMD
  , mdw
  , mdW
  , mnD
  , mDw
  , mDW
  , mno
  , mNo
  -- ** Indexing
  , (!)
  , (!!)
  , im
  , (>!)
  , fim
  , ri
  , fri
  , nim
  , fnm
  , module P.Foldable.Index.Extra
  -- ** Translation
  , tr
  , ftr
  , trM
  , frM
  , tI
  , ftI
  , tIM
  , fIM
  , trB
  -- ** Intersections and Differences
  -- | See also 'cX' and 'fcX' in MonadPlus.
  , nx
  , fnx
  , nxw
  , nxW
  , nxb
  , nxB
  , bI
  , fbI
  , bIw
  , bIW
  , bIb
  , bIB
  , df
  , fdf
  , dfw
  , dfW
  , dfb
  , dfB
  , bD
  , fbD
  , bDw
  , bDW
  , bDb
  , bDB
  , nxe
  , xew
  , xeb
  -- ** Prefixes
  , pxs
  , pXs
  , pxn
  , pXn
  , pxS
  , pXS
  -- *** Prefix maps
  , ƥ
  , fƥ
  , mP
  , fmP
  , mpn
  , mPn
  -- *** Miscellaneous
  , lp
  , lpw
  , lpb
  , lPw
  , lPb
  , sw
  , (%<)
  , fsw
  , nsw
  , fnw
  , swW
  , fSW
  , swB
  , fSB
  , tk
  , (#<)
  , ft
  , tW
  , ftW
  , pSt
  , fpS
  -- ** Unfolding
  , ufo
  , fuf
  , uf1
  , ff1
  , ufl
  , ffl
  , ul1
  , fl1
  , ufu
  , fFu
  , ufU
  , fFU
  , uuf
  , uUf
  , uue
  , fue
  , uUe
  , fUe
  , uef
  , uEf
  , uuz
  , fuz
  , uUz
  , fUz
  , uzf
  , uZf
  -- *** Iterate
  , ix
  , fX
  , ixu
  , ixe
  , fxe
  , ixz
  , fxz
  -- ** Suffixes
  , ms
  , fms
  , mS
  , fmS
  , sxn
  , sXn
  , msn
  , mSn
  , sxs
  , sXs
  , mss
  , mSs
  , sxS
  , sXS
  , msS
  , mSS
  , ew
  , (>%)
  , few
  , new
  , fNw
  , ewW
  , fEW
  , ewB
  , fEB
  , lx
  , lSw
  , lSb
  , yv
  , fY
  , xh
  , fxh
  , sst
  , fss
  -- ** Infixes
  , cg
  , cG
  , iw
  , (%=)
  , fiw
  , iwW
  , fIW
  , iWW
  , fWW
  , iWB
  , fIB
  , ih
  , fih
  , cSt
  , fcS
  , liS
  , fLS
  -- ** Find
  , FindAutomaton
  , mAt
  , mAW
  , afw
  , alf
  , arw
  , avw
  , aRw
  , asw
  , azw
  , fa
  , lfa
  , ra
  , rA
  , va
  , sa
  , za
  , faw
  , raw
  , rAw
  , vaw
  , saw
  , zaw
  -- * Infinite lists
  , IList
  , pattern (:..)
  -- * Fencepost lists
  , Fenceposts (..)
  , dep
  , dhp
  , ep
  , hp
  , po
  -- * Necklaces
  , Necklace (..)
  -- * Maps
  , StrictMap
  , eSm
  , asc
  -- ** Class
  , Indexable (..)
  , qx
  , fqx
  , qxd
  , fqd
  , nmr
  , fns
  , nR
  , fnR
  , iM
  , fiM
  , (!<)
  , fjt
  , ajs
  , aJs
  , ReverseIndexable (..)
  , fIx
  , fIw
  , Map (..)
  , nZ
  , fnZ
  , nzs
  , fzs
  , dks
  , fdk
  , lk
  , module P.Map.Class.Extra
  -- * Reverse
  , Reversable (..)
  , rv
  , pattern Rv
  , pattern Rrv
  , rvi
  , ReversableFunctor (..)
  -- * Expandable
  , Expandable (..)
  , fis
  , fic
  -- * First
  , Uncons (..)
  , Firstable (..)
  , pattern K
  , pattern Fk
  , (*:*)
  , module P.First.Extra
  -- * Last
  , pattern (:>)
  , gj
  , nt
  -- * Zips
  , Zip (..)
  , (=#=)
  , zwp
  , zwK
  , uz
  , z3
  , fz3
  , ź
  , fZ3
  , fzW
  , fzp
  , zWq
  , zWX
  , zWn
  , nZx
  , xZn
  , upZ
  , fpZ
  , module P.Zip.Extra
  -- ** Zip and concat
  , jzW
  , (=#?)
  , fjz
  , ozW
  , (=#*)
  , foz
  , azW
  , (=#+)
  , faZ
  -- ** WeakAlign
  , WeakAlign (..)
  , gSo
  , gMy
  , gPy
  , gSO
  , gMY
  , gPY
  , lGo
  , lGy
  -- ** SemiAlign
  , SemiAlign (..)
  , zD
  , zd
  , zd'
  , fzd
  , zdm
  , fZm
  , fZd
  , zom
  , fzo
  , zdq
  , zdx
  , zdn
  -- ** UnitalZip
  , UnitalZip (..)
  , cnF
  , cFM
  -- ** Align
  , rZd
  , lZd
  , rzd
  , lzd
  , rzm
  , lzm
  -- * Foldable
  , lF
  , flF
  , lF'
  , fLF
  , fo
  , mF
  , fmF
  , (^<)
  , mf
  , fmf
  , rF
  , frF
  , rHM
  , frm
  , lHM
  , flm
  , lhM
  , fLm
  , rH
  , frH
  , lH
  , flH
  , e
  , (?>)
  , fe
  , ne
  , (?<)
  , fE
  , tL
  , uqs
  , uQW
  , uQB
  , dpc
  , dPW
  , dpB
  , sm
  , pd
  , module P.Foldable.Extra
  -- ** Counting
  , cn
  , fcn
  , ce
  , fce
  , cne
  , fcN
  , cnT
  , cnn
  , ca
  , fca
  , cna
  , fcA
  -- *** Tallies
  , tY
  , tyB
  , tBl
  , etY
  -- *** Cumulative counts
  , cc
  , fcc
  , ccn
  , fCn
  , ccQ
  , fcQ
  , cnQ
  , fnQ
  , cca
  , fCa
  , ccA
  , fCA
  , module P.Foldable.Count.Extra
  -- ** Nubs
  , nb
  , nW
  , fnW
  , nB
  , fnB
  , nbc
  , ncW
  , fNW
  , ncB
  , fNB
  , lnb
  , lnB
  , lnW
  , xuq
  , xqW
  , xqB
  , nbT
  , nTf
  , nBT
  , nbr
  , nRw
  , nRb
  , unb
  , uNw
  , uNb
  , aqe
  , nqe
  -- ** Length
  , l
  , lnt
  , lg
  , ø
  , nø
  , eL
  , lL
  , leL
  , mL
  , meL
  , lEq
  , (|=|)
  , nlE
  , (|~|)
  , lLX
  , lGX
  , (|<|)
  , lLE
  , lGE
  , (|>|)
  , lCp
  , (|?|)
  , evL
  , odL
  , pattern Ø
  , pattern NO
  , module P.Foldable.Length.Extra
  -- ** Bags
  , bg
  , bgW
  , bgB
  , rbg
  , rbW
  , rbB
  , bG
  , bGW
  , bGB
  -- ** Scans
  , lS
  , flS
  , rS
  , frS
  , ls
  , fls
  , rs
  , frs
  , lz
  , flz
  , rz
  , frz
  -- ** Minimums and Maximums
  , mx
  , mn
  , x_W
  , xW
  , fxW
  , mW
  , fmW
  , xB
  , fxB
  , mB
  , fmB
  , xM
  , fxM
  , nM
  , fnM
  , xb
  , fxb
  , xbW
  , xbB
  , xbM
  , mb
  , fmb
  , mbW
  , mbB
  , mbM
  , xd
  , fxd
  , xdW
  , xdB
  , xdM
  , md
  , fmd
  , ndW
  , mdB
  , mdM
  , xBl
  , xBL
  , mBl
  , xMl
  , mMl
  -- *** Extract extrema
  , xxo
  , xoW
  , xoB
  , xol
  , mmo
  , moW
  , moB
  , mol
  -- ** Finds
  , g_N
  , gN
  , fgN
  , gNs
  , fGN
  , g1
  , fg1
  , g1s
  , fG1
  , module P.Foldable.Find.Extra
  -- * Scan
  , Scan (..)
  , fmA
  , fsc
  , ixm
  , ixM
  , iXm
  , iXM
  , ixo
  , fio
  , (!//)
  , scP
  , dph
  , dpH
  , eu
  , eU
  , cz
  , cZ
  , zcz
  , czm
  , scp
  , scs
  , fSs
  , zc
  , zcx
  , zcn
  , zcf
  , fzf
  , zce
  , fze
  , zca
  , fza
  , module P.Scan.Extra
  -- ** Paths
  , pxx
  , mpX
  , pxX
  , mPX
  -- * Traversable
  , Traversable
  , sQ
  , tv
  , ftv
  , tv2
  , tvA
  , fvA
  , vA2
  -- * Distributive
  , Distributive (..)
  , fds
  , fcl
  -- * Eq
  , Eq
  , (==)
  , eq
  , (/=)
  , nq
  , qb
  , nqb
  , Eq1 (..)
  , fq1
  , Eq2 (..)
  , fq2
  , q1'
  , q1x
  , rw
  , frw
  , module P.Eq.Extra
  -- * Ord
  , Ord
  , (>)
  , gt
  , (>=)
  , ge
  , (<.)
  , lt
  , (<=)
  , le
  , Ordering (..)
  , cp
  , fcp
  , tcp
  , cb
  , gb
  , geb
  , ltb
  , lb
  , pattern RO
  , pattern RC
  , Ord1 (..)
  , Ord2 (..)
  , c1'
  , module P.Ord.Extra
  -- ** Bounded
  , MinBounded (..)
  , MaxBounded (..)
  , pattern NB
  , pattern XB
  , mJ
  , or
  , ay
  , fay
  , all
  , fll
  , nr
  , no
  , fno
  , mb2
  , ay2
  -- ** Mins
  , mN
  , (!^)
  , mNW
  , mNw
  , mNB
  , mNb
  , mNM
  , mNL
  , mNl
  -- ** Maxes
  , ma
  , (^^)
  , maW
  , maw
  , maB
  , mab
  , maM
  , maL
  , mal
  -- ** Minmax
  , xN
  , xNW
  , xNB
  , xNl
  , nX
  , nXW
  , nXB
  , nXl
  -- ** Sorting
  , Sortable (..)
  , rsr
  , fsW
  , rsW
  , fRW
  , sB
  , fsB
  , rsB
  , fRB
  , sj
  , sJ
  , xsr
  , xsW
  , xsB
  -- ** Compare 3
  , Ordering3 (..)
  , cp3
  , eI
  -- * Ix
  , Ix
  , Range
  , uRg
  , rg
  , frg
  , uDx
  , fux
  , dx
  , fdx
  , uIr
  , fur
  , ir
  , fir
  , uGz
  , gz
  , fgz
  -- * Permutations
  , Permutation (..)
  -- * Linear algebra
  -- ** Vectors
  , Zr
  , Nx
  , Vec (..)
  , pattern (:+)
  , Vec0
  , pattern V0
  , Vec1
  , pattern V1
  , Vec2
  , pattern V2
  , Vec3
  , pattern V3
  , Vec4
  , pattern V4
  , Vec5
  , pattern V5
  , Vec6
  , pattern V6
  , Vec7
  , pattern V7
  , pattern VUC
  , pattern VUS
  -- ** Tensors
  , Tensor (..)
  , ta
  , pattern Mk
  , ti
  -- ** Matrices
  , Matrix
  , pattern CEv
  , pattern (:+-)
  , pattern PEv
  , pattern (:+|)
  , (+:|)
  , pattern TP
  , ColFunctor (..)
  -- * Arithmetic
  , Int
  , Prelude.Integer
  , Number
  , Prelude.Integral
  , (%)
  , mu
  , fmu
  , (//)
  , dv
  , fdv
  , vD
  , fvD
  , qR
  , fqR
  , av
  , sig
  , aD
  , (-|)
  , tNu
  , xrt
  , fxr
  , xlg
  , fxl
  , pwt
  , fpt
  , pwo
  , fpo
  , kvD
  , fkv
  , klg
  , fkl
  , krm
  , xgD
  , module P.Arithmetic.Extra
  -- ** Parity
  , ev
  , od
  -- ** Number theory
  , gcd
  , lcm
  , rPr
  , nrP
  -- ** Patterns
  , pattern X
  , pattern N
  , pattern NP
  , pattern NN
  , pattern NZ
  , pattern EV
  , pattern OD
  -- ** Natural numbers
  , Nat (..)
  -- ** Divisibility and Primes
  , by
  , fby
  , (|?)
  , nby
  , (/|?)
  , fnb
  , a'
  , i'
  , pattern I'
  , module P.Arithmetic.Primes.Extra
  -- ** Fractions
  , (/)
  , iv
  , fiv
  , rc
  -- ** Base Conversion
  , hx
  , bs
  , fbs
  , bS
  , fbS
  , lB
  , flB
  , bi
  , ubi
  , biS
  , ubs
  , fub
  , ubS
  , fuB
  , dgs
  , fDS
  , dgR
  , fDR
  , module P.Arithmetic.Base.Extra
  -- ** Integer partitions
  -- *** Ordered integer partitions
  , ipt
  , ipS
  , ipW
  , ipe
  -- *** Descending integer partitions
  , dip
  , dpS
  , dpW
  -- * Constants
  , nn
  , nN
  , β
  , pattern Β
  , pattern Alf
  , module P.Constants.Language
  , module P.Constants.Math
  -- * Enum
  , Enum
  , pattern Sc
  , pattern Pv
  , eF
  , ef
  , fef
  , (#>#)
  , tef
  , lef
  , xef
  , ref
  , fRf
  , (#<#)
  , eR
  , (##)
  , eR'
  , eRx
  , xR'
  , fln
  , flN
  , icn
  , fIc
  , nic
  , fnc
  , module P.Enum.Extra
  -- * Algebra
  -- ** Free
  , Free (..)
  , fuF
  -- ** Magma
  , Magma (..)
  , mgp
  , WrappedSemigroup (..)
  , FreeMagma (..)
  , FreeNMagma (..)
  , fMc
  , fMC
  , fMa
  , fMA
  , fmc
  , fmC
  , fmn
  , fmN
  -- ** Semigroup
  , Semigroup
  , (<>)
  , fmp
  , mp
  , mp3
  , fP3
  , cy
  , swg
  , fsg
  , omp
  , fom
  , om3
  , fh3
  , Reversed (..)
  -- ** Monoid
  , Monoid
  , i
  , rt
  , frt
  , rt2
  , rt3
  , rt4
  , rt5
  , rt6
  , rt7
  , rt8
  , rt9
  , rt0
  , rt1
  , Commutative (..)
  -- *** Monoid actions
  , Action (..)
  , ($$)
  , faq
  , aqE
  , Regular (..)
  , Conjugate (..)
  -- *** Free
  , FreeMonoid (..)
  , Partition (..)
  -- *** Partitions
  , pt
  , pST
  , pWK
  , iRf
  , fiR
  , rfn
  -- *** Splits
  , sPs
  , sPn
  , sPe
  , sPN
  , mps
  , mqn
  , mpe
  , mpN
  , cxS
  , fxS
  , xuc
  , fxu
  , xlc
  , fxL
  , module P.Algebra.Monoid.Free.Split.Extra
  -- *** Sublists
  , iS
  , fiS
  , nS
  , fnS
  , iWq
  , nWq
  , iBq
  , nBq
  , iSW
  , nSW
  , iSB
  , nSB
  , lsS
  , lWq
  , lBq
  , lSW
  , lSB
  , ss
  , sS
  , sI
  , sD
  , ssn
  , fsn
  , scn
  , fSn
  , sSt
  , lss
  , sss
  , sSm
  , sSn
  , module P.Algebra.Monoid.Free.Substring.Extra
  -- *** Min and Max
  , Min (..)
  , Max (..)
  -- *** First and Last
  , First (..)
  , Last (..)
  -- *** Ap
  , Ap (..)
  -- *** Aligned
  , Aligned (..)
  -- ** Quasigroups
  , Quasigroup (..)
  -- ** Loops
  , Loop (..)
  -- ** Moufang loops
  , Moufang (..)
  , pattern IV
  , iiv
  , Staple (..)
  , pattern M2L
  , pattern M2R
  -- ** Groups
  , Group (..)
  , (~~)
  -- *** Free groups
  , FreeGroup (..)
  -- ** Ring
  , Ring
  , (+)
  , pl
  , (-)
  , sb
  , fsb
  , (*)
  , ml
  , db
  , tp
  , (^)
  , pw
  , fpw
  , sq
  , pattern Ng
  , Polynomial (..)
  -- *** Linear algebra
  , dt
  , (·)
  -- ** Shelves
  , Shelf (..)
  , cj
  -- ** Racks
  , Rack (..)
  , jc
  -- ** Quandles
  , Quandle (..)
  , FreeQuandle (..)
  , pattern FQC
  , pattern QP
  -- ** Kei
  , Kei (..)
  , FreeKei
  , pattern KPr
  , Core (..)
  , lCr
  , (!@>)
  -- * Char
  , Char
  , iC
  , niC
  , pattern IC
  , iW
  , niW
  , pattern IW
  , iL
  , niL
  , pattern IL
  , iU
  , niU
  , pattern IU
  , α
  , nα
  , pattern Α
  , iA
  , niA
  , pattern IA
  , iP
  , niP
  , pattern IP
  , iD
  , niD
  , pattern IS
  , iSy
  , nSy
  , pattern ID
  , pattern TU
  , mtU
  , pattern TL
  , mtL
  , pattern Or
  , pattern Ch
  , pattern Orm
  , pattern Chm
  , pattern Nl
  , pattern NL
  , pattern Sl
  , pattern SL
  , pattern SC
  , pattern Sm
  , scI
  -- ** Character Categories
  , LetterCategory (..)
  , MarkCategory (..)
  , NumberCategory (..)
  , PunctuationCategory (..)
  , SymbolCategory (..)
  , SeparatorCategory (..)
  , OtherCategory (..)
  , GeneralCategory (..)
  , pattern ULu
  , pattern ULl
  , pattern ULt
  , pattern ULm
  , pattern ULo
  , pattern UMn
  , pattern UMc
  , pattern UMe
  , pattern UNd
  , pattern UNl
  , pattern UNo
  , pattern UPc
  , pattern UPd
  , pattern UPs
  , pattern UPe
  , pattern UPi
  , pattern UPf
  , pattern UPo
  , pattern USm
  , pattern USc
  , pattern USk
  , pattern USo
  , pattern UZs
  , pattern UZl
  , pattern UZp
  , pattern UCc
  , pattern UCs
  , pattern UCo
  , pattern UCn
  , gCt
  , iCt
  , iCT
  , nCt
  , nCT
  -- * Strings
  , String
  , pattern Ln
  , pattern Ul
  , wR
  , pattern Wr
  , pattern Uw
  , pattern Dl
  , pattern Ls
  , pattern Pn
  , pattern SB
  , pattern CB
  -- ** Balanced strings
  , bld
  , bla
  , blc
  , blp
  , bls
  , blF
  -- ** Padding
  , rP
  , lP
  , rPL
  , lPL
  , rPp
  , lPp
  -- ** Unicode
  , nfd
  , nfc
  , rmD
  , hsD
  , nhD
  -- * Functor
  , Functor
  , m
  , fm
  , (<)
  , pM
  , (<$)
  , fpM
  , ($>)
  , jpM
  , mw
  , fmw
  , mwn
  , fwn
  , mwc
  , fWC
  , mm
  , (<<)
  , fM
  , mX
  , (^.)
  , fmX
  , m'
  , fm'
  , mof
  , fMF
  , (><)
  , m3
  , (<<<)
  , fm3
  , ma4
  , (<<&)
  , fM4
  , zz
  , (<.<)
  , fzz
  , z2
  , (<.^)
  , fz2
  , xc
  , fxc
  , pxc
  , qxc
  , ly
  , fly
  , (<|<)
  , mi
  , mI
  , mcm
  , fMM
  , mc2
  , fM2
  , dcp
  , fdC
  , dc2
  , fd2
  , module P.Function.Compose.Extra
  -- ** Curry maps
  , cqp
  , (<%)
  , fqp
  , ccq
  , (<<%)
  , fcq
  , cq3
  , (<%%)
  , fq3
  -- ** Fix
  , Fix (..)
  , hFx
  , fFx
  , ctF
  , uFx
  -- ** Functor composition
  , Comp (Co)
  , pattern UC
  , pattern LC
  , mR
  , pattern LAC
  , pattern RAC
  , pattern FeC
  -- ** Bifunctor
  , bm
  , fbm
  , (***)
  , jB
  , fjB
  , (&@)
  , mjB
  , (&@<)
  , mJB
  , (&<<)
  , jBm
  , (<&@)
  , pjB
  , fpJ
  , qjB
  , fqJ
  , kjB
  , fkJ
  , mst
  , fMt
  , m2t
  , f2t
  , bAp
  , fBp
  -- *** Blackbird
  , Blackbird (..)
  -- *** Flip
  , Flip (..)
  , pattern UFl
  , flI
  , ufi
  , pattern LF2
  , Flip2 (..)
  , mF2
  , m22
  -- ** Identity
  , Ident (..)
  , Identity
  , pattern UI
  , pattern Li2
  , pattern UL2
  , pattern La2
  , pattern L2'
  , pattern Ul2
  -- * Applicative
  , Applicative
  , p
  , fp
  , pp
  , fpp
  , p3
  , fp3
  , p4
  , fp4
  , μ
  , fμ
  , ap
  , (*^)
  , fA
  , aT
  , (*>)
  , faT
  , aK
  , (<*)
  , faK
  , l2
  , fl2
  , (**)
  , l3
  , fl3
  , (**#)
  , l4
  , fl4
  , (**$)
  , jl2
  , xly
  , fXy
  , xlm
  , fXm
  , l2p
  , (*^*)
  , l2x
  , (*|*)
  , l2n
  , (*/*)
  , l2m
  , (**<)
  , l2M
  , (*×)
  , l2q
  , flq
  , (×*)
  , u2q
  , j2q
  , l3q
  , u3q
  , j3q
  , l4q
  , u4q
  , j4q
  , eqo
  , (*=*)
  , l2K
  , (*<*)
  , flK
  , l2T
  , (*>*)
  , flT
  , ll2
  , knt
  , fKt
  , (+^$)
  -- * Unpure
  , Unpure (..)
  , pattern Pu
  , upF
  , pattern Le
  -- * Ring Applicative
  , RingApplicative (..)
  -- * Alternative
  , module P.Alternative.Extra
  , Alternative
  , em
  , (++)
  , ao
  , fao
  , gu
  , cx
  , cM
  , (>/)
  , fcM
  , cs
  , (<|)
  , fcs
  , ec
  , (|>)
  , fec
  , rl
  , (#*)
  , frl
  , rm
  , rM
  , (*%)
  -- ** Replace
  , rW
  , frW
  , fr_
  , frp
  -- ** Multiple application
  -- | To apply an alternative an exact number of times see 'xly'.
  -- *** Some
  , so
  , so'
  , sO
  , sO'
  , (+<>)
  , msO
  , (<>+)
  , sOm
  , (+=>)
  , mSO
  , (<=+)
  , sOM
  , (+<*)
  , soa
  , (<*+)
  , aso
  , (+*>)
  , soA
  , (*>+)
  , aSo
  , lSo
  , lSO
  , sot
  , sOt
  , sow
  , soW
  -- *** Many
  , my
  , my'
  , mY
  , mY'
  , lMy
  , lMY
  , myt
  , mYt
  , myw
  , myW
  , (**>)
  , amy
  , may
  , (<**)
  , mya
  , yma
  -- *** Possible
  , py
  , py'
  , pY
  , pY'
  , (?<>)
  , mpY
  , (<>?)
  , pYm
  , (?=>)
  , mPY
  , (<=?)
  , pYM
  , (<*?)
  , apy
  , (?*>)
  , pya
  , lPy
  , lPY
  -- *** At least
  , al
  , fal
  , al'
  , fAl
  , aL
  , faL
  , aL'
  , fAL
  , lAl
  , lAL
  -- *** At most
  , am
  , fam
  , am'
  , fAm
  , aM
  , faM
  , aM'
  , fAM
  , lAm
  , lAM
  -- *** Between
  , bw
  , bw'
  , bW
  , bW'
  , lBw
  , lBW
  -- *** Interspersed
  , isa
  , (>|-)
  , isA
  , (>|+)
  -- * Monad
  , Prelude.Monad
  , bn
  , (>~)
  , fb
  , (~<)
  , lbn
  , (<~)
  , lfb
  , mbn
  , (~~<)
  , (~<<)
  , bn_
  , (>~.)
  , fb_
  , (+>)
  , (<+)
  , jn
  , (>#<)
  , fjn
  , jj
  , fjj
  , mjn
  , mJn
  , mmj
  , mmJ
  , m3j
  , m3J
  , mM
  , fmM
  , m_
  , fm_
  , vd
  , vr
  , vR
  , vl
  , vL
  , itM
  , ftM
  , iyM
  , fyM
  , (>~@)
  , fbM
  , fBM
  , module P.Monad.Extra
  -- ** Fold
  , lfM
  , rfM
  , lM1
  , rM1
  , lsM
  , z1M
  -- ** State
  , State (..)
  , get
  , put
  , pattern RSt
  , fRS
  , vSt
  , fvS
  , xSt
  , fxs
  , cys
  , ty
  , ƭ
  , mD
  , mDm
  , wS
  , fwS
  , (>$=)
  -- ** Free
  , FreeM (..)
  , lFe
  , dFP
  , dPF
  , (*~|)
  , dPr
  , dFe
  , rtc
  , itr
  , foF
  , hoF
  , shp
  , fSp
  , shq
  , fSq
  , maF
  , mfe
  , mpu
  , feu
  , fdm
  , ffm
  , dsy
  , lyr
  , yyF
  -- ** MonadPlus
  -- | No MonadPlus type class has been implemented currently, since it is mostly redundant.
  -- Here we have functions that require both 'Control.Applicative.Alternative' and 'Monad'.

  -- *** Many
  , xmy
  , fxy
  , xmY
  , fxY
  , bmy
  , fBy
  , (>~*)
  , bmY
  , fBY
  -- *** Some
  , xso
  , fxo
  , xsO
  , fXO
  , bso
  , fbo
  , (>~+)
  , bsO
  , fbO
  -- *** Filters
  , wh
  , fl
  , (@~)
  , wn
  , fn
  , (@!)
  , er
  , fer
  , (=-)
  , cX
  , fcX
  , qX
  , fqX
  , fLs
  , fFL
  , f1l
  , f2l
  -- * Profunctor
  , Profunctor (..)
  , arr
  , jbp
  , rmp
  -- * Arrow
  , LooseArrow (..)
  , pe
  , cxa
  , syr
  , (==:)
  , pe2
  , sy3
  , Arrow (..)
  , ot
  , fot
  , oti
  , iot
  , ot2
  -- ** Kleisli
  , Kleisli (..)
  -- ** Cokleisli
  , Cokleisli (..)
  -- ** ArrowM
  , mSM
  , mnM
  , (=:@)
  , prM
  , fPM
  , syM
  , (><@)
  , cxM
  , fXM
  , (-<@)
  , otM
  , foM
  , arM
  -- *** Additional combinators
  , (=:%)
  , prv
  , fpv
  , (==@)
  , p2M
  , f2M
  , (^=:)
  , lpr
  , fLP
  , (=:^)
  , rpr
  , fRP
  -- *** Parser variants
  , (=:>)
  , prR
  , fpR
  , (=:<)
  , prL
  , fpL
  -- * Comonad
  , Comonad (..)
  -- ** Cofree
  , Cofree (..)
  , hCF
  , hCf
  -- * IO
  , IO
  , io
  , iop
  -- ** Input
  , gC
  , gO
  , gl
  , gL
  , ga
  -- ** Output
  , ps
  , pS
  , pC
  , pO
  , pr
  , pW
  , fpW
  -- * Parsing
  , Parser (..)
  -- ** Perform a parse
  , uP
  , gP
  , (-%)
  , gc
  , (-^)
  , gk
  , (-!)
  , glk
  , (-!$)
  , gpW
  , gpB
  , gpg
  , (->#)
  , gpl
  , (-<#)
  , ggL
  , (->|)
  , glL
  , (-<|)
  , gcy
  , (-^*)
  , gcY
  , gky
  , (-!*)
  , gkY
  , sre
  , sk
  , (-!%)
  , ysk
  , (-%%)
  -- *** Tests
  -- | Performs a parse in some way and returns a 'Bool'.
  , pP
  , (-@)
  , pH
  , cP
  , (-#)
  , x1
  , (-$)
  , cpy
  , x1y
  -- ** Generalized forms
  -- | These give more general type signatures but usually require additional type information to type check.
  , gP'
  , gc'
  , gPW
  , gPB
  , gPg
  , gPl
  , gGL
  , gLL
  , gCy
  , gCY
  -- *** Tests
  -- | Performs a parse in some way and returns a 'Bool'.
  , pP'
  , pH'
  , cP'
  , x1'
  , cPy
  , x1Y
  -- ** Parser combinators
  , kB
  , bkB
  , nK
  , χ
  , sχ
  , x_
  , x'
  , lx_
  , lX_
  , lx'
  , lX'
  , bx
  , nχ
  , xx
  , zWx
  , xxh
  , bXx
  , ʃ
  , s_
  , s'
  , hʃ
  , yʃ
  , bH
  , hh
  , zWh
  , bhh
  , νa
  , gy
  , bgy
  , lGk
  , gy'
  , bgY
  , bkw
  , ivP
  , gre
  , laz
  , bʃ
  , bSh
  , isP
  , fiP
  , ($|$)
  , pew
  , peW
  , pEw
  , enk
  , fNk
  , yS
  , yχ
  , mQ
  , blj
  , h_t
  , h't
  , ah_
  , ah'
  , ar_
  , ar'
  , h_a
  , h'a
  , wh_
  , wh'
  , ayw
  , ayW
  -- *** Look-aheads
  , lA
  , nA
  , aχ
  , anx
  , akB
  , aʃ
  , ahd
  , ahS
  -- *** Parse from options
  -- Parses from a list of options.
  -- Similar to a character class in a regex.
  , xay
  , xys
  , nxy
  , nys
  , xyo
  , xyy
  , asy
  , syo
  , syO
  , syy
  , syY
  -- *** Composition
  , lp2
  , (#*>)
  , (#<*)
  , yju
  , fyj
  , (@<*)
  , yku
  , fyk
  , (@*>)
  , ymu
  , fym
  , (@^*)
  -- ** Regex
  -- *** Build parsers
  , rX
  , rXw
  , rxw
  , rXW
  , rxW
  , rXx
  -- *** Build and run regex
  , uPx
  , uPX
  , gPx
  , gPX
  , gcx
  , gcX
  , gkx
  , gkX
  , gXy
  , gXY
  , srX
  , skX
  -- **** Tests
  -- | Returns a 'Bool' value.
  , pPx
  , pPX
  , x1x
  , x1X
  , cPx
  , cPX
  -- ** Pre-made Parsers
  , hd
  , h_
  , h'
  , rh_
  , rh'
  , hdS
  , phd
  , bhd
  , mhd
  , dg
  , νd
  , af
  , p_
  , pc
  , pL
  , pU
  , aN
  , pR
  , wd
  , wr
  , en
  , ens
  , p16
  , lWs
  , tWs
  , ν
  -- | Exports a ton of small parsers for specific tasks.
  , module P.Parse.Extra
  -- * W type combinator
  , W (..)
  , pattern UW
  , wi
  , uwi
  , lfw
  -- * Deprecated
  -- | Exports long form versions of functions.
  -- Using these will produce a warning with the name of the short function to use.
  , module P.Deprecated
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , Enum
  , Int
  )


import qualified Data.List


import P.Algebra.Free
import P.Algebra.Free.Group
import P.Algebra.Free.Kei
import P.Algebra.Free.Magma
import P.Algebra.Free.Quandle
import P.Algebra.Group
import P.Algebra.Kei
import P.Algebra.Kei.Core
import P.Algebra.Linear
import P.Algebra.Loop
import P.Algebra.Loop.Moufang
import P.Algebra.Loop.Moufang.Staple
import P.Algebra.Magma
import P.Algebra.Magma.Wrapper
import P.Algebra.Monoid
import P.Algebra.Monoid.Action
import P.Algebra.Monoid.Action.Conjugate
import P.Algebra.Monoid.Action.Regular
import P.Algebra.Monoid.Aligned
import P.Algebra.Monoid.Ap
import P.Algebra.Monoid.FirstLast
import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Prefix
import P.Algebra.Monoid.Free.Split
import P.Algebra.Monoid.Free.Split.Extra
import P.Algebra.Monoid.Free.Substring
import P.Algebra.Monoid.Free.Substring.Extra
import P.Algebra.Monoid.Free.Suffix
import P.Algebra.Monoid.MinMax
import P.Algebra.Polynomial
import P.Algebra.Quandle
import P.Algebra.Quasigroup
import P.Algebra.Rack
import P.Algebra.Ring
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Semigroup.Extra
import P.Algebra.Semigroup.Reversed
import P.Algebra.Shelf
import P.Aliases
import P.Alternative
import P.Alternative.AtLeast
import P.Alternative.AtMost
import P.Alternative.Between
import P.Alternative.Intersperse
import P.Alternative.Many
import P.Alternative.Possible
import P.Alternative.Some
import P.Alternative.Extra
import P.Applicative
import P.Applicative.Ring
import P.Applicative.Unpure
import P.Arithmetic
import P.Arithmetic.Base
import P.Arithmetic.Base.Extra
import P.Arithmetic.Combinatorics.Partition
import P.Arithmetic.Extra
import P.Arithmetic.Floating
import P.Arithmetic.Nat
import P.Arithmetic.Number
import P.Arithmetic.Pattern
import P.Arithmetic.Primes
import P.Arithmetic.Primes.Extra
import P.Arrow
import P.Arrow.ArrowM
import P.Arrow.Cokleisli
import P.Arrow.Kleisli
import P.Assoc
import P.Bifunctor
import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Bifunctor.Flip
import P.Bifunctor.Profunctor
import P.Bifunctor.These
import P.Bool
import P.Category
import P.Category.Endo
import P.Category.Iso
import P.Category.Iso.Polymorphic
import P.Char
import P.Char.Category
import P.Comonad
import P.Comonad.Cofree
import P.Constants.Language
import P.Constants.Math
import P.Deprecated
import P.Distributive
import P.Enum
import P.Enum.Extra
import P.Eq
import P.Eq.Extra
import P.First
import P.First.Extra
import P.Fix
import P.Foldable
import P.Foldable.Bag
import P.Foldable.Count
import P.Foldable.Count.Extra
import P.Foldable.Extra
import P.Foldable.Find
import P.Foldable.Find.Extra
import P.Foldable.Index.Extra
import P.Foldable.Length
import P.Foldable.Length.Extra
import P.Foldable.Length.Pattern
import P.Foldable.MinMax
import P.Foldable.Unfold
import P.Foldable.Uniques
import P.Function
import P.Function.Compose
import P.Function.Compose.Extra
import P.Function.Curry
import P.Function.Flip
import P.Function.Flip.Extra
import P.Functor
import P.Functor.Compose
import P.Functor.Fix
import P.Functor.Identity
import P.Functor.Identity.Class
import P.Graph
import P.IO
import P.Intersection
import P.Ix
import P.Last
import P.List
import P.List.Chop
import P.List.Chop.Extra
import P.List.Comparison
import P.List.Expandable
import P.List.Extra
import P.List.Fenceposts
import P.List.Find
import P.List.Group
import P.List.Index
import P.List.Infinite
import P.List.Infix
import P.List.Intersperse
import P.List.Intersperse.Extra
import P.List.Padding
import P.List.Pairs
-- import P.List.Partition
import P.List.Permutation
import P.List.Prefix
import P.List.Split
import P.List.Split.Extra
import P.List.Suffix
import P.List.Translate
import P.Map
import P.Map.Class
import P.Map.Class.Extra
import P.Matrix
import P.Maybe
import P.Monad
import P.Monad.Extra
import P.Monad.Fold
import P.Monad.Free
import P.Monad.Plus
import P.Monad.Plus.Filter
import P.Monad.State
import P.Necklace
import P.Ord
import P.Ord.Bounded
import P.Ord.Compare3
import P.Ord.Extra
import P.Ord.MinMax
import P.Parse
import P.Parse.Combinator
import P.Parse.Extra
import P.Parse.Regex
import P.Pattern
import P.Permutation
import P.Reverse
import P.Scan
import P.Scan.Extra
import P.Scan.Foldable
import P.Show
import P.Sort.Class
import P.String
import P.String.Unicode
import P.Swap
import P.Tensor
import P.Traversable
import P.Tuple
import P.Type.Combinator.W
import P.Vector
import P.Zip
import P.Zip.Align
import P.Zip.Extra
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign
