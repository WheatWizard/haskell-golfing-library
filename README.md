The haskell golfing library (also **hgl** or **P**) is a replacement for Haskell's Prelude library with the goal of making programs as small as possible for code golf.

Please do not use this for actual code.

## Documentation

The documentation can be viewed using [gitlab.io](https://wheatwizard.gitlab.io/haskell-golfing-library/), or built using the instructions below.

## Building

To build this library or the documentation you will need nix.

Start a new shell from the root directory of the project:

```
nix-shell
```

And you can build the documentation with:

```
cabal v2-haddock
```

You can play around with it in ghci using:

```
cabal v2-repl
```

You can build the tests using:

```
cabal v1-test
```

You must use `v1` not `v2`.

On a sucess this will surpress the output of the tests, to run that you can execute:
```
./dist/build/spec/spec
```

To use this library in code add it as a dependency import it

```
import qualified Prelude
import P
```

You need to import Prelude qualified to prevent name clashes between the two.
